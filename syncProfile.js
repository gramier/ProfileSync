var fs = require('fs');
var parser = require('xml2json');
var program = require('commander');
 
program
  .version('0.0.1')
  .usage('--profile <profile> --src <path> --outputprofile <profile>')
  .option('-p, --profile <profile>', 'profile to allign')
  .option('-f, --src <path>', 'path to src folder')
  .option('-op, --outputprofile <profile>', 'fixed profile based on src folder')
  .parse(process.argv);


var fileName = program.profile; //process.argv[2];
var path = program.src;
var outputFile = program.outputprofile;

var outputProfile = (function(){


})();

var inputProfile = (function(){
	var jsonObject = 'toto';

	fs.readFile( fileName, handleRead);

	function handleRead(err, data) {
	    if (err) 
	    	console.error('Error opening file: ' + fileName);
	    
	    var json = parser.toJson(data);
	    jsonObject = JSON.parse(json);
 	}

 	return {
 		getObject : function() {
 			return jsonObject;
 		}
 	}

})();

console.log(inputProfile.getObject());


//verify_classAccesses(jsonObject);
//verify_fieldPermissions(jsonObject);



//fs.readdir(path, callback_readdir);

// function callback_readdir (err, items) {
//     console.log(items);
 
//     for (var i=0; i<items.length; i++) {
//         console.log(items[i]);

//         var subFolder = path + '/' + items[i];
//         var fstat = fs.statSync(subFolder);
//         if (fstat.isDirectory())
// 	        console.log('### SUB ' + subFolder);
//         // , function(err, stats) {
//         // 	if (stats.isDirectory()) {
//         // 		fs.readdir(subFolder, callback_readdir);
//         // 	}
//         // });
//     }
// }

function verify_fieldPermissions(json, callback) {
	console.log(json['Profile']['fieldPermissions'].length);
	
	var map = {};
	var mapFile = {};
	var mapNF = {};

	//Parse existing profile permissions
	for(var v = 0 ; v < json['Profile']['fieldPermissions'].length ; v++){
		var object = json['Profile']['fieldPermissions'][v].field.split('.')[0];
		var field = json['Profile']['fieldPermissions'][v].field.split('.')[1];

		if (typeof(map[object]) == 'undefined')
		 	map[object] = [];
		map[object].push(field);
	}
	
	//console.log('PROFILE DATA: ' + map);

	//For each item get the list of fields in src/Object/[Object]
	for(var obj in map) {		
		var file = 'src/objects/' + obj + '.object';

		(function (obj, file, items) {
			fs.readFile( file, function(err, data) {
			    
			    var lObjectFields = [];

			    if (err == null) {
			    	var json = parser.toJson(data);
			    	var jsonObject = JSON.parse(json);
			    	// console.log('OBJECT : ' + file);
			    	// console.log('LENGTH : ' + jsonObject['CustomObject']['fields'].length);
			    	if (typeof(jsonObject['CustomObject']['fields'][0]) != 'undefined') {
				    	for (var i = 0 ; i < jsonObject['CustomObject']['fields'].length; i++ ) {
				    		lObjectFields.push(jsonObject['CustomObject']['fields'][i].fullName); // console.log('OBJECT FILE DATA: ' + jsonObject['CustomObject']['fields'][i].fullName);
				    	}
				    } else {
								lObjectFields.push(jsonObject['CustomObject']['fields'].fullName);				    }
			    }
			    //console.log('ERROR : ' + err);
			    mapFile[obj] = lObjectFields;
					
					//for(var obj in map) {
						mapNF[obj] = [];
						for(i = 0; i < map[obj].length ; i++) {
							var found = false;
							for(j = 0; j < mapFile[obj].length; j++ ) {
								if (map[obj][i] == mapFile[obj][j]) {
									//map[obj][i] = 'OK';
									found = true;
									break;
								}
							}
							if (found == false) {
								mapNF[obj].push(map[obj][i]);
							}
						}
					//}
			   	console.log('---- ' + obj + ' ----');
			    //console.log('PROFILE DATA: ' + JSON.stringify(map[obj]));
			    //console.log('OBJECT DATA :' + JSON.stringify(mapFile[obj]));
			    console.log('OBJECT NF :' + JSON.stringify(mapNF[obj]));
			 });
		})(obj, file, map[obj]);
		//console.log('FILE DATA: ' + mapFile);
	}


	/////For each item in profils check if exists in file.
	// for(var obj in map) {
	// 	for(i = 0; i < map[obj].length ; i++) {
	// 		for(j = 0; j < mapFile[obj].length; j++ ) {
	// 			if (map[obj][i] == mapFile[obj][j]) {
	// 				map[obj][i] = 'OK';
	// 				break;
	// 			}
	// 		}
	// 	}
	// }

}

function verify_classAccesses(json) {
	
	console.log(json['Profile']['classAccesses'].length);

	for(var v = 0 ; v < json['Profile']['classAccesses'].length ; v++){
    	var filename = './src/classes/' + json['Profile']['classAccesses'][v].apexClass + '.cls';

    	if (fs.existsSync(filename)) {
			//console.log('Yes: ' + filename);
		} else {
		  	console.log('apexClass to be removed: ' + filename);	
		};
    	//console.log(json['Profile']['classAccesses'][v].apexClass);
	}
}





