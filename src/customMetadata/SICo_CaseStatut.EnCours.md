<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>En cours</label>
    <protected>false</protected>
    <values>
        <field>CasePicklistValue__c</field>
        <value xsi:type="xsd:string">En cours</value>
    </values>
    <values>
        <field>IsDefaultValue__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>PublicLabel__c</field>
        <value xsi:type="xsd:string">En cours</value>
    </values>
</CustomMetadata>
