<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SE01</label>
    <protected>false</protected>
    <values>
        <field>Heading__c</field>
        <value xsi:type="xsd:string">Objet</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ProvisioningType__c</field>
        <value xsi:type="xsd:string">Station connectée</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">En cours</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Provisionning</value>
    </values>
</CustomMetadata>
