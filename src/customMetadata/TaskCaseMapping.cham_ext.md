<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CHAM</label>
    <protected>false</protected>
    <values>
        <field>BatchCode__c</field>
        <value xsi:type="xsd:string">CHA_02B</value>
    </values>
    <values>
        <field>CaseHeading__c</field>
        <value xsi:type="xsd:string">CHAM Demande Intervention Dépannage</value>
    </values>
    <values>
        <field>CaseRecordType__c</field>
        <value xsi:type="xsd:string">CustomerDemand</value>
    </values>
    <values>
        <field>CaseSubject__c</field>
        <value xsi:type="xsd:string">Partenaires</value>
    </values>
    <values>
        <field>CaseType__c</field>
        <value xsi:type="xsd:string">Depannage</value>
    </values>
    <values>
        <field>EventIntervType__c</field>
        <value xsi:type="xsd:string">EXPERTISE TECHNIQUE</value>
    </values>
</CustomMetadata>
