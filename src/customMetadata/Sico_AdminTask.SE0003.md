<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SE0003</label>
    <protected>false</protected>
    <values>
        <field>BlockingCBPayment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Cases__c</field>
        <value xsi:type="xsd:string">SE01</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Domain__c</field>
        <value xsi:type="xsd:string">Services connectés</value>
    </values>
    <values>
        <field>EndPointType__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>EndPoint__c</field>
        <value xsi:type="xsd:string">/connected-services/edelia-iot/v1/subscriptions</value>
    </values>
    <values>
        <field>IsVisibleEspaceClient__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:type="xsd:string">EDE;SER_C43_LAB_NO_THERM</value>
    </values>
    <values>
        <field>ProvisioningCallOut__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">TaskProvisionning_SC</value>
    </values>
    <values>
        <field>TaskOrder__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>TaskSubject__c</field>
        <value xsi:type="xsd:string">Souscription Connect Lab&apos; sans thermostat auprès d&apos;Edelia</value>
    </values>
    <values>
        <field>TaskTechnique__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
