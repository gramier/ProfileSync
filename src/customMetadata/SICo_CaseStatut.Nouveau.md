<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Nouveau</label>
    <protected>false</protected>
    <values>
        <field>CasePicklistValue__c</field>
        <value xsi:type="xsd:string">Nouveau</value>
    </values>
    <values>
        <field>IsDefaultValue__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>PublicLabel__c</field>
        <value xsi:type="xsd:string">Nouveau</value>
    </values>
</CustomMetadata>
