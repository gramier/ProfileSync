<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>P0004</label>
    <protected>false</protected>
    <values>
        <field>BatchCode__c</field>
        <value xsi:type="xsd:string">GRD_12</value>
    </values>
    <values>
        <field>InterventionType__c</field>
        <value xsi:type="xsd:string">CHF</value>
    </values>
    <values>
        <field>TaskId__c</field>
        <value xsi:type="xsd:string">GA0001_B</value>
    </values>
</CustomMetadata>
