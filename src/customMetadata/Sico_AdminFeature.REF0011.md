<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Station énergie avec thermostat</label>
    <protected>false</protected>
    <values>
        <field>Tasks__c</field>
        <value xsi:type="xsd:string">TH0001</value>
    </values>
    <values>
        <field>ZuoraFeatureNumber__c</field>
        <value xsi:type="xsd:string">Station_Thermostat</value>
    </values>
</CustomMetadata>
