<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Edelia OSF Generic Profile</label>
    <protected>false</protected>
    <values>
        <field>EndPoint__c</field>
        <value xsi:type="xsd:string">/connected-services/edelia-iot/v1/profil/</value>
    </values>
</CustomMetadata>
