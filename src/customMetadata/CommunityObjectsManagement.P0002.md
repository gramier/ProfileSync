<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>P0002</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">RDV</value>
    </values>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">GA0001</value>
    </values>
    <values>
        <field>IsVisibleDashboard__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsVisibleOrderTracking__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Task</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">TaskProvisionning_Gaz</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">GAZ</value>
    </values>
</CustomMetadata>
