<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SE0001</label>
    <protected>false</protected>
    <values>
        <field>BlockingCBPayment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Cases__c</field>
        <value xsi:type="xsd:string">SE01</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Domain__c</field>
        <value xsi:type="xsd:string">Logistique</value>
    </values>
    <values>
        <field>EndPointType__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>EndPoint__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsVisibleEspaceClient__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:type="xsd:string">KITSTA01 - Kit station sans thermostat</value>
    </values>
    <values>
        <field>ProvisioningCallOut__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">TaskProvisionning_Logistique</value>
    </values>
    <values>
        <field>TaskOrder__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>TaskSubject__c</field>
        <value xsi:type="xsd:string">Envoi du pack Station énergie</value>
    </values>
    <values>
        <field>TaskTechnique__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
