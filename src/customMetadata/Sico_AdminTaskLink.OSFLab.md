<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OSF Lab</label>
    <protected>false</protected>
    <values>
        <field>PrimaryTask__c</field>
        <value xsi:type="xsd:string">SE0004</value>
    </values>
    <values>
        <field>RelatedTask__c</field>
        <value xsi:type="xsd:string">TH0002</value>
    </values>
</CustomMetadata>
