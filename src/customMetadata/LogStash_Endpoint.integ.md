<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>integ</label>
    <protected>false</protected>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>NamedCredential__c</field>
        <value xsi:type="xsd:string">LogStash_Integ</value>
    </values>
    <values>
        <field>ShardId__c</field>
        <value xsi:type="xsd:string">shardId-000000000000</value>
    </values>
</CustomMetadata>
