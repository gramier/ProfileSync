<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CHAM</label>
    <protected>false</protected>
    <values>
        <field>BatchCode__c</field>
        <value xsi:type="xsd:string">CHA_02B</value>
    </values>
    <values>
        <field>CaseHeading__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CaseRecordType__c</field>
        <value xsi:type="xsd:string">System</value>
    </values>
    <values>
        <field>CaseSubject__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CaseType__c</field>
        <value xsi:type="xsd:string">Maintenance</value>
    </values>
    <values>
        <field>EventIntervType__c</field>
        <value xsi:type="xsd:string">VISITE D&apos;ENTRETIEN</value>
    </values>
</CustomMetadata>
