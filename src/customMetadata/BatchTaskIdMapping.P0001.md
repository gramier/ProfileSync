<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>P0001</label>
    <protected>false</protected>
    <values>
        <field>BatchCode__c</field>
        <value xsi:type="xsd:string">CHA_02B</value>
    </values>
    <values>
        <field>InterventionType__c</field>
        <value xsi:type="xsd:string">Default</value>
    </values>
    <values>
        <field>TaskId__c</field>
        <value xsi:type="xsd:string">CH0002_B</value>
    </values>
</CustomMetadata>
