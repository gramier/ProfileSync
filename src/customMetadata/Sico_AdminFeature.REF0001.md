<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Fourniture de gaz</label>
    <protected>false</protected>
    <values>
        <field>Tasks__c</field>
        <value xsi:type="xsd:string">GA0001</value>
    </values>
    <values>
        <field>ZuoraFeatureNumber__c</field>
        <value xsi:type="xsd:string">Gaz</value>
    </values>
</CustomMetadata>
