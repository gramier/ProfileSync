trigger SICo_Zuora_CustomerAccountAfterUpdate_TRG on Zuora__CustomerAccount__c(after update) {
  if(PAD.canTrigger('SICo_Zuora_CustomerAccountAfterUpdate_TRG')) {
    SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterUpdate(Trigger.oldMap, Trigger.new);
  }   
}