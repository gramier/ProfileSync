/**
* @author Sebastien Colladon
* @date 13/10/2016
*
* @description Account Trigger After Update
*/
trigger SICo_AttachmentAfterInsert_TRG on Attachment(after insert) {
  if(PAD.canTrigger('SICo_AttachmentAfterInsert_TRG')){
    SICo_AttachmentUtility_CLS.HandleAttachmentFromDocuSign(Trigger.new);
  }
}