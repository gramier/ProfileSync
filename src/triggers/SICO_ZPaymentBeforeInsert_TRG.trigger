/**
* @author Boris Castellani
* @date 06/12/2016
*
* @description Handle Zuora__Payment__c Trigger before insert
*/
trigger SICO_ZPaymentBeforeInsert_TRG on Zuora__Payment__c(before insert) {
 	if(PAD.canTrigger('SICO_ZPaymentBeforeInsert_TRG')) {
		SICo_ZPaymentTrigger_CLS.handleBeforeInsert(trigger.new);
	}  
}