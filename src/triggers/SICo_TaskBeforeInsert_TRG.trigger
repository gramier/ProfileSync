/**
* @author Boris Castellani
* @date 25/10/2016
*
* @description Task Trigger Before insert
*/
trigger SICo_TaskBeforeInsert_TRG on Task(before insert) {
	if(PAD.canTrigger('SICo_TaskBeforeInsert_TRG')) {
		SICo_TaskTrigger_CLS.handleBeforeInsert(trigger.new);
	}     
}