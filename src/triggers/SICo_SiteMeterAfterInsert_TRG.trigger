/**
* @author Boris Castellani
* @date 17/10/2016
*
* @description SICo_SiteMeter__c Trigger After insert
*/
trigger SICo_SiteMeterAfterInsert_TRG on SICo_SiteMeter__c(after insert) {
  if(PAD.canTrigger('SICo_SiteMeterAfterInsert_TRG')) {
    SICo_SiteMeterTrigger_CLS.handleAfterInsertUpdate(Trigger.new);
  }   
}