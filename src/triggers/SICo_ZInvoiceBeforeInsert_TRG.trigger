trigger SICo_ZInvoiceBeforeInsert_TRG on Zuora__ZInvoice__c(before insert, before update) {

	if(PAD.canTrigger('SICo_ZInvoiceBeforeInsert_TRG')) {
		Invoice_Utility.mapSiteMeter(Trigger.new);
		Invoice_Utility.updateDebitDay(Trigger.new);
	}
}