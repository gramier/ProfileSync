/**
* @author Boris Castellani
* @date 25/10/2016
*
* @description Task Trigger After insert
*/
trigger SICo_TaskAfterInsert_TRG on Task(after insert) {
	if(PAD.canTrigger('SICo_TaskAfterInsert_TRG')) {
	    SICo_TaskTrigger_CLS.handleAfterInsert(trigger.new);
	}
}