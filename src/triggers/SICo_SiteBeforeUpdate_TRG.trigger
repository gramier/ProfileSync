/**
* @author Boris Castellani
* @date 19/10/2016
*
* @description SICo_Site__c Trigger Before update
*/
trigger SICo_SiteBeforeUpdate_TRG on SICo_Site__c(before update) {
	if(PAD.canTrigger('SICo_SiteBeforeUpdate_TRG')) {
		SICo_SiteTrigger_CLS.handleBeforeUpdate(Trigger.new);
	}      
}