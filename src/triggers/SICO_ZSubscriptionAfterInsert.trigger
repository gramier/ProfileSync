trigger SICO_ZSubscriptionAfterInsert on Zuora__Subscription__c(after insert) {
  if(PAD.canTrigger('SICO_ZSubscriptionAfterInsert')) {
    SICo_SubscriptionTrigger_CLS.handleSpreadAttachement(Trigger.newMap);
  }
}