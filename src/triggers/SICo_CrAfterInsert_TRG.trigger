/**
* @author boris castellani
* @date 07/11/2016
*
* @description SICo_CR__c Trigger After insert
*/
trigger SICo_CrAfterInsert_TRG on SICo_CR__c(after insert) {
  if(PAD.canTrigger('SICo_CrAfterInsert_TRG')) {
    SICo_CrTrigger_CLS.HandleAfterInsert(Trigger.new);
  }
}