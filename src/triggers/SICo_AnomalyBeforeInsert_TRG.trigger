/**
* @author Boris Castellani
* @date 10/11/2016
*
* @description Anomaly Trigger After Update
*/
trigger SICo_AnomalyBeforeInsert_TRG on SICo_Anomaly__c(before insert) {
  if(PAD.canTrigger('SICo_AnomalyBeforeInsert_TRG')) {
    SICo_AnomalyTrigger_CLS.HandleBeforeInsert(Trigger.new);
  }
}