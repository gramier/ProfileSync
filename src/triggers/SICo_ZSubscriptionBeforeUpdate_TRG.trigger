/**
* @author Boris Castellani
* @date 18/10/2016
*
* @description SICo_SiteMeter__c Trigger After update
*/
trigger SICo_ZSubscriptionBeforeUpdate_TRG on Zuora__Subscription__c(before update) {
	if(PAD.canTrigger('SICo_ZSubscriptionBeforeUpdate_TRG')) {
		SICo_SubscriptionTrigger_CLS.retrieveSubScriptionInfo(Trigger.new);
    SICo_SubscriptionTrigger_CLS.calculateOfferPicklist(Trigger.new);
	}      
}