/**
* @author Boris Castellani
* @date 27/06/2016 
*
* @description Trigger After Insert Call trigger after insert zuora Subscription
*/
trigger SICo_Provisioning_TRG on Zuora__SubscriptionProductFeature__c (after insert) {
  if(PAD.canTrigger('Zuora__SubscriptionProductFeature__c')) {
    // Call methode for calcul provisioning
    SICo_Provisioning_CLS.provisioning(trigger.new);
    SICo_SubscriptionTrigger_CLS.buildSubscribedOffers(trigger.new);
  }
}