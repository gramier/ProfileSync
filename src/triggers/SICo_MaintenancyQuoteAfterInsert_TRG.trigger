/**
* @author Boris Castellani
* @date 25/10/2016
*
* @description Handle MaintenancyQuote Trigger After insert
*/
trigger SICo_MaintenancyQuoteAfterInsert_TRG on MaintenancyQuote__c(after insert) {
		if(PAD.canTrigger('SICo_MaintenancyQuoteAfterInsert_TRG')) {
			SICo_MaintenancyQuoteTrigger_CLS.handleAfterInsert(trigger.new);
		}
    
}