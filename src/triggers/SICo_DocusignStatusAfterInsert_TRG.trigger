trigger SICo_DocusignStatusAfterInsert_TRG on dsfs__DocuSign_Status__c(after insert) {
	if(PAD.canTrigger('SICo_DocusignStatusAfterInsert_TRG')) {
		SICo_DocusignStatusTrigger_CLS.HandleAfterInsert(Trigger.new);
	}  
}