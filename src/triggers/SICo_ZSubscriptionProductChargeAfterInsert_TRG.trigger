/**
* @author Boris Castellani
* @date 21/11/2016
*
* @description Zuora__SubscriptionProductCharge__c Trigger After insert
*/
trigger SICo_ZSubscriptionProductChargeAfterInsert_TRG on Zuora__SubscriptionProductCharge__c(after insert) {
	if(PAD.canTrigger('SICo_ZSubscriptionProductChargeAfterInsert_TRG')) {
		SICo_SubscriptionPrdCharge_Utility.handlerAfterUpsert(trigger.New);
	}      
}