/**
* @author Boris Castellani
* @date 06/12/2016
*
* @description Handle MaintenancyQuote Trigger before insert
*/
trigger SICo_MaintenancyQuoteBeforeInsert_TRG on MaintenancyQuote__c(before insert) {
	if(PAD.canTrigger('SICo_MaintenancyQuoteBeforeInsert_TRG')) {
		SICo_MaintenancyQuoteTrigger_CLS.handleBeforeInsert(trigger.new);
	}    
}