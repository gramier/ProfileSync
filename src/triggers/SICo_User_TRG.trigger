/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi Solutions
Description:   Call trigger after update Account
Test Class:    SICo_User_TEST
History
01/08/2016     Cécile Neaud     Create version
19/09/2016     Gaël Burneau     Modifications
------------------------------------------------------------*/
trigger SICo_User_TRG on User (before update) {
  if(PAD.canTrigger('SICo_User_TRG')) {
    if (trigger.isBefore && trigger.isUpdate) {
      List<User> listUsersToTreat = new List<User>();
      for (User user : trigger.new) {
        //If it's a community user and he changed his email address
        if (user.ContactId != null && user.Email != trigger.oldMap.get(user.Id).Email){
          listUsersToTreat.add(user);
        }
      }
      if (!listUsersToTreat.isEmpty()) {
        SICo_User_CLS.changeEmail(listUsersToTreat);
      }
    }
  }
}