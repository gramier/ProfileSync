/**
* @author Sebastien Colladon
* @date 28/09/2016
*
* @description Account Trigger before Update
*/
trigger SICo_AccountBeforeUpdate_TRG on Account(before update) {
  if(PAD.canTrigger('SICo_AccountBeforeUpdate_TRG')) {
    SICO_AccountTrigger_CLS.HandleBeforeUpdate(Trigger.new, Trigger.OldMap);
  }
}