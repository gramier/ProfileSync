trigger SICo_Zuora_CustomerAccountAfterInsert_TRG on Zuora__CustomerAccount__c(after insert) {
  if(PAD.canTrigger('SICo_Zuora_CustomerAccountAfterInsert_TRG')) {
    SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterInsert(Trigger.new);
  }
}