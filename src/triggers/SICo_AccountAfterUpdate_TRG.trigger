/**
* @author Boris Castellani
* @date 09/12/2016
*
* @description Account Trigger after Update
*/
trigger SICo_AccountAfterUpdate_TRG on Account(after update) {
  if(PAD.canTrigger('SICo_AccountAfterUpdate_TRG')) {
    SICO_AccountTrigger_CLS.HandleAfterUpdate(Trigger.new, Trigger.OldMap);
  }    
}