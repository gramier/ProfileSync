trigger SICo_Zuora_CustomerAccountBeforeInsert_TRG on Zuora__CustomerAccount__c(before insert) {
  if(PAD.canTrigger('SICo_Zuora_CustomerAccountBeforeInsert_TRG')) {
    SICo_Z_CustomerAccountTriggerhandler_CLS.HandleBeforeInsert(Trigger.new);
  }
}