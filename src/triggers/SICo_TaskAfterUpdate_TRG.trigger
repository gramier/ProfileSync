/**
* @author Sebastien Colladon
* @date 09/11/2016
*
* @description Task Trigger After update
*/
trigger SICo_TaskAfterUpdate_TRG on Task(after update) {
  if(PAD.canTrigger('SICo_TaskAfterUpdate_TRG')) {
    SICo_TaskTrigger_CLS.handleAfterUpdate(trigger.new);
  }    
}