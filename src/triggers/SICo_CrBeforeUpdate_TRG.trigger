/**
* @author Boris Castellani
* @date 06/12/2016
*
* @description SICo_CR__c Trigger Before update
*/
trigger SICo_CrBeforeUpdate_TRG on SICo_CR__c(before update) {
	if(PAD.canTrigger('SICo_CrBeforeUpdate_TRG')) {
		SICo_CrTrigger_CLS.HandleBeforeInsert(Trigger.new);
	}  
}