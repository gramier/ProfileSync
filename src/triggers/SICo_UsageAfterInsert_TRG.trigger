/**
* @author Sebastien Colladon
* @date 16/11/2016
*
* @description Trigger After Insert on Usage
*/
trigger SICo_UsageAfterInsert_TRG on Usage__c(after insert, after update) {
  if(PAD.canTrigger('SICo_UsageAfterInsert_TRG')) {
    SICo_UsageTriggerHandler_CLS.handleAfterInsert(trigger.new);
  }   
}