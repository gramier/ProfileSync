<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Confirmation_GAZ</fullName>
        <description>Confirmation GAZ</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_GAZ</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_GAZ_MAINT</fullName>
        <description>Confirmation GAZ_MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_GAZ_MAINT</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_Lick_SE_TH_INST</fullName>
        <description>Confirmation Lick SE/TH/INST</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_Lick_Station_et_ou_Thermostat_et_ou_Inst</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_Lick_SE_TH_INST_GAZ</fullName>
        <description>Confirmation Lick SE/TH/INST/GAZ</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Confirmation_Lick_Gaz</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_Lick_SE_TH_INST_GAZ_MAINT</fullName>
        <description>Confirmation Lick SE/TH/INST/GAZ/MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Confirmation_Lick_Station_Gaz_et_Entretien</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_Lick_SE_TH_INST_MAINT</fullName>
        <description>Confirmation Lick SE/TH/INST/MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Confirmation_Lick_station_et_ou_thermostat_et_ou_maintenance</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_MAINT</fullName>
        <description>Confirmation MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_MAINT</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE</fullName>
        <description>Confirmation SE</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_GAZ</fullName>
        <description>Confirmation SE_GAZ</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_GAZ</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_MAINT</fullName>
        <description>Confirmation SE_MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_MAINT</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_TH_GAZ_et_Eligible_CHAM</fullName>
        <description>Confirmation SE_TH_GAZ et Eligible CHAM</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_GAZ_eligible_CHAM</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_TH_GAZ_non_eligible_CHAM</fullName>
        <description>Confirmation SE_TH_GAZ non eligible CHAM</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_GAZ_non_eligible_CHAM</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_TH_INST</fullName>
        <description>Confirmation SE_TH_INST</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_INST</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_TH_INST_GAZ</fullName>
        <description>Confirmation SE_TH_INST_GAZ</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_INST_GAZ</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_TH_INST_MAINT</fullName>
        <description>Confirmation SE_TH_INST_MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_INST_MAINT</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_SE_TH_non_eligible_CHAM</fullName>
        <description>Confirmation SE_TH non eligible CHAM</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_non_eligible_CHAM</template>
    </alerts>
    <alerts>
        <fullName>EmailFinalization</fullName>
        <description>Email Reprise de parcours</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>user.madteam@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_de_reprise_de_parcours</template>
    </alerts>
    <alerts>
        <fullName>Informations_Station_envoyees_du_lead</fullName>
        <description>Informations Station envoyées du lead</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Infos_fonctionnalites_station_lead</template>
    </alerts>
    <alerts>
        <fullName>ReminderEmail1</fullName>
        <description>Relance 1 par email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_pour_Relance_Souscription</template>
    </alerts>
    <alerts>
        <fullName>ReminderEmail2</fullName>
        <description>Relance 2 par email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_pour_Relance_Souscription</template>
    </alerts>
    <alerts>
        <fullName>Reprise_de_parcours_Lick_SE_TH_sans_Gaz</fullName>
        <description>Reprise de parcours Lick SE_TH sans Gaz</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Reprise_Parcours_Lick_SE_TH_Sans_gaz</template>
    </alerts>
    <alerts>
        <fullName>Reprise_de_parcours_Lick_SE_sans_Gaz</fullName>
        <description>Reprise de parcours Lick SE sans Gaz</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Reprise_Parcours_Lick_SE_Sans_gaz</template>
    </alerts>
    <alerts>
        <fullName>Reprise_de_parcours_Lick_avec_Gaz</fullName>
        <description>Reprise de parcours Lick avec Gaz</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Reprise_Parcours_Lick_avec_Gaz</template>
    </alerts>
    <alerts>
        <fullName>Vente_SE_GAZ</fullName>
        <description>Vente SE_GAZ</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_GAZ_MAINT</template>
    </alerts>
    <alerts>
        <fullName>Vente_SE_TH</fullName>
        <description>Vente SE_TH</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_eligible_CHAM</template>
    </alerts>
    <alerts>
        <fullName>Vente_SE_TH_GAZ_MAINT</fullName>
        <description>Vente SE_TH_GAZ_MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_INST_GAZ_MAINT</template>
    </alerts>
    <alerts>
        <fullName>Vente_SE_TH_INST_GAZ_MAINT</fullName>
        <description>Vente SE_TH_INST_GAZ_MAINT</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_INST_GAZ_MAINT</template>
    </alerts>
    <alerts>
        <fullName>Vente_SE_TH_eligible_CHAM</fullName>
        <description>Vente SE_TH eligible CHAM</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_Confirmation_SE_TH_eligible_CHAM</template>
    </alerts>
    <alerts>
        <fullName>test</fullName>
        <description>test</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>user.madteam@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SiCoSubscriptionEmails/SiCoReminderEmailOne</template>
    </alerts>
    <fieldUpdates>
        <fullName>SICOCustomerNumber</fullName>
        <field>CustomerNumber__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>SICO Remplissage du Numero Client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SICO Remplissage du Numero Client</fullName>
        <actions>
            <name>SICOCustomerNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>[SiCo] Ce WF reprend l&apos;id du record lead et le stocke dans le champ Numéro Client</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente GAZ</fullName>
        <actions>
            <name>Confirmation_GAZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>GAZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = GAZ</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente GAZ_MAINT</fullName>
        <actions>
            <name>Confirmation_GAZ_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>GAZ_MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = GAZ_MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente Lick SE%2FTH info fonctionnalités Station</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Lick</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>notContain</operation>
            <value>INST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>startsWith</operation>
            <value>SE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente Lick SE%2FTH%2FINST</fullName>
        <actions>
            <name>Confirmation_Lick_SE_TH_INST</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Lick</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>notContain</operation>
            <value>MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>notContain</operation>
            <value>GAZ</value>
        </criteriaItems>
        <description>Envoie un email de confirmation de commande poure toute vente Lick contenant de la station et/ou Thermostat et/ou installation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente Lick SE%2FTH%2FINST%2FGAZ%2FMAINT</fullName>
        <actions>
            <name>Confirmation_Lick_SE_TH_INST_GAZ_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Lick</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>contains</operation>
            <value>GAZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>contains</operation>
            <value>MAINT</value>
        </criteriaItems>
        <description>Envoie un email de confirmation de commande poure toute vente Lick contenant du gaz ET de la MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente Lick SE%2FTH%2FINST%2FMAINT</fullName>
        <actions>
            <name>Confirmation_Lick_SE_TH_INST_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Lick</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>contains</operation>
            <value>MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>notContain</operation>
            <value>GAZ</value>
        </criteriaItems>
        <description>Envoie un email de confirmation de commande poure toute vente Lick contenant de la station et/ou Thermostat et/ou installation ET de la maintenance</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente MAINT</fullName>
        <actions>
            <name>Confirmation_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE</fullName>
        <actions>
            <name>Confirmation_SE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_GAZ</fullName>
        <actions>
            <name>Confirmation_SE_GAZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_GAZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_GAZ</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_GAZ_MAINT</fullName>
        <actions>
            <name>Vente_SE_GAZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_GAZ_MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_GAZ_MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_MAINT</fullName>
        <actions>
            <name>Confirmation_SE_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH eligible CHAM</fullName>
        <actions>
            <name>Vente_SE_TH_eligible_CHAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.EligibleForCham__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH and if Eligible CHAM = True</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH non eligible CHAM</fullName>
        <actions>
            <name>Confirmation_SE_TH_non_eligible_CHAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.EligibleForCham__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH and if Eligible CHAM = False</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_GAZ et Eligible CHAM</fullName>
        <actions>
            <name>Confirmation_SE_TH_GAZ_et_Eligible_CHAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_GAZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.EligibleForCham__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_GAZ and Eligible CHAM = True</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_GAZ et Non Eligible CHAM</fullName>
        <actions>
            <name>Confirmation_SE_TH_GAZ_non_eligible_CHAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_GAZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.EligibleForCham__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_GAZ and Eligible CHAM = False</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_GAZ_MAINT</fullName>
        <actions>
            <name>Vente_SE_TH_GAZ_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_GAZ_MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_GAZ_MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_INST</fullName>
        <actions>
            <name>Confirmation_SE_TH_INST</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_INST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_INST</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_INST_GAZ</fullName>
        <actions>
            <name>Confirmation_SE_TH_INST_GAZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_INST_GAZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_INST_GAZ</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_INST_GAZ_MAINT</fullName>
        <actions>
            <name>Vente_SE_TH_INST_GAZ_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_INST_GAZ_MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_INST_GAZ_MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_INST_MAINT</fullName>
        <actions>
            <name>Confirmation_SE_TH_INST_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_INST_MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_INST_MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - Vente SE_TH_MAINT</fullName>
        <actions>
            <name>Confirmation_SE_TH_INST_MAINT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>equals</operation>
            <value>SE_TH_MAINT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web,Beta Test,Kooroo,CRC</value>
        </criteriaItems>
        <description>[SiCo] Sends confirmation email if Offer Name = SE_TH_MAINT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo Calendrier de relances</fullName>
        <active>true</active>
        <description>Ce WF permet d&apos;envoyer les différentes relances si le prospect ne finalise pas sa souscription</description>
        <formula>AND(  OR(ISPICKVAL( Status, &apos;Etape Panier&apos;), ISPICKVAL(Status, &apos;Etape Coordonnées&apos;), ISPICKVAL(Status,&apos;Etape Contrat Gaz&apos;), ISPICKVAL(Status,&apos;Etape Contrat Maintenance&apos;), ISPICKVAL(Status,&apos;Etape Installation&apos;), ISPICKVAL(Status,&apos;Etape Livraison&apos;), ISPICKVAL(Status,&apos;Etape Récapitulatif&apos;)),AND(  ISBLANK( PaymentMethodCreditCard__c ),  ISBLANK( PaymentMethodSepa__c )),  Not(ISPICKVAL( LeadSource , &quot;Lick&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ReminderEmail1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>ReminderEmail2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.LastModifiedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Rappel_Prospect_relance_souscription</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.LastModifiedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SiCo Email Reprise de parcours</fullName>
        <actions>
            <name>EmailFinalization</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ce WF permet d&apos;envoyer un email au prospect contenant l&apos;url drupal afin qu&apos;il puisse retrouver son parcours de souscription</description>
        <formula>OR(  ISPICKVAL( Status, &apos;Etape Panier&apos;),  ISPICKVAL( Status, &apos;Etape Coordonnées&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Gaz&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Maintenance&apos;),  ISPICKVAL( Status, &apos;Etape Installation&apos;),  ISPICKVAL( Status, &apos;Etape Livraison&apos;),  ISPICKVAL( Status, &apos;Etape Récapitulatif&apos;))  &amp;&amp; ProcessStopped__c = True &amp;&amp; OR( ISPICKVAL(LeadSource, &quot;CRC&quot;), ISPICKVAL(LeadSource, &quot;Web&quot;),ISPICKVAL(LeadSource, &quot;Hybride&quot;),ISPICKVAL(LeadSource, &quot;Kooroo&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo Email Reprise de parcours Lick - SE sans Gaz</fullName>
        <actions>
            <name>Reprise_de_parcours_Lick_SE_sans_Gaz</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ce WF permet d&apos;envoyer un email au prospect contenant l&apos;url drupal afin qu&apos;il puisse retrouver son parcours de souscription LICK 
Fonctionne uniquement pour SE et SE_MAINT</description>
        <formula>OR(  ISPICKVAL( Status, &apos;Etape Panier&apos;),  ISPICKVAL( Status, &apos;Etape Coordonnées&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Gaz&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Maintenance&apos;),  ISPICKVAL( Status, &apos;Etape Installation&apos;),  ISPICKVAL( Status, &apos;Etape Livraison&apos;),  ISPICKVAL( Status, &apos;Etape Récapitulatif&apos;))  &amp;&amp; ProcessStopped__c = True &amp;&amp; ISPICKVAL(LeadSource, &quot;Lick&quot;) &amp;&amp; OR(OfferName__c = &quot;SE&quot;, OfferName__c = &quot;SE_MAINT&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo Email Reprise de parcours Lick - SE_TH sans Gaz</fullName>
        <actions>
            <name>Reprise_de_parcours_Lick_SE_TH_sans_Gaz</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ce WF permet d&apos;envoyer un email au prospect contenant l&apos;url drupal afin qu&apos;il puisse retrouver son parcours de souscription LICK 
Fonctionne uniquement pour Offre SE_TH sans gaz</description>
        <formula>OR(  ISPICKVAL( Status, &apos;Etape Panier&apos;),  ISPICKVAL( Status, &apos;Etape Coordonnées&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Gaz&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Maintenance&apos;),  ISPICKVAL( Status, &apos;Etape Installation&apos;),  ISPICKVAL( Status, &apos;Etape Livraison&apos;),  ISPICKVAL( Status, &apos;Etape Récapitulatif&apos;))  &amp;&amp; ProcessStopped__c = True &amp;&amp; ISPICKVAL(LeadSource, &quot;Lick&quot;) &amp;&amp; AND(CONTAINS(OfferName__c, &quot;SE_TH&quot;), NOT(CONTAINS(OfferName__c, &quot;GAZ&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo Email Reprise de parcours Lick - avec Gaz</fullName>
        <actions>
            <name>Reprise_de_parcours_Lick_avec_Gaz</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ce WF permet d&apos;envoyer un email au prospect contenant l&apos;url drupal afin qu&apos;il puisse retrouver son parcours de souscription LICK 
Fonctionne uniquement pour Offre avec du Gaz</description>
        <formula>OR(  ISPICKVAL( Status, &apos;Etape Panier&apos;),  ISPICKVAL( Status, &apos;Etape Coordonnées&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Gaz&apos;),  ISPICKVAL( Status, &apos;Etape Contrat Maintenance&apos;),  ISPICKVAL( Status, &apos;Etape Installation&apos;),  ISPICKVAL( Status, &apos;Etape Livraison&apos;),  ISPICKVAL( Status, &apos;Etape Récapitulatif&apos;))  &amp;&amp; ProcessStopped__c = True &amp;&amp; ISPICKVAL(LeadSource, &quot;Lick&quot;) &amp;&amp; CONTAINS(OfferName__c, &quot;GAZ&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SiCo Site Number</fullName>
        <active>false</active>
        <description>Ajoute le site number</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SicO - Vente Lick Gaz</fullName>
        <actions>
            <name>Confirmation_Lick_SE_TH_INST_GAZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Lick</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>contains</operation>
            <value>GAZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OfferName__c</field>
            <operation>notContain</operation>
            <value>MAINT</value>
        </criteriaItems>
        <description>Envoie un email de confirmation de commande poure toute vente Lick contenant du gaz</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Rappel_Prospect_relance_souscription</fullName>
        <assignedTo>Tous</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Rappeler le prospect pour le relancer sur sa souscription.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Ouverte</status>
        <subject>Rappel Prospect relance souscription</subject>
    </tasks>
</Workflow>
