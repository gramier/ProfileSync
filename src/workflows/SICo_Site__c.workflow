<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SIC</fullName>
        <description>Update site number</description>
        <field>SiteNumber__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>SICO Update Site Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialBoilerAge</fullName>
        <description>[SICo] : this field is updated with the value of the field BoilerAge</description>
        <field>InitialBoilerAge__c</field>
        <formula>TEXT(BoilerAge__c)</formula>
        <name>SICo FU InitialBoilerAge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialCeramicGlassPlatePresent</fullName>
        <description>[SICo] : this field is filled with the initial value contained before any changes in the field PrincipalHeatingSystemType.</description>
        <field>InitialCeramicGlassPlatePresent__c</field>
        <formula>IF( IsCeramicGlassPlatePresent__c =true,&quot;oui&quot;,&quot;non&quot;)</formula>
        <name>SICo FU InitialCeramicGlassPlatePresent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialGasSource</fullName>
        <description>[SICo] : This field is updated with the value of the field GasSource</description>
        <field>InitialGasSource__c</field>
        <formula>TEXT(GasSource__c)</formula>
        <name>SICo FU InitialGasSource</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialNoOfOccupants</fullName>
        <description>[SICO]: this field is updated with the initial value of the field NoOfOccupants__c.</description>
        <field>InitialNoOfOccupants__c</field>
        <formula>NoOfOccupants__c</formula>
        <name>SICo FU InitialNoOfOccupants</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialPrincHeatingSystemTypeDet</fullName>
        <description>[SICo] : this field is updated with the value of the field PrincHeatingSystemTypeDetail</description>
        <field>InitialPrincHeatingSystemTypeDetail__c</field>
        <formula>TEXT(PrincHeatingSystemTypeDetail__c)</formula>
        <name>SICo FU InitialPrincHeatingSystemTypeDet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialPrincipalHeatingSystemTyp</fullName>
        <description>[SICo]: the field Initial PrincipalHeatingSystemType is updated with the initial value of the field InitialPrincipalHeatingSystemType.</description>
        <field>InitialPrincipalHeatingSystemType__c</field>
        <formula>TEXT(PrincipalHeatingSystemType__c)</formula>
        <name>SICo FU InitialPrincipalHeatingSystemTyp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialSanitaryHotWater</fullName>
        <field>InitialSanitaryHotWaterType__c</field>
        <formula>TEXT(SanitaryHotWaterType__c)</formula>
        <name>SICo FU InitialSanitaryHotWater</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_InitialSurfaceInSqMeter</fullName>
        <description>[SICo] : the field IInitialSurfaceInSqMeter is updated with the initial value contained in the field SurfaceInSqMeter__c</description>
        <field>InitialSurfaceInSqMeter__c</field>
        <formula>surfaceInSqMeter__c</formula>
        <name>SICo FU InitialSurfaceInSqMeter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_IsActive</fullName>
        <description>Mise à jour de la case à cocher &quot;IsActive__c&quot;</description>
        <field>IsActive__c</field>
        <literalValue>0</literalValue>
        <name>SICo FU IsActive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_reactiver_le_site</fullName>
        <field>IsActive__c</field>
        <literalValue>1</literalValue>
        <name>SICo FU réactiver le site</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SiCo Site Number</fullName>
        <actions>
            <name>SIC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update SIte Number</description>
        <formula>true &amp;&amp; NOT($User.BypassWF__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
