<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Community_Notification_de_Nouveau_Post_public</fullName>
        <description>Community : Notification de Nouveau Post public</description>
        <protected>false</protected>
        <recipients>
            <recipient>CommuntyModerators</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Community_Nouveau_post_public</template>
    </alerts>
</Workflow>
