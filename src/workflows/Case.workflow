<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SICo_Envoi_de_mail_de_retractation_Resiliation</fullName>
        <description>SICo_Envoi de mail de rétractation/Résiliation</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Fermeture_d_un_case_resiliation_retractation</template>
    </alerts>
    <alerts>
        <fullName>recherche_PCE_inconnu</fullName>
        <description>Recherche_PCE inconnu</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SiCoSubscriptionEmails/HTML_PCE_Inconnu</template>
    </alerts>
    <fieldUpdates>
        <fullName>SICo_Set_Case_Record_Type_DemandeClient</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CustomerDemand</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SICo Set Case Record Type demande client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SICO Confirmer fermeture case  retractation%2Frésiliation</fullName>
        <actions>
            <name>SICo_Envoi_de_mail_de_retractation_Resiliation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject__c</field>
            <operation>equals</operation>
            <value>Rétention</value>
        </criteriaItems>
        <description>Ce WF envoie un mail lorsq&apos;un case Rétractation ou Résiliation est fermé.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SICo Case WebCallBack</fullName>
        <actions>
            <name>SICo_Set_Case_Record_Type_DemandeClient</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsWebCallBack__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Triggered when a case is created with IsWebCallBack__c = true:
Sets the correct record type.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SiCo - PCE inconnu</fullName>
        <actions>
            <name>recherche_PCE_inconnu</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>PCE non reconnu - A chercher</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
