<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EmailCustomerAreaActivation</fullName>
        <description>Email Activation Espace Client</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>user.madteam@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SiCoSubscriptionEmails/SiCoCustomerAreaActivation</template>
    </alerts>
</Workflow>
