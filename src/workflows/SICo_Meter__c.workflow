<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DTR</fullName>
        <description>[SiCo]: DTR = DTR+6</description>
        <field>DTR__c</field>
        <formula>DATE(YEAR(DTR__c),(MONTH(DTR__c)+6),DAY(DTR__c))</formula>
        <name>DTR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DTR6</fullName>
        <description>[SiCo]: DTR= DTR+6</description>
        <field>DTR__c</field>
        <formula>DATE(
year(DTR__c)
+ floor((month(DTR__c) + 6)/12) + if(and(month(DTR__c)=12,6&gt;=12),-1,0)
,
if( mod( month(DTR__c) + 6 , 12 ) = 0, 12 , mod( month(DTR__c) + 6 , 12 ))
,
min(
day(DTR__c),
case(
max( mod( month(DTR__c) + 6 , 12 ) , 1),
9,30,
4,30,
6,30,
11,30,
2,28,
31
)
)
)</formula>
        <name>DTR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SiCo_DTR</fullName>
        <active>true</active>
        <criteriaItems>
            <field>SICo_Meter__c.DTR__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>[SiCo]: DTR + 6 months</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DTR6</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
