<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>[SICo] : Usage received from Zuora</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>BillingAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>[SICo] : Billing Account related to this usage</description>
        <externalId>false</externalId>
        <label>Billing Account</label>
        <referenceTo>Zuora__CustomerAccount__c</referenceTo>
        <relationshipLabel>Usages</relationshipLabel>
        <relationshipName>Usages</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Category__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(InvoiceItem__r.Category__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Category</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConsoKWH__c</fullName>
        <externalId>false</externalId>
        <formula>InvoiceItem__r.ConsoKWH__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ConsoKWH</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConsumptionM3__c</fullName>
        <description>[SICO] consumption (m3) used to retrieve m3 consumption from Zuora</description>
        <externalId>false</externalId>
        <label>ConsumptionM3</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Consumption__c</fullName>
        <description>[SICO] used to retrieve consumption from Zuora</description>
        <externalId>false</externalId>
        <label>Consumption</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EndDateCSO__c</fullName>
        <description>[SICO] End Date of consumption</description>
        <externalId>false</externalId>
        <label>Date CSO fin</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EndDate__c</fullName>
        <description>[SICo] : EndDate</description>
        <externalId>false</externalId>
        <label>Date de fin</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EndIndex__c</fullName>
        <defaultValue>0</defaultValue>
        <description>[SICo] : EndIndex</description>
        <externalId>false</externalId>
        <label>Index de fin</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EndTypeIndex__c</fullName>
        <description>[SICo] : EndTypeIndex</description>
        <externalId>false</externalId>
        <label>Type d&apos;index de fin</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Estimation</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Relève GRDF</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Corrigé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Données capteur Sowee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Auto-relève client</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rectifié</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Auto-relevé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Estimé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Réel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Télé-relevé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Régulé</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Event__c</fullName>
        <description>[SiCo] Déclencheur de la relève (ex de valeur acceptée: RESIL)</description>
        <externalId>false</externalId>
        <label>Evenement de releve</label>
        <length>8</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExtraBilledLine__c</fullName>
        <description>[SICo] : Extra Billed Line from CHAM</description>
        <externalId>false</externalId>
        <label>Ligne à facturer</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InvoiceItem__c</fullName>
        <description>[SICO] Usages linked to the invoice item</description>
        <externalId>false</externalId>
        <label>Ligne de facturation</label>
        <referenceTo>SiCo_InvoiceItem__c</referenceTo>
        <relationshipLabel>Usages</relationshipLabel>
        <relationshipName>Usages</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Invoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>[SICO] Invoice linked to this usage</description>
        <externalId>false</externalId>
        <label>Invoice</label>
        <referenceTo>Zuora__ZInvoice__c</referenceTo>
        <relationshipLabel>Usages</relationshipLabel>
        <relationshipName>Usages</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Name__c</fullName>
        <externalId>false</externalId>
        <formula>InvoiceItem__r.ChargeName__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PCS__c</fullName>
        <description>[SICo] : PCS</description>
        <externalId>false</externalId>
        <label>PCS</label>
        <precision>10</precision>
        <required>false</required>
        <scale>8</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductRatePlanCharge__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>[SICo] : Charge related to this Usage</description>
        <externalId>false</externalId>
        <label>Product Rate Plan Charge</label>
        <referenceTo>zqu__ProductRatePlanCharge__c</referenceTo>
        <relationshipLabel>Usages</relationshipLabel>
        <relationshipName>Usages</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <description>[SICo] : Quantity</description>
        <externalId>false</externalId>
        <label>Quantité</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SiteMeterGas__c</fullName>
        <description>[SICO] Id of the Gas active meter site, linked to this usage</description>
        <externalId>false</externalId>
        <formula>Subscription__r.SiteId__r.ActiveSiteMeterGas__r.Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SiteMeterGas</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SiteMeter__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>[SICO] Technical link, to retrieve SiteMeter data on Usage</description>
        <externalId>false</externalId>
        <label>Compteur du Site Gaz actif</label>
        <referenceTo>SICo_SiteMeter__c</referenceTo>
        <relationshipLabel>Usages</relationshipLabel>
        <relationshipName>Usages</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>StartDateCSO__c</fullName>
        <description>[SICO] Start date of consumption</description>
        <externalId>false</externalId>
        <label>Date CSO début</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>StartDate__c</fullName>
        <description>[SICo] : StartDate</description>
        <externalId>false</externalId>
        <label>Date de début</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>StartIndexType__c</fullName>
        <description>[SICo] : StartIndexType</description>
        <externalId>false</externalId>
        <label>Type d&apos;index de début</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Estimation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Relève GRDF</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Corrigé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Données capteur Sowee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Auto-relève client</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rectifié</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Auto-relevé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Estimé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Réel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Régulé</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Télé-relevé</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>StartIndex__c</fullName>
        <defaultValue>0</defaultValue>
        <description>[SICo] : Start Index</description>
        <externalId>false</externalId>
        <label>Index de début</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Subscription__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>[SICo] : Subscription related to this Usage</description>
        <externalId>false</externalId>
        <label>Subscription</label>
        <referenceTo>Zuora__Subscription__c</referenceTo>
        <relationshipLabel>Usages</relationshipLabel>
        <relationshipName>Usages</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>TVA__c</fullName>
        <description>[SICO] TVA of the linked invoice item</description>
        <externalId>false</externalId>
        <formula>InvoiceItem__r.TauxTVA__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TVA</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>TotalAmountTTC__c</fullName>
        <description>[SICO] Total Amount TTC of the linked invoice item</description>
        <externalId>false</externalId>
        <formula>InvoiceItem__r.TotalAmountTTC__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TotalAmountTTC</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalAmount__c</fullName>
        <description>[SICO] Total amount of the linked invoice item</description>
        <externalId>false</externalId>
        <formula>InvoiceItem__r.TotalAmount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TotalAmount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UOM__c</fullName>
        <description>[SICo] : Unit of Measure</description>
        <externalId>false</externalId>
        <label>UOM</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitPriceInvoiceItem__c</fullName>
        <description>[SICO] Unit Price of the linked Invoice Item</description>
        <externalId>false</externalId>
        <formula>InvoiceItem__r.UnitPrice__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>UnitPrice InvoiceItem</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitPrice__c</fullName>
        <description>[SiCo]: Unit price of the Usage</description>
        <externalId>false</externalId>
        <label>UnitPrice</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UsageMin__c</fullName>
        <description>[SICO] Min of all usages linked to the invoice item</description>
        <externalId>false</externalId>
        <formula>InvoiceItem__r.UsageMin__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>UsageMin</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ZuoraExternalId__c</fullName>
        <description>[SiCo]: External Id to retrieve Usage from Zuora linked to Invoice Item</description>
        <externalId>true</externalId>
        <label>Zuora External Id</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Zuora_Custom_Ext_Id__c</fullName>
        <externalId>true</externalId>
        <label>Zuora Custom Ext Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <gender>Masculine</gender>
    <label>Usage</label>
    <listViews>
        <fullName>All</fullName>
        <columns>OBJECT_ID</columns>
        <columns>NAME</columns>
        <columns>BillingAccount__c</columns>
        <columns>PCS__c</columns>
        <columns>Quantity__c</columns>
        <columns>UOM__c</columns>
        <columns>Zuora_Custom_Ext_Id__c</columns>
        <columns>ZuoraExternalId__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>Tout</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Nom de Usage</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Usages</pluralLabel>
    <searchLayouts>
        <listViewButtons>Delete</listViewButtons>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>Delete</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Delete</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&apos;/soap/ajax/29.0/connection.js&apos;)}
try
{
  var selectedRecords = {!GETRECORDIDS( $ObjectType.Usage__c )};
  if(selectedRecords.length&lt;1)
    alert(&apos;Please Select at Least One Row !&apos;);
  else
  {
    userConsent = confirm(
        selectedRecords.length + 
        &apos; Record(s) will be Deleted. Continue ? &apos;
      );
    if(userConsent == true)
    {
      delResult = sforce.connection.deleteIds(selectedRecords);
      if (delResult[0].getBoolean(&quot;success&quot;))
      {
        alert(&apos;The Record(s) were Deleted Successfully.&apos;); 
        window.location.reload();
      }
      else
        alert(
          &apos;The Record(s) Could Not be Deleted. Error Message: &apos; + 
          delResult[0].errors.message
        );
    }  
  }
}
catch(e)
{
  alert(&apos;The Action Could not be Completed. Error Message: &apos; + e);
}</url>
    </webLinks>
</CustomObject>
