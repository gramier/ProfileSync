<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>[SICo] : Container Object for bundles managing</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Available_FirstBuy__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Disponible à la primo acquisition</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Available_Upsell__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Disponible à l&apos;upsell</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>BundleParent__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>[SiCo] Bundle Parent</description>
        <externalId>false</externalId>
        <label>BundleParent</label>
        <referenceTo>SICo_Bundle__c</referenceTo>
        <relationshipName>Bundles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>DisplayedOrder__c</fullName>
        <description>[SiCo] Displayed order of bundle/ single offer</description>
        <externalId>false</externalId>
        <label>Ordre d&apos;affichage</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ElectricTerminalInstallation__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Electric terminal Installation</description>
        <externalId>false</externalId>
        <label>Installation BVE</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ElectricTerminal__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] indicates if bundle contains electric terminal</description>
        <externalId>false</externalId>
        <label>Borne electrique</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Eligibility_CHAM__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Eligibilité à CHAM</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Eligibility_Elec__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Eligibilité ENEDIS</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Eligibility_TH__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Eligibilité au thermostat chaudière</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Eligibility_gas__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Eligibilité au gaz</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EndDate__c</fullName>
        <externalId>false</externalId>
        <label>Date fin</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>GasPeriodicite__c</fullName>
        <description>[SiCo] Gas Frequency</description>
        <externalId>false</externalId>
        <label>Périodicité Gaz</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>12</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>24</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>36</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Gas__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Indicates if there is gas in the bundle</description>
        <externalId>false</externalId>
        <label>Gaz</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>GazType__c</fullName>
        <description>[SiCo] Gas type</description>
        <externalId>false</externalId>
        <label>Gaz - Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Connecté</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Non Connecté</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>GsxPartners__c</fullName>
        <description>[SiCo] GSX Patners</description>
        <externalId>false</externalId>
        <label>Partenaires GSX</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Lick</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Installation__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Indicates if there is the installation</description>
        <externalId>false</externalId>
        <label>Installation</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MaintenanceFrequency__c</fullName>
        <description>[SiCo] Maintenance Frequency</description>
        <externalId>false</externalId>
        <label>Periodicité Maintenance</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>12</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>24</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>36</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Maintenance__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Indicates if there is the maintenance</description>
        <externalId>false</externalId>
        <label>Maintenance</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MarketingOfferName__c</fullName>
        <description>[SiCo] Marketing Offer Name</description>
        <externalId>false</externalId>
        <label>Nom de l&apos;offre commerciale</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Offer__c</fullName>
        <externalId>false</externalId>
        <label>Offre</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Bundle</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Offre vendue seule</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>OutOfSoweeWebsite__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Offer purchased out of C43 website (Amazon for exemple)</description>
        <externalId>false</externalId>
        <inlineHelpText>Offre souscrite hors parcours interne (concerne le GSB)</inlineHelpText>
        <label>Canal hors site Sowee</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Patrimoine_incompatible_upsell__c</fullName>
        <externalId>false</externalId>
        <label>Patrimoine incompatible upsell</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SICO_Offers</valueSetName>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Patrimoine_necessaire_upsell__c</fullName>
        <externalId>false</externalId>
        <label>Patrimoine nécessaire upsell</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SICO_Offers</valueSetName>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Periodicite__c</fullName>
        <externalId>false</externalId>
        <label>Périodicité</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>12</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>24</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>36</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>PublicLabelLine2__c</fullName>
        <description>[SICo] Line 2 of the label displayed in the Contracts page of the Customer Space.</description>
        <externalId>false</externalId>
        <inlineHelpText>Ligne 2 du libellé affiché dans la section &quot;Contrats&quot; de l&apos;Espace Client.</inlineHelpText>
        <label>Libellé public (ligne 2)</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublicLabel__c</fullName>
        <description>[SICo] Label displayed in the Contracts page of the Customer Space.</description>
        <externalId>false</externalId>
        <inlineHelpText>Libellé affiché dans la section &quot;Contrats&quot; de l&apos;Espace Client.</inlineHelpText>
        <label>Libellé public</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SalesChannel__c</fullName>
        <description>[SiCo] Sales channel to sell bundle</description>
        <externalId>false</externalId>
        <label>Canal de vente</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Site Sowee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Lick</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Kooroo</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>DARTY</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SkuNumber__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>[SiCo] SKU</description>
        <externalId>false</externalId>
        <label>SKU</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>StandardGasConsumption__c</fullName>
        <description>[SiCo] Standard Gas Consumption for public price displaying</description>
        <externalId>false</externalId>
        <label>Consommation Gaz Standard</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>StartDate__c</fullName>
        <externalId>false</externalId>
        <label>Date début</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Station__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Indicates if there is Station in the bundle</description>
        <externalId>false</externalId>
        <label>Station Energie</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TechOfferName__c</fullName>
        <description>[SiCo] Technical Offer Name</description>
        <externalId>false</externalId>
        <label>Nom de l&apos;offre tech</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Thermostat__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Indicates if there is a thermostat</description>
        <externalId>false</externalId>
        <label>Thermostat</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <gender>Masculine</gender>
    <label>Offre SOWEE</label>
    <listViews>
        <fullName>DARTY</fullName>
        <columns>NAME</columns>
        <columns>TechOfferName__c</columns>
        <columns>Available_FirstBuy__c</columns>
        <columns>Available_Upsell__c</columns>
        <columns>Eligibility_CHAM__c</columns>
        <columns>Eligibility_gas__c</columns>
        <columns>Eligibility_TH__c</columns>
        <columns>Eligibility_Elec__c</columns>
        <columns>SkuNumber__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>NAME</field>
            <operation>contains</operation>
            <value>DARTY</value>
        </filters>
        <label>DARTY</label>
    </listViews>
    <listViews>
        <fullName>Kooroo</fullName>
        <columns>NAME</columns>
        <columns>SkuNumber__c</columns>
        <columns>TechOfferName__c</columns>
        <columns>MarketingOfferName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>SalesChannel__c</field>
            <operation>equals</operation>
            <value>Kooroo</value>
        </filters>
        <label>Kooroo</label>
    </listViews>
    <listViews>
        <fullName>Lick</fullName>
        <columns>NAME</columns>
        <columns>MarketingOfferName__c</columns>
        <columns>TechOfferName__c</columns>
        <columns>SkuNumber__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>SalesChannel__c</field>
            <operation>equals</operation>
            <value>Lick</value>
        </filters>
        <label>Lick</label>
    </listViews>
    <nameField>
        <label>Bundle Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Offres SOWEE</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>SICo_Deep_Clone</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>SICo Deep Clone</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/30.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/30.0/apex.js&quot;)}

var idBundle = &quot;{!SICo_Bundle__c.Id}&quot;;

var id = sforce.apex.execute(&quot;SICo_UtilityCloneData_CLS&quot;,&quot;cloneBundle&quot;,{idOffreSowee:idBundle});

window.location.href = &apos;/&apos; + id;</url>
    </webLinks>
</CustomObject>
