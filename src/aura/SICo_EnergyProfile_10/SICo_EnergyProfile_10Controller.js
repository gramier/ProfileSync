({
	updateInformations : function (component, event, pHelper) {
		// Save informations entered at screen on the current site
		var action = component.get("c.setInformations_10");
		component.set("v.epw.wineCellar", $("input[id='wineCellar']").is(':checked'));
		component.set("v.epw.swimmingPool", $("input[id='swimmingPool']").is(':checked'));
		component.set("v.epw.aquarium", $("input[id='aquarium']").is(':checked'));
		var vNoneEquipment = $("input[id='noneEquipment']").is(':checked');
		// "Disable" the validate button
		pHelper.disableValidate(); 
		if (component.get("v.epw.wineCellar") == false 
				&& component.get("v.epw.swimmingPool") == false
				&& component.get("v.epw.aquarium") == false
				&& vNoneEquipment == false) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoValue"), "Error_10");
		} else {
			action.setParams({
				siteId : component.get("v.epw.Id"),
				wineCellar : component.get("v.epw.wineCellar"),
				swimmingPool : component.get("v.epw.swimmingPool"),
				aquarium : component.get("v.epw.aquarium")
			});

			action.setCallback(this, function (response) {
				pHelper.moveToNextStep(component.get("v.epw.heat")); 			
			}); 
			$A.enqueueAction(action);
		}
	},

	uncheckAllEquipments : function (component, event, helper) {
		$(".equipmentOtherCheckbox").prop('checked', false);
		$("#noneEquipment").prop('checked', true);
	},

	uncheckNoneEquipment : function (component, event, helper) {
		$("#noneEquipment").prop('checked', false);
		if ($('#wineCellar').prop('checked') == false && $('#swimmingPool').prop('checked') == false && $('#aquarium').prop('checked') == false) {
			$("#noneEquipment").prop('checked', true);
		}
	},	 
	 	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_10");
	}
})