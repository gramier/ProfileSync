({
	moveToNextStep : function() {  
		var vEPEvent = $A.get("e.c:SICo_EnergyProfileEvent");
		if (vEPEvent != null) {  
			vEPEvent.setParams({
				"componentToDisplayAfterHide" : "EP_05",
				"componentToHide" : "EP_04"
			});
			vEPEvent.fire();
		}
	}, 
	
	moveToPreviousStep : function(pComponent) {
		var vEPEvent = $A.get("e.c:SICo_EnergyProfileEvent");
		if (vEPEvent != null) {  			 
			var vApptOrHouse = pComponent.get("v.epw.houseType"); 
			var vPreviousPage = "EP_03_House";
			if (vApptOrHouse == 'Appartement') {
				vPreviousPage = "EP_03_Appart";
			}
        	vEPEvent.setParams({
	            "componentToDisplayAfterHide" : vPreviousPage,
	            "componentToHide" : "EP_04"
	        });
        	vEPEvent.fire();			 
		}
	},

	displayError : function(pComponent, pError, pErrorContainerId) { 
		$A.createComponents(
			[
                ["ui:message",{
                    "title" : "",
                    "severity" : "error",
                }],
                ["ui:outputText",{
					"value" : pError
				}]
			],

			function(components, status, errorMessage) {
				if (status === "SUCCESS") {
					var vMessage = components[0];
					var vOutputText = components[1];
					// set the body of the ui:message to be the ui:outputText
					vMessage.set("v.body", vOutputText);
					var vErrorContainer = pComponent.find(pErrorContainerId);
					// Replace div body with the dynamic component 
					vErrorContainer.set("v.body", vMessage);
				}
			}
		);
	},

	disableValidate : function() { 
		$(".btnValidate").addClass("btnValidateDisabledTemp");
	},
	enableValidate : function() {
		$(".btnValidate").removeClass("btnValidateDisabledTemp");
	},
	resetErrorContainer : function(pComponent, pErrorContainerId) {
		var vErrorContainer = pComponent.find(pErrorContainerId);
		vErrorContainer.set("v.body", null);
	}
})