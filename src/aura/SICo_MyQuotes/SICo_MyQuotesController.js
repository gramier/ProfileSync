({
	doInit : function(pComponent, pEvent, pHelper) { 
		var vQueryQuotesFunction = pComponent.get("c.queryQuotes"); 
		var vAccountId = pComponent.get("v.accountId"); 
		if (vQueryQuotesFunction != null) {
			vQueryQuotesFunction.setParams({ "pAccountId" : vAccountId});
			vQueryQuotesFunction.setCallback(pComponent, function(pResponse) {
				var vState = pResponse.getState();
				if (vState === "SUCCESS") {   
					pComponent.set("v.quoteList", pResponse.getReturnValue());					 
				} else { 
					pComponent.set("v.quoteList", null);
				}
			});
			$A.enqueueAction(vQueryQuotesFunction);
		}
	}
})