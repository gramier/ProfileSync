({
    updateSoweeNewsLettersSubscription : function(cmp) {
        var action = cmp.get("c.setNewsLettersSubscription");
        action.setParams({
            accountId : cmp.get("v.accountId"),
            checkboxValue : cmp.get("v.soweeOptin"),
            newsLetter : "sowee"
        });
        $A.enqueueAction(action);
	},

    updatePartnersNewsLettersSubscription : function(cmp) {
        var action = cmp.get("c.setNewsLettersSubscription");
        action.setParams({
            accountId : cmp.get("v.accountId"),
            checkboxValue : cmp.get("v.partnerOptin"),
            newsLetter : "partners"
        });
        $A.enqueueAction(action);
	},

})