({
	handleEPEvent : function(pComponent, pEvent) {  
        var vParams = pEvent.getParams();
        var vComponentContainer = pComponent.find("EP_13_Container");
        if (vParams != null && vParams.componentToDisplay != null && vParams.componentToDisplay == "SICo_EnergyProfile_13") {
        	$A.util.removeClass(vComponentContainer, "hiddenComponent");
        }
        if (vParams != null && vParams.componentToHide != null && vParams.componentToHide == "SICo_EnergyProfile_13") {
        	$A.util.addClass(vComponentContainer, "hiddenComponent");        	
        }
	}
})