({
	updateInformations : function(component, event, pHelper) {
		var idAccount = component.get( "v.accountId");

		component.set("v.epw.windowsRestored",  $("input[id='windows']").is(':checked'));
		component.set("v.epw.wallsRestored",  $("input[id='walls']").is(':checked'));
		component.set("v.epw.roofRestored",  $("input[id='roofRestoration']").is(':checked'));
		var vNoneSelected = $("input[id='none']").is(':checked');

		// "Disable" the validate button
		pHelper.disableValidate(); 
		if (component.get("v.epw.windowsRestored") == false 
				&& component.get("v.epw.wallsRestored") == false
				&& component.get("v.epw.roofRestored") == false
				&& vNoneSelected == false) {
			pHelper.displayError(component, $A.get("{!$Label.c.SICo_ErrorNoValue}"), "Error_05");
		} else {
			//put the values in the apex controller parameters
			var action = component.get("c.setInformations_05");

			action.setParams({
				siteId : component.get("v.epw.Id"),
				windows : component.get("v.epw.windowsRestored"),
				walls : component.get("v.epw.wallsRestored"),
				roof : component.get("v.epw.roofRestored")
			});
			//redirect to the next page
			action.setCallback(this, function(response){
	 			 //window.location.href="/apex/EnergyProfile_05Bis?AccountId=" + idAccount + (component.get("v.isMobile") ? '&isMobile=1' : '');
	 			 pHelper.moveToNextStep();
			});
			$A.enqueueAction(action);
		}
	},

	uncheckAll : function(component, event, helper) {
		//uncheck the checkboxes with the class equipmentCheckbox
		$(".equipmentCheckbox").prop('checked', false);
		$("#none").prop('checked', true);
		helper.enableValidate();
		helper.resetErrorContainer(component, "Error_05");
	},

	uncheckNone : function(component, event, helper) {
		//uncheck the checkbox with the id none
		$("#none").prop('checked', false);
		if($('#windows').prop('checked') == false && $('#walls').prop('checked') == false && $('#roof').prop('checked') == false){
 			$("#none").prop('checked', true);
		}
		helper.enableValidate();
		helper.resetErrorContainer(component, "Error_05");
	},
   
	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_05");
	}

})