({
	doInit : function(component, event, helper) {
		var idAccount = component.get( "v.accountId");
		console.log("account id : " + idAccount);
		//get the account id and pass it into the apex controller
		var action = component.get("c.getAccount");
		action.setParams({ accountId : component.get("v.accountId") });

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.title", response.getReturnValue().Salutation);
				component.set("v.lastName", response.getReturnValue().LastName);
				component.set("v.firstName", response.getReturnValue().FirstName);
				component.set("v.newAdressNumberStreet", response.getReturnValue().ContactAddressNumberStreet__c);
				component.set("v.newAdressAddition", response.getReturnValue().ContactAddressAdditionToAddress__c);
				component.set("v.newAdressBuilding", response.getReturnValue().ContactAdresseBuilding__c);
				component.set("v.newAdressStairs", response.getReturnValue().ContactAddressStair__c);
				component.set("v.newAdressFloor", response.getReturnValue().ContactAddressFloor__c);
				component.set("v.newAdressFlat", response.getReturnValue().ContactAddressFlat__c);
				component.set("v.newAdressZipCode", response.getReturnValue().ContactAddressZipCode__c);
				component.set("v.newAdressCity", response.getReturnValue().ContactAddressCity__c);
				component.set("v.newAdressCountry", response.getReturnValue().ContactAddressCountry__c);
				component.set("v.newPhone", response.getReturnValue().Phone);
				component.set("v.newMobilePhone", response.getReturnValue().PersonMobilePhone);
				if (response.getReturnValue().Salutation === "Monsieur") {
					$('#m').css({
				 		'background-color': '#f6f7f8'
					});
				} else {
					$('#mme').css({
						'background-color': '#f6f7f8',
					});
				}
			}
		});
		$A.enqueueAction(action);
 	},

	insertData: function(component, event, helper){
		var idAccount = component.get( "v.accountId");
		var address = $("input[name='address']").val();
		var zipCode = $("input[name='zipCode']").val();
		var city = $("input[name='city']").val();
		var country = $("input[name='country']").val();
		var phone = $("input[name='phone']").val();
		var mobilePhone = $("input[name='mobilePhone']").val();

		var regexTel = /\+\b33[0-9]{9}/;
		if ((phone != null && phone != '' && !regexTel.test(phone)) || (mobilePhone != null && mobilePhone != '' && !regexTel.test(mobilePhone)) || !address || !zipCode || !city || !country || !phone){
			if (phone != null && phone != '' && !regexTel.test(phone)){
				alert($A.get("$Label.c.SICo_Phone_Error"));
			}
			else if (mobilePhone != null && mobilePhone != '' && !regexTel.test(mobilePhone)){
				alert($A.get("$Label.c.SICo_MobilePhone_Error"));
			}
			if (!address || !zipCode || !city || !country || !phone){
				alert($A.get("$Label.c.SICo_ErrorSetAllRequiredFields"));
		 	}
		}
	 	else {
			var action = component.get("c.updateInformations");

	   		action.setParams({
  				accountId : component.get("v.accountId"),
				address: $("input[name='address']").val(),
				addressAddition: $("input[name='addressAddition']").val(),
				building: $("input[name='building']").val(),
				stairs: $("input[name='stairs']").val(),
				floor: $("input[name='floor']").val(),
				flat: $("input[name='flat']").val(),
				zipCode: $("input[name='zipCode']").val(),
				city: $("input[name='city']").val(),
				country: $("input[name='country']").val(),
				phone: $("input[name='phone']").val(),
				mobilePhone: $("input[name='mobilePhone']").val()
	   		});

		 	action.setCallback(this, function(response){
		 		window.location.href="/apex/PersonalInformations?AccountId=" + idAccount;
			});

	 		$A.enqueueAction(action);
 		}
 	}
})