({ 
	updateInformations : function (component, event, pHelper) {
		//set the attibute choiceType and choiceResidence
		var idAccount = component.get("v.accountId");
		component.set("v.epw.houseType", $("input[name='type']:checked").val());
		component.set("v.epw.residenceType", $("input[name='residence']:checked").val());
 
		//verify the values and redirect to the next page
		var house = component.get("v.epw.houseType");
		var type = component.get("v.epw.residenceType");
		if (house == undefined || type == undefined) {
			pHelper.disableValidate();
			pHelper.displayError(component, $A.get("{!$Label.c.SICo_ErrorNoValue}"), "Error_02");
		} else {
			//put the values in the apex controller parameters
			var action = component.get("c.setInformations_02");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				choiceHouse : component.get("v.epw.houseType"),
				choiceResidence : component.get("v.epw.residenceType")
			});
			action.setCallback(this, function (response) {
				pHelper.moveToNextStep(house); 
			});			 
			pHelper.disableValidate();
			$A.enqueueAction(action);
		}
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_02");
	}
 

})