({
	validate : function(component, event, helper) {
		helper.validate(component);
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_MeterRead");
	}
})