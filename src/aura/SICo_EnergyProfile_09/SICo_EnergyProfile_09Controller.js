({ 
	updateInformations : function (component, event, pHelper) {
		var idAccount = component.get("v.accountId");
		component.set("v.epw.cooking", $("input[name='cooking']:checked").val());
		
		// "Disable" the validate button
		pHelper.disableValidate(); 
		if (component.get("v.epw.cooking") == undefined) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoValue"), "Error_09");     
		} else {
			// Save informations entered at screen on the current site
			var action = component.get("c.setInformations_09");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				cooking : component.get("v.epw.cooking")
			});
			action.setCallback(this, function (response) {
				pHelper.moveToNextStep(component.get("v.epw.heating")); 
			}); 
			$A.enqueueAction(action);
		}
	},	
 	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_09");
	}
})