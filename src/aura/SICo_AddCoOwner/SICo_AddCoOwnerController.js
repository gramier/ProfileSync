({
	doInit : function(component, event, helper) {
		var idAccount = component.get( "v.accountId");
		console.log("account id : " + idAccount);
		//get the account id and pass it into the apex controller
		var action = component.get("c.getSite");
		action.setParams({ accountId : component.get("v.accountId") });

		action.setCallback(this, function(response) {
				var state = response.getState();

				if (state === "SUCCESS"){
					component.set("v.site", response.getReturnValue());
				}
		});
		$A.enqueueAction(action);

 },

 insertData: function(component, event, helper){
	 var idAccount = component.get( "v.accountId");
	 var sal = $("input[name='title']").val();
	 var firstN = $("input[name='firstName']").val();
	 var lastN = $("input[name='lastName']").val();

	 console.log("account id : " + idAccount);
	 console.log("salutation : " + sal);
	 console.log("lastName : " + lastN);
	 console.log("firstName : " + firstN);

	 if(!firstN || !lastN){
		 alert($A.get("$Label.c.SICo_ErrorSetAllTheFields"));
	 }
	 else{
		 var action = component.get("c.insertBeneficiary");

	   action.setParams({
	  		accountId : component.get("v.accountId"),
	  		salutation : $("input[name='title']:checked").val(),
				firstName : $("input[name='firstName']").val(),
				lastName : $("input[name='lastName']").val()
	   });

		 action.setCallback(this, function(response){
			 window.location.href="/apex/PersonalInformations?AccountId=" + idAccount;
		 });

		 $A.enqueueAction(action);
	 }

 },

 changeBackgroundMr : function(component, event, helper){
	 $('#m').css({
		 'background-color': '#f6f7f8'
	 });
	 $('#mme').css({
		 'background-color': 'white'
	 });
 },

 changeBackgroundMrs : function(component, event, helper){
	 $('#mme').css({
		 'background-color': '#f6f7f8',
	 });
	 $('#m').css({
		 'background-color': 'white'
	 });
 }
})