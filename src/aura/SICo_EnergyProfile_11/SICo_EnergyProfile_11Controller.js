({
	/*
	 * Save informations entered at screen on the current site when there is AC installation
	 */
	updateInformations : function (component, event, pHelper) {
		var valueSurfaceAC = component.get("v.epw.surfaceAC");
		var valueInstructionValueAC = component.get("v.epw.instructionValueAC");
		var vValueSurfaceHome = component.get("v.epw.surface");
		// "Disable" the validate button
		pHelper.disableValidate(); 
		if ((valueSurfaceAC === undefined || valueSurfaceAC === null || valueSurfaceAC === '')
			 || (valueInstructionValueAC === undefined || valueInstructionValueAC === null || valueInstructionValueAC === '')) { 
			pHelper.displayError(component, $A.get("$Label.c.SICo_EnergyProfile_AC_BadEntry"), "Error_11"); 
		} else if (valueSurfaceAC < 10 || valueSurfaceAC > vValueSurfaceHome) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorEnergyProfileAC"), "Error_11");  
		} else if (valueInstructionValueAC < 19 || valueInstructionValueAC > 50) {			
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorEnergyProfileACTemp"), "Error_11");   
		} else {
			var action = component.get("c.setInformations_11");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				surfaceAC : component.get("v.epw.surfaceAC"),
				instructionValueAC : component.get("v.epw.instructionValueAC"),
				noAC : false
			});
			action.setCallback(this, function (response) {
				pHelper.moveToNextStep(); 			
			}); 
			$A.enqueueAction(action); 
		}
	},

	/*
	 * Save informations entered at screen on the current site when there is no AC
	 */
	updateInformationsNoAC : function (component, event, pHelper) {

		// Save informations entered at screen on the current site
		var action = component.get("c.setInformations_11");
		action.setParams({
			siteId : component.get("v.epw.Id"),
			surfaceAC : null,
			instructionValueAC : null,
			noAC : true
		});
		action.setCallback(this, function (response) {
			pHelper.moveToNextStep(); 			
		});
		$A.enqueueAction(action);
	}, 	

	validateTemp : function (component, event, helper) {
		//to verify that the characters are numbers
		var input = component.get('v.epw.instructionValueAC');
		if (input != null && isNaN(input)) {
			//if when the key is pressed, it is not a number, replace the character
			component.set('v.epw.instructionValueAC', input.substring(0, input.length - 1));
		}
		helper.enableValidate();
		helper.resetErrorContainer(component, "Error_11");
	},	
	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_11");
	}
})