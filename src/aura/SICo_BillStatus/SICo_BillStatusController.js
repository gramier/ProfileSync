({
	doInit : function(component, event, helper) {
    var isUnpaid = component.get("v.isUnpaid");
    var isAwaitingTransfer = component.get("v.isAwaitingTransfer");
    var isPaymentInProgress = component.get("v.isPaymentInProgress");
	var isPaid = true;
    if ((isUnpaid === true || isUnpaid === 'true') || (isAwaitingTransfer === true || isAwaitingTransfer === 'true') || (isPaymentInProgress === true || isPaymentInProgress === 'true')){
        isPaid = false;
    }
	component.set("v.isPaid", isPaid);
 },
})