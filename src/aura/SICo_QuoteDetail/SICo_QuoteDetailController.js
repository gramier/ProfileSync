({
	doInit : function(pComponent, pEvent, pHelper) { 
		var vGetQuoteFunction = pComponent.get("c.getCurrentQuote");
		var vQuoteId = pComponent.get("v.quoteId"); 
		if (vGetQuoteFunction != null) {
			vGetQuoteFunction.setParams({ "pQuoteId" : vQuoteId});
			vGetQuoteFunction.setCallback(pComponent, function(pResponse) {
				var vReturnState = pResponse.getState();
				if (vReturnState == "SUCCESS") {
					pComponent.set("v.currentQuote", pResponse.getReturnValue());
				} else {
					pComponent.set("v.currentQuote", null);
				}
			});
			$A.enqueueAction(vGetQuoteFunction);
		}
	},

	accept : function(pComponent, pEvent, pHelper) {
		if (confirm($A.get('$Label.c.SICo_QuoteAcceptConfirmation'))) {
			var vFunctionAccept = pComponent.get("c.acceptQuote");
			var vQuoteId = pComponent.get("v.quoteId");
			var vQuoteWrap = pComponent.get("v.currentQuote");
			if (vFunctionAccept != null) {
				vFunctionAccept.setParams({
					'pQuoteId' : vQuoteId,
					'pQuoteJSON' : JSON.stringify(vQuoteWrap)
				});
				vFunctionAccept.setCallback(pComponent, function(pResponse) {
					var vState = pResponse.getState();
					if (vState == "SUCCESS") { 
						pComponent.set("v.currentQuote", pResponse.getReturnValue());
					} else {
						pComponent.set("v.currentQuote", null);
					}
				});
				$A.enqueueAction(vFunctionAccept);
			}
		}
	},

	refuse : function(pComponent, pEvent, pHelper) {
		if (confirm($A.get('$Label.c.SICo_QuoteRefusalConfirmation'))) {
			var vFunctionrefuse = pComponent.get("c.refuseQuote");
			var vQuoteId = pComponent.get("v.quoteId");
			var vQuoteWrap = pComponent.get("v.currentQuote");
			if (vFunctionrefuse != null) {
				vFunctionrefuse.setParams({
					'pQuoteId' : vQuoteId,
					'pQuoteJSON' : JSON.stringify(vQuoteWrap)
				});
				vFunctionrefuse.setCallback(pComponent, function(pResponse) {
					var vState = pResponse.getState();
					if (vState == "SUCCESS") {
						pComponent.set("v.currentQuote", pResponse.getReturnValue());
					} else {
						pComponent.set("v.currentQuote", null);
					}
				});
				$A.enqueueAction(vFunctionrefuse);
			}
		}
	}
})