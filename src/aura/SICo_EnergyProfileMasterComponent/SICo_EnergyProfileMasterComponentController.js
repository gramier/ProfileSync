({
	doInit : function (pComponent, pEvent, pHelper) { 
		var vIdAccount = pComponent.get("v.accountId");
		//get the account id and pass it into the apex controller
		var vAction = pComponent.get("c.getCurrentSite");
		vAction.setParams({
			pAccountId : pComponent.get("v.accountId")
		});

		vAction.setCallback(this, function (response) {
			var vState = response.getState();
			if (vState === "SUCCESS") {
				pComponent.set("v.site", response.getReturnValue());
			}
		});

		$A.enqueueAction(vAction);


		var vGetCurrentEP = pComponent.get("c.getCurrentEnergyProfileWrapper");
		vGetCurrentEP.setParams({
			pAccountId : pComponent.get("v.accountId")
		});
		vGetCurrentEP.setCallback(this, function (response) {
			var vState = response.getState();
			if (vState === "SUCCESS") {
				pComponent.set("v.epw", response.getReturnValue());
			}
		});
		$A.enqueueAction(vGetCurrentEP); 
	},

	/**
	* Handle catch of EP Event :
	* if component is the one to be hidden, hide it and then raise an event to display next 
	* (in order to do not display next component until this one finish the fadeout animation)
	* if component is the one to be displayed, just display it by removing the hiddenComponent class
	*/
	displayOrHIdeNextStep : function(pComponent, pEvent, pHelper) { 
		var vDelay = pComponent.get("v.fadeDelay");
        var vParams = pEvent.getParams();   
        // Hide
        if (vParams != null && vParams.componentToHide != null) {   
			var vJQueryComponent = $("." + vParams.componentToHide);	 
			var vEPEventDisplay = $A.get("e.c:SICo_EnergyProfileEvent"); 
        	var vDisplayFunction = function() { 
        		pHelper.displayNextStep(vParams.componentToDisplayAfterHide, vEPEventDisplay, vJQueryComponent); 
        	} 
        	vJQueryComponent.fadeOut(vDelay, vDisplayFunction);
    	}  
    	//Display
        if (vParams != null && vParams.componentToDisplayNow != null) {   
			var vJQueryComponent = $("." + vParams.componentToDisplayNow);	
        	vJQueryComponent.removeClass("hiddenComponent");    
        } 
	}
 
})