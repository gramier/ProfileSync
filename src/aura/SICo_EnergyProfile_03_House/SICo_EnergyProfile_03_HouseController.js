({
 	updateInformations : function (component, event, pHelper) {
		var idAccount = component.get("v.accountId");
		//set the attibute choiceType and choiceResidence
		component.set("v.epw.jointHouse", $("input[name='position']:checked").val() == 'true');
		component.set("v.epw.roofPresent", $("input[id='roofPresent']").is(':checked'));
		component.set("v.epw.floorPresent", $("input[id='floorPresent']").is(':checked'));
		component.set("v.epw.verandaPresent", $("input[id='verandaPresent']").is(':checked'));

 		if ($("input[name='position']:checked").val() == undefined) {
 			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoValue"), "Error_03_House");
 		} else {
			//put the values in the apex controller parameters
			var action = component.get("c.setInformations_03_House");

			action.setParams({
				siteId : component.get("v.epw.Id"),
				position : component.get("v.epw.jointHouse"),
				roof :  component.get("v.epw.roofPresent"),
				floor : component.get("v.epw.floorPresent"),
				veranda : component.get("v.epw.verandaPresent")
			});
			//redirect to the next page
			action.setCallback(this, function (response) {
				pHelper.moveToNextStep();
			});
			// "Disable" the validate button
			$(".btnValidate").addClass("btnValidateDisabledTemp");
			$A.enqueueAction(action);
		}
	}, 
	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_03_House");
	}

}