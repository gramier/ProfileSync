({
    doInit: function(cmp) {
    	var actionCheck = cmp.get("c.isLiveChatAvailable");

        actionCheck.setCallback(this, function(response) {
        	var state = response.getState();

        	if (cmp.isValid() && state === "SUCCESS") {
        		cmp.set("v.isAvailable", response.getReturnValue());

        	}else if (cmp.isValid()) {
        		var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
        	}
        });
        $A.enqueueAction(actionCheck);
    },
    
   openPopUp : function(){
        
    	var newwindow = window.open('/LiveChatPopUp', '_blank','height=380,width=380,left=100,top=100,scrollbars=yes,toolbar=no,status=no');
    	newwindow.focus();

    }

})