({ 
	changePage : function (component, event, helper) {
		//redirect to the next page
		var idAccount = component.get("v.accountId");
		window.location.href = "/apex/EnergyProfile_02?AccountId=" + idAccount + (component.get("v.isMobile") ? '&isMobile=1' : '');
	}
})