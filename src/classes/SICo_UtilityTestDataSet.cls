/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Utility class for test class (dataSet)
Test Class:    All class test call this class
History
28/06/2016      Boris Castellani     Create & develope first version
------------------------------------------------------------*/
public class SICo_UtilityTestDataSet {

/*** Config ***/

    public static final String INVOICING_TYPE_PICKLIST_MENSUEL_REEL = 'Mensuel reel'; //Picklist value for Invoicing type of Case object
	
	
	public static SICo_Anomaly__c createANO(SICo_CR__c cr){
		SICo_Anomaly__c ano = new SICo_Anomaly__c(CR__c = cr.Id);
		
		return ano;
	}
	
	public static SICo_CR__c createCR(){
        //SICo_CR__c cr = new SICo_CR__c();
        /*
        Task tsk = SICo_UtilityTestDataSet.CreateProvisioningTask('Demande Test 1');
        tsk.PCE__c = '00001';
        tsk.OmegaRequestId__c = 'TEST1';
        tsk.BatchFlag__c = true;
        tsk.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisioning');
        insert tsk;  
        */
		SICo_CR__c cr = new SICo_CR__c(Name = 'test', Maintenancy1stRDV__c = true);
		
		return cr;
    }
    
	public static ZuoraWSconfig__c createZuoraWSConfig(){
        ZuoraWSconfig__c zc = new ZuoraWSconfig__c();
		zc.Uri__c = 'Test Uri';
		zc.PageIdSEPA__c = 'Test PageId';
		
		return zc;
    }
    	
    public static JWTSettings__c createChamConfig(){
        return new JWTSettings__c(
            Name = 'Cham',
            Iss__c = 'c43--test.cs83.my.salesforce.com',
            Aud__c = 'test.cham.fr',
            Sub__c = 'C43',
            Exp__c = 24.0
            );
    }

    public static CalloutConfig__c createConfig_Boomi(){
        CalloutConfig__c cm = new CalloutConfig__c(
            Name='Boomi',
            Endpoint__c= 'http://boomi.integ.iotc43.fr:9090/ws/rest/c43',
            Login__c= 'Integ@edfelectricitedefrancesa-1PHLMS',
            Password__c= '57f53031-2052-4c1d-8a10-c4c93eafe6dc',
            isActive__c= true
        );
        return cm;
    }

    public static CalloutConfig__c createCustomSetting(String login, String password, String endpoint){
        CalloutConfig__c cm = new CalloutConfig__c(
            Name='Boomi',
            Endpoint__c= endpoint,
            Login__c= login,
            Password__c= password,
            isActive__c= true
        );
        return cm;
    }

    public static  GrDF_Config__c create_GrDF_Config(){
        GrDF_Config__c cm = new GrDF_Config__c(
            login__c='C43_Login',
            Name='Boomi',
            NumeroCAD__c = 'C43_CAD',
            NumeroVersion__c = 'C43_numV',
            DNEntreprise__c = 'C43_DNE'
        );
        return cm;
    }

    // Create CommunityConfig__c instance
    public static CommunityConfig__c createCustomSetting() {
        String customerCommunityProfileName = SICo_Constants_CLS.CUSTOMER_COMMUNITY_PROFILE_NAME;
        Profile profil = [SELECT Id FROM Profile WHERE Name =: customerCommunityProfileName LIMIT 1];
        User adminUser = createAdminUser();
        insert adminUser;
        CommunityConfig__c commSet = new CommunityConfig__c(
            CommunityClientDefaultProfileId__c = profil.Id,
            CommunityGuestDefaultProfileId__c = profil.Id,
            CommunityGuestUnameSuffix__c = '.guest',
            CommunityDefaultGuestOwnerId__c = string.valueOf(adminUser.Id),
            SiteBaseURL__c='https://test.force.com'
        );
        return commSet;
    }


/*** Object ***/

    // Create & Return: Lead
    // parameter: ID Account (agence)
    public static Lead createLead(ID agenceID){
        Lead lead =createLead(agenceID,'Installateur');
        return lead;
    }

     public static Lead createLead(ID agenceID,String ShippingMode){
        Lead lead = new Lead(
            ShippingAddressFlat__c= '32A',
            ShippingAddressBuilding__c= 'B2',
            ShippingAddressZipCode__c= '44000',
            ShippingAddressAdditionToAddress__c = 'Cedex 12',
            ShippingAddressStairs__c = '1',
            ShippingAddressFloor__c='4',
            ShippingAddressNumberStreet__c= '20 rue Matcher',
            ShippingAddressCountry__c= 'France',
            ShippingAddressCity__c='Nantes',
            CHAMAgencyInstallation__c= agenceID,
            CHAMAgencyEntretien__c= agenceID,
            CGVValidationDate__c= Datetime.now(),
            CPVValidationDate__c= Datetime.now(),
            CHAMInstallationAppointmentDate__c= Date.today().addDays(10),
            CHAMEntretienAppointmentDate__c= Date.today().addDays(10),
            CHAMInstallationTimeSlot__c= '08:00 - 10:00',
            CHAMEntretienTimeSlot__c= '08:00 - 10:00',
            ShippingMode__c= ShippingMode,
            Situation__c='MES',
            ContactAddressFlat__c= '32A',
            ContactAdresseBuilding__c= 'B2',
            ContactAddressZipCode__c= '44000',
            ContactAddressAdditionToAddress__c = 'Cedex 12',
            ContactAddressStair__c = '1',
            ContactAddressFloor__c='4',
            ContactAddressNumberStreet__c= '20 rue Matcher',
            ContactAddressCountry__c= 'France',
            ContactAddressCity__c='Nantes',
            Email='margerie.Boomi@c43.test.com',
            FirstName= 'margerie',
            LastName= 'Boomi',
            MobilePhone='+330620101112',
		    Phone='+330320107856',
            Salutation= 'Madame',
            SubscriberId__c= SICo_Utility.generateRandomString(9,'MUMBER'),
            LeadSource='Web',
            MeterReadingDate__c= Date.today(),
            GasMeter__c= 456789,
            SiteAddressFlat__c= '32A',
            SiteAddressBuilding__c= 'B2',
            SiteAddressZipCode__c= '44000',
            SiteAddressAdditionToAddress__c= 'Cedex 12',
            SiteAddressStairs__c= '1',
            SiteAddressFloor__c='4',
            SiteAddressNumberStreet__c= '20 rue Matcher',
            SiteAddressCountry__c= 'France',
            SiteAddressCity__c= 'Nantes',
            CAR__c= 78266519,
            EligibleForCham__c=true,
            EligibleForThermostat__c=true,
            EligibleForGas__c=true,
            BoilerBrand__c= 'Brand',
            BoilerModel__c= 'ECO 22S',
            NumberOfInhabitants__c= 11,
            SurfaceArea__c= 213,
            HeatingType__c= 'Gaz',
            HomeType__c= 'Principale',
            Usage__c= 'Chauffage',
            OfferName__c= 'SE',
            GasContractEffectiveDate__c= Date.today(),
            PaymentMethodCreditCard__c= '3456789',
            PaymentMethodSepa__c= '34567890',
            DrupalUrl__c='HTTPS://drupalURL',
            PCE__c= SICo_Utility.generateRandomString(10, 'NUMBER')
        );
        return lead;
    }

    // Create & Return: PersonalAccount
    // parameter: First name, Last name, RecordType Name for Account
    public static Account createAccount(String nameFirst, String nameLast, String rtName){
        ID rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_' + rtName);
        Account acc = new Account(
            FirstName= nameFirst,
            LastName= nameLast,
            RecordTypeId= rtAccount,
            BillingStreet= '12 rue de la gare',
            BillingCity= 'Nantes',
            BillingPostalCode='44000',
            BillingCountry= 'France',
            PersonTitle= 'Mr.',
            PersonEmail= 'test@email.com',
            Phone='+330320101112',
            PersonMobilePhone= '+330620101010',
            CustomerNumber__c= 'CUSTOMER1234567890'
        );
        return acc;
    }

    public static Account createBusinessAccount(String name, String rtName){
        ID rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_' + rtName);
        Account acc = new Account(
            Name= name,
            RecordTypeId= rtAccount,
            Zone__c = 'Zone 1'
        );
        return acc;
    }

    // Create & Return: Opportunity
    // parameter: Name Account,Id Account, Id Site
    public static Opportunity createOpportunity(String name,Id idAccount, Id idSite){
        Opportunity opp = new Opportunity(
            AccountId= idAccount,
            Name= name + '-Test',
            Site__c= idSite,
            CloseDate= System.today() + 90,
            StageName= 'Qualification',
            CHAMInstallationAppointmentDate__c= Date.today().addDays(10),
			CHAMEntretienAppointmentDate__c= Date.today().addDays(10),
			CHAMInstallationTimeSlot__c= '08:00 - 10:00',
			CHAMEntretienTimeSlot__c= '13:30 - 15:30'

        );
        return opp;
    }

    // Create & Return: Case
    // parameter: typeCase, Id Account, Id Opportunity, Id Site__c
    public static Case createCase (String typeCase,Id accountID, Id siteId){
        Case newCase = new Case(
            ProvisioningType__c= typeCase,
            AccountId= accountID,
            Site__c= siteId
        );
        return newCase;
    }

    // Create & Return: Case
    // parameter: typeCase, Id Account, Id Opportunity, Id Site__c
    public static Case createCase (String typeCase,Id accountID, Id OpportunityID, Id siteId){
        Case newCase = new Case(
            ProvisioningType__c= typeCase,
            AccountId= accountID,
            Opportunity__c= OpportunityID,
            Site__c= siteId
        );
        return newCase;
    }

    // Create & Return: Task
    // parameter: taskId (unique), subject, Id Case, RecordType Name for Task, isCallout
    public static Task createTask (String taskId, String subject, Id caseId, String rtName,Boolean isCallout){
        ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + rtName);
        Task newTask = new Task(
            TaskId__c= taskId,
            Subject= subject,
            WhatId= caseId,
            Parameters__c= 'SER_C43_CAPTEUR',
            Services__c= '/EDE/Souscription',
            RecordTypeId= rtTask,
            ProvisioningCallOut__c=isCallout,
            FluxStatus__c = 'Traitement non démarré',
            DeliveryPointId__c = 'U12345678'
        );
        //newTask.TICGN__c=Boolean.valueOf('true');
        //newTask.requestedInterventionDate__c=date.today();
        newTask.interventionPlannedDate__c=date.today();
        newTask.plannedRangedHours__c='DMA Début matinée';
        newTask.minimumHour__c='07:00';
        newTask.maximumHour__c='11:00';
        return newTask;
    }

    // Create & Return: Task
    // parameter: taskId (unique), subject, Id Case, RecordType Name for Task, isCallout
    public static Task createProvisioningTask (String taskId, String subject, Id caseId, Id rtTask, Boolean isCallout){
        Task newTask = new Task(
            TaskId__c= taskId,
            Subject= subject,
            RecordTypeId= rtTask,
            WhatId= caseId,
            ProvisioningCallOut__c=isCallout
        );
        return newTask;
    }


    // Create & Return: Task
    // parameter: taskId (unique), subject, Id Case, RecordType Name for Task, isCallout
    public static Task createProvisioningTask (String subject){
        ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisioning');
        Task newTask = new Task(
            Subject= subject,
            RecordTypeId= rtTask,
            FluxStatus__c = 'Traitement non démarré'
        );
        return newTask;
    }

    // // Create & Return: Event
    // // parameter: taskId (unique), subject, Id Case, RecordType Name for Event
    // public static Event createEvent (String taskId, String subject, Id caseId, String rtName,Boolean isCallout){
    //     Datetime actDate = datetime.now();
    //     ID rtEvent = SICo_Utility.m_RTbyDeveloperName.get('Event_' + rtName);
    //     Event newEvent = new Event(
    //     TaskId__c= taskId,
    //     Subject= subject,
    //     WhatId= caseId,
    //     RecordTypeId= rtEvent,
    //     DurationInMinutes= 300,
    //     StartDateTime= actDate,
    //     ActivityDateTime=actDate,
    //     ProvisioningCallOut__c=isCallout,
    //     FluxStatus__c = 'Traitement non démarré'
    //     );
    //     return newEvent;
    // }

    // // Create & Return: Event
    // // parameter: taskId (unique), subject, Id Case, RecordType Name for Event
    // public static Event createEvent (String subject, String rtName,Boolean isCallout){
    //     Datetime actDate = datetime.now();
    //     ID rtEvent = SICo_Utility.m_RTbyDeveloperName.get('Event_' + rtName);
    //     Event newEvent = new Event(
    //     Subject= subject,
    //     RecordTypeId= rtEvent,
    //     DurationInMinutes= 300,
    //     StartDateTime= actDate,
    //     ActivityDateTime=actDate,
    //     ProvisioningCallOut__c=isCallout,
    //     FluxStatus__c = 'Traitement non démarré'
    //     );
    //     return newEvent;
    // }

    // Create & Return: Product2
    // parameter:  String nbr
    public static Product2 createSalesforceProduct(String nbr){
        Product2 product = new Product2(
            Name='STATION ENERGIE - '+nbr,
            zqu__SyncStatus__c='Updated',
            zqu__SKU__c='SKU-0000000'+nbr,
            zqu__EffectiveStartDate__c= Date.today(),
            zqu__EffectiveEndDate__c= Date.today().addYears(10),
            zqu__Category__c='Base Products',
            zqu__Allow_Feature_Changes__c=true,
            zqu__Deleted__c=false,
            zqu__ZuoraId__c='2c92c0f855534502015558425691243'+nbr
        );
        return product;
    }

    // Create & Return: User (Syst Admin)
    // parameter: none
    public static User createAdminUser(){
        //Profile: 'SICo Community - Customer User'
        List<String> customerCommunityProfileName = new List<String> {'Administrateur système', 'System Administrator'};
        Profile profil = [SELECT Id FROM Profile WHERE Name IN :customerCommunityProfileName LIMIT 1];
           String i= SICo_Utility.generateRandomString(20, 'TEXT');
           User newUser = new User(
               FirstName = 'TESTFIRSTNAME '+i,
               LastName  = 'TESTLASTNAME '+i,
               Email     = i+'.email@test.com',
               Username  = i+'.username@test.com',
               Alias     = i.left(8),
               ProfileId = profil.Id,
               TimeZoneSidKey    = 'Europe/Paris',
               LocaleSidKey      = 'fr_FR',
               EmailEncodingKey  = 'UTF-8',
               LanguageLocaleKey = 'FR'
           );
        return newUser;
    }

    // Create & Return: User (Customer Community member)
    // parameter: Id contactId
    public static User createCommunityUser(Id contactId){
        //Profile: 'SICo Community - Customer User'
        String customerCommunityProfileName = SICo_Constants_CLS.CUSTOMER_COMMUNITY_PROFILE_NAME;
        Profile profil = [SELECT Id FROM Profile WHERE Name =:customerCommunityProfileName LIMIT 1];
        String i= SICo_Utility.generateRandomString(10, 'TEXT');
        User newUser = new User(
           FirstName = 'TESTFIRSTNAME '+i,
           LastName  = 'TESTLASTNAME '+i,
           Email     = i+'.email@test.com',
           Username  = i+'.username@test.com',
           Alias     = i.left(8),
           CommunityNickname = ('TESTFIRSTNAME '+i+' '+'TESTLASTNAME '+i).left(40),
           ProfileId = profil.Id,
           ContactId = contactId,
           TimeZoneSidKey    = 'Europe/Paris',
           LocaleSidKey      = 'fr_FR',
           EmailEncodingKey  = 'UTF-8',
           LanguageLocaleKey = 'FR');
        return newUser;
    }

    // Create & Return: User (Customer Community member, attached to a new person account)
    // parameter: NONE
    public static User createCommunityUser(){
        //Insert new PersonAccount
        Account testPersonAccount = createAccount('Test First Name', 'Test Last Name', SICo_Constants_CLS.GUEST_PERSON_ACCOUNT_RECORD_TYPE);
        insert testPersonAccount;
        //retrieve PersonContactId
        testPersonAccount = [SELECT Id, IsPersonAccount, PersonContactId FROM Account WHERE ID=: testPersonAccount.Id  LIMIT 1];
        //new User
        User newUser = createCommunityUser(testPersonAccount.PersonContactId);
        return newUser;
    }

    // Create & Return: Attachment
    // parameter: Id Parent
    public static Attachment createAttachment(Id parentId) {
        Attachment attach = new Attachment();
    	attach.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
    	attach.body = bodyBlob;
        attach.parentId = parentId;

        return attach;
    }

/*** CUSTOM SICO_OBJECT  ***/

    // Create & Return: SICo_Pairing__c
    public static SICo_Pairing__c createPairing (String sKey, String sId, Id sType, String sAction) {
        SICo_Pairing__c oAp = New SICo_Pairing__c();
        oAp.TemporaryKey__c = sKey;
        oAp.ObjectId__c = sId;
        oAp.ObjectType__c = sType;
        oAp.ActionType__c = sAction;
        return oAp;
    }   


    // Create & Return: MaintenancyQuote__c
    // parameter: Id Site, Id Account
    public static MaintenancyQuote__c createMaintenancyQuote(Id siteId, Id accountId){
        MaintenancyQuote__c maintenancyQuote = new MaintenancyQuote__c(
            Site__c = siteId,
            MaintenancyQuoteNumber__c = 'DEVTEST01',
            QuoteId__c = SICo_Utility.generateRandomString(10, 'NUMBER'),
            Customer__c = accountId
            );
        return maintenancyQuote;
    }


  // Create & Return: SICo_Basket__c
  // parameter: Id Rate Plan, Id Charge, Id Charge Tier
  public static SICo_Basket__c createPanier(Id leadId, String ratePlanId, String rpName, String chargeId, String chargeName){
    SICo_Basket__c panier= new SICo_Basket__c(
        Lead__c=leadId,
        RatePlanId__c= ratePlanId,
        ChargeId__c= chargeId,
        /*ChargeTierId__c=chareTierId,*/
        /*ObjectType__c= 'Station',*/
        ChargeName__c= chargeName,
        RatePlanName__c = rpName,
        Quantity__c  = 1
        );
    return panier;
    }

    // Create & Return: SICo_Basket__c
    // parameter: Id Rate Plan, Id Charge, Id Charge Tier
    public static SICo_Basket__c createPanier(Id leadId, String ratePlanId, String ratePlanName,String ratePlanType){
        SICo_Basket__c panier= new SICo_Basket__c(
            Lead__c=leadId,
            RatePlanId__c= ratePlanId,
            RatePlanName__c = ratePlanName,
            RatePlanType__c = ratePlanType,
            ChargeName__c= 'StationName',
            Quantity__c  = 1
        );
    return panier;
    }

    // Create & Return: SICo_Site__c
    // parameter: Id Account
    public static SICo_Site__c createSite(Id idAccount){
        SICo_Site__c site = new SICo_Site__c(
            Subscriber__c= idAccount,
            Street__c= '13 boulevard de la gare',
            City__c= 'Paris',
            PostalCode__c= '75000',
            Country__c= 'France',
            SiteNumber__c= 'SITE12345678901234',
            SubscribedPower__c= '12kVA',
            ElectricityTariffOption__c= 'HP_HC'
        );
    return site;
    }

    // Add Field & Return: SICo_Site__c
    // parameter: SICo_Site__c
    public static SICo_Site__c addFieldForSite(SICo_Site__c site){
        site.hasSwimmingPool__c = false;
        site.IsArrangedRoofRestored__c= false;
        site.IsWallRestored__c= true;
        site.IsWindowRestored__c= false;
        site.NoOfOccupants__c= 5;
        site.IsArrangedRoofPresent__c= false;
        site.IsFloorPresent__c= true;
        site.IsJointHouse__c= false;
        site.IsVerandaPresent__c= false;
        site.PrincHeatingSystemTypeDetail__c= 'GazChaudiereStandard';
        site.PrincipalHeatingSystemType__c= 'Electricite';
        site.IsWineCellarPresent__c= false;
        site.IsCeramicGlassPlatePresent__c= true;
        site.GasSource__c= 'Bouteille';
        site.InstructionValueAC__c= 24;
        site.IsACPresent__c= false;
        site.SurfaceAC__c= 0;
        site.HousingLocationType__c= 'RDC';
        site.IsCornerApartment__c= false;
        site.ConstructionDate__c= 2004;
        site.HousingType__c= 'Maison';
        site.surfaceInSqMeter__c= 200;
        site.ElectricityTariffOption__c= 'HP_HC';
        return site;
    }

    // Create & Return: SICo_Meter__c
    public static SICo_Meter__c createMeter(){
        SICo_Meter__c meter = new SICo_Meter__c(Id__c = SICo_Utility.generateRandomString(10, 'NUMBER'));
        return meter;
    }

    // Create & Return: SICo_Meter__c
    public static SICo_Meter__c createMeter(String rtName){
        ID rtMeter = SICo_Utility.m_RTbyDeveloperName.get('SICo_Meter__c_' +rtName);
        SICo_Meter__c meter = new SICo_Meter__c(
            Id__c = SICo_Utility.generateRandomString(10, 'NUMBER'),
            RecordTypeId= rtMeter
        );
        return meter;
    }

    // Create & Return: SICo_SiteMeter__c
    // parameter: Id Site
    public static SICo_SiteMeter__c createSiteMeter(String rtName, Id siteId, id meterId){
        ID rtSiteMeter = SICo_Utility.m_RTbyDeveloperName.get(rtName);
        SICo_SiteMeter__c siteMeter = new SICo_SiteMeter__c(
            RecordTypeId= rtSiteMeter,
            Site__c = siteId,
            Meter__c= meterId,
            IsActive__c= true
        );
        return siteMeter;
    }

    // Create & Return: SICo_SiteMeter__c
    // parameter: Id Site
    public static SICo_SiteMeter__c createSiteMeter(Id siteId){
        SICo_Meter__c meter = createMeter();
        insert meter;
        SICo_SiteMeter__c siteMeter = new SICo_SiteMeter__c(
            Site__c = siteId,
            Meter__c= meter.Id,
            IsActive__c= true
        );
        return siteMeter;
    }

    // Add Field & Return: SICo_SiteMeter__co_Site__c
    // parameter: SICo_SiteMeter__c
    public static SICo_SiteMeter__c addFieldForSiteMeter(SICo_SiteMeter__c siteMeter){
        siteMeter.Tariff__c= 'BLEU';
        siteMeter.Millesime__c= 12;
        siteMeter.Zone__c= 'Z4';
        return siteMeter;
    }

    // Create & Return: SICo_Beneficiary__c
    // parameter: Id Account Client, Id SICo_Site__c
    public static SICo_Beneficiary__c createBeneficiary(Id client,Id site){
        SICo_Beneficiary__c beneficiary = new SICo_Beneficiary__c(
            Client__c= client,
            Site__c= site
        );
        return beneficiary;
    }

    // Create & Return: SICo_LeadBeneficiary__c
    // parameter: Id Account Lead, First name, Last name
    public static SICo_LeadBeneficiary__c createLeadBeneficiary(Id lead,String nameFirst, String nameLast, String salutation){
        SICo_LeadBeneficiary__c beneficiary = new SICo_LeadBeneficiary__c(
            Lead__c= lead,
            FirstName__c= nameFirst,
            LastName__c= nameLast,
            Salutation__c=salutation
        );
        return beneficiary;
    }

  /*** CUSTOM ZUORA OBJECT ***/

    // Create & Return: zqu__ZProduct__c
    // parameter: String nbr, Id Product Salesforce
    public static zqu__ZProduct__c createZuoraProduct(String nbr,Id product2){
        zqu__ZProduct__c product = new zqu__ZProduct__c(
            Name='STATION ENERGIE - '+nbr,
            zqu__Description__c='Create by Utility Class',
            zqu__SKU__c='SKU-0000000'+nbr,
            zqu__EffectiveStartDate__c= Date.today(),
            zqu__EffectiveEndDate__c= Date.today().addYears(10),
            zqu__Category__c='Base Products',
            zqu__Allow_Feature_Changes__c=true,
            zqu__Deleted__c=false,
            zqu__Active__c=false,
            zqu__Product__c=product2,
            zqu__ZuoraId__c='2c92c0f85553450201555842569124'+nbr
            );
        return product;
        }

    // Create & Return: zqu__ProductRatePlan__c
    // parameter: String nbr,Id zqu__ProductRatePlan__c, Id Product Salesforce
    public static zqu__ProductRatePlan__c createZuoraProductRatePlan(String nbr,Id product2,Id zProduct, String ratePlanType){
        zqu__ProductRatePlan__c productRate = new zqu__ProductRatePlan__c(
            Name='Colissimo - '+nbr,
            zqu__ProductRatePlanFullName__c='Colissimo',
            zqu__EffectiveStartDate__c= Date.today(),
            zqu__EffectiveEndDate__c= Date.today().addYears(10),
            zqu__ZProduct__c=zProduct,
            zqu__Product__c=product2,
            zqu__ActiveCurrencies__c='EUR',
            RatePlanType__c = ratePlanType,
			zqu__ZuoraId__c='2c92c0f855a0a9d60155b65c8eb55e4'+nbr
            );
        return productRate;
        }

    // Create & Return: zqu__ProductRatePlanCharge__c
    // parameter: String nbr, Id zqu__ProductRatePlan__c
    public static zqu__ProductRatePlanCharge__c createZuoraProductRatePlanCharge(string nbr, Id productRatePlan){
        zqu__ProductRatePlanCharge__c planCharge = new zqu__ProductRatePlanCharge__c(
            Name='Station Energie One Shot - '+nbr,
            zqu__ProductRatePlanChargeFullName__c='Station Energie One Shot',
            zqu__Type__c='One-Time',
            zqu__EndDateCondition__c='One-Time',
            zqu__Model__c='Flat Fee Pricing',
            zqu__TriggerEvent__c='  Upon Contract Effective',
            zqu__ProductRatePlan__c = productRatePlan,
            zqu__DefaultQuantity__c=1,
            zqu__ListPriceBase__c='Per Billing Period',
            zqu__UpToPeriodsType__c='Billing Periods',
            zqu__TaxMode__c='Tax Exclusive',
            zqu__ZuoraId__c='2c92c0f85553450201555842569124'+nbr,
			zqu__PriceTable__c='0.0'
        );
        return planCharge;
    }

    // Create & Return: zqu__Quote__c
    // parameter: Name Zuora Quote, Id Account,Id contact, Id Opportunity
    public static zqu__Quote__c createZuoraQuote(String name,Id idAccount,Id idPersContact,Id idOpportunity){
        zqu__Quote__c quote = new zqu__Quote__c(
            Name= name + '-Test',
            zqu__Account__c= idAccount,
            zqu__Opportunity__c= idOpportunity,
            zqu__ValidUntil__c= System.today(),
            zqu__BillToContact__c= idPersContact,
            zqu__SoldToContact__c= idPersContact
        );
        return quote;
    }

    // Create & Return: zqu__Quote__c
    // parameter: Name Zuora Quote, Id Account,Id contact, Id Opportunity
    public static zqu__Quote__c createZuoraQuote(String name,Id idAccount,Id idPersContact,Id idOpportunity, Id idSite){
        zqu__Quote__c quote = new zqu__Quote__c(
            Name= name + '-Test',
            zqu__Account__c= idAccount,
            zqu__Opportunity__c= idOpportunity,
            zqu__ValidUntil__c= System.today(),
            zqu__BillToContact__c= idPersContact,
            zqu__SoldToContact__c= idPersContact
        );
        quote.SiteId__c = idSite;
        return quote;
    }

    // Create & Return: Zuora__Subscription__c
    // parameter: Name Zuora Subscription, Id Account, Id Quote
    public static Zuora__Subscription__c createZuoraSubscription(String name,Id idAccount,Id idQuote,String quoteNumber, Id idSite){
        Zuora__Subscription__c zuoraSub = new Zuora__Subscription__c(
            Zuora__Status__c= 'Active',
            Zuora__OriginalId__c='original',
            Zuora__PreviousSubscriptionId__c='original',
            Name= name + '-Test',
            Zuora__Account__c= idAccount,
            Quote__c= idQuote,
            Zuora__QuoteNumber__c= quoteNumber,
            SiteId__c= idSite,
            //OfferName__c = 'GAZ_PF2_1',
            OfferName__c = 'GAZ',
            Zuora__SubscriptionNumber__c= quoteNumber,
            Zuora__ContractEffectiveDate__c= date.today()
        );
        return zuoraSub;
    }

    // Create & Return: Zuora__SubscriptionProductFeature__c
    // parameter: Name, Feature code name (unique), Id Account, Id Subscription
    public static Zuora__SubscriptionProductFeature__c createZuoraSubscriptionProductFeature(String name,String nameFeatureCode,Id idAccount, Id idSubscription){
        Zuora__SubscriptionProductFeature__c zuoraSubProdFeature = new Zuora__SubscriptionProductFeature__c(
            Name= name + '-Test',
            Zuora__FeatureCode__c= nameFeatureCode,
            Zuora__ProductZuoraId__c= 'zuoraid0000000',
            Zuora__Account__c= idAccount,
            Zuora__Subscription__c= idSubscription
        );
        return zuoraSubProdFeature;
    }

    // Create & Return: Zuora__PaymentMethod__c
    // parameter: Id Account
    public static Zuora__PaymentMethod__c createZuoraPaymentMethod(Id idAccount, Boolean isDefault){
    	Zuora__CustomerAccount__c zuoraCA = new Zuora__CustomerAccount__c(
    		Zuora__Account__c = idAccount
		);
		insert zuoraCA;
        zuoraCA = [SELECT Id, Zuora__Account__c FROM Zuora__CustomerAccount__c WHERE Id = :zuoraCA.Id LIMIT 1];

        Zuora__PaymentMethod__c zuoraPM = new Zuora__PaymentMethod__c(
            Zuora__DefaultPaymentMethod__c = isDefault,
            Zuora__Type__c = SICo_Constants_CLS.ZUORA_PAYMENT_METHOD_BANK_TRANSFERT,
            Zuora__IBAN__c = 'FR76300000908765',
            Zuora__BillingAccount__c = zuoraCA.Id,
            Zuora__Active__c = true
        );
        return zuoraPM;
    }

    // Create & Return: Zuora__ZInvoice__c
    // parameter: Id Account
    public static Zuora__ZInvoice__c createZuoraInvoice(Id accountId) {

        Zuora__ZInvoice__c invoice = new Zuora__ZInvoice__c();
        invoice.Zuora__InvoiceDate__c = Date.today();
        // invoice.NextInvoiceDate__c = Date.today();
        invoice.Zuora__Balance2__c = 0;
        invoice.Zuora__Account__c = accountId;
        return invoice;
    }


    // Create & Return: SiCo_InvoiceItem__c
    // parameter: Id Zuora Invoice
    public static SiCo_InvoiceItem__c createZuoraInvoiceItem(Id invoiceId, String name, Decimal amount) {

        SiCo_InvoiceItem__c invoiceItem = new SiCo_InvoiceItem__c();
        invoiceItem.Name = name;
        invoiceItem.TotalAmount__c = amount;
        invoiceItem.Category__c = 'GAZ-ABO';
        invoiceItem.InvoiceId__c = invoiceId;
        return invoiceItem;
    }

} //END