/**
* @author Boris Castellani
* @date 01/08/2016
* @LastModify 27/01/2017
*
* @description Create JSON & CallOut
*/
public with sharing class SICo_FluxSite_CLS {

	public static void launchProfilesNow(List<SICo_Site__c> listSites){
		final SICO_QueueChaining_Utility.SICO_QueueChainOrchestrator anOrchestrator = new SICO_QueueChaining_Utility.SICO_QueueChainOrchestrator();
		final Map<String, String> endPoint = new Map<String, String>();
		for (SICo_BoomiEndpoint__mdt ep : [SELECT DeveloperName, EndPoint__c FROM SICo_BoomiEndpoint__mdt]){
			endPoint.put(ep.DeveloperName, ep.endPoint__c);
		}
		doProfileCalloutNow('POST', endPoint.get('GenericProfile'), BuildSiteGenericProfile(listSites));
		doProfileCalloutNow('POST', endPoint.get('ElecProfile'), BuildSiteElecProfile(listSites));
		doProfileCalloutNow('POST', endPoint.get('GasProfile'), BuildSiteGasProfile(listSites));
	}

	public static String BuildSiteGenericProfile(list<SICo_Site__c> siteList){
		
		final SICo_FluxProfileModel.ListGenericProfiles listprofiles = new SICo_FluxProfileModel.ListGenericProfiles();
		listprofiles.listGenericProfiles = new List<SICo_FluxProfileModel.GenericProfile>();
		String genericProfileJSON ='';
		try {
	    if(siteList.size() >0){
				for(SICo_Site__c forSite : [SELECT Id, Subscriber__c,hasSwimmingPool__c, IsArrangedRoofRestored__c, IsWallRestored__c,
																		IsWindowRestored__c, NoOfOccupants__c, IsArrangedRoofPresent__c, IsFloorPresent__c,
																		IsJointHouse__c, IsVerandaPresent__c, PrincHeatingSystemTypeDetail__c, PrincipalHeatingSystemType__c,
																		IsWineCellarPresent__c, IsCeramicGlassPlatePresent__c, GasSource__c, InstructionValueAC__c,
																		IsACPresent__c, SurfaceAC__c, HousingLocationType__c,IsCornerApartment__c, ConstructionDate__c, 
																		HousingType__c, ResidenceType__c, SurfaceInSqMeter__c, Subscriber__r.CustomerNumber__c, SanitaryHotWaterType__c
																		FROM SICo_Site__c WHERE Id IN :siteList]) {


					final SICo_FluxProfileModel.SwimmingPool swimmingPool = new SICo_FluxProfileModel.SwimmingPool();
					final SICo_FluxProfileModel.Restoration restoration = new SICo_FluxProfileModel.Restoration();
					final SICo_FluxProfileModel.LifeStyle lifeStyle = new SICo_FluxProfileModel.LifeStyle();
					final SICo_FluxProfileModel.InsertWood insertWood = new SICo_FluxProfileModel.InsertWood();
					final SICo_FluxProfileModel.House house = new SICo_FluxProfileModel.House();
					final SICo_FluxProfileModel.HeatingSystem heatingSystem = new SICo_FluxProfileModel.HeatingSystem();
					final SICo_FluxProfileModel.SanitoryHotWater sanitoryHotWater = new SICo_FluxProfileModel.SanitoryHotWater();
					final SICo_FluxProfileModel.OtherEquipments otherEquipments = new SICo_FluxProfileModel.OtherEquipments();
					final SICo_FluxProfileModel.ElecEquipments elecEquipments = new SICo_FluxProfileModel.ElecEquipments();
					final SICo_FluxProfileModel.CookingEquipments cookingEquipments= new SICo_FluxProfileModel.CookingEquipments();
					final SICo_FluxProfileModel.AudioEquipments audioEquipments= new SICo_FluxProfileModel.AudioEquipments();
					final SICo_FluxProfileModel.AirConditioning airConditioning= new SICo_FluxProfileModel.AirConditioning();
					final SICo_FluxProfileModel.Equipments equipments= new SICo_FluxProfileModel.Equipments();
					final SICo_FluxProfileModel.Apartment apartment = new SICo_FluxProfileModel.Apartment();
					final SICo_FluxProfileModel.Housing housing= new SICo_FluxProfileModel.Housing();
					final SICo_FluxProfileModel.GenericProfile genericProfile = new SICo_FluxProfileModel.GenericProfile();

					swimmingPool.hasSwimmingPool= forSite.hasSwimmingPool__c;

					restoration.isArrangedRoofRestored= forSite.IsArrangedRoofRestored__c;
					restoration.isWallRestored= forSite.IsWallRestored__c;
					restoration.isWindowRestored= forSite.IsWindowRestored__c;

					lifeStyle.noOfOccupants= String.valueOf(forSite.NoOfOccupants__c);

					house.isArrangedRoofPresent= forSite.IsArrangedRoofPresent__c;
					house.isFloorPresent= forSite.IsFloorPresent__c;
					house.isJointHouse= forSite.IsJointHouse__c;
					house.isVerandaPresent= forSite.IsVerandaPresent__c;

					heatingSystem.princHeatingSystemTypeDetail= forSite.PrincHeatingSystemTypeDetail__c;
					heatingSystem.principalHeatingSystemType= forSite.PrincipalHeatingSystemType__c;

					sanitoryHotWater.sanitoryHotWaterType = forSite.SanitaryHotWaterType__c;	

					otherEquipments.isWineCellarPresent= forSite.IsWineCellarPresent__c;

					cookingEquipments.isCeramicGlassPlatePresent= forSite.IsCeramicGlassPlatePresent__c;
					cookingEquipments.gasSource= forSite.GasSource__c;

					airConditioning.instructionValueAC= String.valueOf(forSite.InstructionValueAC__c);
					airConditioning.isACPresent= forSite.IsACPresent__c;
					airConditioning.surfaceAC= String.valueOf(forSite.SurfaceAC__c);

					equipments.airConditioning= airConditioning;
					equipments.audioEquipments= audioEquipments;
					equipments.cookingEquipments= cookingEquipments;
					equipments.elecEquipments= elecEquipments;
					equipments.otherEquipments= otherEquipments;
					equipments.sanitoryHotWater= sanitoryHotWater;

					apartment.housingLocationType= forSite.HousingLocationType__c;
					apartment.isCornerApartment= forSite.IsCornerApartment__c;	

					housing.apartment= apartment;
					housing.constructionDate= String.valueOf(forSite.ConstructionDate__c);
					housing.equipments= equipments;
					housing.heatingSystem= heatingSystem;
					housing.house= house;
					housing.housingType= forSite.HousingType__c;
					housing.insertWood= insertWood;
					housing.lifeStyle= lifeStyle;
					housing.residenceType= forSite.ResidenceType__c;
					housing.restoration= restoration;
					housing.surfaceInSqMeter= String.valueOf(forSite.surfaceInSqMeter__c);
					housing.swimmingPool= swimmingPool;

					
					genericProfile.personExternalId= SICo_Utility.generate18CharId( forSite.Subscriber__r.CustomerNumber__c );
					genericProfile.siteExternalId=  SICo_Utility.generate18CharId( forSite.id );
					genericProfile.modificationDate= Datetime.now();
					genericProfile.housing= housing;

        	listprofiles.listGenericProfiles.add(genericProfile);        
				}
				genericProfileJSON = JSON.serialize(listprofiles);
			}
		}catch(Exception e) {
    	SICo_LogManagement.sendErrorLog('BuildSiteGenericProfile',Label.SICO_LOG_USER_Site_Generic_Profile,e.getMessage());
  	} 
		return genericProfileJSON;
	}

	public static String BuildSiteElecProfile(list<SICo_Site__c> siteList){
		
		final SICo_FluxProfileModel.ListElecProfiles listprofiles = new SICo_FluxProfileModel.ListElecProfiles();
		listprofiles.elecProfiles = new List<SICo_FluxProfileModel.ElecSupplyContractChange>();

		String elecProfileJSON ='';
		try {
	    if(siteList.size() >0){
				for(SICo_Site__c forSite : [SELECT Id, Subscriber__r.CustomerNumber__c, ElectricityTariff__c, 
																		ElectricityTariffOption__c, SubscribedPower__c, Millesime__c 
																		FROM SICo_Site__c WHERE Id IN :siteList]) {

	        SICo_FluxProfileModel.Parameters parameters = new SICo_FluxProfileModel.Parameters();
	        SICo_FluxProfileModel.ElecSupplyContractChange elecSupplyContractChange = new SICo_FluxProfileModel.ElecSupplyContractChange();
	      	elecSupplyContractChange.siteId= SICo_Utility.generate18CharId(forSite.Id);
	      	elecSupplyContractChange.personId= SICo_Utility.generate18CharId(String.valueof(forSite.Subscriber__r.CustomerNumber__c));
	      	elecSupplyContractChange.changeDate= datetime.now();
	      	elecSupplyContractChange.tariff= forSite.ElectricityTariff__c;
	      	elecSupplyContractChange.tariffOption= forSite.ElectricityTariffOption__c;
	      	elecSupplyContractChange.subscribedPower = !String.isBlank(forSite.SubscribedPower__c)
		        ? forSite.SubscribedPower__c.toLowerCase().replace('kva','')
		        : forSite.SubscribedPower__c;
	      	elecSupplyContractChange.millesime= String.valueof(forSite.Millesime__c);
	      	elecSupplyContractChange.parameters = parameters;

	      	listprofiles.elecProfiles.add(elecSupplyContractChange);
				}         	
      	elecProfileJSON = JSON.serialize(listprofiles);
  		}
    }catch(Exception e){
    	SICo_LogManagement.sendErrorLog('BuildSiteElecProfile',Label.SICO_LOG_USER_Site_Elec_Profile,e.getMessage());
    }   
		return elecProfileJSON;
	}

	public static String BuildSiteGasProfile(list<SICo_Site__c> siteList){
		final Map<Id, Zuora__SubscriptionProductCharge__c> existingSub = new Map<Id, Zuora__SubscriptionProductCharge__c>();
		final SICo_FluxProfileModel.ListGasProfiles listprofiles = new SICo_FluxProfileModel.ListGasProfiles();
		listprofiles.gasProfiles = new List<SICo_FluxProfileModel.GasSupplyContractChange>();
		
		String gasProfileJSON ='';
		try {
  		List<Zuora__SubscriptionProductCharge__c> listcharges = new List<Zuora__SubscriptionProductCharge__c>(
  			[SELECT Zuora__Product__r.Name, Zuora__RatePlanName__c, Zuora__Subscription__r.Zuora__TermStartDate__c,
  			 Zuora__Subscription__r.SiteId__c, Zuora__Subscription__r.Millesime__c,Zuora__Subscription__r.Zuora__OriginalCreated_Date__c
				 FROM Zuora__SubscriptionProductCharge__c
				 WHERE Zuora__RatePlanName__c LIKE 'Conso%' AND Zuora__Subscription__r.SiteId__c IN :siteList]);
	  		
      for (Zuora__SubscriptionProductCharge__c zSC : listcharges) {
  			existingSub.put(zSC.Zuora__Subscription__r.SiteId__c , zSC);
      }
			
			if(siteList.size() >0){
			  Set<String> zipCodes = new Set<String>();
			  Map<String, String> zoneByZipCode = new Map<String, String>();
			  final List<SICo_Site__c> reloadSiteForZipCode = [SELECT Id, Zone__c, PostalCode__c 
			  																					 			 FROM SICo_Site__c WHERE Id in :siteList];

			  for (SICo_Site__c s : reloadSiteForZipCode) {
		      if (String.isBlank(s.Zone__c)) {
	          zipCodes.add(s.PostalCode__c);
		      }
			  }
	           
        zoneByZipCode = SICo_WithoutSharing_CLS.getZoneByZipCode( zipCodes );
				for(SICo_Site__c forSite : siteList){	          	
          SICo_FluxProfileModel.Parameters parameters = new SICo_FluxProfileModel.Parameters();
          SICo_FluxProfileModel.GasSupplyContractChange gasSupplyContractChange = new SICo_FluxProfileModel.GasSupplyContractChange();
          gasSupplyContractChange.siteId = SICo_Utility.generate18CharId(forSite.Id);
          gasSupplyContractChange.personId= SICo_Utility.generate18CharId(forSite.Subscriber__r.CustomerNumber__c);
          gasSupplyContractChange.changeDate= datetime.now();
          if (!existingSub.containsKey(forSite.Id)) {
          	gasSupplyContractChange.tariff = !String.isBlank(forSite.GasTariff__c)
              ? forSite.GasTariff__c
              : System.Label.SICo_Default_Offre;        
          	gasSupplyContractChange.option = forSite.GasTariffOption__c;
          	gasSupplyContractChange.zone = String.valueof(forSite.Zone__c);
          	
						Date originalDate = Date.today();
						for(SICo_Millesime__c mill : SICo_Utility.list_Millesimes){
							if(mill.StartDate__c <= originalDate && mill.EndDate__c >= originalDate){
								gasSupplyContractChange.millesime = mill.millesime__c;
								break;	
							}		
						}		
          }else{
          	gasSupplyContractChange.tariff = String.valueOf(existingSub.get(forSite.Id).Zuora__Product__r.Name);
          	gasSupplyContractChange.option = String.valueOf(existingSub.get(forSite.Id).Zuora__RatePlanName__c).right(4).left(2);
          	gasSupplyContractChange.zone = String.valueOf(existingSub.get(forSite.Id).Zuora__RatePlanName__c).right(2);
          	if(existingSub.get(forSite.Id).Zuora__Subscription__r.Millesime__c != null){
							gasSupplyContractChange.millesime =	existingSub.get(forSite.Id).Zuora__Subscription__r.Millesime__c;
						}else{
							if(existingSub.get(forSite.Id).Zuora__Subscription__r.Zuora__OriginalCreated_Date__c != null){
								Date originalDate = existingSub.get(forSite.Id).Zuora__Subscription__r.Zuora__OriginalCreated_Date__c.date();
								for(SICo_Millesime__c mill : SICo_Utility.list_Millesimes){
									if(mill.StartDate__c <= originalDate && mill.EndDate__c >= originalDate){
										gasSupplyContractChange.millesime = mill.millesime__c;
										break;	
									}
								}
							}
						}
          }
					gasSupplyContractChange.tariff = (!String.isBlank(gasSupplyContractChange.tariff))
						? gasSupplyContractChange.tariff.replaceAll(' ', '').replace('Gaz-', 'GAZ-')
						: gasSupplyContractChange.tariff;
          gasSupplyContractChange.zone = (String.isBlank(gasSupplyContractChange.zone) && zoneByZipCode.containsKey(forSite.PostalCode__c))
            ? zoneByZipCode.get(forSite.PostalCode__c)
            : !String.isBlank(gasSupplyContractChange.zone) ? gasSupplyContractChange.zone : 'Z2';                                 
          gasSupplyContractChange.parameters = parameters;
					
          listprofiles.gasProfiles.add(gasSupplyContractChange);        
				}
        gasProfileJSON = JSON.serialize(listprofiles);
     	}
    }catch(Exception e){
			SICo_LogManagement.sendErrorLog('BuildSiteGasProfile',Label.SICO_LOG_USER_Site_Gaz_Profile,e.getMessage());
    }  
		return gasProfileJSON;
	}

	//doProfileCallout without @Future (when called from a method that is already @Future)
	public static void doProfileCalloutNow(String method, String endPoint, String body) {
    SICo_Utility.getCallout(method, endPoint, body);
	}

} //END