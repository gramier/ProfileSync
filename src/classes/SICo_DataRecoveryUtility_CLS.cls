public with sharing class SICo_DataRecoveryUtility_CLS {

	/**
	 * Data Recovery: Mapping new RecordType for sObject.
	 * methode Build Dynamic Query SOQL and Update sObject List
	 * sobType = Salesforce Object
	 * fieldCondition = Salesforce condition field 
	 * mapping = [new field, old field]
	 */
	public static void DynamicChangeRecordType(String sobType, String fieldCondition, Map<String,String> mapping){

		String S_VIRGULE = ', ';
		String S_SLASH = '\'';

		String soql = 'SELECT Id, RecordTypeId, ' + fieldCondition + ' FROM ' + sobType 
								+ ' WHERE ' + fieldCondition 
								+ ' IN ('+ S_SLASH + String.join(new list<String>(mapping.keySet()), S_SLASH + S_VIRGULE + S_SLASH) + S_SLASH + ')';

		List<sObject> sobjList = Database.query(soql);
		system.debug(sobjList);
		if(!sobjList.isEmpty()){
			for(sObject aSobjet : sobjList){
				aSobjet.put('RecordTypeId', Id.valueOf(SICo_Utility.m_RTbyDeveloperName.get(sobType+'_'+mapping.get((String)aSobjet.get(fieldCondition)))));					
			}
			UPDATE sobjList;
		}
	}

	/**
	 * Data Recovery: Mapping new fields to old fields for sObject.
	 * methode Build Dynamic Query SOQL and Update sObject List
	 * sobType = SAlesforce Object
	 * mappingField = [new field, old field]
	 * newDataField = [field, Data]
	 * whereClause = [field, Data]
	 */
	public static void DynamicChangeFields (String sobType, Map<String,String> mappingField, Map<String,String> newDataField,
																					Map<String,String> conditionFields, Map<String,List<String>> conditionList){
		String S_VIRGULE = ', ';
		String S_AND = ' AND ';
		String S_SLASH = '\'';
		String S_WHERE = ' WHERE ';


		String soql = 'SELECT ';
		if(!mappingField.isEmpty()){
			soql += String.join(new list<String>(mappingField.keySet()), S_VIRGULE);
			soql += S_VIRGULE + String.join(mappingField.values(), S_VIRGULE);
		}
		if(!newDataField.isEmpty()){
			if(!mappingField.isEmpty()){
				soql += S_VIRGULE;
			}
			soql += String.join(new list<String>(newDataField.keySet()), S_VIRGULE);
		}
		soql += ' FROM ' + sobType;

		if(!conditionFields.isEmpty()){
			soql += S_WHERE;
			for(String aCondition : conditionFields.keySet()){
				soql += aCondition.toUpperCase() == 'RECORDTYPEID'
				? aCondition + ' = ' + S_SLASH + SICo_Utility.m_RTbyDeveloperName.get(sobType+'_'+conditionFields.get(aCondition)) + S_SLASH + S_AND
				: aCondition + ' = ' + S_SLASH +conditionFields.get(aCondition)+ S_SLASH + S_AND;
			}	
			soql = soql.removeEnd(S_AND);
		}
		if(!conditionList.isEmpty()){
			if(!conditionFields.isEmpty()){
				soql += S_AND;
			}else{
				soql += S_WHERE;			
			}
			for(String aConditionKey : conditionList.keySet()){
				soql += aConditionKey + ' IN ('+S_SLASH + String.join(conditionList.get(aConditionKey),S_SLASH + S_VIRGULE + S_SLASH) + S_SLASH + ')';
				soql += S_AND;
			}	
			soql = soql.removeEnd(S_AND);		
		}

		system.debug(soql);
		List<sObject> sobjList = Database.query(soql);
		if(!sobjList.isEmpty()){
			for(sObject aSobjet : sobjList){
				for(String field : mappingField.keySet()){
					aSobjet.put(field, aSobjet.get(mappingField.get(field)));
				}
				for(String field : newDataField.keySet()){
					aSobjet.put(field, newDataField.get(field));
				}
			}
			UPDATE sobjList;
		}
	}

	// update records copy fields from Key to value
	public static void recoverFields(List<SObject> l_records, Map<String, String> m_fields) {

		for (SObject o : l_records) {
			for (String key : m_fields.keySet())
				o.put(m_fields.get(key), o.get(key));
		}
		update l_records;
	}

	//ok
	public static void updateTaskData(List<Task> l_tasks) {
		List<ID> l_whatid = new List<ID>();
		for (Task t : l_tasks)
			l_whatid.add(t.WhatId);

		Map<Id,Case> m_cases = new Map<Id,Case>([SELECT 
									Id, Subject, 
									Subscription__c,
									Opportunity__r.ShippingMode__c,
									Opportunity__r.ShippingAddressBuilding__c,
									Opportunity__r.ShippingAddressCity__c,
									Opportunity__r.ShippingAddressFlat__c,
									Opportunity__r.ShippingAddressFloor__c,
									Opportunity__r.ShippingAddressStairs__c,
									Opportunity__r.ShippingAddressAdditionToAddress__c,
									Opportunity__r.ShippingAddressZipCode__c,
									Opportunity__r.ShippingAddressNumberStreet__c
								FROM Case WHERE Id in :l_whatid]);

		for (Task t : l_tasks) if (m_cases.get(t.WhatId) <> null){

			if (t.TaskId__c == 'SE0001' || t.TaskId__c == 'TH0001') {
				t.ShippingMode__c = m_cases.get(t.WhatId).Opportunity__r.ShippingMode__c;
				t.ShippingAddressCity__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressCity__c;
				t.ShippingAddressFlat__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressFlat__c;
				t.ShippingAddressFloor__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressFloor__c;
				t.ShippingAddressStairs__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressStairs__c;
				t.ShippingAddressAdditionToAddress__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressAdditionToAddress__c;
				t.ShippingAddressZipCode__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressZipCode__c;
				t.ShippingAddressNumberStreet__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressNumberStreet__c;
				t.ShippingAddressBuilding__c = m_cases.get(t.WhatId).Opportunity__r.ShippingAddressBuilding__c;
				t.Description += '\n\nRattrapage ' + System.today();
			}

			t.InterventionType__c = t.InterventionType__c ==null ? t.OmegaRequestType__c : t.InterventionType__c;
			t.ErrorLog__c = t.Reason__c;
			t.Subscription__c = m_cases.get(t.WhatId).Subscription__c;
		}
		update l_tasks;
	}

	//ok
	public static void UpdateSite(List<SICo_Site__c> l_sites) {
		List<Zuora__Subscription__c> l_sub = [SELECT Id, SICO_Subscribed_offers__c, OfferName__c, SiteId__c
											  FROM Zuora__Subscription__c
											  WHERE 
												Siteid__c in :l_sites];		

		Map<Id, List<Zuora__Subscription__c>> m_site_sub = new Map<Id, List<Zuora__Subscription__c>>();
		for (Zuora__Subscription__c s : l_sub) {
			if (m_site_sub.get(s.SiteId__c) == null)
				m_site_sub.put(s.SiteId__c, New List<Zuora__Subscription__c>());
			m_site_sub.get(s.SiteId__c).add(s);
		}

		for (SICo_Site__c sit : l_sites) {
			List<String> offers = new List<String>();
			
			for (Zuora__Subscription__c sub : m_site_sub.get(sit.Id)) {
				 offers.addall(sub.OfferName__c.split('_'));
			}
			sit.SICO_Subscribed_offers__c = String.join(offers, ';');

			for (Zuora__Subscription__c sub : m_site_sub.get(sit.Id)) {
				 sub.SICO_Subscribed_offers__c = String.join(offers, ';');
			}
		}		

		update l_sub;
		update l_sites;
	}

	//ok
	public static void migrateEventToTask(List<Event> l_events) {
		List<Task> l_tasks = new List<Task>();
		List<ID> l_whatid = new List<ID>();
		for (Event e : l_events)
			l_whatid.add(e.WhatId);

		Map<Id, Case> m_case = new Map<Id, Case>([SELECT Id, Subscription__c FROM Case WHERE Id IN :l_whatid]);

		for (Event e : l_events) {
			Task t = new Task();
			t.WhoId = e.WhoId;
			t.WhatId = e.WhatId;
			t.IsVisibleInSelfService = true;
			t.TaskId__c = e.TaskId__c;	
			t.ProvisioningCallOut__c = e.ProvisioningCallOut__c;
			t.BatchFlag__c = e.BatchFlag__c;
			t.RequestState__c = e.CHAMInterventionStatus__c;
			t.InterventionPlannedDate__c = Date.newInstance(e.StartDateTime.year(), e.StartDateTime.month(), e.StartDateTime.day());
			t.InterventionNumber__c = e.InterventionNumber__c;
			t.Subject = e.Subject;
			t.SiteID__c = e.SiteID__c;
			t.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
			t.Status = e.StartDateTime >= System.now() ? 'Ouverte' : 'Fermée';

			if(e.WhatId != null){
			t.Subscription__c = m_case.get(e.WhatId).Subscription__c;
			}
			String rangeHour = getRangeHour(e.StartDateTime.hour());

			t.PlannedRangedHours__c = rangeHour;

			l_tasks.add(t);
		}
		insert l_tasks;
		//delete l_events;
	}

	public static void changeCaseType(List<Case> l_cases) {

		for(Case c : l_cases) if(c.ProvisioningType__c == 'Objet'){
			c.ProvisioningType__c = Label.SICo_StationConnecte;
		}
		update l_cases;

	}

	public static void setSiteOnCaseFromAccount(List<Account> l_accs) {

		List<Case> l_cases = [SELECT Id, AccountId FROM Case WHERE AccountId IN :l_accs];

		Map<Id, SICo_Site__c> m_sites = new Map<Id, SICo_Site__c>();
		for(SICo_Site__c s : [SELECT Id, Subscriber__c FROM SICo_Site__c WHERE Subscriber__c IN :l_accs]){
			m_sites.put(s.Subscriber__c, s);
		}

		for(Case c : l_cases) if(m_sites.get(c.AccountId) != null){
			c.Site__c = m_sites.get(c.AccountId).Id;
		}

		update l_cases;

	}

	public static void setParamsOnTask(List<Account> l_accs){

		List<Task> m_se4 = [SELECT Id, WhatId, Parameters__c, TaskId__c FROM Task WHERE TaskId__c = 'SE0004' AND AccountId IN :l_accs AND Parameters__c = 'OSF;DUMMY_COD'];

		Map<Id, Task> m_withTh = new Map<Id, Task>();

		Map<Id, Task> m_withoutTh = new Map<Id, Task>();

		List<Task> taskToUpdate = new List<Task>();

		System.debug('yyyyyyyyyyyyyyyyyyyyyyy' + [SELECT Id, AccountId, TaskId__c FROM Task WHERE TaskId__c = 'SE0004']);
		System.debug('yyyyyyyyyyyyyyyyyyyyyyy' + [SELECT Id, WhatId, Parameters__c, TaskId__c FROM Task WHERE TaskId__c = 'TH0002' AND AccountId IN :l_accs]);
		System.debug('yyyyyyyyyyyyyyyyyyyyyyy' + [SELECT Id, WhatId, Parameters__c, TaskId__c FROM Task WHERE TaskId__c = 'SE0003' AND AccountId IN :l_accs]);
		for(Task t : [SELECT Id, WhatId, Parameters__c, TaskId__c FROM Task WHERE TaskId__c = 'TH0002' AND AccountId IN :l_accs])
			m_withTh.put(t.WhatId, t);

		for(Task t : [SELECT Id, WhatId, Parameters__c, TaskId__c FROM Task WHERE TaskId__c = 'SE0003' AND AccountId IN :l_accs])
			m_withoutTh.put(t.WhatId, t);


		for(Task t_se4 : m_se4){
			if(m_withTh.get(t_se4.WhatId) != null && t_se4.Parameters__c != 'OSF;SER_C43_LAB_THERM'){
				t_se4.Parameters__c = 'OSF;SER_C43_LAB_THERM';
				taskToUpdate.add(t_se4);
			}
			else if(m_withoutTh.get(t_se4.WhatId) != null && t_se4.Parameters__c != 'OSF;SER_C43_LAB_NO_THERM'){
				t_se4.Parameters__c = 'OSF;SER_C43_LAB_NO_THERM';
				taskToUpdate.add(t_se4);
			}
		}

		update taskToUpdate;

	}

	private static String getRangeHour(Integer starthour) {
		Map<String,List<Integer>> rangeMap =
   			new Map<String,List<Integer>> {'08:00 - 10:00' => new List<Integer> { 0, 9 },
									      '10:00 - 12:00' => new List<Integer> { 10, 12 },
									      '13:30 - 15:30' => new List<Integer> { 13, 15 },
									      '15:30 - 17:30' => new List<Integer> { 15, 24 }
				   					  };
		String result = null;
   		
   		for (String text : rangeMap.keySet()) {
     	 	if (starthour >= rangeMap.get(text)[0] &&
          		starthour < rangeMap.get(text)[1]) {
        	 	result = text;
         		break;
      		}
   		}
   		return result;
	}


}