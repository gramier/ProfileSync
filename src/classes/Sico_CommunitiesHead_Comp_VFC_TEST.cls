/*------------------------------------------------------------
Author:        Selim BEAUJOUR
Company:       Ikumbi
Description:  Test Class for Sico_CommunitiesHead_Comp_VFC
Test Class:   
History
11/1O/2016      Selim BEAUJOUR      Create version
------------------------------------------------------------*/
@isTest
public  class Sico_CommunitiesHead_Comp_VFC_TEST {

    @isTest static void methodTest(){



        SICo_Live_Agent_Setting__c liveAgent = new SICo_Live_Agent_Setting__c();
        liveAgent.Name ='liveagent';
        liveAgent.IdLiveAgentInit1__c ='test';
        liveAgent.IdLiveAgentInit2__c='test';
        liveAgent.IdShow__c ='test';
        liveAgent.URLInit__c = 'www.google.com';
        insert liveAgent;
        Sico_CommunitiesHead_Comp_VFC live = new Sico_CommunitiesHead_Comp_VFC();
        System.assertEquals('liveagent', liveagent.Name);
        System.assertEquals('test', liveAgent.IdLiveAgentInit1__c);
        System.assertEquals('test', liveAgent.IdLiveAgentInit2__c);
        System.assertEquals('www.google.com', liveAgent.URLInit__c);

    }




}