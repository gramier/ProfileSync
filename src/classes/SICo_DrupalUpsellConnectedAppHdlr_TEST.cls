@istest
private class SICo_DrupalUpsellConnectedAppHdlr_TEST {

  //set test data
  @testSetup
  static void testData() {
    //Insert new community user
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;

        User us = [SELECT accountId FROM User WHERE Id =:communityUser.Id LIMIT 1];

        //Site creation
        SICo_Site__c site = SICo_UtilityTestDataSet.createSite(us.accountID);
        site.BoilerBrand__c = 'brand';
        site.BoilerModel__c = 'model';
        site.IsActive__c = true;
        site.IsSelectedInCustomerSpace__c = true;
        insert site;

        //Subscription creation
        Zuora__Subscription__c subSowee = new Zuora__Subscription__c(
            Zuora__Status__c= 'Active',
            Zuora__OriginalId__c='original',
            Zuora__PreviousSubscriptionId__c='original',
            Name= 'soweeContract',
            Zuora__Account__c= us.accountID,
            SiteId__c= site.Id,
            Zuora__TermStartDate__c = Date.today(),
            Zuora__MRR__c = 22,
            SICO_Subscribed_offers__c = 'SE;TH;GAZ',
            OfferName__c = 'SE_TH_GAZ',
            Zuora__TermEndDate__c = Date.today(),
            MarketingOfferName__c ='test Offer'
            );
        insert subSowee;
  }

  @isTest
  static void test() {

    User us = [SELECT Id, accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

    SICo_DrupalUpsellConnectedAppHandler handler = new SICo_DrupalUpsellConnectedAppHandler();

    Map<String,String> formulaDefinedAttributes = new Map<String,String>();

    String connectedAppId = [SELECT Id FROM ConnectedApplication WHERE Name = 'ConnectedApp - Drupal Upsell'].Id;

    formulaDefinedAttributes = handler.customAttributes(us.Id, connectedAppId, formulaDefinedAttributes, null);
    
  }

}