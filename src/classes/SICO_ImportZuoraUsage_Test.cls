@isTest
private class SICO_ImportZuoraUsage_Test {

  public static String CRON_EXP = '0 0 0 15 3 ? 2022';

  static testMethod void processInvoiceTest() {
    String reportId = '';
    String reportRunId = '';
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockHTTPResponse_Zconnect());
    SICO_ImportZuoraUsage.launchProcess(SICO_ImportZuoraUsage.InvoceItemReportName);
    Test.stopTest();
  }

  static testMethod void processUsageTest() {
    String reportId = '';
    String reportRunId = '';
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockHTTPResponse_Zconnect());
    SICO_ImportZuoraUsage.launchProcess(SICO_ImportZuoraUsage.UsageReportName);
    Test.stopTest();
  }

  static testMethod void importZuoraReportJobInvoiceTest(){
    List<String> listmes = new List<String>();
    listmes.add('InvoiceItem.Id,Invoice.Id,Subscription.Id,ProductRatePlanCharge.Id,InvoiceItem.ServiceStartDate,InvoiceItem.ServiceEndDate,InvoiceItem.UOM,InvoiceItem.UnitPrice,InvoiceItem.AccountingCode,InvoiceItem.ChargeAmount,InvoiceItem.TaxCode,InvoiceItem.ChargeName,InvoiceItem.ChargeDate,InvoiceItem.TaxAmount,InvoiceItem.TaxExemptAmount,InvoiceItem.Quantity,InvoiceItem.TaxMode,Product.Name,ProductRatePlanCharge.Category__c,InvoiceItem.ProcessingType,RatePlanCharge.ChargeNumber,ProductRatePlanCharge.ChargeType,Account.Currency');
    listmes.add('2c92c08558aa2e4f0158b438f07c7df0,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca296320edc,2016-11-28,2016-11-28,,27.73,Ventes de Gaz - services reseau Gaz factures (autres frais),"27,73",TVA Normale,Frais GRDF - Releve special,2016-11-30T07:53:00+0000,5.55,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025467,OneTime,EUR');
    listmes.add('2c92c08558aa2e4f0158b438f0827df5,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca2958d0ec0,2016-11-28,2016-11-28,,14.9,Ventes de Gaz - services reseau Gaz factures (mise en service),14.9,TVA Normale,Frais GRDF - Mise en Service,2016-11-30T07:53:00+0000,2.98,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025468,OneTime,EUR');
    listmes.add('2c92c08558aa2e4f0158b438f0857df6,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca296320edc,2016-11-28,2016-11-28,,27.73,Ventes de Gaz - services reseau Gaz factures (autres frais),27.73,TVA Normale,Frais GRDF - Releve special,2016-11-30T07:53:00+0000,5.55,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025489,OneTime,EUR');
    listmes.add('2c92c08558aa2e4f0158b438f0897df7,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca2958d0ec0,2016-11-28,2016-11-28,,14.9,Ventes de Gaz - services reseau Gaz factures (mise en service),14.9,TVA Normale,Frais GRDF - Mise en Service,2016-11-30T07:53:00+0000,2.98,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025490,OneTime,EUR');
    listmes.add('2c92c08558aa2e4f0158b438f08c7df8,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f85721ffeb0157447e033970b7,2018-07-11,2018-08-10,,0,Compte technique,5.5,,Facilite de paiement Station Energie,2016-11-30T07:53:00+0000,0,0,0,,Reductions et facilites de paiement,MATER-SE-MEN,0,C-00024649,Usage,EUR');
    listmes.add('2c92c08558aa2e4f0158b438f08c7df9,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f8564ed88a01564f9ae73d559f,2018-08-11,2018-09-10,,2.85,Clients Zuora - PCA Gaz,2.85,,Abo - Part distribution - T1,2016-11-30T07:53:00+0000,0,0,1,,GAZ - Taxes & Distribution,GAZ-ABO-DIST,0,C-00024654,Recurring,EUR');
    listmes.add('2c92c08558aa2e4f0158b438f08c7dfa,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f8564ed88a01564f9ae76655a6,2018-08-11,2018-09-10,,-2.85,Clients Zuora - PCA Gaz,-2.85,,Abo - Part distribution - T1 - Annulation,2016-11-30T07:53:00+0000,0,0,1,,GAZ - Taxes & Distribution,GAZ-ABO-DIST-ANN,0,C-00024655,Recurring,EUR');   
    SICO_ImportZuoraUsage.importZuoraReportJob(listmes,SICO_ImportZuoraUsage.InvoceItemReportName);  
  }

  static testMethod void importZuoraReportJobUsagesTest(){
    List<String> listmes = new List<String>();
    listmes.add('Account.Id,Usage.Id,Invoice.Id,InvoiceItem.Id,ProductRatePlanCharge.Id,Subscription.Id,Usage.MeterReadStart__c,Usage.MeterReadEnd__c,Usage.StartDateTime,Usage.EndDateTime,Usage.PCS__c,Usage.MeterReadStartType__c,Usage.MeterReadEndType__c,Usage.UOM,Usage.Quantity,Usage.m3Volume__c,Usage.DateCsoStart__c,Usage.DateCsoEnd__c,Usage.Event__c,Usage.ExtraBilledLine__c');
    listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa95015757ca940617d9,2c92c0855753c0df015757cf84cf597c,2c92c0855753c0df015757cf84e15981,2c92c0f8564ed88a01564f9af45556f5,2c92c0f957220b8c0157482cb1b64ebe,23710,23592,2017-03-23T23:00:00+0000,2017-03-23T23:00:00+0000,10,E,E,kwh,1022,118,02/25/2017,03/25/2017,,');
    listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e0157576be2e921ae,2c92c0855753c0df01575774e1e153ab,2c92c0855753c0df01575774e1f653b0,2c92c0f8564ed88a01564f9af45556f5,2c92c0f957220b8c0157482cb1b64ebe,23087,23000,2016-10-23T22:00:00+0000,2016-10-23T22:00:00+0000,10,M,E,kwh,1022,87,09/22/2016,10/25/2016,,');
    listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e0157576be2a721ab,2c92c0855753c0df01575774e1e153ab,2c92c0855753c0df01575774e1ff53b4,2c92c0f8564ed88a01564f9ae8a555d7,2c92c0f957220b8c0157482cb1b64ebe,23087,23000,2016-10-23T22:00:00+0000,2016-10-23T22:00:00+0000,10,M,E,kwh,1022,87,09/22/2016,10/25/2016,,');
    listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8162c199d,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757ccccf8519e,2c92c0f8564ed88a01564f9ae9ce5603,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
    listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8163d199e,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757cccd0151a1,2c92c0f8564ed88a01564f9af45556f5,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
    listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8160c199b,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757cccd0951a5,2c92c0f8564ed88a01564f9ae8a555d7,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
    listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8161b199c,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757cccd0c51a6,2c92c0f8564ed88a01564f9ae8d355de,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
    SICO_ImportZuoraUsage.importZuoraReportJob(listmes,SICO_ImportZuoraUsage.UsageReportName);  
  }

  static testMethod void SchedulableTest() {
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockHTTPResponse_Zconnect());
      // create account 
      Account acc= SICo_UtilityTestDataSet.createAccount('toto','titi','PersonAccount');
      insert acc;
      // Schedule the test job
      String jobId = System.schedule('SICO_ImportZuoraUsageScheduler',
      CRON_EXP, 
      new SICO_ImportZuoraUsageScheduler());
      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
      NextFireTime
      FROM CronTrigger WHERE id = :jobId];
      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
      ct.CronExpression);
      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);
    Test.stopTest();
  }

  static testMethod void batchTest() {
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockHTTPResponse_Zconnect());
      SICo_Batch_BILL_RUN batch = new SICo_Batch_BILL_RUN(SICO_ImportZuoraUsage.InvoceItemReportName);
      Database.executeBatch(batch);
    Test.stopTest();
  }
 
  public Class  MockHTTPResponse_Zconnect implements HttpCalloutMock {

    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse response = new HttpResponse();
      if(req.getEndpoint().contains('batch-query/') && !req.getEndpoint().contains('/jobs')){
        String query ='';
        if(req.getBody().contains('ProcessedUsage')){
          query = '"name": "ProcessedUsage",'+
                  '"query": "select Account.Id,Usage.Id,Invoice.Id,InvoiceItem.Id,ProductRatePlanCharge.Id,Subscription.Id,Usage.MeterReadStart__c,Usage.MeterReadEnd__c,Usage.StartDateTime,Usage.EndDateTime,Usage.PCS__c,Usage.MeterReadStartType__c,Usage.MeterReadEndType__c,'+
                  'Usage.UOM,Usage.Quantity,Usage.m3Volume__c,Usage.DateCsoStart__c,Usage.DateCsoEnd__c,Usage.Event__c,Usage.ExtraBilledLine__c from ProcessedUsage where Invoice.Status = \'Posted\' and UpdatedDate >= \'2016-11-30\'"';        
        }else{
          query = '"name": "InvoiceItem",'+
                  '"query": "select UpdatedDate,Id,Invoice.Id,Subscription.Id,ProductRatePlanCharge.Id,ServiceStartDate,ServiceEndDate,UOM,UnitPrice,AccountingCode,ChargeAmount,TaxCode,ChargeName,ChargeDate,TaxAmount,TaxExemptAmount,'+
                  'Quantity,TaxMode,Product.Name,ProductRatePlanCharge.Category__c,ProcessingType,RatePlanCharge.ChargeNumber,ProductRatePlanCharge.ChargeType,Account.Currency from InvoiceItem where Invoice.Status = \'Posted\' and UpdatedDate >= \'2016-11-30\'"';                 
        }
        response.setHeader('Connection','keep-alive');
        response.setHeader('Content-Type','application/json');
        response.setHeader('Date','Fri, 07 Oct 2016 11:27:00 GMT');
        response.setHeader('Expires','Fri, 07 Oct 2016 11:27:00 GMT');
        response.setHeader('Server','Zuora App');
        response.setBody( '{"batches": ['+
                              '{"localizedStatus": "pending",'+
                              '"recordCount": 0,'+
                              '"batchId": "2c92c0f958aa455e0158cf673ebd1e4d",'+
                              '"apiVersion": "81.0",'+
                              '"batchType": "zoqlexport",'+
                              '"full": false,'+
                              '"status": "pending",'+
                              query +
                            '}],'+
                          '"encrypted": "pgp",'+
                          '"partner": "salesforce",'+
                          '"project": "00d4e0000008nfq",'+
                          '"useLastCompletedJobQueries": false,'+
                          '"status": "submitted",'+
                          '"name": "Example4",'+
                          '"id": "2c92c0f958aa455e0158cf673eba1e4c",'+
                          '"version": "1.2",'+
                          '"format": "CSV"}'
                          );
          return response;
      }else if(req.getEndpoint().contains('/jobs')){
        String query ='';
        if(req.getBody().contains('ProcessedUsage')){
          query = '"name": "ProcessedUsage",'+
                  '"query": "select Account.Id,Usage.Id,Invoice.Id,InvoiceItem.Id,ProductRatePlanCharge.Id,Subscription.Id,Usage.MeterReadStart__c,Usage.MeterReadEnd__c,Usage.StartDateTime,Usage.EndDateTime,Usage.PCS__c,Usage.MeterReadStartType__c,Usage.MeterReadEndType__c,'+
                  'Usage.UOM,Usage.Quantity,Usage.m3Volume__c,Usage.DateCsoStart__c,Usage.DateCsoEnd__c,Usage.Event__c,Usage.ExtraBilledLine__c from ProcessedUsage where Invoice.Status = \'Posted\' and UpdatedDate >= \'2016-11-30\'"';        
        }else{
          query = '"name": "InvoiceItem",'+
                  '"query": "select UpdatedDate,Id,Invoice.Id,Subscription.Id,ProductRatePlanCharge.Id,ServiceStartDate,ServiceEndDate,UOM,UnitPrice,AccountingCode,ChargeAmount,TaxCode,ChargeName,ChargeDate,TaxAmount,TaxExemptAmount,'+
                  'Quantity,TaxMode,Product.Name,ProductRatePlanCharge.Category__c,ProcessingType,RatePlanCharge.ChargeNumber,ProductRatePlanCharge.ChargeType,Account.Currency from InvoiceItem where Invoice.Status = \'Posted\' and UpdatedDate >= \'2016-11-30\'"';                 
        }
        response.setHeader('Connection','keep-alive');
        response.setHeader('Content-Type','application/json');
        response.setHeader('Date','Fri, 07 Oct 2016 11:27:00 GMT');
        response.setHeader('Expires','Fri, 07 Oct 2016 11:27:00 GMT');
        response.setHeader('Server','Zuora App');

        response.setBody( '{"batches": ['+
                      '{"localizedStatus": "completed",'+
                      '"recordCount": 15,'+
                      '"batchId": "2c92c0f958aa455e0158cf673ebd1e4d",'+
                      '"apiVersion": "81.0",'+
                      '"fileId": "2c92c09558aa39c30158cf67420205c3",'+
                      '"batchType": "zoqlexport",'+
                      '"full": false,'+
                      '"status": "completed",'+
                      '"message": "",'+
                      query +
                    '}],'+
                  '"encrypted": "pgp",'+
                  '"partner": "salesforce",'+
                  '"project": "00d4e0000008nfq",'+
                  '"useLastCompletedJobQueries": false,'+
                  '"status": "submitted",'+
                  '"name": "Example4",'+
                  '"id": "2c92c0f958aa455e0158cf673eba1e4c",'+
                  '"startTime": "2016-12-05T15:33:19+0100",'+
                  '"version": "1.2",'+
                  '"format": "CSV"}'
                  );
        return response;
      }else if(req.getEndpoint().contains('/file')){
  			response.setHeader('Connection','keep-alive');
        response.setHeader('Content-Type', 'text/csv');
        response.setHeader('Date','Fri, 07 Oct 2016 11:27:00 GMT');
        response.setHeader('Expires','Fri, 07 Oct 2016 11:27:00 GMT');
        response.setHeader('Server','Zuora App');

        List<String> listmes = new List<String>();
        System.debug('*** getEndpoint '+ req.getEndpoint());
        if(req.getBody().contains('ProcessedUsage')){
          listmes.add('Account.Id,Usage.Id,Invoice.Id,InvoiceItem.Id,ProductRatePlanCharge.Id,Subscription.Id,Usage.MeterReadStart__c,Usage.MeterReadEnd__c,Usage.StartDateTime,Usage.EndDateTime,Usage.PCS__c,Usage.MeterReadStartType__c,Usage.MeterReadEndType__c,Usage.UOM,Usage.Quantity,Usage.m3Volume__c,Usage.DateCsoStart__c,Usage.DateCsoEnd__c,Usage.Event__c,Usage.ExtraBilledLine__c');
    			listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa95015757ca940617d9,2c92c0855753c0df015757cf84cf597c,2c92c0855753c0df015757cf84e15981,2c92c0f8564ed88a01564f9af45556f5,2c92c0f957220b8c0157482cb1b64ebe,23710,23592,2017-03-23T23:00:00+0000,2017-03-23T23:00:00+0000,10,E,E,kwh,1022,118,02/25/2017,03/25/2017,,');
    			listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e0157576be2e921ae,2c92c0855753c0df01575774e1e153ab,2c92c0855753c0df01575774e1f653b0,2c92c0f8564ed88a01564f9af45556f5,2c92c0f957220b8c0157482cb1b64ebe,23087,23000,2016-10-23T22:00:00+0000,2016-10-23T22:00:00+0000,10,M,E,kwh,1022,87,09/22/2016,10/25/2016,,');
    			listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e0157576be2a721ab,2c92c0855753c0df01575774e1e153ab,2c92c0855753c0df01575774e1ff53b4,2c92c0f8564ed88a01564f9ae8a555d7,2c92c0f957220b8c0157482cb1b64ebe,23087,23000,2016-10-23T22:00:00+0000,2016-10-23T22:00:00+0000,10,M,E,kwh,1022,87,09/22/2016,10/25/2016,,');
    			listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8162c199d,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757ccccf8519e,2c92c0f8564ed88a01564f9ae9ce5603,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
    			listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8163d199e,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757cccd0151a1,2c92c0f8564ed88a01564f9af45556f5,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
    			listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8160c199b,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757cccd0951a5,2c92c0f8564ed88a01564f9ae8a555d7,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
    			listmes.add('2c92c0f957220b8c0157482cb1874ebb,2c92c0f95753fa8e015757c8161b199c,2c92c0855753c0df015757cccce35198,2c92c0855753c0df015757cccd0c51a6,2c92c0f8564ed88a01564f9ae8d355de,2c92c0f957220b8c0157482cb1b64ebe,23430,23269,2017-01-23T23:00:00+0000,2017-01-23T23:00:00+0000,10,E,E,kwh,1022,161,12/25/2016,01/25/2017,,');
        }else{  
          listmes.add('InvoiceItem.Id,Invoice.Id,Subscription.Id,ProductRatePlanCharge.Id,InvoiceItem.ServiceStartDate,InvoiceItem.ServiceEndDate,InvoiceItem.UOM,InvoiceItem.UnitPrice,InvoiceItem.AccountingCode,InvoiceItem.ChargeAmount,InvoiceItem.TaxCode,InvoiceItem.ChargeName,InvoiceItem.ChargeDate,InvoiceItem.TaxAmount,InvoiceItem.TaxExemptAmount,InvoiceItem.Quantity,InvoiceItem.TaxMode,Product.Name,ProductRatePlanCharge.Category__c,InvoiceItem.ProcessingType,RatePlanCharge.ChargeNumber,ProductRatePlanCharge.ChargeType,Account.Currency');
          listmes.add('2c92c08558aa2e4f0158b438f07c7df0,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca296320edc,2016-11-28,2016-11-28,,27.73,Ventes de Gaz - services reseau Gaz factures (autres frais),27.73,TVA Normale,Frais GRDF - Releve special,2016-11-30T07:53:00+0000,5.55,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025467,OneTime,EUR');
          listmes.add('2c92c08558aa2e4f0158b438f0827df5,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca2958d0ec0,2016-11-28,2016-11-28,,14.9,Ventes de Gaz - services reseau Gaz factures (mise en service),14.9,TVA Normale,Frais GRDF - Mise en Service,2016-11-30T07:53:00+0000,2.98,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025468,OneTime,EUR');
          listmes.add('2c92c08558aa2e4f0158b438f0857df6,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca296320edc,2016-11-28,2016-11-28,,27.73,Ventes de Gaz - services reseau Gaz factures (autres frais),27.73,TVA Normale,Frais GRDF - Releve special,2016-11-30T07:53:00+0000,5.55,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025489,OneTime,EUR');
          listmes.add('2c92c08558aa2e4f0158b438f0897df7,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f956bc8fb40156bca2958d0ec0,2016-11-28,2016-11-28,,14.9,Ventes de Gaz - services reseau Gaz factures (mise en service),14.9,TVA Normale,Frais GRDF - Mise en Service,2016-11-30T07:53:00+0000,2.98,0,1,TaxExclusive,FRAIS,PSP-GRDF,0,C-00025490,OneTime,EUR');
          listmes.add('2c92c08558aa2e4f0158b438f08c7df8,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f85721ffeb0157447e033970b7,2018-07-11,2018-08-10,,0,Compte technique,5.5,,Facilite de paiement Station Energie,2016-11-30T07:53:00+0000,0,0,0,,Reductions et facilites de paiement,MATER-SE-MEN,0,C-00024649,Usage,EUR');
          listmes.add('2c92c08558aa2e4f0158b438f08c7df9,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f8564ed88a01564f9ae73d559f,2018-08-11,2018-09-10,,2.85,Clients Zuora - PCA Gaz,2.85,,Abo - Part distribution - T1,2016-11-30T07:53:00+0000,0,0,1,,GAZ - Taxes & Distribution,GAZ-ABO-DIST,0,C-00024654,Recurring,EUR');
          listmes.add('2c92c08558aa2e4f0158b438f08c7dfa,2c92c08558aa2e4f0158b438f0727ded,2c92c0f858aa38ab0158ab2de0235117,2c92c0f8564ed88a01564f9ae76655a6,2018-08-11,2018-09-10,,-2.85,Clients Zuora - PCA Gaz,-2.85,,Abo - Part distribution - T1 - Annulation,2016-11-30T07:53:00+0000,0,0,1,,GAZ - Taxes & Distribution,GAZ-ABO-DIST-ANN,0,C-00024655,Recurring,EUR');
        }
        system.debug( listmes);
        String mes = String.join(listmes, '\n');
        response.setBody(mes);
    		return response;
      }else{
  			return response;
	    }
    }
  }
}