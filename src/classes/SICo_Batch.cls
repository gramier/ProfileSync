/*------------------------------------------------------------
Author:        Gerald Ramier
Company:       Salesforce.com
Description:   
Test Class:    SICo_BatchNotify_RWS_TEST
History
12/08/2016     Gerald Ramier     Creation
------------------------------------------------------------*/

global abstract class SICo_Batch implements Database.Batchable<sObject> //, Database.Stateful,Database.AllowsCallouts
{
	
	public SICo_Batch.Response response;
    public SICo_Batch.Request request;
    protected ID batchId;
    protected integer batchSize = 1000;
    protected DateTime batchStartDate;

    /**
     * Response class
     */
    global class Response {
        
        public Boolean hasError {get;set;}
        public String errorCode {get;set;}
        public String errorMessage {get;set;}
        public String sfdcBatchId {get;set;}
        public String batchCode {get;set;}

        
        public Response (String batchId, String ibatchCode) {
            hasError = false;
            errorCode = '000';
            errorMessage = '';
            sfdcBatchId = batchId;
            batchCode = ibatchCode;
        }
    }
    
    /**
     * Request class
     */
    global class Request {
        public String batchCode {get;set;}
        public Datetime batchStartDate {get;set;}

        public Request(){}
    }

    /**
     * @return SICO_Batch : Constructor for SICO_Batch class
     */
	public SICo_Batch() {
        request = new SICo_Batch.Request();
        response = new SICo_Batch.Response('', '');
	}

    global abstract Database.QueryLocator start(Database.BatchableContext BC);

    global abstract void execute(Database.BatchableContext BC, List<sObject> scope);

    global virtual void finish(Database.BatchableContext BC) {}

    /**
     * This class execute a Batch.
     * @return SICO_Batch.Response
     */
	public SICO_Batch.Response execute() {
        try {
            this.response.sfdcBatchId = Database.executeBatch(this,batchSize);
        } catch (Exception e) {
            this.response.errorMessage = e.getMessage();
            this.response.hasError = true;
        }
        
        System.debug(this.request);
        System.debug(this.response);
        return this.response;
	}
	

}