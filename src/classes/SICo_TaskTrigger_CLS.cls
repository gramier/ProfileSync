/**
* @author Boris Castellani
* @date 25/10/2016
*
* @description Handle Task Trigger Business Rule
*/
public with sharing class SICo_TaskTrigger_CLS {

/* -------- BEFORE METHODE -------- */

  public static void handleBeforeInsert(List<Task> allTasks){
    handleBeforeUpsert(allTasks);
  }

	public static void handleBeforeUpsert(final List<Task> allTasks){
		final Map<Id,Id> agenceId = new Map<Id,Id>();
    final List<Id> contactId = new List<Id>();
    Id rtType = SICo_Utility.m_RTbyDeveloperName.get('Task_'+ Label.SICo_ChamDemand_Name);
    Sico_AdminTask__mdt adminTask = [SELECT DeveloperName, TaskOrder__c, ProvisioningCallOut__c, BlockingCBPayment__c 
                                     FROM Sico_AdminTask__mdt 
                                     WHERE DeveloperName = :Label.SICo_ChamDemand_TaskID Limit 1];
    system.debug('### adminTask' +adminTask);
		for (Task aTask : allTasks) {
      contactId.add(aTask.WhoId);
			if(aTask.RecordTypeId == rtType && aTask.TaskId__c == null){
        aTask.TaskId__c = adminTask.DeveloperName;
        aTask.Tech_ProvisioningReady__c = adminTask.BlockingCBPayment__c;
        aTask.ProvisioningCallOut__c = adminTask.ProvisioningCallOut__c;
        aTask.OrderFlux__c = adminTask.TaskOrder__c;
        agenceId.put(aTask.Id,aTask.CHAMAgency__c);
			}
		}
  	final Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id, Zone__c, PersonEmail, PersonContactId FROM Account 
                                                          WHERE Id in :agenceId.values()
                                                          OR PersonContactId in :contactId ]);
    if(!accounts.isEmpty()){
      final Map<Id,String> personEmails = new Map<Id,String>();
      for(Account acc : accounts.values()){
        personEmails.put(acc.PersonContactId, acc.PersonEmail);      
      }  
			for (Task aTask : allTasks) {
        if(personEmails.containsKey(aTask.WhoId)){
          aTask.Email__c = personEmails.get(aTask.whoId);          
        }         
				if(accounts.containsKey(aTask.CHAMAgency__c)) {
					aTask.Zone__c = accounts.get(aTask.CHAMAgency__c).Zone__c;
				}
			}
		}
	}

/* -------- AFTER METHODE -------- */

	public static void handleAfterInsert(List<Task> tasks){
		List<Task> taskRelatedToCase = new List<Task>();
  	for(Task aTask : tasks){
      if(aTask.WhatId != null && aTask.WhatId.getSObjectType() == Case.SObjectType){
  			taskRelatedToCase.add(aTask);
  		}
  	}
  	if(!taskRelatedToCase.isEmpty()){
  		checkStatus(tasks);	
  	}
  }

  public static void handleAfterUpdate(List<Task> tasks){
  	List<Task> taskRelatedToCase = new List<Task>();
    List<Id> crsId = new List<Id>();
  	for(Task aTask : tasks){
      if(aTask.Subject == Label.SICo_ChamCR_Subject && aTask.Status == SICo_Constants_CLS.STR_STATUS_CLOSE){
        
        if(aTask.CR__c != null && String.valueOf(aTask.CR__c) != ''){  
          crsId.add(aTask.CR__c);
        }
      }
  		if(aTask.WhatId != null && aTask.WhatId.getSObjectType() == Case.SObjectType){
  			taskRelatedToCase.add(aTask);
  		}
  	}
  	if(!taskRelatedToCase.isEmpty()){
  		checkStatus(tasks);	
  	}

    // Update Eligible Billing Ligne for CR
    if(!crsId.isEmpty()){
      List<SICo_CR__c> crs = new List<SICo_CR__c>();
      for(ID aId : crsId){
        crs.add(
          new SICo_CR__c(
            id = aId,
            IsEligibleBillingLign__c = true
          )
        );
      }   
      update crs;
    }
  }

/* -------- PRIVATE METHODE -------- */

  private static void checkStatus(List<Task> tasks){
    Set<Id> casesId = new Set<Id>();
    for(Task aTask : tasks) {
      casesId.add(aTask.whatId);
    }
    Map<Id,Case> casesMap = new Map<Id,Case>([SELECT Id FROM Case WHERE Id in :casesId 
                                              AND RecordTypeId = :SICo_Utility.m_RTbyDeveloperName.get(Schema.SObjectType.Case.Name + '_' + SICO_Constants_CLS.STR_CASE_SYSTEM_RT)]);
    
    Map<Id,Task> taskPerIds = new Map<Id,Task>(tasks);
    Map<Id,Set<Id>> tasksPerCase = new Map<Id,Set<Id>>();  
  	for(Task aTask : tasks) if(casesMap.containsKey(aTask.WhatId)) {
  		if(!tasksPerCase.containsKey(aTask.WhatId)) {
  			tasksPerCase.put(aTask.WhatId, new Set<Id>());
  		}
  		tasksPerCase.get(aTask.WhatId).add(aTask.id);
  	}
    for(Task aTask : [SELECT id, WhatId, Status FROM Task WHERE WhatId in :tasksPerCase.keySet()]) {
      if(!taskPerIds.containsKey(aTask.id)) {
        tasksPerCase.get(aTask.WhatId).add(aTask.Id);
        taskPerIds.put(aTask.id,aTask);
      }
    }
    List<Case> cases = new List<Case>();
    for(id aCaseId : tasksPerCase.keySet()) {
      Case aCase = new Case(id=aCaseId);
      Boolean open = false;
      
      for(Id aTaskId : tasksPerCase.get(aCaseId)){
        Task aTask = taskPerIds.get(aTaskId);
        if(aTask.Status != SICo_Constants_CLS.STR_STATUS_CLOSE) {
          open = true;
          break;
        }
      }
      aCase.Status = open 
      ? 'New' 
      : 'Closed';

      cases.add(aCase);
    }
    if(!cases.isEmpty()) {
      update cases;
    }
  } 
}