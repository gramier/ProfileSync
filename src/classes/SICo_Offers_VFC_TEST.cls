/*------------------------------------------------------------
Author:        Abd-slem OUALI
Company:       Ikumbi
Description:   Test of the Controller for the offers component in the dashborad page (SICo_Offers_VFC)
History
28/09/2016      Abd-slem OUALI 		Create version
------------------------------------------------------------*/
@isTest
private class SICo_Offers_VFC_TEST {
    
    @isTest static void test_ConstructorUserHome() {
        //Insert new community user
        User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser1;
        communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

        //select account
        Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

        //Insert new site
        SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
		site1.IsActive__c = true;
		site1.HousingType__c = Label.SICo_House;
        site1.SICO_Subscribed_offers__c = 'SE;TH;GAZ';
        insert site1;
        site1 = [SELECT Id, IsSelectedInCustomerSpace__c FROM SICo_Site__c WHERE Id = :site1.Id LIMIT 1];

        //Insert new opportunity
    	Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    	insert testOpportunity1;

        //Insert new opportunity
        Opportunity testOpportunity2 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity2', communityUser1.AccountId, site1.Id);
        insert testOpportunity2;        
        //Insert new opportunity
        Opportunity testOpportunity3 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity3', communityUser1.AccountId, site1.Id);
        insert testOpportunity3;

        //Insert new quote
    	zqu__Quote__c testQuote1 = SICo_UtilityTestDataSet.createZuoraQuote('testQuote1', communityUser1.AccountId, communityUser1.ContactId, testOpportunity1.Id);
    	insert testQuote1;
        testQuote1 = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :testQuote1.Id LIMIT 1];

        //Insert new quote
        zqu__Quote__c testQuote2 = SICo_UtilityTestDataSet.createZuoraQuote('testQuote2', communityUser1.AccountId, communityUser1.ContactId, testOpportunity2.Id);
        insert testQuote2;
        testQuote2 = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :testQuote2.Id LIMIT 1];
        //Insert new quote
        zqu__Quote__c testQuote3 = SICo_UtilityTestDataSet.createZuoraQuote('testQuote3', communityUser1.AccountId, communityUser1.ContactId, testOpportunity3.Id);
        insert testQuote3;
        testQuote3 = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :testQuote3.Id LIMIT 1];


        //Insert new subscription
    	Zuora__Subscription__c testSubscription1 = SICo_UtilityTestDataSet.createZuoraSubscription('testSubscription1', communityUser1.AccountId, testQuote1.Id, testQuote1.zqu__Number__c, site1.Id);
    	testSubscription1.OfferName__c = 'SE_TH_GAZ_INST';
        testSubscription1.MarketingOfferName__c ='Station thermo et gaz';
    	insert testSubscription1;
    	testSubscription1 = [SELECT Id, Zuora__Account__c, SiteId__c FROM Zuora__Subscription__c WHERE Id = :testSubscription1.Id LIMIT 1];

        //Insert new subscription
        Zuora__Subscription__c testSubscription2 = SICo_UtilityTestDataSet.createZuoraSubscription('testSubscription2', communityUser1.AccountId, testQuote2.Id, testQuote2.zqu__Number__c, site1.Id);
        testSubscription2.OfferName__c = 'MAINT';
        testSubscription2.MarketingOfferName__c ='Maintenance';
        insert testSubscription2;
        testSubscription2 = [SELECT Id, Zuora__Account__c, SiteId__c FROM Zuora__Subscription__c WHERE Id = :testSubscription2.Id LIMIT 1];
        //Insert new subscription
        Zuora__Subscription__c testSubscription3 = SICo_UtilityTestDataSet.createZuoraSubscription('testSubscription3', communityUser1.AccountId, testQuote3.Id, testQuote3.zqu__Number__c, site1.Id);
        testSubscription3.OfferName__c = 'GAZ';
        testSubscription3.MarketingOfferName__c ='Gaz';
        insert testSubscription3;
        testSubscription3 = [SELECT Id, Zuora__Account__c, SiteId__c FROM Zuora__Subscription__c WHERE Id = :testSubscription3.Id LIMIT 1];

        //Insert new payment method
    	Zuora__PaymentMethod__c zuoraPM1 = SICo_UtilityTestDataSet.createZuoraPaymentMethod(communityUser1.AccountId, true);
    	insert zuoraPM1;

        Date dateTest = Date.parse('26/08/16');
        
        //Insert Bundle 
        SICo_Bundle__c bundle1 = new SICo_Bundle__c(StartDate__c =dateTest, PublicLabel__c = 'ABCD', TechOfferName__c='SE_TH_GAZ');
        insert bundle1; 

        SICo_Bundle__c bundle2 = new SICo_Bundle__c (StartDate__c =dateTest,PublicLabel__c = 'DCBA', TechOfferName__c='MAINT');
        insert bundle2; 

        bundle1=[SELECT PublicLabel__c from SICo_Bundle__c where TechOfferName__c ='SE_TH_GAZ' ];
        bundle2=[SELECT PublicLabel__c from SICo_Bundle__c where TechOfferName__c ='MAINT' ];


        //Run as Community User
        system.runAs(communityUser1){
            Test.startTest();

            //set controller
            SICo_Offers_VFC controller1 = new SICo_Offers_VFC();
            //set VF
            Test.setCurrentPageReference(new PageReference('Page.Dashboard'));
            //set url parameter
			System.currentPageReference().getParameters().put('AccountId', communityUser1.AccountId);
			SICo_Offers_VFC.MyOffersWrapper vMow = SICo_Offers_VFC.getMOW(communityUser1.AccountId);  

            //asserts
            System.assert(vMOW.offerLabel != null, 'Offer Label is null and shouldnt be');
            System.assert(vMOW.offerNumber != null, 'Offer number is null and shouldnt be');
            System.assert(vMOW.hasContract != null, 'hascontract is null and shouldnt be'); 

            Test.stopTest();
        }
    }

}