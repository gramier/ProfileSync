/**
* @author Sebastien COLLADON
* @date 10/08/2016
*
* @description Standard controller extension on Account for the Person Account view header in the console
*/
public with sharing class SICO_ConsoleHeader_VFC {
	private account o_account;

	public transient Integer SiteNumber {get;private set;}

	public transient Zuora__CustomerAccount__c o_billingAccount {get;private set;}

	public transient Boolean b_isEligible {get;private set;}

  public transient Boolean b_isMaintenance {get;private set;}
  public transient Boolean b_isStation {get;private set;}
  public transient Boolean b_isGaz {get;private set;}
  public transient Boolean b_isThermostat {get;private set;}

  public transient String s_invoicingType {get;private set;}

	/*******************************************************************************************************
	* @description Constructor instanciates class members
	* @param con the standard controller
	*/
  public SICO_ConsoleHeader_VFC(ApexPages.StandardController con) {
  	this.o_account = (Account)con.getRecord();
    this.SiteNumber = 0;
    this.b_isEligible = false;
    this.b_isMaintenance = false;
    this.b_isStation = false;
    this.b_isGaz = false;
    this.b_isThermostat = false;

  	try {

			// Fetch Billing Account		
  		this.o_billingAccount = [SELECT id, Zuora__DefaultPaymentMethod__c, Zuora__Balance__c 
  													 FROM Zuora__CustomerAccount__c 
  													 where Zuora__Account__c = :this.o_account.id];
			
      this.b_isMaintenance = false;
      this.b_isStation = false;
      this.b_isGaz = false;
      this.b_isThermostat = false;
			// Fetch Souscription
      List<String> sl_invoicingTypes = new List<String>();
  		for(Zuora__Subscription__c o_sub : [SELECT InvoicingType__c, OfferName__c
  													 FROM Zuora__Subscription__c 
  													 WHERE Zuora__Account__c = :this.o_account.id 
  													 AND Zuora__Status__c = 'Active']) {
        if(string.isNotBlank(o_sub.OfferName__c)) {
          this.b_isMaintenance |= o_sub.OfferName__c.contains('MAINT');
          this.b_isStation |= o_sub.OfferName__c.contains('SE');
          this.b_isGaz |= o_sub.OfferName__c.contains('GAZ');
          this.b_isThermostat |= o_sub.OfferName__c.contains('TH');
          sl_invoicingTypes.add(o_sub.InvoicingType__c);
        }
      }
      this.s_invoicingType = string.join(sl_invoicingTypes,', ');
  	
  		// Fetch eligibility
		Set<String> ss_postalCode = new Set<String>();
		for(SICo_Site__c o_site : [SELECT PostalCode__c FROM SICo_Site__c WHERE Subscriber__c = :this.o_account.id AND isActive__c = true]) {
			ss_postalCode.add(o_site.PostalCode__c);
    		++this.SiteNumber;
		}
			
  		List<String> sl_insee = new List<String>();
  		List<SICo_INSEE__c> codeInsees = [ SELECT Id, INSEECode__c, City__c, DeliveryLabel__c FROM SICo_INSEE__c 
  			WHERE Id IN (SELECT CodeInsee__c FROM SICo_InseeCPJunction__c WHERE ZipCode__r.ZipCode__c IN :ss_postalCode)]; 
  		
  		for(SICo_INSEE__c o_insee : codeInsees) {
  			sl_insee.add(o_insee.INSEECode__c);
  		}

  		List<Account> ol_agency = SICo_Eligibility_CLS.getMaintenanceEligibility(sl_insee);
  		this.b_isEligible = ol_agency != null && !ol_agency.isEmpty();

  	} catch (Exception ex) {
  		SICo_LogManagement.sendErrorLog( 
      	'SICo',
      	'SICO_ConsoleHeader_VFC.constructor',
      	String.valueOf( ex )
      );
  	}
  }
}