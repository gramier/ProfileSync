/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Serialise & deserialize JSON subscription Flux
Test Class:    SICo_FluxSubscription_Test
History
04/07/2016      Boris Castellani     Create & develope first version
11/07/2016      Boris Castellani     Modify Name & dataSet for method
11/07/2016      Boris Castellani     Add Return request
------------------------------------------------------------*/
public with sharing class SICo_FluxSubscriptionModel {

	// Build Request
	public class Request {
		public List<Action> action {get; set;}

		public Request parse (String json) {
			return (Request) System.JSON.deserialize(json, Request.class);

			}
		}

	// Build return Request
	public class ReturnRequest {
		public List<Responses> Responses {get; set;}
		}

	public static ReturnRequest parseReturnRequest (String json) {
			return (ReturnRequest) System.JSON.deserialize(json, ReturnRequest.class);
			}


/**********************************
    Wrapper
 **********************************/

	public class Response {
		public String ActivityId;
		public String statusCode;
		public String errorDescription;
		}

	public class Responses {
		public Response Response;
	}

	public class Action {
		public String personId;
		public String siteId;
		public String activityId;
		public String endPoint;
		public String externalOfferCode;
		public SubscriptionRequest SubscriptionRequest;
		}

	public class SubscriptionRequest {
		public Datetime ts;
		public String siteElecExternalId;
		public String siteGasExternalId;
		public Person person;
		public PersonParameters personParameters;
		public Address siteAddress;
		public SiteParameters siteParameters;
		public String subscribedOfferExternalId;
		public SubscribedOfferParameters subscribedOfferParameters;
		}

	public class Person {
		public String title;
		public String firstName;
		public String lastName;
		public Address address;
		public String email;
		public Phones phones;
		}

	public class Address {
		public List<String> lines;
		public String zipCode;
		public String city;
		public String country;
		}

	public class Phones {
		public String portable;
		public String principal;
	}

  // class vide => Map vide JSON
	public class PersonParameters {}
	public class SiteParameters {}
	public class SubscribedOfferParameters {}

//END
}