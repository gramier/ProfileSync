/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi Solutions
Description:   Check the email
Test Class:    SICo_User_TEST
History
16/09/2016     Cécile Neaud     Create version
19/09/2016     Gaël Burneau     Modifications
------------------------------------------------------------*/
public with sharing class SICO_User_CLS {

	/**
     * [Called when a community user changes his email address, this method changes username and
	 * person account email according to new email address choosen.
	 * Add an error to the record if email address is already used.]
     * @param  listUser		[List of users - Users that have changed their email address]
     */
	public static void changeEmail(List<User> listUser) {

		//Retrieve contact linked to users and person accounts references, put them in a map
		Map<Id, Id> mapContactIdAccountId = new Map<Id, Id>();
		for (Contact personContact : [SELECT Id, AccountId
										FROM Contact WHERE IsPersonAccount = true
										 AND Id IN (SELECT ContactId FROM User WHERE Id IN :listUser)]) {
			mapContactIdAccountId.put(personContact.Id, personContact.AccountId);
		}

		//Put new email choosen by users in a list
		List<String> listUserEmails = new List<String>();
		for (User user : listUser) {
			listUserEmails.add(user.Email);
		}
		//Query users with same emails to trigger an error if needed
		Set<String> setUserEmails = new Set<String>();
		for (User user : [SELECT Id, Email FROM User WHERE Email In : listUserEmails]) {
			setUserEmails.add(user.Email);
		}

		//For all user
		List<Account> listAccountsToUpdate = new List<Account>();
		for (User user : listUser) {

			//If a user with same email already exists, add error to the record
			if (setUserEmails.contains(user.Email)) {
				user.addError(Label.SICo_UserTrg_ChangeEmail_Error);
			}
			//Else
			else {

				//Change username according to new email address and add suffux if it's a guest
				String sUsernamSuffix = '';
				// if (user.Username.contains('guest'))
				if (user.Username.endsWith(SICo_Constants_CLS.CUSTOMER_COMMUNITY_GUEST_SUFFIX))
					sUsernamSuffix = SICo_Constants_CLS.CUSTOMER_COMMUNITY_GUEST_SUFFIX;//'.guest';
				user.Username = user.Email + sUsernamSuffix;

				//Also change email addreses on person accounts
				listAccountsToUpdate.add(new Account(Id = mapContactIdAccountId.get(user.ContactId),
													 PersonEmail = user.Email));
			}
		}

		//Update person accounts
		if (!listAccountsToUpdate.isEmpty()) {
			System.enqueueJob(new asyncChangePersonAccountEmail(listAccountsToUpdate));
		}
	}

	public class asyncChangePersonAccountEmail implements Queueable {

		public List<Account> listAccountsToUpdate;

		public asyncChangePersonAccountEmail(List<Account> listAccountsToUpdate) {
			this.listAccountsToUpdate = listAccountsToUpdate;
		}

		public void execute(QueueableContext context) {
	        changePersonAccountEmail(listAccountsToUpdate);
	    }
	}

	public static void changePersonAccountEmail(List<Account> listAccountsToUpdate) {

		update listAccountsToUpdate;
	}
}