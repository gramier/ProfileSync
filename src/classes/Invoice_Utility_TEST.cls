@isTest
private class Invoice_Utility_TEST {

	@testsetup
    static void Init() {
        //User us =
        System.debug('XXX ===> START TEST SETUP');

        Account accnt = SICo_UtilityTestDataSet.createAccount('TestInv First Name1', 'TestInv Last Name', SICo_Constants_CLS.GUEST_PERSON_ACCOUNT_RECORD_TYPE);
        insert accnt;

        System.debug('XXX ==> ACCNT ID = ' + accnt.Id);

        Zuora__CustomerAccount__c zAccnt = new Zuora__CustomerAccount__c (Zuora__Account__c = accnt.Id);
        zAccnt.Zuora__BillCycleDay__c = '1st of the month';
        zAccnt.Zuora__Batch__c = 'Batch22';
        insert zAccnt;

        Account accnt1 = SICo_UtilityTestDataSet.createAccount('TestInv First Name2', 'TestInv Last Name', SICo_Constants_CLS.GUEST_PERSON_ACCOUNT_RECORD_TYPE);
        insert accnt1;

        Zuora__CustomerAccount__c zAccnt1 = new Zuora__CustomerAccount__c (Zuora__Account__c = accnt1.Id);
        zAccnt1.Zuora__BillCycleDay__c = '27th of the month';
        zAccnt1.Zuora__Batch__c = 'Batch22';
        insert zAccnt1;

        Account accnt2 = SICo_UtilityTestDataSet.createAccount('TestInv First Name3', 'TestInv Last Name', SICo_Constants_CLS.GUEST_PERSON_ACCOUNT_RECORD_TYPE);
        insert accnt2;

        Zuora__CustomerAccount__c zAccnt2 = new Zuora__CustomerAccount__c (Zuora__Account__c = accnt1.Id);
        zAccnt2.Zuora__BillCycleDay__c = 'th of the month';
        zAccnt2.Zuora__Batch__c = 'Batch';
        insert zAccnt2;


        //SICo_Site__c
        //Zuora__Subscription__c v_Sub = SICo_UtilityTestDataSet.createZuoraSubscription('Test Sub', accnt.Id, Id idQuote, String quoteNumber, String idSite)

        System.debug('XXX ===> END TEST SETUP');
    }


    @isTest
    static void testDebitDay() {

    	List<Account> l_accounts = [SELECT Id FROM Account WHERE lastname = 'TestInv Last Name'];
    	List<Zuora__CustomerAccount__c> l_zAccounts = [SELECT Id, Zuora__BillCycleDay__c, Zuora__Batch__c, Zuora__Account__c 
    													FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c in :l_accounts];

    	System.debug('XXX ACCNT LIST ===> ' + l_accounts);
    	System.debug('XXX ZUORA ACCNT LIST ===> ' + l_zAccounts);
    	
    	test.startTest(); 

    	List<Zuora__ZInvoice__c> l_zinvoice = new List<Zuora__ZInvoice__c>();
    	List<Zuora__ZInvoice__c> l_zinvoice_bad = new List<Zuora__ZInvoice__c>();


    	for (Zuora__CustomerAccount__c v_zAccount : l_zAccounts) {
    		Zuora__ZInvoice__c v_invoice = SICo_UtilityTestDataSet.createZuoraInvoice(v_zAccount.Zuora__Account__c);
    		v_invoice.Zuora__BillingAccount__c = v_zAccount.Id;		
    		if (v_zAccount.Zuora__Batch__c != 'Batch') {
	        	l_zinvoice.add( v_invoice );
	        }
	        else {
	        	l_zinvoice_bad.add( v_invoice );
	        }
		}

		INSERT l_zinvoice;
		INSERT l_zinvoice_bad;

    	test.stopTest();

    	List<Zuora__ZInvoice__c> l_zinvoice_result = [SELECT Id, scheduleDebitDate__c FROM Zuora__ZInvoice__c WHERE Id IN :l_zinvoice];
    	for (Zuora__ZInvoice__c z_inv : l_zinvoice_result) {
    		System.debug('XXX ===> INVOICE : '+ z_inv);
    		System.assertNotEquals(z_inv.scheduleDebitDate__c, null);
    	}

    	l_zinvoice_result = [SELECT Id, scheduleDebitDate__c FROM Zuora__ZInvoice__c WHERE Id IN :l_zinvoice_bad];
    	for (Zuora__ZInvoice__c z_inv : l_zinvoice_result) {
    		System.debug('XXX ===> BAD INVOICE : '+ z_inv);
    		System.assertEquals(z_inv.scheduleDebitDate__c, null);
    	}


    }
}