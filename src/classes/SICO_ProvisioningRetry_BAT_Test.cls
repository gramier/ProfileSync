/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SICO_ProvisioningRetry_BAT_Test {
	
	@testSetup
	private static void setup(){
    insert SICo_UtilityTestDataSet.createChamConfig();
    insert SICo_UtilityTestDataSet.createCustomSetting();
    insert SICo_UtilityTestDataSet.createConfig_Boomi();
    insert SICo_UtilityTestDataSet.create_GrDF_Config();

    Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
    insert account;
    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    insert site;

    // Create Meter & SiteMeter
    SICo_Meter__c meter = SICo_UtilityTestDataSet.createMeter();
    insert meter;
    SICo_SiteMeter__c siteMeter = SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE,site.id,meter.id);
    insert siteMeter;

    Case caseObjet = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet,account.Id,site.Id);
    Case caseMaintenance = SICo_UtilityTestDataSet.createCase('Maintenance',account.Id,site.Id);
    Case caseGaz = SICo_UtilityTestDataSet.createCase('Gaz',account.Id,site.Id);
    insert new List<Case>{caseObjet,caseMaintenance, caseGaz};

    List<Task> listTask = new List<Task>();
    listTask.add(SICo_UtilityTestDataSet.createProvisioningTask('GA0001','TEST GA0001',caseGaz.Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz'),true));
    listTask.add(SICo_UtilityTestDataSet.createProvisioningTask('GA0003','TEST GA0003',caseGaz.Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisioning'),true));
    listTask.add(SICo_UtilityTestDataSet.createProvisioningTask('SE0003','TEST SE0003',caseObjet.Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisioning'),true));
    listTask.add(SICo_UtilityTestDataSet.createProvisioningTask('SE0004','TEST SE0004',caseObjet.Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisioning'),true));
    listTask.add(SICo_UtilityTestDataSet.createProvisioningTask('SE0005','TEST SE0005',caseObjet.Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisioning'),true));
    listTask.add(SICo_UtilityTestDataSet.createProvisioningTask('TH0002','TEST TH0002',caseObjet.Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisioning'),true));
    listTask.add(SICo_UtilityTestDataSet.createProvisioningTask('IN0001','TEST IN0001',caseObjet.Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_CHAM'),true));

		for (Task aTask : listTask) {
			aTask.ProvisioningCallOut__c = true;
			aTask.Tech_ProvisioningReady__c = true;
      aTask.Status = 'Ouverte';
		}
		insert listTask;		
	}
	
  static testMethod void testSICO_ProvisioningRetry_BAT() {
    test.startTest();
    Database.executeBatch(new SICO_ProvisioningRetry_BAT(), 10);
    test.stopTest();
  }    
}