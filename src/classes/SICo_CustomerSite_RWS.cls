/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   REST Service in order to provide/set customer and site information
Test Class:    SICo_CustomerSite_RWS_TEST
History
19/07/2016      Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@RestResource(urlMapping='/CustomerSite')
global with sharing class SICo_CustomerSite_RWS {

    @HttpGet
    global static CustomerInfo getUniqueSiteInfo_RWS() {

        CustomerInfo customerJSON = null;

        try {

            //REST SERVICES PARAMETERS
            String CustomerNumber = RestContext.request.params.get('CustomerNumber');

            //GET INFO FROM CUSTOMER SITE
            SICo_Site__c oSite = [ SELECT Id, 
                                          SiteNumber__c,
                                          NumberStreet__c,
                                          AdditionalInformation__c,
                                          PostalCode__c,
                                          City__c,
                                          Country__c,
                                          Subscriber__c,
                                          IsActive__c,
                                          ( SELECT Id, ObjectType__c, Option__c
                                            FROM Assets__r
                                            WHERE SerialNumber = '' ),
                                          ( SELECT Id
                                            FROM Subscriptions__r
                                            WHERE Zuora__Status__c != 'Cancelled' AND Zuora__Status__c != 'Suspended' )
                                   FROM SICo_Site__c
                                   WHERE Subscriber__r.CustomerNumber__c = : CustomerNumber LIMIT 1 ];

            //GET CUSTOMER SUBSCRIPTION
            List<String> sIdList = new List<String>();
            for ( Zuora__Subscription__c oZS : oSite.Subscriptions__r ) {

                sIdList.add( oZS.Id );

            }

            //GET CUSTOMER SUBSCRIPTION FEATURES
            List<Zuora__SubscriptionProductFeature__c> productFeatureList = [ SELECT Id, Zuora__FeatureCode__c, Zuora__Subscription__c
                                                                              FROM Zuora__SubscriptionProductFeature__c
                                                                              WHERE Zuora__Subscription__c IN : sIdList ];

            //VERIFY IF CUSTOMER HAS GAS & MAINT FEATURE
            Boolean bIsGasCustomer = false;
            Boolean bIsMaintCustomer = false;
            for ( Zuora__SubscriptionProductFeature__c oSPF : productFeatureList ) {
                if ( oSPF.Zuora__FeatureCode__c == 'Gaz' ) { bIsGasCustomer = true; }
                if ( oSPF.Zuora__FeatureCode__c == 'Maintenance' ) { bIsMaintCustomer = true; }
            }

            //GET DETAILS OF DEVICE TO INSTALL
            String sDeviceDetails = '';
            if ( oSite.Assets__r != null ) {
                for ( Asset oA : oSite.Assets__r ) {
                    sDeviceDetails = oA.ObjectType__c + ' ' + oA.Option__c;
                }
            }
            Boolean bHasMaintenanceSchedule = false;
            /* 
            	// DOES NOT WORK FOR COMMUNITY USER
            	Case oCase = [ SELECT Id, ( SELECT Id, TaskId__c FROM Events ) FROM CASE WHERE ProvisioningType__c = 'Objet' AND Site__c = : oSite.Id LIMIT 1 ];
            */
            Map<Id, Case> cases = new Map<Id, Case>([ SELECT Id FROM Case WHERE ProvisioningType__c = :System.Label.SICo_ProvisionningObjet AND Site__c = : oSite.Id LIMIT 1]);
            system.debug('### cases: ' + cases);
            if (!cases.isEmpty()) {
            	List<Task> evts = [SELECT Id, TaskId__c, whatId FROM Task WHERE TaskId__c = 'IN0002' and whatId in :cases.keyset()];
	            if ( evts != null ) {
	                for ( Task oE : evts ) {
	                  system.debug('### oE: ' + oE);
	                  if( cases.containsKey(oE.whatId) ) bHasMaintenanceSchedule = true;
	                }
	            }
            }

            //FORMAT CUSTOMER INFO
            CustomerInfo oCI = new CustomerInfo( CustomerNumber,
                                                 oSite.Id,
                                                 oSite.SiteNumber__c,
                                                 oSite.NumberStreet__c,
                                                 oSite.AdditionalInformation__c,
                                                 oSite.PostalCode__c,
                                                 oSite.City__c,
                                                 oSite.Country__c,
                                                 sDeviceDetails,
                                                 bHasMaintenanceSchedule,
                                                 oSite.IsActive__c,
                                                 bIsGasCustomer,
                                                 bIsMaintCustomer,
                                                 false );
            system.debug('### oCI: ' + oCI);
            customerJSON = oCI;
        } catch ( Exception e ) {
          System.debug(e);
            SICo_LogManagement.sendErrorLog( 
                'SICo',
                Label.SICO_LOG_LEAD_Site_Info,
                String.valueOf( e )
            );
        }
        system.debug( '##### getUniqueSiteInfo_RWS - customerJSON : ' + customerJSON );
        return customerJSON;
    }

    @HttpPost
    global static Boolean confirmSiteInfo( String sIdCustomer,
                                           String sIdSite,
                                           String sSiteNumber,
                                           String sNumberStreet,
                                           String sAdditionalInformation,
                                           String sPostalCode,
                                           String sCity,
                                           String sCountry,
                                           String sDeviceDetails,
                                           Boolean bHasMaintenanceSchedule,
                                           Boolean bIsActive,
                                           Boolean bIsGasCustomer,
                                           Boolean bIsMaintCustomer,
                                           Boolean bHasBeenModified ) {
      system.debug('### confirmSiteInfo - sIdCustomer: ' + sIdCustomer);
      system.debug('### confirmSiteInfo - sIdSite: ' + sIdSite);
      system.debug('### confirmSiteInfo - sSiteNumber: ' + sSiteNumber);
      system.debug('### confirmSiteInfo - sAdditionalInformation: ' + sAdditionalInformation);
      system.debug('### confirmSiteInfo - sPostalCode: ' + sPostalCode);
      system.debug('### confirmSiteInfo - sCity: ' + sCity);
      system.debug('### confirmSiteInfo - sCountry: ' + sCountry);
      system.debug('### confirmSiteInfo - sDeviceDetails: ' + sDeviceDetails);
      system.debug('### confirmSiteInfo - bHasMaintenanceSchedule: ' + bHasMaintenanceSchedule);
      system.debug('### confirmSiteInfo - bIsActive: ' + bIsActive);
      system.debug('### confirmSiteInfo - bIsGasCustomer: ' + bIsGasCustomer);
      system.debug('### confirmSiteInfo - bIsMaintCustomer: ' + bIsMaintCustomer);
      system.debug('### confirmSiteInfo - bHasBeenModified: ' + bHasBeenModified);

        Boolean bStatus = true;

        try {

          SICo_Site__c oSite = [  SELECT Id, SiteNumber__c, Street__c, NumberStreet__c, AdditionalInformation__c, PostalCode__c, 
                                  City__c, Country__c, IsActive__c, ActiveDate__c
                                  FROM SICo_Site__c 
                                  WHERE Id = : sIdSite LIMIT 1 ];

            //UPDATE SITE INFORMATION IF REQUESTED AND IF CUSTOMER HAS NO GAS
            if ( ( bHasBeenModified && !bIsGasCustomer ) || bIsActive == false ) {
                if ( bHasBeenModified && !bIsGasCustomer ) {
                    oSite.NumberStreet__c = String.isBlank(sNumberStreet) ? oSite.NumberStreet__c : sNumberStreet;
                    oSite.AdditionalInformation__c = String.isBlank(sNumberStreet) ? oSite.AdditionalInformation__c : sAdditionalInformation;
                    oSite.PostalCode__c = String.isBlank(sNumberStreet) ? oSite.PostalCode__c : sPostalCode;
                    oSite.City__c = String.isBlank(sNumberStreet) ? oSite.City__c : sCity;
                    oSite.Country__c = String.isBlank(sNumberStreet) ? oSite.Country__c : sCountry;
                }
                oSite.IsActive__c = true;
                oSite.ActiveDate__c = date.today();
            } else if ( bHasBeenModified && bIsGasCustomer ) {
                bStatus = false;
            }

            //UPDATE PARTNER SUBSCRIPTIONS IF STATUS IS TRUE
            if ( bStatus ) {
                Case oCase = [ SELECT Id FROM Case 
                               WHERE Site__c = : sIdSite AND Account.CustomerNumber__c = : sIdCustomer AND ProvisioningType__c = :System.Label.SICo_ProvisionningObjet ];
                system.debug( '##### oCase : ' + oCase );
                
                List<Task> oTaskList = new List<Task>();
                // To Remove after the case is solved
                List<Task> tmpTask = [ SELECT Id, Subject, ProvisioningCallOut__c, TaskId__c, WhatId, FluxStatus__c, Parameters__c
                                        FROM Task
                                        WHERE Subject LIKE 'Souscription%' AND WhatId = :oCase.Id AND Tech_ProvisioningReady__c = true];
                
                tmpTask = [ SELECT Id, Subject, ProvisioningCallOut__c, TaskId__c, WhatId, FluxStatus__c, Parameters__c
                                        FROM Task WHERE WhatId = :oCase.Id AND FluxStatus__c != 'OK' AND Tech_ProvisioningReady__c = true];
				
				if (tmpTask.size() > 0) {
	                for (Task t : tmpTask) {
	                  if (!String.isBlank(t.Subject) && t.Subject.startsWithIgnoreCase('Souscription')) {
	                    t.ProvisioningCallOut__c = true;
	                    oTaskList.add(t);
	                  }
	                }
	                system.debug( '### oTaskList : ' + oTaskList );
	                SICo_FluxSubscriptionModel.Address newSiteAdress = new SICo_FluxSubscriptionModel.Address();
	                List<String> newSiteLines = new List<String>();
	                newSiteLines.add(oSite.NumberStreet__c);
	                newSiteAdress.lines = newSiteLines;
	                newSiteAdress.zipCode = oSite.PostalCode__c;
	                newSiteAdress.city = oSite.City__c;
	                newSiteAdress.country = oSite.Country__c;
	            
	                bStatus = SICo_ProvisioningActivities_CLS.launchProvisioningActivitiesSync(oTaskList, newSiteAdress);
	                
	                if(bStatus && oSite != null) {
						update oSite;
		            }
				} else {
          			system.debug( '### oSite.IsActive__c : ' + oSite.IsActive__c );
					if ( !oSite.IsActive__c ) bStatus = false; //MODIFIE PAR OV CAR SITE PEUT ETRE ACTIF AVEC SOUSCRITIONS DEJA ENVOYE - A REVOIR
				}
            }
        } catch ( Exception e ) {
            SICo_LogManagement.sendErrorLogAsync( 
                'SICo',
                Label.SICO_LOG_LEAD_Site_Confirm,
                String.valueOf( e )
            );
            bStatus = false;
        }
        system.debug( '### bStatus : ' + bStatus );
        return bStatus;
    }

    //WRAPPER CUSTOMER INFO
    global class CustomerInfo {
        String sIdCustomer;
        String sIdSite;
        String sSiteNumber;
        String sNumberStreet;
        String sAdditionalInformation;
        String sPostalCode;
        String sCity;
        String sCountry;
        String sDeviceDetails;
        Boolean bHasMaintenanceSchedule;
        Boolean bIsActive;
        Boolean bIsGasCustomer;
        Boolean bIsMaintCustomer;
        Boolean bHasBeenModified;

        global CustomerInfo ( String sIdCustomerToUse,
                              String sIdSiteToUse, 
                              String sSiteNumberToUse,
                              String sNumberStreetToUse, 
                              String sAdditionalInformationToUse, 
                              String sPostalCodeToUse, 
                              String sCityToUse, 
                              String sCountryToUse,
                              String sDeviceDetailsToUse,
                              Boolean bHasMaintenanceScheduleToUse,
                              Boolean bIsActiveToUse,
                              Boolean bIsGasCustomerToUse,
                              Boolean bIsMaintCustomerToUse,
                              Boolean bHasBeenModifiedToUse ) {

            sIdCustomer = sIdCustomerToUse;
            sIdSite = sIdSiteToUse;
            sSiteNumber = sSiteNumberToUse;
            sNumberStreet = sNumberStreetToUse;
            sAdditionalInformation = sAdditionalInformationToUse;
            sPostalCode = sPostalCodeToUse;
            sCity = sCityToUse;
            sCountry = sCountryToUse;
            sDeviceDetails = sDeviceDetailsToUse;
            bHasMaintenanceSchedule = bHasMaintenanceScheduleToUse;
            bIsActive = bIsActiveToUse;
            bIsGasCustomer = bIsGasCustomerToUse;
            bIsMaintCustomer = bIsMaintCustomerToUse;
            bHasBeenModified= bHasBeenModifiedToUse;
        }
    }
}