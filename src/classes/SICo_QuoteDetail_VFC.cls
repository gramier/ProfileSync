/*------------------------------------------------------------
Author:        David LEROUX
Company:       Ikumbi Solutions
Description:   Controller for Quote Detail page.
Test Class:    SICo_QuoteDetail_VFC_TEST
History
24/10/2016     David LEROUX     Create version
------------------------------------------------------------*/
public with sharing class SICo_QuoteDetail_VFC {

	// Account linked
	public String accountId { get; set; }
	// Quote linked
	public String quoteId { get; set; }

	public SICo_QuoteDetail_VFC() { 
		String vAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
		String vQuoteId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_QUOTEID);
        doInit(vAccountId, vQuoteId);
    } 

    private void doInit(String pAccountId, String pQuoteId) {
        try {
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(pAccountId);
            this.quoteId = pQuoteId;
            System.debug('### CPE current user id : ' + UserInfo.getUserId());
        } catch(exception e) {
            System.debug('Unable to return accountId');
        }
    }

    /**
    * Get the quote number from the quote id
    * @Param : the quote id
    */
    public String getQuoteNumberById() {
    	if (quoteId == null) {
    		return null;
    	}
    	return SICo_Quotes_Helper.getQuoteNumberById(quoteId);
    }

    /**
    * Return the current quote as a wrapped object
    * @Param : pQuoteId => Id of the quote
    */
    @AuraEnabled
	public static SICo_Quotes_Helper.QuoteDetailWrap getCurrentQuote(String pQuoteId) { 
		if (pQuoteId == null) {
			return null;
		}
		 SICo_Quotes_Helper.QuoteDetailWrap vQuote = SICo_Quotes_Helper.getQuoteDetailById(pQuoteId);

		return vQuote;
	}


    /**
    * Return the response of the change of the status value
    * @Param : The quote id
    * @Return : The response (exception message or success)
    */
    @AuraEnabled
	public static SICo_Quotes_Helper.QuoteDetailWrap acceptQuote(String pQuoteId, String pQuoteJSON) { 
		if (pQuoteId == null || pQuoteJSON == null) {
			return null;
		}  
		// Get the quote view to refresh (even if just one field is refreshed for now)
		Type vQuoteType = Type.forName('SICo_Quotes_Helper.QuoteDetailWrap');
		SICo_Quotes_Helper.QuoteDetailWrap vQuote = (SICo_Quotes_Helper.QuoteDetailWrap) JSON.deserializeStrict(pQuoteJSON, vQuoteType);
 
		// Now change the status
		vQuote = SICo_Quotes_Helper.changeQuoteStatus(pQuoteId, vQuote, SICo_Quotes_Helper.STATUS_ACCEPTED);  

		return vQuote;
	}

    /**
    * Return the response of the change of the status value
    * @Param : The quote id
    * @Return : The response (exception message or success)
    */
    @AuraEnabled
	public static SICo_Quotes_Helper.QuoteDetailWrap refuseQuote(String pQuoteId, String pQuoteJSON) { 
		if (pQuoteId == null || pQuoteJSON == null) {
			return null;
		} 
		// Get the quote view to refresh (even if just one field is refreshed for now)
		Type vQuoteType = Type.forName('SICo_Quotes_Helper.QuoteDetailWrap');
		SICo_Quotes_Helper.QuoteDetailWrap vQuote = (SICo_Quotes_Helper.QuoteDetailWrap) JSON.deserializeStrict(pQuoteJSON, vQuoteType);
		// Now change the status
		vQuote = SICo_Quotes_Helper.changeQuoteStatus(pQuoteId, vQuote, SICo_Quotes_Helper.STATUS_REFUSED); 

		return vQuote;
	}


}