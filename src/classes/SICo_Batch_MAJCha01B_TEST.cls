@isTest
private class SICo_Batch_MAJCha01B_TEST {

	@testSetup 
    static void setup() {
    	Test.startTest();
	    	insert SICo_UtilityTestDataSet.createChamConfig();
	        insert SICo_UtilityTestDataSet.createCustomSetting();
	        insert SICo_UtilityTestDataSet.createConfig_Boomi();

	    	Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Death Star','Agency');
			agency.AgencyNumber__c = 'ANUMBER';
			Insert agency;

	    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Anakin', 'PersonAccount');
	    	Insert acc;

	    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
	    	site.CHAMAgencyEntretien__c = agency.Id;
	    	Insert site;

	    	Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('Croiseur interstellaire',acc.Id,Site.Id);
	        opportunity.MaintenanceParts__c = 'Avec pièces';
	        opportunity.Maintenance_Mode_de_facturation__c = 'Annuel';
			Insert opportunity;

	    	zqu__Quote__c zuoraquote = SICo_UtilityTestDataSet.createZuoraQuote('Lightsaber', acc.Id, acc.PersonContactId,opportunity.id);
			Insert zuoraquote;

			Zuora__Subscription__c subscription = SICo_UtilityTestDataSet.createZuoraSubscription('Tatooine', acc.Id, zuoraquote.Id, 'R2D2', site.Id);
			Insert subscription;

			List<Case> cases = new List<Case>();
	    	cases.add(SICo_UtilityTestDataSet.createCase('Maintenance', acc.Id, opportunity.Id , site.Id));
	    	cases.add(SICo_UtilityTestDataSet.createCase('Objet', acc.Id, opportunity.Id , site.Id));

	    	for(Case theCase : cases){
	    		theCase.Subscription__c = subscription.Id;    	
	    	}
	    	Insert cases;

			Task thetask = SICo_UtilityTestDataSet.createTask('MA0001', 'Shadows of the Empire', cases.get(1).Id, 'TaskProvisioning', false);
			Insert thetask;
    	Test.stopTest();
   	}

    static testMethod void test_oneTask() {
		Test.setMock(HttpCalloutMock.class, new SICo_GlobalRespond_MOCK());
		Test.startTest();
			system.debug('***** startTest *****');
			SICo_Batch_MAJCha01B updateFluxCham = new SICo_Batch_MAJCha01B();
			system.debug('***getDMLStatements '+limits.getDMLStatements());
			system.debug('***getDMLRows '+limits.getDMLRows());
			system.debug('***getEmailInvocations '+limits.getEmailInvocations());
			system.debug('***getFutureCalls '+limits.getFutureCalls());
			Id batchId = Database.executeBatch(updateFluxCham);
			system.debug('***getDMLStatements '+limits.getDMLStatements());
			system.debug('***getDMLRows '+limits.getDMLRows());
			system.debug('***getEmailInvocations '+limits.getEmailInvocations());
			system.debug('***getFutureCalls '+limits.getFutureCalls());
		Test.stopTest();
    }

    static testMethod void test_allTask() {

    	List<Task> tasks = new List<Task>();
		List<Case> cases = [Select Id, ProvisioningType__c FROM Case];
		for(Case theCase : cases){
			if(theCase.ProvisioningType__c == 'Objet'){
				tasks.add(SICo_UtilityTestDataSet.createTask('IN0001', 'Shadows of the Empire', theCase.Id, 'TaskProvisioning', false));
			}else{
				tasks.add(SICo_UtilityTestDataSet.createTask('MA0001', 'Shadows of the Empire', theCase.Id, 'TaskProvisioning', false));
			}
		}
		Insert tasks;

		Test.setMock(HttpCalloutMock.class, new SICo_GlobalRespond_MOCK());
		Test.startTest();
			SICo_Batch_MAJCha01B updateFluxCham = new SICo_Batch_MAJCha01B();
			Id batchId = Database.executeBatch(updateFluxCham);
		Test.stopTest();
    }
}