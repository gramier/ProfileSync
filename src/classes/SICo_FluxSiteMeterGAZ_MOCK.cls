/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Mock GAZ responce for SICo_FluxSiteMeter
History
01/08/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
global class SICo_FluxSiteMeterGAZ_MOCK implements HttpCalloutMock {
	// Implement this interface method
	global HTTPResponse respond(HTTPRequest request) {
		SICo_FluxProfileModel.GasSupplyContractChange gaz = new SICo_FluxProfileModel.GasSupplyContractChange().parse(request.getBody());
		// Create a fake response
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		if(gaz.millesime == '10'){
			response.setBody('{"ownerId":"'+gaz.personId+'","StatusCode":"400","errorDescription":"NO ERROR"}');
			}else{
				response.setBody('{"ownerId":"'+gaz.personId+'","StatusCode":"200","errorDescription":"NO ERROR"}');
				}
		response.setStatusCode(200);
		return response;
		}
}