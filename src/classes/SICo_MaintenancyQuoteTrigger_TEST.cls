/**
* @author Boris Castellani
* @date 26/10/2016
*
* @description TEST MaintenancyQuote SICo_MaintenancyQuoteTrigger_CLS
*/

@isTest
private class SICo_MaintenancyQuoteTrigger_TEST {

    static testMethod void createCaseAndTaskInsert() {

        GenericSetting__c maintenancyQuotes = new GenericSetting__c(
            Name = 'Devis',
            Days__c = 60
        );
        Insert maintenancyQuotes;

     	//DataSet
		Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Cham TEST','Agency');
		agency.AgencyNumber__c = 'ANUMBER';
		Insert agency;

    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;

    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    	site.CHAMAgencyEntretien__c = agency.Id;
    	Insert site;

		// TEST & ASSERT
		Test.startTest();
	    	MaintenancyQuote__c maintenancyQuote = SICo_UtilityTestDataSet.createMaintenancyQuote(site.Id, acc.Id);
			maintenancyQuote.MaintenancyQuoteStatus__c = 'Accepté';    	
	    	Insert maintenancyQuote;

			Case myCase = [SELECT Id, ProvisioningType__c FROM Case Limit 1];
			List<Task> myTask = [SELECT Id, WhatId FROM Task WHERE WhatId = :myCase.Id];
		Test.stopTest();

        System.assertEquals(myCase.ProvisioningType__c , 'Depannage');
        System.assertEquals(myTask.size(), 1);
    }

    static testMethod void createCaseAndTaskUpdate() {

     	//DataSet
		Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Cham TEST','Agency');
		agency.AgencyNumber__c = 'ANUMBER';
		Insert agency;

    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;

    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    	site.CHAMAgencyEntretien__c = agency.Id;
    	Insert site;

		MaintenancyQuote__c maintenancyQuote = SICo_UtilityTestDataSet.createMaintenancyQuote(site.Id, acc.Id);	
		Insert maintenancyQuote;

		// TEST & ASSERT
		Test.startTest();

			MaintenancyQuote__c maintenancyQuoteNow = [SELECT Id, MaintenancyQuoteStatus__c FROM MaintenancyQuote__c Limit 1];
			maintenancyQuoteNow.MaintenancyQuoteStatus__c = 'Accepté';
			Update maintenancyQuoteNow;  

			Case myCase = [SELECT Id, ProvisioningType__c FROM Case Limit 1];
			List<Task> myTask = [SELECT Id, WhatId FROM Task WHERE WhatId = :myCase.Id];

		Test.stopTest();

        System.assertEquals(myCase.ProvisioningType__c , 'Depannage');
        System.assertEquals(myTask.size(), 1);
    }

}