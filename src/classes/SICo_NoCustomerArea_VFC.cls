/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the NoCustomerArea page
Test Class:    SICo_NoCustomerArea_VFC_TEST
History
16/09/2016     Mehdi BEGGAS      Create version
20/09/2016     Gaël BURNEAU      Modifications
------------------------------------------------------------*/
public with sharing class SICo_NoCustomerArea_VFC {

	public String firstName { get { return UserInfo.getFirstName(); } set; }

	//Called when VF is loading, redirects user if it's not a guest
	public PageReference doRedirect() {

		//If it's not a guest user, redirect to Dashbord page
		Boolean isGuestUser = (UserInfo.getUserName().endsWith(SICo_Constants_CLS.CUSTOMER_COMMUNITY_GUEST_SUFFIX));
		if (!isGuestUser) {
			PageReference pageRef = new PageReference(SICo_Constants_CLS.CUSTOMER_SPACE_START_URL);//Page.Dashboard;
			//pageRef.getParameters().put('accountId', oUser.AccountId);
			return pageRef;
		}
		//Else, stay on that page
		else
			return null;
	}
}