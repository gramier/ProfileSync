/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Controller for MyBills page
Test Class:    SICo_MyBills_VFC_TEST
History
14/09/2016     Gaël BURNEAU     Create version
------------------------------------------------------------*/
public with sharing class SICo_MyBills_VFC {

	public Id accountId { get; set; }
	public Boolean isSoweeContract { get; set; }
	public Zuora__ZInvoice__c oZuoraZInvoice { get; set; }
	public Id attachmentId { get; set; }
	public String sInvoiceDate { get; set; }
	public String sNextInvoiceDate { get; set; }
	public Boolean hasTooManyInvoices { get; set; }
	public Boolean nbBills {get; set;}
	public Boolean hasSubscriptionContract { get; set; }

	private Class LastInvoiceWrapper {
		public Id id { get; set; }
		public String sInvoiceDate { get; set; }
		public Decimal dInvoiceAmount { get; set; }
		public Decimal dInvoiceBalance { get; set; }
		public Boolean dInvoicePaymentInProgress { get; set; }
		public Boolean isUnpaid { get; set; }
		public String sInvoiceAttachmentId { get; set; }

		public LastInvoiceWrapper(Zuora__ZInvoice__c oZuoraZInvoice, String sInvoiceDate, Id attachmentId) {
			this.id = oZuoraZInvoice.Id;
			this.sInvoiceDate = sInvoiceDate;
			this.dInvoiceAmount = oZuoraZInvoice.TotalInvoiceAmountTTC__c;
			this.dInvoiceBalance = (oZuoraZInvoice.Zuora__Balance2__c != null ? oZuoraZInvoice.Zuora__Balance2__c : 0);
			this.dInvoicePaymentInProgress = oZuoraZInvoice.IsPaymentInProgress__c;
			this.isUnpaid = oZuoraZInvoice.IsUnpaid__c;
			this.sInvoiceAttachmentId = attachmentId;
		}
	}

	public List<LastInvoiceWrapper> listLastInvoiceWrappers { get; set; }

	public Map<Id, Datetime> mapParentIdCreateDate { get; set; }
	public Map<String, Id> mapParentIdAttachmentId { get; set; }

	public Decimal totalUnpaidBillsAmount { get; set; }
	public Integer numberUnpaidBills { get; set; }

	/**
     * [Contructor]
     */
    public SICo_MyBills_VFC() {

        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        doInit(requestedAccountId);
    }
    public SICo_MyBills_VFC(String requestedAccountId) {
        doInit(requestedAccountId);
    }

    private void doInit(String requestedAccountId) {

        try {
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        } catch (exception e) {
            System.debug('Unable to return accountId');
        }

		//Retrieve last bill and previous bills if there are
		isSoweeContract = false;
		// Init to false the flag that check if the account has a contract in zuora subscription
		hasSubscriptionContract = false;
		if (accountId != null) {

			//Retriev all sites of the account
			List<SICo_Site__c> listSICoSites = [SELECT Id
											      FROM SICo_Site__c
											     WHERE Subscriber__c = :accountId
											       AND IsSelectedInCustomerSpace__c = true
											     LIMIT 1000];
			//Retriev gaz subscrition for those sites to idetify if index block needs to be shown
			for (Zuora__Subscription__c oZuoraSubscription : [SELECT Id, IsMaintenance__c
																FROM Zuora__Subscription__c
															   WHERE Zuora__Account__c = :accountId
																 AND SiteId__c IN :listSICoSites
																 AND IsContract__c = true
															   LIMIT 1000]) {
				hasSubscriptionContract = true; // At least one contract for zuora subscription
 				if (!oZuoraSubscription.IsMaintenance__c)
 					isSoweeContract = true;
			}

			//Retrieve all invoices of the account
			List<Zuora__ZInvoice__c> listZuoraZInvoices = new List<Zuora__ZInvoice__c>();
			listZuoraZInvoices = [SELECT Id, Zuora__InvoiceDate__c, TotalInvoiceAmountTTC__c, NextInvoiceDate__c, IsAwaitingTransfer__c,
										 Zuora__Status__c, Zuora__Balance2__c, IsPaymentInProgress__c, IsUnpaid__c,
										 (SELECT Id FROM Attachments ORDER BY CreatedDate DESC LIMIT 1) //IKUMBI-480
									FROM Zuora__ZInvoice__c
								   WHERE Zuora__Account__c = :accountId
								ORDER BY Zuora__InvoiceDate__c DESC LIMIT 1000];
 			
 			// IKUMBI-480 : remove invoices with no pdf
			for (Integer i = 0; i < listZuoraZInvoices.size(); i++) {
				System.debug('########' + listZuoraZInvoices[i].attachments);
				if (listZuoraZInvoices[i].attachments == null || listZuoraZInvoices[i].attachments.isEmpty()) {
					listZuoraZInvoices.remove(i);
					i--;
				}
			}

			if (listZuoraZInvoices.size() > 1){
				nbBills = true;
			}
 
			//If there is at least one invoice
			if (!listZuoraZInvoices.isEmpty()) {

				oZuoraZInvoice = listZuoraZInvoices[0];

				//Format last invoice date and next invoice date
				sInvoiceDate = String.valueOf(oZuoraZInvoice.Zuora__InvoiceDate__c.day())
								+ ' ' + SICo_Constants_CLS.MONTHES.get(oZuoraZInvoice.Zuora__InvoiceDate__c.month()).toUpperCase()
								+ ' ' + String.valueOf(oZuoraZInvoice.Zuora__InvoiceDate__c.year());
				sNextInvoiceDate = '';
				if(oZuoraZInvoice.NextInvoiceDate__c != null){
					sNextInvoiceDate = String.valueOf(oZuoraZInvoice.NextInvoiceDate__c.day())
									+ ' ' + SICo_Constants_CLS.MONTHES.get(oZuoraZInvoice.NextInvoiceDate__c.month()).toUpperCase()
									+ ' ' + String.valueOf(oZuoraZInvoice.NextInvoiceDate__c.year());
				}

				//Retrieve PDF document for that bill 
			    attachmentId = oZuoraZInvoice.attachments[0].Id; 
				 
			} 

			//If there is more than one invoice
			if (listZuoraZInvoices.size() > 1) {
 
				//Create wrapper for all the invoices and add them in a list
				listLastInvoiceWrappers = new List<LastInvoiceWrapper>();
				for (Integer i = 1; i < listZuoraZInvoices.size(); i++) {
					Zuora__ZInvoice__c oInvoice = listZuoraZInvoices[i];
					String sInvDate = String.valueOf(oInvoice.Zuora__InvoiceDate__c.day())
										+ ' ' + SICo_Constants_CLS.MONTHES.get(oInvoice.Zuora__InvoiceDate__c.month()).toUpperCase()
										+ ' ' + String.valueOf(oInvoice.Zuora__InvoiceDate__c.year());
					//Id attId = mapParentIdAttachmentId.get(oInvoice.Id + '' + mapParentIdCreateDate.get(oInvoice.Id));
					Id attId = oInvoice.attachments[0].Id;
					// Add invoice only if attachment exists
					if (attId != null) {
						listLastInvoiceWrappers.Add(new LastInvoiceWrapper(oInvoice, sInvDate, attId));
					}
				}
			}

			SICo_UnpaidBills_VFC unpaidBillsController = new SICo_UnpaidBills_VFC(accountId);
			this.totalUnpaidBillsAmount = unpaidBillsController.totalUnpaidBillsAmount;
			this.numberUnpaidBills = unpaidBillsController.numberUnpaidBills;
			this.hasTooManyInvoices = false;
			if(listLastInvoiceWrappers != null){
				if(listLastInvoiceWrappers.size() > 4) this.hasTooManyInvoices = true;
			}
		}
    }
}