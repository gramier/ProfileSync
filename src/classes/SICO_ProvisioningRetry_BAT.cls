global with sharing class SICO_ProvisioningRetry_BAT implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
  global SICO_ProvisioningRetry_BAT() {
  }

  global Database.QueryLocator start(Database.BatchableContext bc) {
	Date fromDate = System.today().addDays(-3);
	Set<Id> recordTypeIds = new Set<Id>();
  for(Sico_AdminTask__mdt adminTask : [SELECT RecordType__c FROM Sico_AdminTask__mdt]){
    recordTypeIds.add(SICo_Utility.m_RTbyDeveloperName.get('Task_' + adminTask.RecordType__c));
  }
      	
  return Database.getQueryLocator([SELECT Id, ProvisioningCallOut__c, TaskId__c, WhoId, WhatId, FluxStatus__c,  
                                   Tech_ProvisioningReady__c, InterventionType__c, Parameters__c, InterventionPlannedDate__c
                                   FROM Task 
                                   WHERE LastModifiedDate >= :fromDate
                                   AND RecordTypeId IN :recordTypeIds
                                   AND Status = 'Ouverte'
                                   AND FluxStatus__c != 'OK'
                                   AND ProvisioningCallOut__c = true
                                   AND Tech_ProvisioningReady__c = true]);
  }

  global void execute(Database.BatchableContext BC, list<Sobject> scope) {
    List<Task> tasks = new List<Task>();
    for (Task aTask : (List<Task>) scope) {
      tasks.add(aTask);
    }
    SICo_ProvisioningActivities_CLS.launchProvisioningActivitiesSync(tasks, null);
  }

    global void finish(Database.BatchableContext BC) {
    }
}