/**
* @author Sebastien Colladon
* @date 09/01/2016
*
* @description Get WebService to trigger the product catalog from Zuora
* @url: /services/apexrest/updateprices
* @data: 
* {
*   "isSuccess": boolean,
*   "error": {
*     "errorCode": number,
*     "errorMessage": string
*   }
* }
*/
@RestResource(urlMapping='/updateprices')
global with sharing class SICO_UpdateProductCatalog {

  @future (callout=true)
  private static void executeSynchroAsync(Set<String> zuoraProductIdSet) {
    executeSyncchroSync(zuoraProductIdSet);
  }

  private static void executeSyncchroSync(Set<String> zuoraProductIdSet) {
    Zuora.zApi zApiInstance = new Zuora.zApi();
    if(!Test.isRunningTest()){
      zApiInstance.zlogin();
    }

    Zuora.OnDemandSyncManager syncManager = new Zuora.OnDemandSyncManager();
    syncManager.syncObjectType = Zuora.OnDemandSyncManager.ObjectType.ZPRODUCT; 
    syncManager.syncZuoraObjectIdSet = zuoraProductIdSet;
    if(!Test.isRunningTest()){
      List<Zuora.SyncResult> syncResult = syncManager.sendRequest();
      for(Zuora.SyncResult aResult : syncResult) {
        System.Debug(aResult);
      }
    }
  }

  @HttpGet
  global static UpdateProductCatalogResponse UpdateProductCatalogResponse() {
    UpdateProductCatalogResponse response = new UpdateProductCatalogResponse();

    try {
      Zuora.zApi zApiInstance = new Zuora.zApi();
      List<Zuora.zObject> zobjs = new List<Zuora.zObject>();
      if(!Test.isRunningTest()) {
        zApiInstance.zlogin();
        zobjs = zApiInstance.zquery('SELECT Id from Product');
      } else {
        zobjs = createMockZuoraProduct();
      }
    
      List<Set<String>> zuoraProductIdSetChunk = new List<Set<String>>();
      Set<String> zuoraProductIdSet = new Set<String>();
      for (Zuora.zObject o : zobjs) {
        zuoraProductIdSet.add((String)o.getValue('Id'));
        if(zuoraProductIdSet.size() == Limits.getLimitCallouts()){
          zuoraProductIdSetChunk.add(zuoraProductIdSet);
          zuoraProductIdSet.clear();
        }
      }  
      zuoraProductIdSetChunk.add(zuoraProductIdSet);

      Boolean firstIteration = true;
      for(Set<String> ids : zuoraProductIdSetChunk) {
        if(firstIteration) {
          executeSyncchroSync(ids);
          firstIteration = false;
        } else {
          executeSynchroAsync(ids);          
        }
      }
    } catch (Exception ex) {
      System.debug(ex.getMessage());
      response.isSuccess = false;
      response.error = ex.getMessage();
    }

    return response;
  }

  private static List<Zuora.zObject> createMockZuoraProduct() {
    List<Zuora.zObject> zobjs = new List<Zuora.zObject>();
    for(Integer i = 0 ; i < Limits.getLimitCallouts() + 1 ; ++i){
      Zuora.zObject aZobj = new Zuora.zObject('Product');
      aZobj.setValue('Id',''+i);
      zobjs.add(aZobj);
    }
    return zobjs;
  }


  global class UpdateProductCatalogResponse {
    public Boolean isSuccess = true;
    public String error;
  }
}