/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Web Service REST in order to provide signed JSON - call by Mobile & Xively Platform
Test Class:    SICo_SignedJson_RWS_TEST
History
21/07/2016     Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@RestResource(urlMapping='/SignedJson')
global with sharing class SICo_SignedJson_RWS {

    @HttpGet
    global static String getSignedJson_RWS() {

        String token = '';
        try {
            
            //REST SERVICES PARAMETERS
            String sIssued_at = String.valueOf(Datetime.now().getTime());
            String sAccess_Token = RestContext.request.params.get( 'Access_Token' );
            String sApplication = RestContext.request.params.get( 'Application' );

            //RETRIEVE CURRENT USER AND SITE INFO
            String sCustomerId = UserInfo.getUserId();
            User oUser = [ SELECT Id, Username, Contact.AccountId, Contact.Account.CustomerNumber__c FROM User WHERE Id = : sCustomerId LIMIT 1 ];
            SICo_Site__c oSite = [ SELECT Id, SiteNumber__c FROM SICo_Site__c WHERE Subscriber__c = : oUser.Contact.AccountId LIMIT 1 ];
            String sSiteId = '';
            if ( oSite != null ) sSiteId = oSite.SiteNumber__c;

            //GET TOKEN
            token = SICo_JsonSignature_CLS.getJWT( sApplication, oUser.Username, oUser.Contact.Account.CustomerNumber__c , sAccess_Token, sIssued_at, sSiteId );

        } catch ( Exception e ) {

            SICo_LogManagement.sendErrorLog(
                'SICo',
                Label.SICO_LOG_AUTHENTICATION_Signed_JSON,
                String.valueOf( e )
            );

        }
        return token;
        
    }

}