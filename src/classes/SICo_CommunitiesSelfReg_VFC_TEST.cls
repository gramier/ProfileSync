/*------------------------------------------------------------
Author:        SFDC // Std Community Controller for Self Registration Test
Company:       Salesforce.com
Description:   Std Community Controller for Self Registration Test
History
05/08/2016      SFDC                Create version
09/08/2016      Dario Correnti      Modified version
16/08/2016      Mehdi Cherfaoui     Added Guest User registration test
------------------------------------------------------------*/
@IsTest 
public with sharing class SICo_CommunitiesSelfReg_VFC_TEST {

    @testSetup static void initData(){
        CommunityConfig__c commSett = SICo_UtilityTestDataSet.createCustomSetting ();
        insert commSett;
    }
    
    @IsTest(SeeAllData=false) 
    public static void testSuccessCommunitiesSelfRegController() {
        Account client = SICo_UtilityTestDataSet.createAccount('nameFirst', 'nameLast', 'PersonAccount');
        Account guest = SICo_UtilityTestDataSet.createAccount('guestFirst', 'guestLast', 'PersonAccountGuest');
        insert new List<Account> {client, guest};
                
        guest = [select id, PersonContactId from Account where id = :guest.Id];
        User guestUser = SICo_UtilityTestDataSet.createCommunityUser(guest.PersonContactId);
        guestUser.email = client.PersonEmail;
        guestUser.userName = client.PersonEmail + SICo_Constants_CLS.CUSTOMER_COMMUNITY_GUEST_SUFFIX;
        
        insert guestUser;
        
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        selfRegVF.getParameters().put('noClient', client.CustomerNumber__c);
        selfRegVF.getParameters().put('email', client.PersonEmail);
        
        Test.setCurrentPage(selfRegVF);
        Test.startTest();
        
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        String pwd = 'Xyz0123456!';
        ctrl.password = pwd;
        ctrl.confirmPassword = pwd;
        ctrl.pseudo = 'Xyz0123456';
        ctrl.registerUser();
        ctrl.doredirect();
        Test.stopTest();

        //Expect Results
        //no error (well in fact due to run as with a salesforce standard license, an error must be raised but only one)
        Boolean vErrorRaised = false;
        if (ApexPages.hasMessages()) {
        	ApexPages.Message[] vErrorMessage = ApexPages.getMessages();
        	if (vErrorMessage.size() > 1) {
				vErrorRaised = true;
        	} 
        }
        System.assert(!vErrorRaised, 'There should be only one error due to run as'); 

    }
    
    @IsTest(SeeAllData=false) 
    public static void testErrorMissingFields () {
        Account client = SICo_UtilityTestDataSet.createAccount('nameFirst', 'nameLast', 'PersonAccount');
        insert new List<Account> {client};
        
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        
        Test.startTest();
        Test.setCurrentPage(selfRegVF);
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        String pwd = 'Xyz0123456!';
        ctrl.password = pwd;
        ctrl.confirmPassword = pwd;
        ctrl.pseudo = 'Xyz0123456';
        ctrl.registerUser();
        System.assert(ApexPages.hasMessages());
                
        Test.stopTest();
    }   
    
    @IsTest(SeeAllData=false) 
    public static void testErrorWrongNoClient () {
        Account client = SICo_UtilityTestDataSet.createAccount('nameFirst', 'nameLast', 'PersonAccount');
        insert new List<Account> {client};
        
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        
        Test.startTest();
        selfRegVF.getParameters().put('noClient', client.CustomerNumber__c + 't');
        selfRegVF.getParameters().put('email', client.PersonEmail);
        
        Test.setCurrentPage(selfRegVF);
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        String pwd = 'Xyz0123456!';
        ctrl.password = pwd;
        ctrl.confirmPassword = pwd;
        ctrl.pseudo = 'Xyz0123456';
        ctrl.registerUser();
        ctrl.doredirect();
        System.assert(ApexPages.hasMessages());
        
        Test.stopTest();
    }   
    
    @IsTest(SeeAllData=false) 
    public static void testErrorWrongConfirmPwd () {
        Account client = SICo_UtilityTestDataSet.createAccount('nameFirst', 'nameLast', 'PersonAccount');
        insert new List<Account> {client};
        
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        
        Test.startTest();
        selfRegVF.getParameters().put('noClient', client.CustomerNumber__c);
        selfRegVF.getParameters().put('email', client.PersonEmail);
        
        Test.setCurrentPage(selfRegVF);
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        String pwd = 'Xyz0123456!';
        ctrl.password = pwd;
        ctrl.confirmPassword = pwd + '3';
        ctrl.pseudo = 'Xyz0123456';
        ctrl.registerUser();
        System.assert(ApexPages.hasMessages());
        
        Test.stopTest();
    }   

    /*
    [Test of a guest user]
     */
    @IsTest(SeeAllData=false) 
    public static void testGuestUser () {
        
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        String testEmail =  SICo_Utility.generateRandomString(20, 'TEXT')+'@testGuestUser.salesforce.com';
        selfRegVF.getParameters().put('email', testEmail) ;

        Test.startTest();
        
        Test.setCurrentPage(selfRegVF);
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        ctrl.email = testEmail;
        ctrl.firstName = 'TestFirstName';
        ctrl.lastName = 'TestLastName';
        ctrl.pseudo = 'Xyz0123456';
        ctrl.registerUser();

        Test.stopTest();


        //Expect Results
        //no error
        Boolean vErrorRaised = false;
        if (ApexPages.hasMessages()) {
        	ApexPages.Message[] vErrorMessage = ApexPages.getMessages();
        	if (vErrorMessage.size() > 1) {
				vErrorRaised = true;
        	} 
        }
        System.assert(!vErrorRaised, 'There should be only one error'); 
        //new Community User has been created
        List<Account> listAccounts = [SELECT Id, IsCustomerPortal FROM Account WHERE PersonEmail =: testEmail];
        System.assert(!listAccounts.isEmpty());
        System.assert(true, listAccounts[0].IsCustomerPortal);

    }

    @IsTest(SeeAllData=false) 
    public static void testGuestUserUsernameExists () {
        User testUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert testUser;
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        String testEmail =  SICo_Utility.generateRandomString(20, 'TEXT')+'@testGuestUser.salesforce.com';
        selfRegVF.getParameters().put('email', testEmail) ;

        Test.startTest();
        
        Test.setCurrentPage(selfRegVF);
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        ctrl.email = testEmail;
        ctrl.firstName = testUser.LastName;
        ctrl.lastName = testUser.FirstName;
        String pwd = 'Xyz012éè3456!';
        ctrl.password = pwd;
        ctrl.confirmPassword = pwd;
        ctrl.pseudo = 'Xyz0123456';
        ctrl.registerUser();

        Test.stopTest();

        //Expect Results
        //no error (more than one in fact)
        Boolean vErrorRaised = false;
        if (ApexPages.hasMessages()) {
        	ApexPages.Message[] vErrorMessage = ApexPages.getMessages();
        	if (vErrorMessage.size() > 1 && vErrorMessage[1].getSummary() == Label.SICo_UserExist) {
				vErrorRaised = true;
        	} 
        }
        System.assert(!vErrorRaised, 'There should be only one error');
        //new Community User has been created
        List<Account> listAccounts = [SELECT Id, IsCustomerPortal FROM Account WHERE PersonEmail =: testEmail];
        System.assert(!listAccounts.isEmpty());
        System.assert(true, listAccounts[0].IsCustomerPortal);

    }

    @IsTest(SeeAllData=false) 
    public static void testGuestUserAlreadyExists () {
        
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        User testUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert testUser;
        String testEmail = testUser.Email; //Already Taken
        selfRegVF.getParameters().put('email', testEmail) ;


        Test.startTest();
        
        Test.setCurrentPage(selfRegVF);
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        ctrl.email = testEmail;
        ctrl.firstName = 'TestFirstName';
        ctrl.lastName = 'TestLastName';
        ctrl.pseudo = 'Xyz0123456';
        String pwd = 'Xyz012éè3456!';
        ctrl.registerUser();

        Test.stopTest();

        //Expect Results : error
        System.assert(ApexPages.hasMessages());
    }

    @IsTest(SeeAllData=false) 
    public static void testErrorAlreadyTakenPseudo () {
        
        PageReference selfRegVF = Page.CommunitiesSelfReg;
        User testUser = SICo_UtilityTestDataSet.createCommunityUser();
        testUser.communityNickname = 'Xyz0123456';
        insert testUser;
        String testEmail = testUser.Email; //Already Taken
        selfRegVF.getParameters().put('email', testEmail) ;


        Test.startTest();
        
        Test.setCurrentPage(selfRegVF);
        SICo_CommunitiesSelfReg_VFC ctrl = new SICo_CommunitiesSelfReg_VFC();
        ctrl.email = testEmail;
        ctrl.firstName = 'TestFirstName';
        ctrl.lastName = 'TestLastName';
        ctrl.pseudo = 'Xyz0123456';
        String pwd = 'Xyz012éè3456!';
        ctrl.registerUser();

        Test.stopTest();

        //Expect Results : error
        System.assert(ApexPages.hasMessages());
    }   

}