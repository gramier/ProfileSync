@isTest
private class SICo_SubscriptionPrdCharge_Trigger_Test {
    
    static testMethod void testMethod1() {

    	// create Account 
    	Account acc1 = new Account(Name ='SalesCloud');
    	Account acc2 = new Account(Name ='ServiceCloud');
    	List<Account> listAccount = new List<Account>();
    	listAccount.add(acc1);
    	listAccount.add(acc2);
    	insert listAccount;

    	// Create 2 Zuora Billing Account
    	Zuora__CustomerAccount__c ZbillA = new Zuora__CustomerAccount__c(Name='SalesCloud', Zuora__AccountNumber__c='123',Zuora__Account__c=acc1.id);
    	Zuora__CustomerAccount__c ZbillB = new Zuora__CustomerAccount__c(Name='ServiceCloud', Zuora__AccountNumber__c='124',Zuora__Account__c=acc2.id);
    	List<Zuora__CustomerAccount__c> listAcc = new List<Zuora__CustomerAccount__c>();
    	listAcc.add(ZbillA);
    	listAcc.add(ZbillB);
    	insert listAcc;

    	// Create Zuora Subscription
    	Zuora__Subscription__c Zsub =  new Zuora__Subscription__c( Zuora__CustomerAccount__c =ZbillA.id,Zuora__Account__c=acc1.Id);
    	insert Zsub;
    	// Create Zuora Subscription Product Rate plan charge
    	Zuora__SubscriptionProductCharge__c ZPRCharge = new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c=Zsub.Id);
        system.debug('ZPRCharge :'+ZPRCharge);
    	insert ZPRCharge;
    	// Validate that the billing Account Has be set up on Subscription Product Rate plan Charge
    	ZPRCharge  = [Select id,Zuora__Subscription__c,ZBillingAccountNumber__c From Zuora__SubscriptionProductCharge__c limit 1 ];
    	//system.assertEquals('123',ZPRCharge.ZBillingAccountNumber__c);

    	//Change  the billing Account on the Zuora Subscription
    	Zsub =[Select id,Zuora__CustomerAccount__c from Zuora__Subscription__c limit 1];
    	Zsub.Zuora__CustomerAccount__c= ZbillB.Id;
    	update Zsub;
    	// perfom an update on the Subscription product Rate plan charge
    	update ZPRCharge;

    	// validate the good billing Account Number are on the Subscription
    	ZPRCharge  =  [Select id,Zuora__Subscription__c,ZBillingAccountNumber__c From Zuora__SubscriptionProductCharge__c limit 1 ];
    	//system.assertEquals('124',ZPRCharge.ZBillingAccountNumber__c);
        
    }
}