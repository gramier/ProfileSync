/*------------------------------------------------------------
Author:        Gerald Ramier
Company:       Salesforce.com
Description:   Web Service REST in order to provide notify SFDC of Integration process completion for Batch processing
Test Class:    SICo_BatchNotify_RWS_TEST
History
21/09/2016     Gerald Ramier     Creation
------------------------------------------------------------*/

global with sharing class SICo_Batch_CHA_02B extends SICo_Batch {

    global SICo_Batch_CHA_02B() {

    }

    override global Database.QueryLocator start(Database.BatchableContext bc) {

        List<Id> l_rtIds = new List<Id>();

        l_rtIds.add(SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_CHAM'));
        l_rtIds.add(SICo_Utility.m_RTbyDeveloperName.get('Task_ChamDemand'));

        return Database.getQueryLocator([SELECT Id, SiteId__c, InterventionType__c, TaskID__c 
                                            FROM Task 
                                            WHERE 
                                                whatid = null
                                                AND BatchFlag__c = true 
                                                AND RecordTypeId in :l_rtIds
                                            ORDER BY SiteId__c]);
    }

    override global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        
        SICO_Batch_Utility.getActivityCases(scope, 'CHA_02B');
    }

}