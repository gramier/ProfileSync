/**
* @author Boris Castellani
* @date 18/10/2016
*
* @description TEST SICo_SubscriptionTrigger_CLS Call by trigger
*/

@isTest
private class SICo_SubscriptionTrigger_TEST {

    @testSetup
    private static void setup(){
        insert SICo_UtilityTestDataSet.createChamConfig();
        insert SICo_UtilityTestDataSet.createCustomSetting();
        insert SICo_UtilityTestDataSet.createConfig_Boomi();
        insert SICo_UtilityTestDataSet.create_GrDF_Config();
    }

    static testMethod void handleActiveSiteMeter() {

    	//DataSet
    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;
    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    	Insert site;
    	SICo_Meter__c meter = SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PCE);
    	Insert meter;
    	SICo_SiteMeter__c siteMeter = SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, site.Id, meter.Id);
		Insert siteMeter;

		Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('SKYOpp',acc.Id,Site.Id);
        opportunity.MaintenanceParts__c = 'Avec pièces';
        opportunity.Maintenance_Mode_de_facturation__c = 'Annuel';
		Insert opportunity;
		zqu__Quote__c zuoraquote = SICo_UtilityTestDataSet.createZuoraQuote('SKYQuote', acc.Id, acc.PersonContactId,opportunity.id);
		Insert zuoraquote;

		// TEST & ASSERT
		Test.setMock(HttpCalloutMock.class, new SICo_GlobalRespond_MOCK());
		Test.startTest();
	        zqu__Quote__c quoteNumber = [SELECT zqu__Number__c FROM zqu__Quote__c WHERE ID= :zuoraquote.id LIMIT 1];
	        Zuora__Subscription__c zuoraSubscription = SICo_UtilityTestDataSet.createZuoraSubscription('SKYSub',acc.id,zuoraquote.id,quoteNumber.zqu__Number__c,site.id);
			insert zuoraSubscription;

			//ASSERT
			Zuora__Subscription__c subscription = [SELECT Id, SiteMeterGasActive__c FROM Zuora__Subscription__c WHERE Id =:zuoraSubscription.Id Limit 1];
			System.assertEquals(siteMeter.Id,subscription.SiteMeterGasActive__c);
			
		Test.stopTest();


    }
}