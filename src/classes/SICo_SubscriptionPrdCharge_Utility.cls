/**
* @author ???
* @date Modification 21/11/16 by Boris Castellani (add handlerBeforeUpsert & handlerAftreInsert)
*
* @description SICo_SubscriptionPrdCharge_Utility Trigger After update
*/
public with sharing class SICo_SubscriptionPrdCharge_Utility {

	public static void handlerBeforeUpsert(List<Zuora__SubscriptionProductCharge__c> subscriptionProductCharges){
		updateWithBillingAccountNumber(subscriptionProductCharges);
	}
	
	public static void handlerAfterUpsert(List<Zuora__SubscriptionProductCharge__c> subscriptionProductCharges){
		updateSubscription(subscriptionProductCharges);
	}
		
	private static void updateWithBillingAccountNumber(List<Zuora__SubscriptionProductCharge__c> listIdSubPrdCh){		
		List<Id> listIdZSPC= new List<Id>();
		for( Zuora__SubscriptionProductCharge__c dd : listIdSubPrdCh ){
			listIdZSPC.add(dd.Zuora__Subscription__c);
		}

		Map<ID,Zuora__Subscription__c> MapIdSubsSubs = new Map<ID,Zuora__Subscription__c>([select Id,Zuora__CustomerAccount__r.Zuora__AccountNumber__c  From Zuora__Subscription__c where Id in :listIdZSPC]);
		for( Zuora__SubscriptionProductCharge__c sub : listIdSubPrdCh){
			if(((Zuora__Subscription__c)MapIdSubsSubs.get(sub.Zuora__Subscription__c)).Zuora__CustomerAccount__r.Zuora__AccountNumber__c !=null){
				sub.ZBillingAccountNumber__c =((Zuora__Subscription__c)MapIdSubsSubs.get(sub.Zuora__Subscription__c)).Zuora__CustomerAccount__r.Zuora__AccountNumber__c;		
			}
			// if(((Zuora__Subscription__c)MapIdSubsSubs.get(sub.Zuora__Subscription__c)).SubscribedPower__c !=null){
			// 	sub.SubscribedPower__c =((Zuora__Subscription__c)MapIdSubsSubs.get(sub.Zuora__Subscription__c)).SubscribedPower__c;
			// }
		}
	}

	private static void updateSubscription(List<Zuora__SubscriptionProductCharge__c> subscriptionProductCharges){

		Set<String> values = new Set<String>{ Label.SICo_SubProCharge_MensuelAvecPieces, Label.SICo_SubProCharge_MensuelSansPieces,
											  Label.SICo_SubProCharge_AnnuelAvecPieces, Label.SICo_SubProCharge_AnnuelSansPieces };

		Map<String,List<String>> datas = new Map<String,List<String>>();
		datas.put(Label.SICo_SubProCharge_MensuelAvecPieces, new List<String>{SICo_Constants_CLS.STR_MENSUEL, SICo_Constants_CLS.STR_AVECPIECE});
		datas.put(Label.SICo_SubProCharge_MensuelSansPieces, new List<String>{SICo_Constants_CLS.STR_MENSUEL, SICo_Constants_CLS.STR_SANSPIECE});
		datas.put(Label.SICo_SubProCharge_AnnuelAvecPieces, new List<String>{SICo_Constants_CLS.STR_ANNUEL, SICo_Constants_CLS.STR_AVECPIECE});
		datas.put(Label.SICo_SubProCharge_AnnuelSansPieces, new List<String>{SICo_Constants_CLS.STR_ANNUEL, SICo_Constants_CLS.STR_SANSPIECE});

		Map<Id,Zuora__Subscription__c> updateSubscriptions = new Map<Id,Zuora__Subscription__c>();
		for(Zuora__SubscriptionProductCharge__c subPCharge : subscriptionProductCharges){
			if(values.contains(subPCharge.Name)){
				updateSubscriptions.put(subPCharge.Zuora__Subscription__c, new Zuora__Subscription__c( Id=subPCharge.Zuora__Subscription__c,
																	Maintenance_Mode_de_facturation__c = datas.get(subPCharge.Name)[0],
																	MaintenanceParts__c = datas.get(subPCharge.Name)[1]));
			}
		}

		if(!updateSubscriptions.isEmpty()){
			update updateSubscriptions.values();
		}
	}
}