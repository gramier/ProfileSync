// TODO
// Handle all trigger for provisioing
// Do batch job to periodically Billing Account
public with sharing class SICo_Z_CustomerAccountTriggerhandler_CLS {

  public static void HandleBeforeInsert(List<Zuora__CustomerAccount__c> bAccounts) {

    List<Zuora__CustomerAccount__c> billingAccounts = new List<Zuora__CustomerAccount__c>();
    Set<Id> accountIds = new Set<Id>();
    for(Zuora__CustomerAccount__c aZAccount : bAccounts) {
      if(!aZAccount.Tech_PaymentOK__c){
        accountIds.add(aZAccount.Zuora__Account__c);
        billingAccounts.add(aZAccount);
      }
    }
    Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id, Tech_PaymentCB__c, Tech_PaymentSEPA__c FROM Account WHERE Id in : accountIds]);
    accountIds.clear();
    for(Zuora__CustomerAccount__c aZAccount : billingAccounts) {
    	if (aZAccount.Zuora__Account__c != null) {
			accountIds.add(aZAccount.Zuora__Account__c);
			if (accounts.containsKey(aZAccount.Zuora__Account__c)) {
				aZAccount.Tech_PaymentCB__c = accounts.get(aZAccount.Zuora__Account__c).Tech_PaymentCB__c;
				aZAccount.Tech_PaymentSEPA__c = accounts.get(aZAccount.Zuora__Account__c).Tech_PaymentSEPA__c;
			}
    	}
    }

    List<Account> accountToUpdate = new List<Account>();
    for(Id anAccountId : accountIds) {
      Account anAccount = accounts.get(anAccountId);
      anAccount.Tech_PaymentCB__c = null;
      anAccount.Tech_PaymentSEPA__c = null;
      accountToUpdate.add(anAccount);
    }
    update accounts.values();
  }

  public static void HandleAfterInsert(List<Zuora__CustomerAccount__c> bAccounts) {
    List<Zuora__CustomerAccount__c> billingAccounts = new List<Zuora__CustomerAccount__c>();
    for(Zuora__CustomerAccount__c aZAccount : bAccounts) {
      if(!aZAccount.Tech_PaymentOK__c){
        billingAccounts.add(aZAccount);
      }
    }
    SICo_Z_CustomerAccountTriggerhandler_CLS.HandlePaymentAssociationProcess(billingAccounts);    
  }

  public static void HandleAfterUpdate(Map<Id, Zuora__CustomerAccount__c> oldBAccounts, List<Zuora__CustomerAccount__c> newBAccounts) {
    //List<Zuora__CustomerAccount__c> billingAccounts = new List<Zuora__CustomerAccount__c>();
    List<Id> accountForProvisioning = new List<Id>();
     
    for(Zuora__CustomerAccount__c aZAccount : newBAccounts) {
    	system.debug('### SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterUpdate aZAccount: ' + aZAccount);
   		system.debug('### SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterUpdate aZAccount.Tech_PaymentOK__c: ' + aZAccount.Tech_PaymentOK__c);
   		system.debug('### SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterUpdate aZAccount.Tech_PaymentSEPA__c: ' + aZAccount.Tech_PaymentSEPA__c);
   		system.debug('### SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterUpdate aZAccount.Tech_PaymentCB__c: ' + aZAccount.Tech_PaymentCB__c);
   		
      	//if(!aZAccount.Tech_PaymentOK__c && (String.isNotBlank(aZAccount.Tech_PaymentSEPA__c) || String.isNotBlank(aZAccount.Tech_PaymentCB__c))) {
        //	billingAccounts.add(aZAccount);
      	//} else 
      	
      	if(aZAccount.Tech_PaymentOK__c && !oldBAccounts.get(aZAccount.Id).Tech_PaymentOK__c){
        	accountForProvisioning.add(aZAccount.Zuora__Account__c);
      	}
    }
    
    //system.debug('### SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterUpdate billingAccounts: ' + billingAccounts);
    system.debug('### SICo_Z_CustomerAccountTriggerhandler_CLS.HandleAfterUpdate accountForProvisioning: ' + accountForProvisioning);
    
    /*
    if (!billingAccounts.isEmpty()) {
        SICo_Z_CustomerAccountTriggerhandler_CLS.HandlePaymentAssociationProcess(billingAccounts);
    }
    */
    if (!accountForProvisioning.isEmpty()) {
        SICo_Z_PaymentMethodHandler_CLS.readyAccountTask(accountForProvisioning);
    }
  }

  public static void HandlePaymentAssociationProcess(List<Zuora__CustomerAccount__c> bAccounts) {
    if (!System.isFuture() && !System.isBatch() && !Test.isRunningTest() ) {
        Map<String,Zuora__CustomerAccount__c> billingAccountsMap = new Map<String,Zuora__CustomerAccount__c>();
        for(Zuora__CustomerAccount__c aZAccount : bAccounts) {
          billingAccountsMap.put(aZAccount.Zuora__Zuora_Id__c, aZAccount);
        }
        
        system.debug('### HandlePaymentAssociationProcess - billingAccountsMap: ' + billingAccountsMap);
        SICo_Z_PaymentMethodHandler_CLS.attachZPaymentMethods(billingAccountsMap.keySet());
    }
  } 
}