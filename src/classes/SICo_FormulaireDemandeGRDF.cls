public with sharing class SICo_FormulaireDemandeGRDF {

  @future(callout=true)
    public static  void executeTaskCalloutAsync(String methode, String endPoint, String body){
    executeTaskCallout(methode,endPoint,body);
  }

  public static Boolean executeTaskCallout(String methode, String endPoint, String body) {
    Boolean result = true;
    try {
      HttpResponse response = SICo_Utility.getCallout(methode, endPoint, body);
    } catch (Exception e) {
      //Do not send to Logstash
      System.debug('### An error occurred on executeTaskCallout: ' + e.getMessage());
      result = false;
    }
    
    return result;
  }

  public static String buildFluxFormulaireDemandeGRDF(Map<Id,Task> grdfMap){
    Set<Task> listTask = new Set<Task>(grdfMap.values());
    List<Id> listIdTask= new List<Id>();
    List<Id> listIdCase= new List<Id>();
    List<SICO_FormulaireDemandeGRDF_Model.DemandeGRDF> listform= new List <SICO_FormulaireDemandeGRDF_Model.DemandeGRDF>();
    for(Task task : listTask){
      listIdTask.add(task.id);
    }
    
    Grdf_Config__c config = [SELECT login__c,Name,NumeroCAD__c,NumeroVersion__c,DNEntreprise__c 
                             FROM Grdf_Config__c limit 1];
    
    Map<ID, Task> taskByIdMap = new Map<ID, Task>([ SELECT Id, WhatId,Type, Subject, InterventionPlannedDate__c, PlannedRangedHours__c, MinimumHour__c, 
                                                    MaximumHour__c, InterventionType__c, Subscription__r.Opportunity__r.GrdfAppointmentDate__c 
                                                    /* , Subscription__r.Opportunity__r.GrdfAppointmentDate__c,TICGN__c */
                                                    FROM Task WHERE Id in: listIdTask]);
    for(Task task : taskByIdMap.values()){
      listIdCase.add(task.WhatId);
    }

    Map<ID, Case> caseByIdMap = new Map<ID, Case>([ SELECT Account.CustomerNumber__c, Site__r.SiteNumber__c, Account.FirstName, Account.Salutation, 
                                                    Account.PersonTitle, Account.LastName, Account.Phone, Account.PersonMobilePhone, 
                                                    Account.ContactAdresseBuilding__c, Account.ContactAddressFlat__c, Account.ContactAddressZipCode__c,  
                                                    Account.ContactAddressAdditionToAddress__c, Account.ContactAddressStair__c, Account.ContactAddressFloor__c, 
                                                    Account.ContactAddressNumberStreet__c, Account.ContactAddressCountry__c, Account.ContactAddressCity__c, 
                                                    Account.PersonEmail, Site__r.Street__c, Site__r.PostalCode__c, Site__r.City__c, Site__r.Country__c, 
                                                    Site__r.PCE__c, site__r.ActiveSiteMeterGas__c,Site__r.EstimatedGasConsumption__c
                                                    FROM Case WHERE id IN :listIdCase]);

    Set<Id> listIdSiteMeter = new Set<Id> ();
    for (Case c : caseByIdMap.values()) {
      if (c.site__c != null && c.site__r.ActiveSiteMeterGas__c != null) {
        listIdSiteMeter.add(c.site__r.ActiveSiteMeterGas__c);
      }
    }
                
    // Construct map id,SICo_SiteMeter__c 
    Map<Id, SICo_SiteMeter__c> siteMetersMap = new Map<Id, SICo_SiteMeter__c> ([SELECT Id, Meter__c, Meter__r.ReadingFrequency__c, Meter__r.CAR__c,GasType__c 
                                                                                FROM SICo_SiteMeter__c where id IN :listIdSiteMeter]); 
       
    // construct map ids siteMeter,Meter
    Map<Id,SICo_SiteMeter__c> siteMeterByMeterId = new Map<Id,SICo_SiteMeter__c>();
    for(SICo_SiteMeter__c sm: siteMetersMap.values()){
        siteMeterByMeterId.put(sm.Meter__c, sm);
    }
   //Construct map ids meter ,SICo_MeterReading__c 
    List<SICo_MeterReading__c> listIndex = new List <SICo_MeterReading__c>([  SELECT Id, ReadingDate__c, Meter__c, MeterReading__c, Type__c, CreatedDate 
                                                                              FROM SICo_MeterReading__c 
                                                                              WHERE createdDate >= :(Date.today().addDays(-10)) 
                                                                              AND Meter__c IN :siteMeterByMeterId.keySet() order by createdDate desc]);

    Map <Id,SICo_MeterReading__c> lastIndexByMeter = new Map<Id,SICo_MeterReading__c>();
    for(SICo_MeterReading__c index : listIndex){
      if(!lastIndexByMeter.containsKey(index.Meter__c)){
        lastIndexByMeter.put(index.Meter__c, index);
      }
    }
             
    for (Id id : listIdTask){
      Task taskFromId = taskByIdMap.get(id);
      Case caseFromId = caseByIdMap.get(taskFromId.WhatId); 
      SICo_SiteMeter__c cptDeSite = siteMetersMap.get(caseFromId.site__r.ActiveSiteMeterGas__c);
      SICo_MeterReading__c index = lastIndexByMeter.get(cptDeSite.Meter__c);
      listform.add(buildFormulaire(taskFromId, caseFromId, config, cptDeSite, index));    
    }

    // Build Ojbject & finalise JSON
    String requestJSON ='';
    SICO_FormulaireDemandeGRDF_Model.ListeDemandeGRDF FluxRequest = new SICO_FormulaireDemandeGRDF_Model.ListeDemandeGRDF ();
    FluxRequest.listeFormulaire = listform;
    requestJSON = JSON.serialize(FluxRequest);
    return requestJSON;
  }

  private static  SICO_FormulaireDemandeGRDF_Model.DemandeGRDF buildFormulaire(Task tache, Case rcase, Grdf_Config__c config,Sico_SiteMeter__c cptDeSite, SICo_MeterReading__c index){

    Map<String,String> codeDecode = new Map<String,String>();
    codeDecode.put('Monsieur','Mr');
    codeDecode.put('Madame','Mme');

    SICO_FormulaireDemandeGRDF_Model.DemandeGRDF form= new SICO_FormulaireDemandeGRDF_Model.DemandeGRDF();
    form.SF_ID_Task=tache.Id;
    form.DNEntreprise=config.DNEntreprise__c;
    form.numeroCAD=config.NumeroCAD__c;
    form.Demande = new  SICO_FormulaireDemandeGRDF_Model.Demande();
    form.NumeroVersion=config.NumeroVersion__c;
    form.Utilisateur= new SICO_FormulaireDemandeGRDF_Model.Utilisateur();
    form.Utilisateur.Login=config.login__c;

    // Mapping de la demande
    form.Demande.TypeDemande = tache.InterventionType__c;
    form.Demande.DonneesFournisseur = new SICO_FormulaireDemandeGRDF_Model.DonneesFournisseur();
    form.Demande.PremierRdvDemande = new SICO_FormulaireDemandeGRDF_Model.PremierRdvDemande();
    form.Demande.DonneesTypeDemande = new SICO_FormulaireDemandeGRDF_Model.DonneesTypeDemande();
    form.Demande.NumeroPCE =rcase.site__r.PCE__c ;
    form.Demande.CodePostal=rcase.Account.ContactAddressZipCode__c;
    form.Demande.Commentaire= '';
    form.Demande.ContactIntervention = new SICO_FormulaireDemandeGRDF_Model.ContactIntervention();

    // Mapping Données Fournisseur
    form.Demande.DonneesFournisseur.CommentaireFournisseur ='';
    form.Demande.DonneesFournisseur.TelephoneFournisseur='';
    form.Demande.DonneesFournisseur.GroupeAppartenance='';
    form.Demande.DonneesFournisseur.ReferenceDemande='';

    // Mapping Premier Rdv Demande 
    if(tache.Subscription__r.Opportunity__r.GrdfAppointmentDate__c != null){ 
      form.Demande.PremierRdvDemande.RdvDate = String.valueOf(tache.Subscription__r.Opportunity__r.GrdfAppointmentDate__c);
    }
    form.Demande.PremierRdvDemande.NumeroSynchroIntervention= '0';
    form.Demande.PremierRdvDemande.Plage= tache.PlannedRangedHours__c;
     
    //Donnée type demande
    form.Demande.DonneesTypeDemande.IndexClient ='';
    form.Demande.DonneesTypeDemande.Contrat= new SICO_FormulaireDemandeGRDF_Model.Contrat();
    form.Demande.DonneesTypeDemande.CAR = String.valueOf(rcase.Site__r.EstimatedGasConsumption__c);
    form.Demande.DonneesTypeDemande.DateIndexClient='';
    form.Demande.DonneesTypeDemande.CJA =new SICO_FormulaireDemandeGRDF_Model.CJA() ;
    form.Demande.DonneesTypeDemande.MIG='';
    form.Demande.DonneesTypeDemande.CodeProfil='';
    form.Demande.DonneesTypeDemande.OffreHistorique='N';
    form.Demande.DonneesTypeDemande.FrequenceReleve = cptDeSite.Meter__r.ReadingFrequency__c;
    form.Demande.DonneesTypeDemande.Tarif=cptDeSite.GasType__c ;
    form.Demande.DonneesTypeDemande.OptionPrestation=''; // Recupérer de la réponse de l'appel au WS verifierRecevabilite
    form.Demande.DonneesTypeDemande.IndexReleve = new  SICO_FormulaireDemandeGRDF_Model.IndexReleve();
  
    // Mapping contrat
    form.Demande.DonneesTypeDemande.Contrat.ClientFinal = new  SICO_FormulaireDemandeGRDF_Model.ClientFinal();
    form.Demande.DonneesTypeDemande.Contrat.TypeUsageLocal='R';
    form.Demande.DonneesTypeDemande.Contrat.UtilisateurFinal = new SICO_FormulaireDemandeGRDF_Model.UtilisateurFinal();
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier= new SICO_FormulaireDemandeGRDF_Model.ContactCourrier();
    
    // Mapping CJA
    form.Demande.DonneesTypeDemande.CJA.CJAA='';
    form.Demande.DonneesTypeDemande.CJA.CJAAJ = new SICO_FormulaireDemandeGRDF_Model.CJAAJ();
    form.Demande.DonneesTypeDemande.CJA.CJAAM = new SICO_FormulaireDemandeGRDF_Model.CJAAM();
    form.Demande.DonneesTypeDemande.CJA.CJAAJ.Valeur='';
    form.Demande.DonneesTypeDemande.CJA.CJAAJ.CJAAJDate='';
    form.Demande.DonneesTypeDemande.CJA.CJAAM.Valeur='';
    form.Demande.DonneesTypeDemande.CJA.CJAAM.CJAAMDate='';

    //Client Final
    form.Demande.DonneesTypeDemande.Contrat.ClientFinal.Civilite=codeDecode.get(rcase.Account.Salutation);
    form.Demande.DonneesTypeDemande.Contrat.ClientFinal.Complement=rcase.Account.LastName;
    form.Demande.DonneesTypeDemande.Contrat.UtilisateurFinal.Prenom=rcase.Account.FirstName;
    form.Demande.DonneesTypeDemande.Contrat.UtilisateurFinal.Nom=rcase.Account.LastName;
    form.Demande.DonneesTypeDemande.Contrat.UtilisateurFinal.RaisonSociale=rcase.Account.LastName;
    form.Demande.DonneesTypeDemande.Contrat.UtilisateurFinal.CodeNAF='';
    form.Demande.DonneesTypeDemande.Contrat.UtilisateurFinal.Civilite=codeDecode.get(rcase.Account.Salutation);
    form.Demande.DonneesTypeDemande.Contrat.UtilisateurFinal.CodeSIRET='';

    //ContactCourrier
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier.Commune=rcase.Account.ContactAddressCity__c;
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier.CodePostal=rcase.Account.ContactAddressZipCode__c;
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier.eMail=rcase.Account.PersonEmail;
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier.Nom=rcase.Account.LastName;
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier.Rue=rcase.Account.ContactAddressNumberStreet__c;
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier.Pays=rcase.Account.ContactAddressCountry__c;
    form.Demande.DonneesTypeDemande.Contrat.ContactCourrier.ComplementAdresse=rcase.Account.ContactAddressAdditionToAddress__c;
   
    // // TICGN
    form.Demande.DonneesTypeDemande.TICGN = new SICO_FormulaireDemandeGRDF_Model.TICGN();
    //form.Demande.DonneesTypeDemande.TICGN.ValeurTICGN='';
    form.Demande.DonneesTypeDemande.TICGN.TICGN='0'; //Boolean.valueOf(tache.TICGN__c)?'1':'0';
    
    //Mapping IndexReleve
    if (index != null) {
      form.Demande.DonneesTypeDemande.IndexReleve.ValeurIndexReleve = String.valueOf(index.MeterReading__c);
      form.Demande.DonneesTypeDemande.IndexReleve.IndexReleveDate = String.valueof(index.ReadingDate__c);  
    }
  
    form.Demande.contactIntervention.Nom=rcase.Account.LastName;
    if(!String.isBlank(rcase.Account.Phone)){
      form.Demande.contactIntervention.Telephone=rcase.Account.Phone.removeStart('+');
    }else if(!String.isBlank(rcase.Account.PersonMobilePhone)){
      form.Demande.contactIntervention.Telephone=rcase.Account.PersonMobilePhone.removeStart('+');
    }

    system.debug('Formulaire de DemandeGRDF :'+ form);
    return form;
  }
}