/**
* @author Boris Castellani
* @date 26/10/2016
*
* @description TEST MaintenancyQuote SICo_MaintenancyQuoteBatch_CLS
*/

@isTest
private class SICo_MaintenancyQuoteBatch_TEST {

    @testSetup
    private static void setup(){

        GenericSetting__c maintenancyQuote = new GenericSetting__c(
            Name = 'Devis',
            Days__c = 60
        );

        insert new DevisCaduque__c(
            Name = 'Devis',
            nbrDay__c = 60
        );
        Insert maintenancyQuote;

     	//DataSet
		Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Cham TEST','Agency');
		agency.AgencyNumber__c = 'ANUMBER';
		Insert agency;

    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;

    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    	site.CHAMAgencyEntretien__c = agency.Id;
    	Insert site;

    	List<MaintenancyQuote__c> quotes = new List<MaintenancyQuote__c>();
    	quotes.add(SICo_UtilityTestDataSet.createMaintenancyQuote(site.Id, acc.Id));
    	quotes.add(SICo_UtilityTestDataSet.createMaintenancyQuote(site.Id, acc.Id));
    	quotes.add(SICo_UtilityTestDataSet.createMaintenancyQuote(site.Id, acc.Id));

    	for(MaintenancyQuote__c quote : quotes){
    		quote.MaintenancyQuoteStatus__c = 'A valider';
	
    	}
    	Insert quotes;
    }

    static testMethod void maintenancyQuoteBatch() {

    	List<MaintenancyQuote__c> quotes = [SELECT Id FROM MaintenancyQuote__c];
		Datetime caduque = Datetime.now().addDays(-65);

		// TEST & ASSERT
		Test.setCreatedDate(quotes.get(0).Id, caduque);   	
		Test.startTest();
    		Database.executeBatch(new SICo_MaintenancyQuoteBatch_CLS());
		Test.stopTest();

		List<MaintenancyQuote__c> quotesCaduque = [	SELECT Id, MaintenancyQuoteStatus__c FROM MaintenancyQuote__c
													where MaintenancyQuoteStatus__c = 'Caduque'];
		System.assertEquals(1,quotesCaduque.size());
    }

}