/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test Class & Trigger SICo_ProvisioningActivities
History
13/07/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
private class SICo_ProvisioningActivities_TEST {

	@testSetup
	private static void setup(){
		insert SICo_UtilityTestDataSet.createChamConfig();
		insert SICo_UtilityTestDataSet.createCustomSetting();
		insert SICo_UtilityTestDataSet.createConfig_Boomi();
		insert SICo_UtilityTestDataSet.create_GrDF_Config();
	}

	//TODO : REVOIR GRDF avec DERVAL, Le try catch ajouter masque l'erreur GRDF de la methode de test donc je les commenter (boris).

	// @isTest static void provisioningAll() {

	// 	// create custom setting 
	// 	insert SICo_UtilityTestDataSet.create_Grdf_Config();
 //        insert SICo_UtilityTestDataSet.createCustomSetting();
 //        Test.setMock(HttpCalloutMock.class, new SICo_FluxSubscription_MOCK());
	// 		// Create Account, Site, Opportunity, Tasks
	// 		Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
	// 		insert account;
	// 		SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
	// 		insert site;
	// 		Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('opp001',account.Id,Site.Id);
	// 		insert opportunity;
	// 		Case myCase = SICo_UtilityTestDataSet.createCase('Gaz',account.Id,opportunity.Id,site.Id);
	// 		insert myCase;

	// 		List<Task> listTask = new List<Task>();
	// 		listTask.add(SICo_UtilityTestDataSet.createTask('GA0001','sujet GA0001',myCase.Id,'TaskProvisioning',true));
	// 		listTask.add(SICo_UtilityTestDataSet.createTask('GA0003','sujet GA0003',myCase.Id,'TaskProvisioning',true));
	// 		listTask.add(SICo_UtilityTestDataSet.createTask('SE0003','sujet SE0003',myCase.Id,'TaskProvisioning',true));
	// 		listTask.add(SICo_UtilityTestDataSet.createTask('SE0004','sujet SE0004',myCase.Id,'TaskProvisioning',true));
	// 		for (Task t : listTask) {
	// 			t.FluxStatus__c = 'Traitement non démarré';
	// 		}

	// 		insert listTask;

	// 	Test.startTest();
	// 	try{
	// 		SICo_ProvisioningActivities_CLS.launchProvisioningActivitiesSync(listTask);
	// 	} catch(Exception ex){
	// 	}
	// 	Test.stopTest();

	// }
	// 

    /*---------------------------------------------------------------------
    * @description SICo_ProvisioningActivities_CLS Subscription ASYNC-Part
    */
    @isTest static void provisioningSubscription() {

    	Test.setMock(HttpCalloutMock.class, new SICo_FluxSubscription_MOCK());


			// Create Account, Site, Opportunity
    	Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
    	insert account;
    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    	insert site;
    	Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('opp001',account.Id,Site.Id);
    	insert opportunity;

			// Create Meter & SiteMeter
    	SICo_Meter__c meter = SICo_UtilityTestDataSet.createMeter();
    	insert meter;
    	SICo_SiteMeter__c siteMeter = SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE,site.id,meter.id);
    	insert siteMeter;

    	Case caseObjet = SICo_UtilityTestDataSet.createCase('Objet',account.Id,opportunity.Id,site.Id);
    	Case caseGaz = SICo_UtilityTestDataSet.createCase('Gaz',account.Id,opportunity.Id,site.Id);
    	insert new List<Case>{caseObjet,caseGaz};

    	List<Task> listTask = new List<Task>();
    	listTask.add(SICo_UtilityTestDataSet.createTask('GA0003','sujet GA0003',caseGaz.Id,'TaskProvisioning',true));
    	listTask.add(SICo_UtilityTestDataSet.createTask('SE0003','sujet SE0003',caseObjet.Id,'TaskProvisioning',true));
    	listTask.add(SICo_UtilityTestDataSet.createTask('SE0004','sujet SE0004',caseObjet.Id,'TaskProvisioning',true));
    	listTask.add(SICo_UtilityTestDataSet.createTask('SE0005','sujet SE0005',caseObjet.Id,'TaskProvisioning',true));
    	listTask.add(SICo_UtilityTestDataSet.createTask('TH0002','sujet TH0002',caseObjet.Id,'TaskProvisioning',true));

    	Test.startTest();
    	SICo_ProvisioningActivities_CLS.launchProvisioningActivitiesAsync(listTask);
			//insert listTask;

    	Test.stopTest();

    }


    /*---------------------------------------------------------------------
    * @description SICo_ProvisioningActivities_CLS CHAM ASYNC-Part
    */
    @isTest static void provisioningCHAM() {

			// Create Account, Site, Opportunity
			Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Agence Detest','Agency');
			agency.AgencyNumber__c = 'ANUMBER';
			insert agency;
    	Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
    	insert account;
    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    	insert site;
    	Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('opp001',account.Id,Site.Id);
    	opportunity.CHAMAgencyEntretien__c = agency.id;
    	opportunity.CHAMAgencyInstallation__c = agency.id;
    	insert opportunity;

			// Create Meter & SiteMeter
    	SICo_Meter__c meter = SICo_UtilityTestDataSet.createMeter();
    	insert meter;
    	SICo_SiteMeter__c siteMeter = SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE,site.id,meter.id);
    	insert siteMeter;

    	Case caseObjet = SICo_UtilityTestDataSet.createCase('Objet',account.Id,opportunity.Id,site.Id);
    	Case caseMaintenance = SICo_UtilityTestDataSet.createCase('Maintenance',account.Id,opportunity.Id,site.Id);
    	insert new List<Case>{caseObjet,caseMaintenance};

    	List<SObject> listSObject = new List<SObject>();
    	listSObject.add(SICo_UtilityTestDataSet.createTask('IN0001','sujet IN0001',caseObjet.Id,'TaskProvisioning',true));
    	//listSObject.add(SICo_UtilityTestDataSet.createEvent('IN0002','sujet IN0002',caseObjet.Id,'EventProvisioning',true));
    	// listSObject.add(SICo_UtilityTestDataSet.createTask('MA0001','sujet MA0001',caseMaintenance.Id,'TaskProvisioning',true));
    	// listSObject.add(SICo_UtilityTestDataSet.createEvent('MA0002','sujet MA0002',caseMaintenance.Id,'EventProvisioning',true));

    	Test.setMock(HttpCalloutMock.class, new SICo_GlobalRespond_MOCK());
    	Test.startTest();
    	SICo_ProvisioningActivities_CLS.launchProvisioningActivitiesAsync(listSObject);

    	Test.stopTest();

    }

	    //   Creation de demande GRDF
    @isTest static void creerDemandeGRDF() {        




			// Create Account, Site, Opportunity
    	Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
    	insert account;
    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    	insert site;
    	Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('opp001',account.Id,Site.Id);
    	insert opportunity;

			// Create Meter & SiteMeter
    	SICo_Meter__c meter = SICo_UtilityTestDataSet.createMeter();
    	insert meter;
    	SICo_SiteMeter__c siteMeter = SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE,site.id,meter.id);
    	insert siteMeter;

    	Case caseObjet = SICo_UtilityTestDataSet.createCase('Objet',account.Id,opportunity.Id,site.Id);
    	Case caseMaintenance = SICo_UtilityTestDataSet.createCase('Maintenance',account.Id,opportunity.Id,site.Id);
    	insert new List<Case>{caseObjet,caseMaintenance};

    	List<SObject> listSObject = new List<SObject>();
    	listSObject.add(SICo_UtilityTestDataSet.createTask('GA0001','sujet IN0001',caseObjet.Id,'TaskProvisioning',true));

    	Test.startTest();
    	Test.setMock(HttpCalloutMock.class, new SICo_FluxSubscription_MOCK());
    	SICo_ProvisioningActivities_CLS.launchProvisioningActivitiesAsync(listSObject);
    	Test.stopTest();

    }

  }