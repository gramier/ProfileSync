/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test class for the SICo_CancelContract_VFC Controller
History
30/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
@isTest
public class SICo_WebCallback_VFC_TEST {

	//set test data
	@testSetup
	static void testData() {

		User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
	    insert communityUser;
	}

	@isTest
	static void testCasesWithoutURLId() {

		User us = [SELECT Id, accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User
	    system.runAs(us){
	        Test.startTest();
	            SICo_WebCallback_VFC controller = new SICo_WebCallback_VFC();
	            PageReference pRef = Page.WebCallback;
            	pRef.getParameters().put('accountId', us.accountId);
		        Test.setCurrentPageReference(pRef);

		        controller.createCase();

		        controller.getObjects();

		        controller.getMotifs();
            
            	controller.updateObjects();

            	controller.doredirect();

		        controller.cancelWebCallback();

		        List<Case> listCases = [SELECT Id, Description FROM Case WHERE AccountId = :us.accountId];
		        System.assertNotEquals(listCases.size(), null);
            	
	        Test.stopTest();
        }
	}
    
    @IsTest
    static void testCasesWithURLId() {
        
        Test.startTest();
            SICo_WebCallback_VFC controller = new SICo_WebCallback_VFC();
            PageReference pRef = Page.WebCallback;
            Test.setCurrentPageReference(pRef);
            
            System.assertNotEquals(controller.listMotif.size(), 0);
        
        Test.stopTest();
}

}