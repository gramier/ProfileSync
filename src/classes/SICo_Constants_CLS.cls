/*------------------------------------------------------------
Author:        Dario Correnti
Company:       Salesforce.com
Description:   Constants for SICo
Test Class:
History
27/07/2016      Dario Correnti     Create version
------------------------------------------------------------*/
public with sharing class SICo_Constants_CLS {

  /* --- SICO PARAMS --- */
  public static final String CUSTOMER_DEMAND_CASE_RECORD_TYPE = 'CustomerDemand'; //Record Type Name for CustomerDemand of Case object
  public static final String WEB_CALLBACK_CASE_RECORD_TYPE = 'WebCallBack'; //Record Type Name for WebCallBack of Case object
  public static final String ORIGIN_PICKLIST_VALUE_WEB = 'Web'; //Picklist value for Origin of Case object
  public static final String STATUS_PICKLIST_VALUE_NEW = 'New'; //Picklist value for Statut of Case object
  public static final String THEME_PICKLIST_VALUE_WEBCALLBACK = 'Web Call Back'; //Picklist value for THEME of Case object

  /* --- SICO CODE OFFER NAME --- */
  public static final String OFFER_GAZ = 'GAZ';
  public static final String OFFER_MAINT = 'MAINT';
  public static final String OFFER_INST = 'INST'; 
  public static final String OFFER_SE = 'SE'; //Offer with station only
  public static final String OFFER_TH = 'TH'; //Offer with thermostat
  public static final String OFFER_SE_TH = 'SE_TH'; //Offer with station and thermostat
  public static final String OFFER_SE_TH_MULTIPL = 'SE;TH'; //Offer with station and thermostat

  /* --- CONVERSION CONSTANTE --- */
  public static final String STR_DATA_FRANCE = 'France';
  public static final String STR_DATA_CUSTOMER = 'Customer';
  public static final String STR_DATA_WON = 'Closed Won';
  public static final String STR_DATA_ASSET = 'Asset_';
  public static final String STR_DATA_ACHETE = 'Acheté';
  public static final Integer INT_DATA_QTS = 1;
  public static final String STR_PCE = 'PCE';
  public static final String STR_PDL = 'PDL';
  public static final String STR_SITEMETER_PCE = 'SICo_SiteMeter__c_PCE';
  public static final String STR_SITEMETER_PDL = 'SICo_SiteMeter__c_PDL';
  public static final String STR_METER_PCE = 'SICo_Meter__c_PCE';
  public static final String STR_METER_PDL = 'SICo_Meter__c_PDL';
  public static final String STR_METERREAD_PCE = 'SICo_MeterReading__c_PCE';
  public static final String STR_METERREAD_PDL = 'SICo_MeterReading__c_PDL';
  public static final String STR_DATA_EUR = 'EUR';
  public static final String STR_DATA_NEW = 'New';
  public static final String STR_DATA_EMAIL = 'Email';
  public static final String STR_DATA_ACCOUNT = 'Account';
  public static final String STR_DATA_READING = 'Auto-reading';

  public static final Set<String> SET_STR_ZCUSTOMFIELD = 
    new Set<String>{
      'OfferName__c','SiteId__c','PCE__c','PDL__c','Name','LeadSource__c','InvoicingType__c','MarketingOfferName__c',
      'Renonciation_Retractation_Maintenance__c','Renonciation_Retractation_Gaz__c'
    };

  /* --- TASK --- */   
  public static final String STR_MENSUEL = 'Mensuel';
  public static final String STR_ANNUEL = 'Annuel';
  public static final String STR_SANSPIECE = 'Sans pièces';
  public static final String STR_AVECPIECE = 'Avec pièces';
  public static final String SICO = 'SICO';
  public static final String STR_DEFAULT = 'DEFAULT';
  public static final String STR_STATUS_CLOSE = 'Fermée';
  public static final String STR_STATUS_OPEN = 'Ouverte';
  public static final Integer INT_STATUSCODE = 200;
  public static final Integer INT_LIMITCALL = 20;
  public static final String STR_CALLTYPE = 'POST';
  public static final String STR_CHAMTOKEN_ISS = 'oauth.c43.fr';
  public static final String STR_CHAMTOKEN_EXP = '24400320';
  public static final String STR_CHAMTOKEN_SUB = '24855091';
  public static final String STR_CHAMTOKEN_AUD = 'CHAM';
  public static final String STR_PROVISION_SUB = 'Souscription';
  public static final String STR_PROVISION_GRDF = 'GrDF';
  public static final String STR_PROVISION_CHAM = 'Cham';
  public static final String STR_PROVISION_CALLOUT = 'ProvisioningCallOut__c';
  public static final String STR_PROVISION_TYPE = 'ProvisioningType__c';
  public static final String STR_PROVISION_TASKID = 'TaskId__c';
  public static final String STR_MAINT = 'Maintenance';
  public static final String STR_INSTALL = 'Installation';
  public static final String STR_DATA_PERSONACC = 'Account_PersonAccount';
  public static final String STR_CASE_SYSTEM_RT = 'System';

  /* --- CONSTANTE CLASS: FLUX  ENDPOINT --- */
  public static final Map<String, String[]> STR_ENDPOINT =
    new Map<String, String[] > {
      'GAZ'=> new String[]{'GAZ','POST','connected-services/edelia/v1/gasContractUpdate/'},
      'ELEC'=> new String[]{'ELEC','POST','connected-services/edelia/v1/elecContractUpdate/'},
      'PROFILE'=> new String[]{'PROFILE','POST','connected-services/edelia-iot/v1/profil/'}
    };

  /* --- START COMMUNITY PARAMS --- */
  public static final String CUSTOMER_COMMUNITY_TIMEZONE = 'Europe/Paris'; //Name of the profile for Customer Community users
  public static final String ACTIVE_PAYMENT_METHOD_STATUS = 'Active'; //Picklist value in Zuora__PaymentMethod__c.Zuora__PaymentMethodStatus__c for Active Payment Methods
  public static final String CUSTOMER_COMMUNITY_PROFILE_NAME = 'SICo Community - Customer User'; //Name of the profile for Customer Community users
  public static final String CUSTOMER_COMMUNITY_GUEST_SUFFIX = '.guest'; //Guest users (self-registered community usernames have this suffix added at the end of their email address)
  public static final String DEFAULT_EMAIL_ENCODING = 'UTF-8'; //Email encoding for new users
  public static final String CUSTOMER_COMMUNITY_START_URL = '/s/'; //StartUrl for the public community
  public static final String CUSTOMER_COMMUNITY_ALT_START_URL = '/sos/s/'; //Alternative StartUrl for the public community
  public static final String CUSTOMER_LIVECHATPOPUP_URL = Page.LiveChatPopUp.getUrl(); // url for the live chat Page
  public static final String CUSTOMER_COMMUNITITESLOGIN_URL = Page.CommunitiesLogin.getUrl(); // url for the login Page.
  public static final String CUSTOMER_SEND_MESSAGE_URL = Page.SendMessage.getUrl(); // url for the SendMessage Page
  public static final String CUSTOMER_NO_SITE_URL = Page.NoSite.getUrl(); //url for the NoSite page
  public static final String CUSTOMER_SPACE_START_URL = Page.Dashboard.getUrl(); //StartUrl for the internal customer sapce
  public static final String CLIENT_REGISTRATION_URL = ''; //FIXME: IKUMBI-109 //URL for creating a new Client account
  public static final String USER_TYPE_SALESFORCE = 'Standard'; //Value associated to Salesforce "full" licences when executing UserInfo.getUserType()
  public static final String USER_TYPE_GUEST = 'Guest'; //Value associated to a not logged in user on the community
  public static final String URL_PARAM_ACCOUNTID = 'AccountId'; //URL parameter for passing the accountId within the "Espace Client"
  public static final String URL_PARAM_CALLBACK = 'callback'; // URL parameter for check the checkbox or not in the SendMessage form
  public static final String URL_PARAM_BANKINGACCOUNTID = 'bankingAccountId'; //URL parameter for passing the bankingAccountId within the "Espace Client"
  public static final String URL_PARAM_QUOTEID = 'quoteId'; //URL parameter for passing the bankingAccountId within the "Espace Client"
  public static final String URL_PARAM_ZUORAOLDPAYMENTMETHODID = 'zopmi'; //URL parameter for passing the old zuora payment method id within the "Espace Client"
  public static final String URL_PARAM_ZUORANEWPAYMENTMETHODID = 'paramRefId';//URL parameter for passing the new zuora payment method id within the "Espace Client"
  public static final String GUEST_PERSON_ACCOUNT_RECORD_TYPE = 'PersonAccountGuest'; //Record Type Name for Guest Users in the Community
  public static final String ZUORA_PAYMENT_METHOD_BANK_TRANSFERT = 'BankTransfer'; //Picklist value for payment methods to be displayed in the "Informations Personnelles"
  public static final String ZUORA_PAYMENT_METHOD_BANK_TRANSFERT_WHITESPACE = 'Bank Transfer'; //Picklist value for payment methods to be displayed in the "Informations Personnelles"
  public static final String URL_PARAM_CONTRACT_TYPE = 'ConType'; //URL parameter for passing the contract type within the "Espace Client"
  public static final String CANCEL_SUBJECT_DEVNAME = 'Resiliation'; //CustomMetadataType for cancellation cases subject__c ("Case Motif")
  public static final String GAZ_CONSO_CATEGORY = 'GAZ-CSO'; //zqu__ProductRatePlanCharge__c.Category__c for "consommation" charges
  public static final String GAZ_ABO_CATEGORY = 'GAZ-ABO'; //zqu__ProductRatePlanCharge__c.Category__c for "abonnement" charges
  public static final String SESSION_CACHE_KEY_PAYMENTMETHODUPDATE = 'local.SICoCustomerSpace.IsPaymentMethodUpdated'; //Session Cache Key set to "true" when a customer creates the first payment method

  public static final String SITE_PARAM_SSTERRASSE = 'SousTerrasse'; //Picklist value on SICo_Site__c.HousingLocationType__c
  public static final String SITE_PARAM_INTERMEDIAIRE = 'Intermediaire'; //Picklist value on SICo_Site__c.HousingLocationType__c
  public static final String SITE_PARAM_RDC = 'RDC'; //Picklist value on SICo_Site__c.HousingLocationType__c
  public static final String SITE_PARAM_PAC = 'SplitAvecPAC'; //Picklist value on SICo_Site__c.PrincHeatingSystemTypeDetail__c
  public static final String SITE_PARAM_ELEC = 'Electricite'; //Picklist value on SICo_Site__c.PrincipalHeatingSystemType__c
  public static final String SITE_PARAM_ELEC2 = 'Electrique'; //Picklist value on SICo_Site__c.SanitaryHotWaterType__c
  public static final String SITE_PARAM_VILLE = 'Ville'; //Picklist value on SICo_Site__c.GasSource__c
  public static final String SITE_PARAM_BOUT = 'Bouteille'; //Picklist value on SICo_Site__c.GasSource__c
  public static final String SITE_PARAM_HPHC = 'HP_HC'; //Picklist value on SICo_Site__c.ElectricityTariffOption__c
  public static final String WITH_PARTS = 'Avec pièces'; //Picklist value on Opportunity.MaintenanceParts__c
  public static final String WITHOUT_PARTS = 'Sans pièces'; //Picklist value on Opportunity.MaintenanceParts__c
  public static final String CR_VEN_CODE = 'VISITE D\'ENTRETIEN'; //Value of SICo_CR__c.FirstReturnCode__c for Annual Maintenance

  public static final String GUEST_COMMUNITY_PROFILE_NAME = 'SICo Community - Guest User';

  // WS PATH
  public static final String BOOMI = 'BOOMI';
  public static final String BOOMI_SERVICE_OK_RESPONSE_STATUS = 'OK';
  public static final String BOOMI_SERVICE_WARNING_RESPONSE_STATUS = 'WARNING';
  public static final String BOOMI_SERVICE_KO_RESPONSE_STATUS = 'KO';

  //Task type Dashborad Activities
  public static final String TASK_ACTIVITY_TYPE_GAZ = 'GAZ';
  public static final String TASK_ACTIVITY_TYPE_EQUIPMENT = 'EQUIPMENT';
  public static final String TASK_ACTIVITY_TYPE_MAINTENANCE = 'MAINTENANCE';

  public static final Map<Integer, String> MONTHES = new Map<Integer, String> {   
    1 => Label.SICo_January, 2 => Label.SICo_February, 3 => Label.SICo_March,
    4 => Label.SICo_April, 5 => Label.SICo_May, 6 => Label.SICo_June,
    7 => Label.SICo_July, 8 => Label.SICo_August, 9 => Label.SICo_September,
    10 => Label.SICo_October, 11 => Label.SICo_November, 12 => Label.SICo_December
  };

  /* --- SICO CHAM JETON --- */
  public static final Map<Integer,String> DAYS = new Map<Integer,String>{  
    0 => 'MondayTimeRange1__c', 1 => 'MondayTimeRange2__c', 2 => 'MondayTimeRange3__c', 3 => 'MondayTimeRange4__c',
    4 => 'TuesdayTimeRange1__c', 5 => 'TuesdayTimeRange2__c', 6 => 'TuesdayTimeRange3__c', 7 => 'TuesdayTimeRange4__c',
    8 => 'WednesdayTimeRange1__c', 9 => 'WednesdayTimeRange2__c', 10 => 'WednesdayTimeRange3__c', 11 => 'WednesdayTimeRange4__c',
    12 => 'ThursdayTimeRange1__c', 13 => 'ThursdayTimeRange2__c', 14 => 'ThursdayTimeRange3__c', 15 => 'ThursdayTimeRange4__c',
    16 => 'FridayTimeRange1__c', 17 => 'FridayTimeRange2__c', 18 => 'FridayTimeRange3__c', 19 => 'FridayTimeRange4__c',
    20 => 'SaturdayTimeRange1__c', 21 => 'SaturdayTimeRange2__c', 22 => 'SaturdayTimeRange3__c', 23 => 'SaturdayTimeRange4__c'
  };

  public static final Map<Integer,Map<String,String>> CRENEAUX = new Map<Integer,Map<String,String>> {  
    0 => new Map<String,String> {
      '08:00 - 10:00' => 'MondayTimeRange1__c', '10:00 - 12:00' => 'MondayTimeRange2__c',
      '13:30 - 15:30' => 'MondayTimeRange3__c', '15:30 - 17:30' => 'MondayTimeRange4__c'
    },
    1 => new Map<String,String>{
      '08:00 - 10:00' => 'TuesdayTimeRange1__c', '10:00 - 12:00' => 'TuesdayTimeRange2__c',
      '13:30 - 15:30' => 'TuesdayTimeRange3__c', '15:30 - 17:30' => 'TuesdayTimeRange4__c'
    },
    2 => new Map<String,String>{
      '08:00 - 10:00' => 'WednesdayTimeRange1__c', '10:00 - 12:00' => 'WednesdayTimeRange2__c',
      '13:30 - 15:30' => 'WednesdayTimeRange3__c', '15:30 - 17:30' => 'WednesdayTimeRange4__c'
    },
    3 => new Map<String,String>{
      '08:00 - 10:00' => 'ThursdayTimeRange1__c', '10:00 - 12:00' => 'ThursdayTimeRange2__c',
      '13:30 - 15:30' => 'ThursdayTimeRange3__c', '15:30 - 17:30' => 'ThursdayTimeRange4__c'
    },
    4 => new Map<String,String>{
      '08:00 - 10:00' => 'FridayTimeRange1__c', '10:00 - 12:00' => 'FridayTimeRange2__c',
      '13:30 - 15:30' => 'FridayTimeRange3__c', '15:30 - 17:30' => 'FridayTimeRange4__c'
    },
    5 => new Map<String,String>{
      '08:00 - 10:00' => 'SaturdayTimeRange1__c', '10:00 - 12:00' => 'SaturdayTimeRange2__c',
      '13:30 - 15:30' => 'SaturdayTimeRange3__c', '15:30 - 17:30' => 'SaturdayTimeRange4__c'
    }
  };

  public static final Map<Integer, String> FRENCHDAYS = new Map<Integer, String> {   
    0 => 'Lundi', 1 => 'Mardi', 2 => 'Mercredi', 3 => 'Jeudi', 4 => 'Vendredi', 5 => 'Samedi', 6 => 'Dimanche'
  };
}