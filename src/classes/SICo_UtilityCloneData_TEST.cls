@isTest
private class SICo_UtilityCloneData_TEST {

  @testSetup
  static void initializeData(){

    SICo_Bundle__c sicoBundle = new SICo_Bundle__c(
      StartDate__c = System.today()
    );
    insert sicoBundle;

    zqu__ZProduct__c product = new zqu__ZProduct__c();
    insert product;

    zqu__ProductRatePlan__c prodcutRP = new zqu__ProductRatePlan__c(
      zqu__ZProduct__c = product.Id
    );
    insert prodcutRP;

    SICo_BundleRP__c sicoBundleRP = new SICo_BundleRP__c(
      Bundle__c = sicoBundle.Id,
      ProductRatePlan__c = prodcutRP.Id
    );
    insert sicoBundleRP;

  }

  @isTest
    static void testcloneBundle() {
        test.startTest();
            SICo_UtilityCloneData_CLS.cloneBundle([SELECT Id FROM SICo_Bundle__c].Id);
        test.stopTest();
    }

}