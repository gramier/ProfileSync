/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Test Class of SICo_SignedJson_RWS
Test Class:    SICo_SignedJson_RWS_TEST
History
22/07/2016      Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@isTest
public with sharing class SICo_SignedJson_RWS_TEST {

    static testMethod void testGet() {

        // Create account
        Account account = SICo_UtilityTestDataSet.createAccount( 'Jean','Dupond','PersonAccount' );
        insert account;
        // Create User related Account
        Contact oContact = [ SELECT Id FROM Contact WHERE AccountId = : account.Id ];
        User oUser = new User();
        oUser.Firstname = 'Jean';
        oUser.Lastname = 'Dupond';
        oUser.Email = 'jean.dupond@gmail.com';
        oUser.Username = 'jean.dupond@gmail.com';
        oUser.ProfileId = [ SELECT Id FROM Profile WHERE Name = : 'SICo Community - Customer User'LIMIT 1 ].Id;
        oUser.ContactId = oContact.Id;
        oUser.Alias = 'jdupond';
        oUser.TimeZoneSidKey = 'Europe/Paris';
        oUser.LocaleSidKey = 'fr_FR';
        oUser.EmailEncodingKey = 'ISO-8859-1';
        oUser.LanguageLocaleKey = 'fr';
        insert oUser;
        // Create Site
        SICo_Site__c site = SICo_UtilityTestDataSet.createSite( account.Id );
        insert site;

        System.runAs( oUser ) {

	        ConnectedApplication__c csCA = new ConnectedApplication__c();
	        csCA.Name = 'MobileApplication';
	        csCA.ClientId__c = '3MVG9w8uXui2aB_prJmaYkPZ9srZ1ulGb.pLi5NF6ycKZMh9Y4LwvUlD251swDirz.GSTVTryqboRT9ngqhP2';
	        csCA.SessionTimeOut__c = 4;
	        insert csCA;

	        //TEST GET WITHOUT ERROR
	        RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();

	        req.requestURI = '/services/apexrest/SignedJson';  
	        req.addParameter('Application', 'MobileApplication' );
	        req.addParameter('Access_Token', '00D4E0000008dyh!AQ0AQFP8ocMdCU4td8X_MASF.gT8PJsk9SD0McPANe4wNWb66M5sUfY4_adF.YUSY.ir7Pk9dYW3pU8yzJNZ2hHwYREyLexs' );
	        req.addParameter('issued_at', String.valueOf( System.currentTimeMillis() + 60 * 60 * 1000 ) );

	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;

	        String sResults = SICo_SignedJson_RWS.getSignedJson_RWS();

	        //TEST GET WITH ERROR
	        req = new RestRequest(); 
	        res = new RestResponse();

	        req.requestURI = '/services/apexrest/SignedJson';  
	        req.addParameter('Application', 'xxxxx' );

	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;

	        sResults = SICo_SignedJson_RWS.getSignedJson_RWS();

    	}

    }

}