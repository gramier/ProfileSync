/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Controller for the MeterRead page (test for SICo_MeterRead_VFC)
History
08/09/2016     Gaël BURNEAU     Create version
31/10/2016     Selim BEAUJOUR 	Test for two sites with one account (line 35 in class)
------------------------------------------------------------*/
@isTest
private class SICo_MeterRead_VFC_TEST {

	@testSetup static void initTestData() {

		Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
		insert account;
		SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite.IsActive__c = true;
		oSICoSite.PrincipalHeatingSystemType__c = 'Gaz';
		oSICoSite.SanitaryHotWaterType__c = 'Electrique';
		oSICoSite.NoOfOccupants__c = 4;
		oSICoSite.surfaceInSqMeter__c = 80;
		oSICoSite.InseeCode__c = '44109';
		oSICoSite.ConstructionDate__c = 1980;
		oSICoSite.PCE__c = 'TestPCE';
		insert oSICoSite;
		insert SICo_UtilityTestDataSet.createSiteMeter(oSICoSite.Id);
		oSICoSite.IsActive__c = true;
		update oSICoSite;

		CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
	    insert boomiConfig;

	    //Create another account for testing an account with two sites.
	    Account account2 = SICo_UtilityTestDataSet.createAccount('Test02', 'Test02', 'PersonAccount');
	    insert account2;

	    SICo_Site__c oSICoSite2 = SICo_UtilityTestDataSet.createSite(account2.Id);
	    oSICoSite2.IsActive__c = true;
		oSICoSite2.PrincipalHeatingSystemType__c = 'Gaz';
		oSICoSite2.SanitaryHotWaterType__c = 'Electrique';
		oSICoSite2.NoOfOccupants__c = 4;
		oSICoSite2.surfaceInSqMeter__c = 80;
		oSICoSite2.InseeCode__c = '44109';
		oSICoSite2.ConstructionDate__c = 1980;
		oSICoSite2.PCE__c = 'TestPCE';
		insert oSICoSite2;
		SICo_Site__c oSICoSite3 = SICo_UtilityTestDataSet.createSite(account2.Id);
	    oSICoSite3.IsActive__c = true;
		oSICoSite3.PrincipalHeatingSystemType__c = 'Gaz';
		oSICoSite3.SanitaryHotWaterType__c = 'Electrique';
		oSICoSite3.NoOfOccupants__c = 4;
		oSICoSite3.surfaceInSqMeter__c = 80;
		oSICoSite3.InseeCode__c = '44109';
		oSICoSite3.ConstructionDate__c = 1980;
		oSICoSite3.PCE__c = 'TestPCE';
		insert oSICoSite3;


	}

	@isTest static void constructor_test() {

		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		PageReference pref = Page.MeterRead;
	    pref.getParameters().put(SICo_Constants_CLS.URL_PARAM_ACCOUNTID, account.Id);
	    Test.setCurrentPage(pref);

	    SICo_MeterRead_VFC controller = new SICo_MeterRead_VFC();

		System.assertEquals(account.Id, controller.accountId);
	}

	@isTest static void getMyHome_test() {

		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		SICo_Site__c oSICoSiteRetrieved = SICo_MeterRead_VFC.getMyHome(account.Id);
		System.assertNotEquals(null, oSICoSiteRetrieved.Id);
		//Account with 2 sites
		Account account2 = [SELECT Id FROM Account WHERE FirstName= 'Test02' LIMIT 1];
		SICo_Site__c oSICoSiteRetrieved2 = SICo_MeterRead_VFC.getMyHome(account2.Id);

	}	
	@isTest static void callout_test() {

		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		// Cas OK
		SICo_MeterRead_VFC.StoreMeterReadResponse oStoreMeterReadResponse = new SICo_MeterRead_VFC.StoreMeterReadResponse('OK', '', '');
		Test.setMock(HttpCalloutMock.class, new SICo_FluxStoreMeterRead_MOCK(200, 'OK', JSON.serialize(oStoreMeterReadResponse), new Map<String, String>()));
		Test.startTest();
		String sResponse = SICo_MeterRead_VFC.updatePCEIndex(account.Id, 1000, false);
		System.assertEquals(SICo_Constants_CLS.BOOMI_SERVICE_OK_RESPONSE_STATUS, sResponse);

		// Cas Warning
		oStoreMeterReadResponse = new SICo_MeterRead_VFC.StoreMeterReadResponse('WARNING', 'Warning', 'Warning');
		Test.setMock(HttpCalloutMock.class, new SICo_FluxStoreMeterRead_MOCK(200, 'OK', JSON.serialize(oStoreMeterReadResponse), new Map<String, String>()));
		sResponse = SICo_MeterRead_VFC.updatePCEIndex(account.Id, 1000, false);
		System.assertEquals('Warning', sResponse);

		// Cas KO
		oStoreMeterReadResponse = new SICo_MeterRead_VFC.StoreMeterReadResponse('KO', 'Error', 'Error');
		Test.setMock(HttpCalloutMock.class, new SICo_FluxStoreMeterRead_MOCK(200, 'OK', JSON.serialize(oStoreMeterReadResponse), new Map<String, String>()));
		sResponse = SICo_MeterRead_VFC.updatePCEIndex(account.Id, 1000, false);
		System.assertEquals(SICo_Constants_CLS.BOOMI_SERVICE_KO_RESPONSE_STATUS, sResponse);

		// Cas Status KO
		oStoreMeterReadResponse = new SICo_MeterRead_VFC.StoreMeterReadResponse('KO', '', '');
		Test.setMock(HttpCalloutMock.class, new SICo_FluxStoreMeterRead_MOCK(400, 'KO', JSON.serialize(oStoreMeterReadResponse), new Map<String, String>()));
		sResponse = SICo_MeterRead_VFC.updatePCEIndex(account.Id, 1000, false);
		System.assertEquals(SICo_Constants_CLS.BOOMI_SERVICE_KO_RESPONSE_STATUS, sResponse);
	}

}