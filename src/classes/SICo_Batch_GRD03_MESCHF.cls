/**
* @author Sebastien Colladon
* @date 22/11/2016
*
* @description Use SICo batch - SICO-1509
*/
global class SICo_Batch_GRD03_MESCHF implements Database.Batchable<sObject> {
    public String query;

    global SICo_Batch_GRD03_MESCHF() {
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([ SELECT Id, Status, InterventionType__c, WhatId FROM Task
                                          WHERE Status = 'A traiter CRC'
                                          AND (InterventionType__c = 'MES' OR InterventionType__c = 'CHF') ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        Set<Id> idCases = new Set<Id>();
        for(Task tk : (List<Task>)scope){
            if(tk.WhatId != null && tk.WhatId.getSObjectType() == Schema.Case.getSObjectType()){
                idCases.add(tk.WhatId);
            }
        }

        String CaseType = String.valueOf(Schema.Case.getSobjectType());
        ID QueueID = [SELECT QueueId FROM QueueSobject WHERE SobjectType = :CaseType AND Queue.DeveloperName = :Label.SICO_CASE_QUEUE_GENERALE LIMIT 1][0].QueueId; // TODO match the right name
        List<Case> cases = new List<Case>();
        for(ID caseId : idCases) {
          cases.add(
            new Case(
              id = caseId,
              ownerid = QueueID
            )
          );
        }

        if(!cases.isEmpty()){
            update cases;
        }      
    }

    global void finish(Database.BatchableContext BC) {
    }
}