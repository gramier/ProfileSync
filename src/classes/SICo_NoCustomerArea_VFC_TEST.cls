/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test class for the NoCustomerArea Controller
History
16/09/2016     Mehdi BEGGAS      Create version
20/09/2016     Gaël BURNEAU      Modifications
------------------------------------------------------------*/
@isTest
public class SICo_NoCustomerArea_VFC_TEST {

	//set test data
	@testSetup
	static void testData() {
		//Insert new community user
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
		insert communityUser;
    }

	@isTest
	static void customer_test() {

		User us = [SELECT AccountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User
	    System.runAs(us) {

            PageReference pRef = Page.NoCustomerArea;
	        Test.setCurrentPageReference(pRef);

            SICo_NoCustomerArea_VFC controller = new SICo_NoCustomerArea_VFC();

			Test.startTest();

			PageReference pRefRedirect = controller.doRedirect();
			String firtName = controller.firstName;

	        Test.stopTest();

	        String pRefExpected =SICo_Constants_CLS.CUSTOMER_SPACE_START_URL;//Page.Dashboard;
			System.assert(pRefRedirect.getUrl().startsWith(pRefExpected));
        }
	}

    @isTest
	static void guest_test() {

		User us = [SELECT Id, Username FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];
		us.Username += SICo_Constants_CLS.CUSTOMER_COMMUNITY_GUEST_SUFFIX;//'.guest';
		update us;

        //Run as Guest Community User
	    System.runAs(us) {

            PageReference pRef = Page.NoCustomerArea;
	        Test.setCurrentPageReference(pRef);

            SICo_NoCustomerArea_VFC controller = new SICo_NoCustomerArea_VFC();

			Test.startTest();

			PageReference pRefRedirect = controller.doRedirect();
			String firtName = controller.firstName;

	        Test.stopTest();

	        System.assertEquals(null, pRefRedirect);
        }
	}

}