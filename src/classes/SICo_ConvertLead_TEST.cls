/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test SICo_ConvertLead_CLS & SICo_ConvertLead_TRIGGER
History
19/07/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
private class SICo_ConvertLead_TEST {

	static testMethod void convertLead() {
		
		insert SICo_UtilityTestDataSet.createZuoraWSConfig();
		
		// Create Account Provider & Agency
		ID rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_ServiceProvider');
		rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
		Account prestaAccount = new Account(Name='CHAM',RecordTypeId=rtAccount);
		insert prestaAccount;
		Account agencyAccount = new Account(Name='agence1',Parent=prestaAccount,RecordTypeId=rtAccount);
		insert agencyAccount;

		// Create Product2
		Product2 product = SICo_UtilityTestDataSet.createSalesforceProduct('1');
		insert  product;
		zqu__ZProduct__c productZuora = SICo_UtilityTestDataSet.createZuoraProduct('1',product.id);
		insert productZuora;
		zqu__ProductRatePlan__c productPlan = SICo_UtilityTestDataSet.createZuoraProductRatePlan('1',product.id,productZuora.id, 'SE_GAZ');
		insert productPlan;
		zqu__ProductRatePlanCharge__c productPlanCharge = SICo_UtilityTestDataSet.createZuoraProductRatePlanCharge('1',productPlan.id);
		insert productPlanCharge;

		// Create Lead - Create Basket - Ayant Droit
		Lead lead= SICo_UtilityTestDataSet.createLead(agencyAccount.Id, 'Colissimo');
		//lead.PDL__c='123456798';
		insert lead;
		SICo_Basket__c sbasket= SICo_UtilityTestDataSet.createPanier(lead.id,'2c92c0f855a0a9d60155b65c8eb55e41','Colissimo','2c92c0f855a0a9d60155b65c8ec65e45','Frais de livraison');
		sbasket.ObjectType__c = 'Station';
		insert sbasket;
		SICo_LeadBeneficiary__c slead = SICo_UtilityTestDataSet.createLeadBeneficiary(lead.id,'Anaelle','Boomi','Mme');
		insert slead;

		// Update Lead (true = Flag convert)
		lead.ToConvert__c=true;
		update lead;

		system.debug('lead.ContactAddressFlat__c '+lead.ContactAddressFlat__c);
		// Assert
		List<Account> personalAccount = [SELECT id,RecordType.Name FROM account where ContactAddressFlat__c= :lead.ContactAddressFlat__c];
		//System.assertEquals(1,personalAccount.size());
	}
	
	/*
	static testMethod void convertLeadWS() {

		// Create Account Provider & Agency
		ID rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_ServiceProvider');
		rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
		Account prestaAccount = new Account(Name='CHAM',RecordTypeId=rtAccount);
		insert prestaAccount;
		Account agencyAccount = new Account(Name='agence1',Parent=prestaAccount,RecordTypeId=rtAccount);
		insert agencyAccount;

		// Create Product2
		Product2 product = SICo_UtilityTestDataSet.createSalesforceProduct('1');
		insert  product;
		zqu__ZProduct__c productZuora = SICo_UtilityTestDataSet.createZuoraProduct('1',product.id);
		insert productZuora;
		zqu__ProductRatePlan__c productPlan = SICo_UtilityTestDataSet.createZuoraProductRatePlan('1',product.id,productZuora.id, 'SE_GAZ');
		insert productPlan;
		zqu__ProductRatePlanCharge__c productPlanCharge = SICo_UtilityTestDataSet.createZuoraProductRatePlanCharge('1',productPlan.id);
		insert productPlanCharge;

		// Create Lead - Create Basket - Ayant Droit
		Lead lead= SICo_UtilityTestDataSet.createLead(agencyAccount.Id, 'Colissimo');
		//lead.PDL__c='123456798';
		insert lead;
		SICo_Basket__c sbasket= SICo_UtilityTestDataSet.createPanier(lead.id,'2c92c0f855a0a9d60155b65c8eb55e41','Colissimo','2c92c0f855a0a9d60155b65c8ec65e45','Frais de livraison');
		sbasket.ObjectType__c = 'Station';
		insert sbasket;
		SICo_LeadBeneficiary__c slead = SICo_UtilityTestDataSet.createLeadBeneficiary(lead.id,'Anaelle','Boomi','Mme');
		insert slead;
		
		system.debug('### lead: ' + lead);
		
		SICo_ConvertLead_RWS.ConvertLeadResponse resp = SICo_ConvertLead_RWS.doPost(lead.Id);

		system.debug('### resp: ' + resp);
		
		// Assert
		List<Account> personalAccount = [SELECT id,RecordType.Name FROM account where ContactAddressFlat__c= :lead.ContactAddressFlat__c];
		System.assertEquals(1,personalAccount.size());
	}
	*/

//END
}