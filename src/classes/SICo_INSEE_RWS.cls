/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Web Service REST in order to provide INSEE Information (all or from postal code) - call by Mobile & Xively Platform
Test Class:    SICo_INSEE_RWS_TEST
History
23/07/2016     Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@RestResource(urlMapping='/INSEE')
global with sharing class SICo_INSEE_RWS {

    @HttpGet
    global static List<SICo_INSEE__c> getInfoINSEE_RWS() {

        List<SICo_INSEE__c> oInseeList = new List<SICo_INSEE__c>();
        try {

            //REST SERVICES PARAMETERS
            String sPostalCode = RestContext.request.params.get( 'postal_code' );
            
            //GET CITY LIST FROM POSTAL CODE
            Integer i = Integer.valueOf( sPostalCode );
            oInseeList = [ SELECT Id, INSEECode__c, City__c, DeliveryLabel__c FROM SICo_INSEE__c 
            	Where Id IN (SELECT CodeInsee__c FROM SICo_InseeCPJunction__c WHERE ZipCode__r.ZipCode__c = :sPostalCode) ];

        } catch ( Exception e ) {
            SICo_LogManagement.sendErrorLog(
                'SICo',
                Label.SICO_LEAD_LEAD_Eligibility,
                String.valueOf( e )
            );

        }

        return oInseeList;

    }

}