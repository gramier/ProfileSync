/**********************************************************************
 * Created Gerald Ramier
 *
 * @url: /services/apexrest/rest
 * @data:
 *  {
        "partner":"partnername"
    }
*************************************************************************/
@RestResource(urlMapping='/getJWT')
global with sharing class SICo_SignedJWT_RWS {
    
    @HttpGet
    global static String doGet() {
    	String partnerName = RestContext.request.params.get('partnerName');

    	System.debug(RestContext.request.params);


        return SICo_JsonSignature_CLS.getStandardJWT(partnerName, false);
    }
}