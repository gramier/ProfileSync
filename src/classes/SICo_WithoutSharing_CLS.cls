/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Class used to bypass restrictions due permissions
Test Class:    SICo_PairObject_RWS_TEST && SICo_WithoutSharing_TEST
History
27/07/2016     Olivier Vrbanac     Create & develope first version
11/08/2016     Mehdi Cherfaoui     Add reactivateUser() method, used by SICo_CommunitiesLogin_VFC class
23/08/2016     Olivier Vrbanac     Replace delete oPain by delete all pairing records related ObjectId__c
------------------------------------------------------------*/
//DELETE PAIRING RECORDS RELATED OBJECT ID JUST PAIRED
public without sharing class SICo_WithoutSharing_CLS {
    
    public static boolean deletePairingRequest( SICo_Pairing__c oPair ) {

        Boolean result = false;

        try {

            String sDeviceId = oPair.ObjectId__c;
            List<SICo_Pairing__c> oPairingList = [ SELECT Id, ObjectId__c FROM SICo_Pairing__c WHERE ObjectId__c = : sDeviceId ];

            //delete oPair;
            delete oPairingList; 
            result = true;

        } catch ( Exception e ) {

            SICo_LogManagement.sendErrorLog(
                'SICo',
                'SICo_WithoutSharing_CLS.deletePairingRequest ( SICo_Pairing__c oPair )',
                String.valueOf( e )
            );
            result = false;

        }

        return result;

    }

    /**
     * [Method that tries to re-activate an inactive user - Used in SICo_CommunitiesLogin_VFC, for Guest users]
     * @param inactiveUser [User to be reactivated]
     */
    public static void reactivateUser( User inactiveUser ){
        try
        {
            inactiveUser.IsActive = true;
            database.update(inactiveUser);
        }catch (exception ex){
            //Log error if unable to activate the user
            String errorDescription = 'Unable to re-activate user: '+inactiveUser.username+' STACK_TRACE: '+ex.getStackTraceString()+' EXCEPTION: '+ex;
            if(!Test.isRunningTest()) SICo_LogManagement.sendErrorLogAsync('SFDC','DML',errorDescription);
        }//END try-catch
    }//END reactivateUser()
	
	 /**
     * [Method that retrive all the eligibility for zipCode]
     * @param zipCodes [Set<String> of zip codes] 
     */
	public static Map<String, String> getZoneByZipCode( Set<String> zipCodes ){
		Map<String, String> zoneByZipCode = new Map<String, String>();
		System.debug('### getZoneByZipCode - zipCodes: ' + zipCodes);
	    try {
	        if (!zipCodes.isEmpty()) {
				List<SICo_Eligibility__c> zoneTariffaires = [SELECT Id,INSEECode__c,PostalCode__c,RateZone__c FROM SICo_Eligibility__c where PostalCode__c = :zipCodes];   
			    if (!zoneTariffaires.isEmpty()) {
			        for (SICo_Eligibility__c zt : zoneTariffaires) {
			            zoneByZipCode.put(zt.PostalCode__c, zt.RateZone__c);
			        }
			    }
			}
	    }catch (exception ex) {
	        if(!Test.isRunningTest()) SICo_LogManagement.sendErrorLogAsync('SFDC','DML',ex.getMessage());
	    }
		
		System.debug('### getZoneByZipCode - zoneByZipCode: ' + zoneByZipCode);
	    return zoneByZipCode;
	}

   /**
    *  [Method that create Case if maintenancy Quotes is accepted by the Customer]
    *  @param newCases [List<cases>]
    *  @return cases [List<cases>]
    */
    public static Map<Id,Case> insertCases(Map<Id,Case> newCases){
      insert newCases.values();
      return newCases;
    }

   /**
    *  [Method that create Case if maintenancy Quotes is accepted by the Customer]
    *  @param newCases [List<newTasks>]
    */
    public static void insertTasks(List<Task> newTasks){
      insert newTasks;
    }
}