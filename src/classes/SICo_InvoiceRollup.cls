//Database.executeBatch(new Sico_InvoiceRollup());
global class SICo_InvoiceRollup implements Database.Batchable<sObject> {
    public String query;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        Set<Id> invoiceIds = new Set<Id>();
        for(SiCo_InvoiceItem__c it : [SELECT InvoiceId__c FROM SiCo_InvoiceItem__c WHERE lastModifiedDate > YESTERDAY]){
            invoiceIds.add(it.InvoiceId__c);
        }

        return Database.getQueryLocator([SELECT Id,TotalInvoiceAmount__c,TotalInstallationAmountHT__c,TotalInstallationAmountTTC__c,
                                                TotalAmount10__c,TotalAmount55__c,TotalAmount20__c,TotalAmountABOHT__c,
                                                TotalAmountABOTTC__c,TotalAmountCTAHT__c,TotalAmountCTATTC__c,GazTotalAmount__c,
                                                GasTotalAmountTTC__c,TotalAmountEquipmentHT__c,TotalEquipementAmountTTC__c,
                                                TotalAmountCHAM__c,TotalMaterialAmount__c,TotalAmountLateFeesTTC__c,
                                                TotalAmountPenaltiesLateFees__c,TotalAmountPSPGrDFTTC__c,TotalAmountMainServicesTTC__c,
                                                TotalAmountDiscountRebates__c,TotalAmountDicountsTTC__c,TotalAmountSEHT__c,
                                                TotalAmountSETTC__c,TotalAmountSPP__c,TotalAmoutCHAMSPP__c,TvaAmount10__c,
                                                TvaAmount55__c,TvaAmount20__c,TotalAmountMaintenanceHT__c,TotalAmountMaintenanceTTC__c 
                                         FROM Zuora__ZInvoice__c 
                                         WHERE id in :invoiceIds]
                                        );
    }

    global void execute(Database.BatchableContext BC, list<Zuora__ZInvoice__c> scope) {
        Map<Id,Zuora__ZInvoice__c> invoices = new Map<Id,Zuora__ZInvoice__c>(scope);
        Map<Id,List<SiCo_InvoiceItem__c>> invoiceItemsPerInvoice = new Map<Id,List<SiCo_InvoiceItem__c>>();
        Set<Id> invoiceItemsId = new Set<Id>();
        Map<Id,List<Usage__c>> usagesPerInvoiceItem = new Map<Id,List<Usage__c>>();

        for(SiCo_InvoiceItem__c anInvoiceItem : [SELECT Id, InvoiceId__c, TotalAmountTTC__c, TotalAmount__c, TaxAmount__c, Category__c, SICO_TauxTVAWF__c FROM SiCo_InvoiceItem__c WHERE InvoiceId__c in :invoices.keySet()]){
            if(!invoiceItemsPerInvoice.containsKey(anInvoiceItem.InvoiceId__c)){
                invoiceItemsPerInvoice.put(anInvoiceItem.InvoiceId__c, new List<SiCo_InvoiceItem__c>());
            }
            invoiceItemsPerInvoice.get(anInvoiceItem.InvoiceId__c).add(anInvoiceItem);
            invoiceItemsId.add(anInvoiceItem.Id);
        }

        for(Usage__c anUsage : [SELECT Id, InvoiceItem__c FROM Usage__c WHERE InvoiceItem__c in :invoiceItemsId]){
            if(!usagesPerInvoiceItem.containsKey(anUsage.InvoiceItem__c)){
                usagesPerInvoiceItem.put(anUsage.InvoiceItem__c, new List<Usage__c>());
            }
            usagesPerInvoiceItem.get(anUsage.InvoiceItem__c).add(anUsage);
        }

        for(Zuora__ZInvoice__c anInvoice : invoices.values()){
            List<SiCo_InvoiceItem__c> relatedInvoiceItems = invoiceItemsPerInvoice.get(anInvoice.id);
            List<Usage__c> relatedUsages = usagesPerInvoiceItem.get(anInvoice.id);
            if(relatedInvoiceItems != null){
                Set<String> categories = new Set<String>();
                anInvoice.TotalInvoiceAmount__c = 0.0;
                anInvoice.TotalInstallationAmountHT__c = 0.0;
                anInvoice.TotalInstallationAmountTTC__c = 0.0;
                anInvoice.TotalAmount10__c = 0.0;
                anInvoice.TotalAmount10__c = 0.0;
                anInvoice.TotalAmount20__c = 0.0;
                anInvoice.TotalAmountABOHT__c = 0.0;
                anInvoice.TotalAmountABOTTC__c = 0.0;
                anInvoice.TotalAmountCTAHT__c = 0.0;
                anInvoice.TotalAmountCTATTC__c = 0.0;
                anInvoice.GazTotalAmount__c = 0.0;
                anInvoice.GasTotalAmountTTC__c = 0.0;
                anInvoice.TotalAmountEquipmentHT__c = 0.0;
                anInvoice.TotalEquipementAmountTTC__c = 0.0;
                anInvoice.TotalAmountCHAM__c = 0.0;
                anInvoice.TotalMaterialAmount__c = 0.0;
                anInvoice.TotalAmountLateFeesTTC__c = 0.0;
                anInvoice.TotalAmountPenaltiesLateFees__c = 0.0;
                anInvoice.TotalAmountPSPGrDFTTC__c = 0.0;
                anInvoice.TotalAmountMainServicesTTC__c = 0.0;
                anInvoice.TotalAmountDiscountRebates__c = 0.0;
                anInvoice.TotalAmountDicountsTTC__c = 0.0;
                anInvoice.TotalAmountSEHT__c = 0.0;
                anInvoice.TotalAmountSETTC__c = 0.0;
                anInvoice.TotalAmountSPP__c = 0.0;
                anInvoice.TotalAmoutCHAMSPP__c = 0.0;
                anInvoice.TvaAmount10__c = 0.0;
                anInvoice.TvaAmount55__c = 0.0;
                anInvoice.TvaAmount20__c = 0.0;
                anInvoice.TotalAmount10__c = 0.0;
                anInvoice.TotalAmount55__c = 0.0;
                anInvoice.TotalAmount20__c = 0.0;
                anInvoice.TotalAmountMaintenanceHT__c = 0.0;
                anInvoice.TotalAmountMaintenanceTTC__c = 0.0;
                for(SiCo_InvoiceItem__c anInvoiceItem : relatedInvoiceItems) {
                	try {
	                    categories.add(anInvoiceItem.Category__c);
	                    if(anInvoiceItem.TotalAmount__c != null){
	                        system.debug(anInvoiceItem);
	                        anInvoice.TotalInvoiceAmount__c += anInvoiceItem.TotalAmountTTC__c;
	
	                        if(anInvoiceItem.Category__c.startsWith('GAZ')){
	                            anInvoice.GazTotalAmount__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.GasTotalAmountTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c.startsWith('MAINT-MEN-POS')
	                        || anInvoiceItem.Category__c.startsWith('MAINT-ANN')){
	                            anInvoice.TotalAmountMaintenanceHT__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountMaintenanceTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        }
	
	                        if(anInvoiceItem.Category__c.contains('MATER-SE') && anInvoiceItem.Category__c != 'MATER-SE-PROMO'){
	                            anInvoice.TotalAmountSEHT__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountSETTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        }
	                        if(anInvoiceItem.Category__c.contains('MATER') && anInvoiceItem.Category__c != 'MATER-SE-PROMO'){
	                            anInvoice.TotalAmountEquipmentHT__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalEquipementAmountTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        }
	
	                        if(anInvoiceItem.Category__c.contains('MAINT-MEN')){
	                            anInvoice.TotalAmountCHAM__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c.contains('INST')){
	                            anInvoice.TotalInstallationAmountHT__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalInstallationAmountTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c.contains('PROMO')){
	                            anInvoice.TotalAmountDiscountRebates__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountDicountsTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        } 
	
	                        if(anInvoiceItem.Category__c == 'GAZ-ABO'){
	                            anInvoice.TotalAmountABOHT__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountABOTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c == 'GAZ-CTA'){
	                            anInvoice.TotalAmountCTAHT__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountCTATTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c == 'MATER-SE-MEN'){
	                            anInvoice.TotalMaterialAmount__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c == 'PENALITE-INTERET'){
	                            anInvoice.TotalAmountPenaltiesLateFees__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountLateFeesTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c == 'PSP-GrDF'){
	                            anInvoice.TotalAmountSPP__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountPSPGrDFTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        } else if(anInvoiceItem.Category__c == 'MAINT-PSP'){
	                            anInvoice.TotalAmoutCHAMSPP__c += anInvoiceItem.TotalAmount__c;
	                            anInvoice.TotalAmountMainServicesTTC__c += anInvoiceItem.TotalAmountTTC__c;
	                        }
	
	                        if(anInvoiceItem.SICO_TauxTVAWF__c == 10.0){
	                            anInvoice.TvaAmount10__c += anInvoiceItem.TaxAmount__c;
	                            anInvoice.TotalAmount10__c += anInvoiceItem.TotalAmount__c;
	
	                        } else if(anInvoiceItem.SICO_TauxTVAWF__c == 5.5){
	                            anInvoice.TvaAmount55__c += anInvoiceItem.TaxAmount__c;
	                            anInvoice.TotalAmount55__c += anInvoiceItem.TotalAmount__c;
	
	                        } else if(anInvoiceItem.SICO_TauxTVAWF__c == 20.0){
	                            anInvoice.TvaAmount20__c += anInvoiceItem.TaxAmount__c;
	                            anInvoice.TotalAmount20__c += anInvoiceItem.TotalAmount__c;
	                        }
	                    }
                	} catch (Exception e) {
                		System.debug ('### an error occurred on SICo_InvoiceRollup: ' + e.getMessage());
                	}
                }
                anInvoice.techrunconga__c = true;
                if(!categories.isEmpty()){
                    List<String> categoriesList = new List<String>(categories);
                    aninvoice.Category__c = String.join(categoriesList,';');
                }
                // Roll Up on Invoice Item
            }

            if(relatedUsages != null){
                // Roll Up on Usage
            }
        }

        database.update(invoices.values(), false);
    }
    

    global void finish(Database.BatchableContext BC) {

    }
}