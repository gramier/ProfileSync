@isTest
private class SICo_UsageTriggerHandler_CLS_TEST {
  @testSetup
  static void setup(){
    SICo_CR__c aLF = new SICo_CR__c();
    insert aLF;
    insert new SICo_ExtraBilledLign__c(CR_LigneFacture__c=aLF.id);
  }

  @isTest
  static void testLink() {
      SICo_ExtraBilledLign__c aBillRun = [SELECT Id FROM SICo_ExtraBilledLign__c LIMIT 1];

      Zuora__ZInvoice__c aI = new Zuora__ZInvoice__c();
      insert aI;
      SICo_InvoiceItem__c aII = new SICo_InvoiceItem__c(InvoiceId__c=aI.id);
      insert aII;

      Usage__c aUsage = new Usage__c(ExtraBilledLine__c = aBillRun.id, InvoiceItem__c = aII.id);
      insert aUsage;

      System.assertEquals(aUsage.Id, [SELECT Usage__c FROM SICo_ExtraBilledLign__c LIMIT 1][0].Usage__c);
  }
}