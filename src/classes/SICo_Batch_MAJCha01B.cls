global class SICo_Batch_MAJCha01B implements Database.Batchable<sObject>, Database.AllowsCallouts {
    public String query;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('**** HELLO1');
        this.query = 'SELECT id, LastModifiedDate, SiteId__c, SiteId__r.LastModifiedDate, Zuora__Account__c, Zuora__Account__r.LastModifiedDate '+
                     'FROM Zuora__Subscription__c '+
                     'WHERE (LastModifiedDate = TODAY AND isAmendment__c = true) '+
                     'OR (SiteId__r.LastModifiedDate = TODAY AND SiteId__r.CreatedDate != TODAY) '+
                     'OR (Zuora__Account__r.LastModifiedDate = TODAY AND Zuora__Account__r.CreatedDate != TODAY)';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Zuora__Subscription__c> subscriptions) {


        Set<Id> subscriptionsID = new Set<Id>();
        Set<Id> sitesID = new Set<Id>();
        Set<Id> accountsID = new Set<Id>();

        for(Zuora__Subscription__c sub : subscriptions){
            subscriptionsID.add(sub.Id);
            sitesID.add(sub.SiteId__c);
            accountsID.add(sub.Zuora__Account__c);
        }

        if(!subscriptionsID.isEmpty() && !sitesID.isEmpty() && !accountsID.isEmpty()){


            List<Case> cases = [    SELECT Id, Site__c, AccountId, Subscription__c, 
                                    (SELECT Id, WhatId, TaskId__c, InterventionType__c FROM Tasks where TaskId__c = 'IN0001' OR TaskId__c = 'MA0001') 
                                    FROM Case 
                                    WHERE Site__c IN :sitesID AND AccountId IN :accountsID AND Subscription__c IN :subscriptionsID
                                    AND (ProvisioningType__c = 'Maintenance' OR ProvisioningType__c = :System.Label.SICo_ProvisionningObjet)
                                ];

            // GET TASKS IN CASE
            Map<String,Task> tasks = new Map<String,Task>();
            if(!cases.isEmpty()){
                for(Case caseNow : cases){
                    String key = String.valueOf(caseNow.Site__c) + String.valueOf(caseNow.AccountId) + String.valueOf(caseNow.Subscription__c);
                    for(Task taskNow : caseNow.Tasks){
                        if(taskNow.TaskId__c == 'MA0001'){
                            tasks.put(key, taskNow);     
                        } else if(!tasks.containsKey(key) || tasks.get(key).TaskId__c !='MA0001'){
                            tasks.put(key, taskNow);     
                        }
                    }
                }

                if(!tasks.isEmpty()){
                    try {

                        //BUILD TASK LIST FOR JSON
                        Map<Id,Task> theTasks = new Map<Id,Task>();
                        for(Task theTask : tasks.values()){
                            theTasks.put(theTask.Id, theTask);
                        }

                        // BUILD JSON FOR SEND IN CHAM  
                        String messageJSON = SICo_FluxCham_CLS.builCHamClientContrat(theTasks);
                        system.debug('***getDMLStatements '+limits.getDMLStatements());
                        system.debug('***getDMLRows '+limits.getDMLRows());
                        system.debug('***getEmailInvocations '+limits.getEmailInvocations());
                        system.debug('***getFutureCalls '+limits.getFutureCalls());
                        if(messageJSON !=''){
                            SICo_BoomiEndpoint__mdt endpointCHAM = [SELECT EndPoint__c FROM SICo_BoomiEndpoint__mdt where DeveloperName = 'ChamClientUpdate' Limit 1];
                            if(endpointCHAM != null){
                                system.debug('***getDMLStatements '+limits.getDMLStatements());
                                system.debug('***getDMLRows '+limits.getDMLRows());
                                system.debug('***getEmailInvocations '+limits.getEmailInvocations());
                                system.debug('***getFutureCalls '+limits.getFutureCalls());
                                SICo_FluxCham_CLS.executeFluxCham('POST',endpointCHAM.EndPoint__c,messageJSON);
                            }
                        }
                    }catch(Exception e) {
                        system.debug('**** ERROR '+ e);
                        SICo_LogManagement.addErrorLog(SICo_Constants_CLS.SICO, 'SICo_ProvisioningActivities_CLS - fluxChamClient', e.getMessage());
                    }
                }
            }
        }        
    }

    global void finish(Database.BatchableContext BC) {
    }
}