/**
* @author Boris Castellani
* @date 26/10/2016
*
* @description TEST Shedule job MaintenancyQuote : SICo_MaintenancyQuoteShedule_CLS
*/

@isTest
private class SICo_MaintenancyQuoteShedule_TEST {

    static testMethod void maintenancyQuoteShedule() {

        GenericSetting__c maintenancyQuote = new GenericSetting__c(
            Name = 'Devis',
            Days__c = 60
        );
        insert new DevisCaduque__c(
            Name = 'Devis',
            nbrDay__c = 60
        );
        Insert maintenancyQuote;

     	//DataSet
		Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Cham TEST','Agency');
		agency.AgencyNumber__c = 'ANUMBER';
		Insert agency;

    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;

    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    	site.CHAMAgencyEntretien__c = agency.Id;
    	Insert site;

    	MaintenancyQuote__c quote = SICo_UtilityTestDataSet.createMaintenancyQuote(site.Id, acc.Id);
    	quote.MaintenancyQuoteStatus__c = 'A valider';
    	Insert quote;
    	
		// // TEST & ASSERT
		Datetime caduque = Datetime.now().addDays(-65);
		Test.setCreatedDate(quote.Id, caduque);
		Test.startTest();
	    	// Schedule the test job
			Datetime dt = Datetime.now().addMinutes(1);
			String sch = '0 ' + dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ' + ' ? ' + dt.year();
    		System.schedule('TEST', sch, new SICo_MaintenancyQuoteShedule_CLS());
		Test.stopTest();  


		List<MaintenancyQuote__c> quotesCaduque = [	SELECT Id, MaintenancyQuoteStatus__c FROM MaintenancyQuote__c
													where MaintenancyQuoteStatus__c = 'Caduque'];

		System.assertEquals(0,quotesCaduque.size());  
    }

}