/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Test Class of SICo_CustomerSite_RWS
Test Class:    SICo_CustomerSite_RWS_TEST
History
19/07/2016      Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@isTest
public with sharing class SICo_INSEE_RWS_TEST {
	
    static testMethod void testGet() {
		
		SICo_ZipCode__c zc = new SICo_ZipCode__c(ZipCode__c = '78720');
		insert zc; 
		
    	// Create INSEE References
		SICo_INSEE__c oINSEE1 = new SICo_INSEE__c();
		oINSEE1.INSEECode__c = '12345';
		oINSEE1.City__c = 'DAMPIERRE EN YVELINES';
		oINSEE1.DeliveryLabel__c = 'DAMPIERRE EN YVELINES';
		insert oINSEE1;

		SICo_INSEE__c oINSEE2 = new SICo_INSEE__c();
		oINSEE2.INSEECode__c = '12346';
		oINSEE2.City__c = 'CERNAY LA VILLE';
		oINSEE2.DeliveryLabel__c = 'CERNAY LA VILLE';
		insert oINSEE2;
		
		SICo_InseeCPJunction__c jo1 = new SICo_InseeCPJunction__c(ZipCode__c = zc.Id, CodeInsee__c = oINSEE1.Id);
		SICo_InseeCPJunction__c jo2 = new SICo_InseeCPJunction__c(ZipCode__c = zc.Id, CodeInsee__c = oINSEE2.Id);
		insert new List<SICo_InseeCPJunction__c> {jo1, jo2};

        //TEST GET WITHOUT ERROR
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/INSEE';  
        req.addParameter('postal_code', '78720');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        List<SICo_INSEE__c> oINSEEList = SICo_INSEE_RWS.getInfoINSEE_RWS();

        //TEST GET WITH ERROR
        req = new RestRequest(); 
        res = new RestResponse();

        req.requestURI = '/services/apexrest/INSEE';  
        req.addParameter('postal_code', 'ABCDE');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        oINSEEList = SICo_INSEE_RWS.getInfoINSEE_RWS();

	}

}