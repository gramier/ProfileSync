/**
* @author Boris Castellani
* @date 01/02/2017
* @LastModify 
*
* @description 
*/
@isTest
private class SICo_ChamJeton_RWS_TEST {
    static testMethod void doPost() {
		final Integer nbrDay = Integer.valueOf(label.SICo_weeklyCalendars_Number)*Integer.valueOf(label.SICo_weeklyCalendars_Days);
		Id rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
		Account prestaAccount = new Account(Name='CHAM', RecordTypeId=rtAccount);
		prestaAccount.MondayTimeRange1__c = 4; prestaAccount.MondayTimeRange2__c = 4;
		prestaAccount.MondayTimeRange3__c = 4; prestaAccount.MondayTimeRange4__c = 4;
		prestaAccount.TuesdayTimeRange1__c = 4; prestaAccount.TuesdayTimeRange2__c = 4;
		prestaAccount.TuesdayTimeRange3__c = 4; prestaAccount.TuesdayTimeRange4__c = 4;
		prestaAccount.WednesdayTimeRange1__c = 4; prestaAccount.WednesdayTimeRange2__c = 4;
		prestaAccount.WednesdayTimeRange3__c = 4; prestaAccount.WednesdayTimeRange4__c = 4;
		prestaAccount.ThursdayTimeRange1__c = 4; prestaAccount.ThursdayTimeRange2__c = 4;
		prestaAccount.ThursdayTimeRange3__c = 4; prestaAccount.ThursdayTimeRange4__c = 4;		 
		prestaAccount.FridayTimeRange1__c = 4; prestaAccount.FridayTimeRange2__c = 4;
		prestaAccount.FridayTimeRange3__c = 4; prestaAccount.FridayTimeRange4__c = 4;			 
		prestaAccount.SaturdayTimeRange1__c = 4; prestaAccount.SaturdayTimeRange2__c = 4;
		prestaAccount.SaturdayTimeRange3__c = 4; prestaAccount.SaturdayTimeRange4__c = 4;	
		INSERT prestaAccount;

		SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
		chamJeton.initCalendars(new List<Account>{prestaAccount});

		test.startTest();
			SICo_ChamJeton_RWS.ChamJetonResponse aChamJetonResponse = SICo_ChamJeton_RWS.doPost(prestaAccount.Id);
		test.stopTest();

		// Test json data
		//System.assert(jsonRequest != null);         
        
    }
}