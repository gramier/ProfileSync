/**
* @author Sebastien Colladon
* @date 13/10/2016
*
* @description Account Trigger After Update
*/
public with sharing class SICo_AttachmentUtility_CLS {

  private static id docusignIntegrationUser;

  static{
    try{
      docusignIntegrationUser = [Select ID From User where DocuSign_Integration_User__c = true limit 1][0].id;
    } catch (Exception ex){}
  } 

  public static void HandleAttachmentFromDocuSign(List<Attachment> newAttachment){
    Set<id> accountId = new Set<id>();
    for(Attachment att : newAttachment) {
      if(att.CreatedById == docusignIntegrationUser
      && att.ParentId.getSObjectType() == Account.sObjectType) {
        accountid.add(att.ParentId);
      }
    }
    List<Account> accounts = new List<Account>();
    for(Id anId : accountid) {
      accounts.add(new Account(id=anId,SICo_hasSubscriptionAttachment__c = true));
    }
    update accounts;
  }
}