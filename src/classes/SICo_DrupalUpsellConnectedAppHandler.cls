global class SICo_DrupalUpsellConnectedAppHandler extends Auth.ConnectedAppPlugin{
    
    global override Map<String,String> customAttributes(Id userId, Id connectedAppId, Map<String,String> formulaDefinedAttributes, Auth.InvocationContext context){

      try{
        Id accountId = [Select AccountId from user where id = :userId].AccountId;

        Account account = [SELECT Id, 
                              FirstName, 
                              LastName, 
                              Salutation, 
                              Phone, 
                              EmailAddress__c,
                              ContactAddressCity__c, 
                              ContactAddressCountry__c, 
                              ContactAddressFlat__c, 
                              ContactAddressFloor__c, 
                              ContactAddressNumberStreet__c, 
                              ContactAddressStair__c, 
                              ContactAddressZipCode__c, 
                              ContactAdresseBuilding__c,
                              ContactAddressAdditionToAddress__c
                        FROM Account
                        WHERE Id = :accountId];

        List<SICo_Site__c> sites = [SELECT Id,
                                      surfaceInSqMeter__c,
                                      EstimatedGasConsumption__c,
                                      BoilerBrand__c,
                                      BoilerModel__c,
                                      ConstructionDate__c,
                                      CHAMAgencyInstallation__c, 
                                      CHAMAgencyEntretien__c,
                                      EligibleForGas__c,
                                      RenewableHeating__c,
                                      InseeCode__c,
                                      EligibleForThermostat__c,
                                      EligibleForCham__c,
                                      Street__c, 
                                      Stairs__c, 
                                      PostalCode__c, 
                                      Floor__c, 
                                      Flat__c, 
                                      City__c, 
                                      Country__c,
                                      NumberStreet__c,
                                      Building__c,
                                      AdditionToAddress__c,
                                      NoOfOccupants__c,
                                      PrincipalHeatingSystemType__c,
                                      Usage__c,
                                      ResidenceType__c,
                                      HousingType__c,
                                      SICO_Subscribed_offers__c,
                                      IsActive__c
                                FROM SICo_Site__c
                                WHERE Subscriber__c = :accountId];

        SICo_Site__c theSite = new SICo_Site__c();

        if(sites.size() > 1){
          for(SICo_Site__c s : sites) if(s.IsActive__c){
            theSite = s;
            break;
          }
        }
        else if(sites.size() == 1){
          theSite = sites.get(0);
        }

        JSONUserInfo customerInfo = new JSONUserInfo(theSite, account);

        formulaDefinedAttributes.put('user_info_sico', JSON.serialize(customerInfo));

        return formulaDefinedAttributes;

    }
    catch(Exception e){
      formulaDefinedAttributes.put('error', e.getMessage());

      return formulaDefinedAttributes;
    }

  }

  private class JSONUserInfo {
    protected Account account;
    protected SICo_Site__c theSite;
    protected List<Zuora__Subscription__c> subscriptions;

    public JSONUserInfo(final SICo_Site__c theSite, final Account account){
      this.theSite = theSite;
      this.account = account;
    }
  }

}