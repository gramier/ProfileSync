/**
* @author Boris Castellani
* @date 02/02/2017
* @LastModify 
*
* @description 
*/
@isTest
private class SICo_ChamJetonService_TEST {

	static testMethod void buildJson(){
		final Integer nbrDay = Integer.valueOf(label.SICo_weeklyCalendars_Number)*Integer.valueOf(label.SICo_weeklyCalendars_Days);
		Id rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
		Account prestaAccount = new Account(Name='CHAM', RecordTypeId=rtAccount);
		prestaAccount.MondayTimeRange1__c = 4; prestaAccount.MondayTimeRange2__c = 4;
		prestaAccount.MondayTimeRange3__c = 4; prestaAccount.MondayTimeRange4__c = 4;
		prestaAccount.TuesdayTimeRange1__c = 4; prestaAccount.TuesdayTimeRange2__c = 4;
		prestaAccount.TuesdayTimeRange3__c = 4; prestaAccount.TuesdayTimeRange4__c = 4;
		prestaAccount.WednesdayTimeRange1__c = 4; prestaAccount.WednesdayTimeRange2__c = 4;
		prestaAccount.WednesdayTimeRange3__c = 4; prestaAccount.WednesdayTimeRange4__c = 4;
		prestaAccount.ThursdayTimeRange1__c = 4; prestaAccount.ThursdayTimeRange2__c = 4;
		prestaAccount.ThursdayTimeRange3__c = 4; prestaAccount.ThursdayTimeRange4__c = 4;		 
		prestaAccount.FridayTimeRange1__c = 4; prestaAccount.FridayTimeRange2__c = 4;
		prestaAccount.FridayTimeRange3__c = 4; prestaAccount.FridayTimeRange4__c = 4;			 
		prestaAccount.SaturdayTimeRange1__c = 4; prestaAccount.SaturdayTimeRange2__c = 4;
		prestaAccount.SaturdayTimeRange3__c = 4; prestaAccount.SaturdayTimeRange4__c = 4;	
		INSERT prestaAccount;

		SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
		chamJeton.initCalendars(new List<Account>{prestaAccount});

		test.startTest();
			SICo_ChamJetonModel.ChamJeton jsonRequest = chamJeton.buildJsonWeeklyCalendars(prestaAccount.Id);
		test.stopTest();

		// Test json data
		System.assert(jsonRequest != null); 
	}




	static testMethod void initCalendars(){
		Id rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
		Account prestaAccount = new Account(Name='CHAM', RecordTypeId=rtAccount);
		prestaAccount.ThursdayTimeRange2__c = 1; prestaAccount.ThursdayTimeRange3__c = 2;
		prestaAccount.ThursdayTimeRange4__c = 3; prestaAccount.ThursdayTimeRange1__c = 4;
		prestaAccount.MondayTimeRange2__c = 5; prestaAccount.MondayTimeRange3__c = 6;
		prestaAccount.MondayTimeRange4__c = 7; prestaAccount.MondayTimeRange1__c = 8;
		prestaAccount.TuesdayTimeRange2__c = 9; prestaAccount.TuesdayTimeRange3__c =10;
		INSERT prestaAccount;

		test.startTest();
			SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
			chamJeton.initCalendars(new List<Account>{prestaAccount});
		test.stopTest();

		// Test Account data
		List<Account> accounts = chamJeton.getAccounts(new List<Id>{prestaAccount.Id});
		for(Account aAccount : accounts){
			System.assertEquals(prestaAccount.ThursdayTimeRange2__c,aAccount.ThursdayTimeRange2__c);
			System.assertEquals(prestaAccount.MondayTimeRange1__c,aAccount.MondayTimeRange1__c);
			System.assertEquals(prestaAccount.TuesdayTimeRange3__c,aAccount.TuesdayTimeRange3__c);
		}

		// Test Calendar data
		final List<SICO_AvailabilyWeeklyCalendar__c> weeklyCalendars = [SELECT ThursdayTimeRange2__c, MondayTimeRange1__c, TuesdayTimeRange3__c
																																		FROM SICO_AvailabilyWeeklyCalendar__c];

		System.assertEquals(5,weeklyCalendars.size());
		for(SICO_AvailabilyWeeklyCalendar__c aWeeklyCalendars : weeklyCalendars){
			System.assertEquals(aWeeklyCalendars.ThursdayTimeRange2__c,prestaAccount.ThursdayTimeRange2__c);
			System.assertEquals(aWeeklyCalendars.MondayTimeRange1__c,prestaAccount.MondayTimeRange1__c);
			System.assertEquals(aWeeklyCalendars.TuesdayTimeRange3__c,prestaAccount.TuesdayTimeRange3__c);		
		}	
	}

	static testMethod void processCalendar(){
		final Integer nbrDay = Integer.valueOf(label.SICo_weeklyCalendars_Number)*Integer.valueOf(label.SICo_weeklyCalendars_Days);
		Id rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
		Account prestaAccount = new Account(Name='CHAM', RecordTypeId=rtAccount);
		prestaAccount.ThursdayTimeRange2__c = 1; 
		prestaAccount.MondayTimeRange1__c = 2;
		prestaAccount.TuesdayTimeRange3__c = 3;
		INSERT prestaAccount;

		SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
		final List<SICO_AvailabilyWeeklyCalendar__c> weeklyCalendars = chamJeton.buildCalendar(new List<Account>{prestaAccount},Date.today().toStartOfWeek());
		INSERT weeklyCalendars;

		test.startTest();
			chamJeton.processCalendar(new List<Account>{prestaAccount});
		test.stopTest();

		// Test Calendar data
		final List<SICO_AvailabilyWeeklyCalendar__c> weeklyCalendarsTest = [SELECT StartDate__c, Status__c, ThursdayTimeRange2__c, 
																																				MondayTimeRange1__c, TuesdayTimeRange3__c
																																				FROM SICO_AvailabilyWeeklyCalendar__c];

		System.assertEquals(2,weeklyCalendarsTest.size());
		// weeklyCalendars Close
		System.assertEquals(Date.today().toStartOfWeek(),weeklyCalendarsTest[0].StartDate__c);
		System.assertEquals(label.SICo_weeklyCalendars_Close,weeklyCalendarsTest[0].Status__c);
		// weeklyCalendars Open
		System.assertEquals(Date.today().toStartOfWeek().addDays(nbrDay),weeklyCalendarsTest[1].StartDate__c);
		System.assertEquals(label.SICo_weeklyCalendars_Open,weeklyCalendarsTest[1].Status__c);
	}

	static testMethod void decrementCreneau(){
		final Integer nbrDay = Integer.valueOf(label.SICo_weeklyCalendars_Number)*Integer.valueOf(label.SICo_weeklyCalendars_Days);
		Id rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
		Account prestaAccount = new Account(Name='CHAM', RecordTypeId=rtAccount);
		prestaAccount.MondayTimeRange1__c = 4; prestaAccount.MondayTimeRange2__c = 4;
		prestaAccount.MondayTimeRange3__c = 4; prestaAccount.MondayTimeRange4__c = 4;
		prestaAccount.TuesdayTimeRange1__c = 4; prestaAccount.TuesdayTimeRange2__c = 4;
		prestaAccount.TuesdayTimeRange3__c = 4; prestaAccount.TuesdayTimeRange4__c = 4;
		prestaAccount.WednesdayTimeRange1__c = 4; prestaAccount.WednesdayTimeRange2__c = 4;
		prestaAccount.WednesdayTimeRange3__c = 4; prestaAccount.WednesdayTimeRange4__c = 4;
		prestaAccount.ThursdayTimeRange1__c = 4; prestaAccount.ThursdayTimeRange2__c = 4;
		prestaAccount.ThursdayTimeRange3__c = 4; prestaAccount.ThursdayTimeRange4__c = 4;		 
		prestaAccount.FridayTimeRange1__c = 4; prestaAccount.FridayTimeRange2__c = 4;
		prestaAccount.FridayTimeRange3__c = 4; prestaAccount.FridayTimeRange4__c = 4;			 
		prestaAccount.SaturdayTimeRange1__c = 4; prestaAccount.SaturdayTimeRange2__c = 4;
		prestaAccount.SaturdayTimeRange3__c = 4; prestaAccount.SaturdayTimeRange4__c = 4;	
		INSERT prestaAccount;

		SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
		chamJeton.initCalendars(new List<Account>{prestaAccount});

		// Create installation Lead
		Lead aLead = SICo_UtilityTestDataSet.createLead(prestaAccount.Id, 'Colissimo');
		aLead.CHAMInstallationAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
		aLead.CHAMEntretienAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
		aLead.CHAMInstallationTimeSlot__c= '08:00 - 10:00';
		aLead.CHAMEntretienTimeSlot__c= '08:00 - 10:00';
		
		test.startTest();
			chamJeton.decrementCreneau(aLead);
		test.stopTest();

		final SICO_AvailabilyWeeklyCalendar__c aWeeklyCalendar = [SELECT StartDate__c, ThursdayTimeRange2__c, ThursdayTimeRange3__c, 
																															ThursdayTimeRange4__c, ThursdayTimeRange1__c, MondayTimeRange2__c,
																															MondayTimeRange3__c, MondayTimeRange4__c, MondayTimeRange1__c,
																															TuesdayTimeRange2__c, TuesdayTimeRange3__c, TuesdayTimeRange4__c, 
																															TuesdayTimeRange1__c, WednesdayTimeRange2__c, WednesdayTimeRange3__c, 
																															WednesdayTimeRange4__c, WednesdayTimeRange1__c, SaturdayTimeRange2__c,
																															SaturdayTimeRange3__c, SaturdayTimeRange4__c, SaturdayTimeRange1__c,
																										 					FridayTimeRange2__c, FridayTimeRange3__c, FridayTimeRange4__c, 
																										 					FridayTimeRange1__c, Status__c
																										 					FROM SICO_AvailabilyWeeklyCalendar__c
																										 					WHERE StartDate__c = :Date.today().toStartOfWeek() Limit 1];

		Integer day =    aLead.CHAMInstallationAppointmentDate__c.toStartOfWeek().daysBetween(aLead.CHAMInstallationAppointmentDate__c);
		String fieldCreneau = SICo_Constants_CLS.CRENEAUX.get(day).get(aLead.CHAMInstallationTimeSlot__c);
		System.assertEquals(3,aWeeklyCalendar.get(fieldCreneau));
	}
}