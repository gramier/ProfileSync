/*------------------------------------------------------------
Author:        SFDC // Std Community Controller for Self Registration
Company:       Salesforce.com
Description:   Std Community Controller for Self Registration. This Class is Without Sharing to check if username already exists.
Test Class:    SICo_CommunitiesSelfReg_VFC_TEST
History
05/08/2016      SFDC                Create version
09/08/2016      Dario Correnti      Modified version
16/08/2016      Mehdi Cherfaoui     Added Guest User registration
22/08/2016      Mehdi Beggas        Added Error Message
21/10/2016      Selim BEAUJOUR      Added doredirect function
------------------------------------------------------------*/
public without sharing class SICo_CommunitiesSelfReg_VFC {

    //Public Variables - Existing Customer
    public String email {get; set;}
    public String noClient {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public Boolean isGuestUser {get; set;}

    public String errorMessage {get;set;}
    public Boolean preventUserCreation { get; set; }

    //Public Variables - New Guest
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String pseudo {get; set;}
    //public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }


    //Private Variables
    private static final String CREATE_COMM_USR = 'CREATE_COMM_USR'; 

    private Account newGuestAccount;


    /**
     * [Constructor]
     */
    public SICo_CommunitiesSelfReg_VFC() {
        noClient = System.currentPageReference().getParameters().get('noClient');
        email = System.currentPageReference().getParameters().get('email');
        isGuestUser = String.isBlank(noClient);
        preventCreationForSalesforceUsers();
    }//END SICo_CommunitiesSelfReg_VFC()

    /**
    * Add error message and prevent creation if the user has a salesforce licence (means already logged in)
    */
    public void preventCreationForSalesforceUsers() {
        if (UserInfo.getUserType() != SICo_Constants_CLS.USER_TYPE_GUEST) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.SICo_ErrorAlreadyLogged);
			ApexPages.addMessage(msg);
			errorMessage = msg.getDetail();  
			preventUserCreation = true;
        } else { 
        	preventUserCreation = false;
        }
    }

    /**
     * [Register new Client User (has a Client number)]
     * @return [Registration Confirmation Page, or null in case an error occured]
     */
    public PageReference registerUser() {

        CommunityConfig__c commSetting = CommunityConfig__c.getInstance();

        //Check if all manadorty fields have a value
        if (!checkMandatoryFields()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SICo_RegistrationError);
            ApexPages.addMessage(msg);
            errorMessage = msg.getDetail();  
            return null;
        }

        //Check if password are matching
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            errorMessage = msg.getDetail();
            return null;
        }

        // Check if pseudo is free to use
        if (isAlreadyTakenNickName(pseudo)) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.SICo_PseudoExist);
            ApexPages.addMessage(msg);
            errorMessage = msg.getDetail();
            return null;
        }

        //Check if email is not already associated to an existing user
        if (isGuestUser && !isEmailAvailable(email)) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SICo_UserExist);
            ApexPages.addMessage(msg);
            errorMessage = msg.getDetail();
            return null;
        }


        //Retrive Account, or Create new one if guest user
        try{
            List<Account> accounts = new List<Account>();
            if (isGuestUser) {
                //Guest User - Insert new guest account
                Id guestAccountRecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Account_'+SICo_Constants_CLS.GUEST_PERSON_ACCOUNT_RECORD_TYPE);
                newGuestAccount = new Account (
                                                    FirstName = firstName,
                                                    LastName = lastName,
                                                    PersonEmail = email,
                                                    OwnerId = commSetting.CommunityDefaultGuestOwnerId__c,
                                                    RecordTypeId =guestAccountRecordTypeId);
                database.insert(newGuestAccount, false);
                //retrieve Account information
                accounts = [SELECT Id, PersonEmail, FirstName, LastName, PersonContactId FROM Account WHERE ID=: newGuestAccount.Id LIMIT 1];
            } else {
                //Client User - Retrieve existing accoun
                accounts = [SELECT FirstName, LastName, PersonEmail, PersonContactId FROM Account WHERE CustomerNumber__c = :noClient LIMIT 1]; //Should return 1 account max
            }
            if (!accounts.isEmpty()) {
                String accountId = accounts.get(0).Id;
                //Profile
                String profileId = commSetting.CommunityClientDefaultProfileId__c;
                if (isGuestUser) profileId = commSetting.CommunityGuestDefaultProfileId__c;
                //Username = email (+ suffix for guest users)
                String userName = accounts.get(0).PersonEmail;
                if (isGuestUser) userName = userName +SICo_Constants_CLS.CUSTOMER_COMMUNITY_GUEST_SUFFIX;
                //New Community User
                User u = new User();
                u.ProfileId = profileId;
                u.ContactId = accounts.get(0).PersonContactId;
                u.Username = userName;
                u.Email = accounts.get(0).PersonEmail;
                u.FirstName = accounts.get(0).firstName;
                u.LastName = accounts.get(0).lastName;
                u.TimeZoneSidKey = SICo_Constants_CLS.CUSTOMER_COMMUNITY_TIMEZONE;
                u.EmailEncodingKey = SICo_Constants_CLS.DEFAULT_EMAIL_ENCODING;
                u.CommunityNickname = pseudo;
                //Try to create user 
                Id userId;
                try {
                    userId = Site.createExternalUser(u, accountId, password);
                } catch(Site.ExternalUserCreateException e) {                
                    //Delete person account if guest creation failed
                    if(isGuestUser) database.delete(newGuestAccount, false);
                    //Retrieve and display message
                    List<String> errors = e.getDisplayMessages();
                    for (String error : errors)  {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
                    }
                    SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO, CREATE_COMM_USR, e.getMessage());                 
                }
                if (Test.isRunningTest() || userId != null) { 
                    if (password != null && password.length() > 1 && !isGuestUser) {
                       deactivateRelatedGuestCommunityUser(accounts.get(0).PersonEmail + commSetting.CommunityGuestUnameSuffix__c);
                       PageReference pageRef = Site.login(userName, password, SICo_Constants_CLS.CUSTOMER_SPACE_START_URL);
                        if(pageRef == null){
                            errorMessage = Label.SICo_errorMdpSyntax;
                        }
                        return pageRef;
                    } else {
                        //Confirmation page (which handle reset password logic for this user)
                        PageReference page = System.Page.CommunitiesSelfRegConfirm;
                        page.getParameters().put('username', userName) ;
                        page.setRedirect(true);
                        return page;
                    }
                }//ENDIF Test.isRunningTest() || userId != null

                //if createExternalUser return null without error
                else if(userId == null){
                    if([SELECT ID FROM User WHERE AccountId = :accountId LIMIT 1].size() > 0){
                        errorMessage = Label.SICo_ExistingUser;
                    }
                    else{
                        errorMessage = Label.SICo_errorMdpSyntax;
                    }
                }

            } else {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SICo_UnableToCreateUser);
                ApexPages.addMessage(msg);
                errorMessage = msg.getDetail();
            }//ENDIF !accounts.isEmpty()
            return null;
        }catch (exception ex){
            //unexpected error
            System.debug(ex);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SICo_UnableToCreateUser);
            ApexPages.addMessage(msg);
            errorMessage = msg.getDetail();
            SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO, Label.SICO_LOG_USER_Registration, 'STACK_TRACE: '+ex.getStackTraceString()+' EXCEPTION: '+ex);
            return null;
        }//END try-catch block

    }//END registerUser()
    
    //Redirect Customer on the login page if he is a community user
    public PageReference doRedirect(){
                            
       if(noclient != null){

            Account[] acc = [SELECT Id, CustomerNumber__c, IsCustomerPortal from Account WHERE CustomerNumber__c=:noClient and IsCustomerPortal=true  LIMIT 1];
            if(acc.size() !=0){
                PageReference pageRef = new PageReference(Sico_Constants_CLS.CUSTOMER_COMMUNITITESLOGIN_URL);
                 return pageref;   
        }
            else return null;
        }
         else return null;
    }
   

    
    /* HELPER METHODS */

    private Boolean isValidPassword() {
        return password == confirmPassword;
    }

    private Boolean isAlreadyTakenNickName(String pNickName) {
    	List<User> vUsers = [SELECT Id FROM USER WHERE CommunityNickname = :pNickName LIMIT 1];
    	if (vUsers != null && vUsers.size() > 0) {
    		return true;
    	}

        return false;
    }
    
    private Boolean checkMandatoryFields () {
        if(!isGuestUser)
        {//Client User
            return !String.isBlank(noClient) && !String.isBlank(email) && password.length() > 0 && !String.isBlank(pseudo);
        }else{//Guest User
            return !String.isBlank(firstName) && !String.isBlank(lastName) && !String.isBlank(email) && !String.isBlank(pseudo);
        }
    }
    
    private Boolean isEmailAvailable (String email){
        List<User> existingUsers = [SELECT Id FROM User WHERE Email =: email LIMIT 1];
        return existingUsers.isEmpty();
    }

    private void deactivateRelatedGuestCommunityUser(String uname) {
        List<User> guestUsr = [SELECT Id, isActive FROM User WHERE Username = :uname AND isActive = true LIMIT 1];
        if (!guestUsr.isEmpty()) {
            guestUsr.get(0).isActive = false;
            database.update(guestUsr.get(0), false);
        }
    }

    //close mobile error popup
    public PageReference closeErrorPopup(){
        errorMessage = '';
        return null;
    }

}