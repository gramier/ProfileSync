/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Test Class of SICo_Eligibility_RWS
Test Class:    SICo_Eligibility_RWS_TEST
History
25/07/2016      Olivier Vrbanac     Create & develope first version
09/08/2016      Dario Correnti		Add RateZone__c to WS response
------------------------------------------------------------*/
@isTest
public with sharing class SICo_Eligibility_RWS_TEST {
	
    static testMethod void testGet() {

    	// Create Service Provider and Agency
        Account oAccountGRDF = new Account();
        oAccountGRDF.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get( 'Account_ServiceProvider' );
        oAccountGRDF.Name = 'GRDF';
        insert oAccountGRDF;

        Account oAccountCHAM = new Account();
        oAccountCHAM.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get( 'Account_ServiceProvider' );
        oAccountCHAM.Name = 'CHAM';
        insert oAccountCHAM;

        Account oAccountAgency = new Account();
        oAccountAgency.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get( 'Account_Agency' );
        oAccountAgency.Name = 'CHAM';
        oAccountAgency.ParentId = oAccountCHAM.Id;
        insert oAccountAgency;
		
		SICo_ZipCode__c zc = new SICo_ZipCode__c(ZipCode__c = '78720');
		insert zc; 
		
    	// Create INSEE References
		SICo_INSEE__c oINSEE1 = new SICo_INSEE__c();
		oINSEE1.INSEECode__c = '12345';
		oINSEE1.PostalCode__c = '78720';
		oINSEE1.City__c = 'DAMPIERRE EN YVELINES';
		oINSEE1.DeliveryLabel__c = 'DAMPIERRE EN YVELINES';
		insert oINSEE1;

		SICo_INSEE__c oINSEE2 = new SICo_INSEE__c();
		oINSEE2.INSEECode__c = '12346';
		oINSEE2.PostalCode__c = '78720';
		oINSEE2.City__c = 'CERNAY LA VILLE';
		oINSEE2.DeliveryLabel__c = 'CERNAY LA VILLE';
		insert oINSEE2;
		
		SICo_InseeCPJunction__c jo1 = new SICo_InseeCPJunction__c(ZipCode__c = zc.Id, CodeInsee__c = oINSEE1.Id);
		SICo_InseeCPJunction__c jo2 = new SICo_InseeCPJunction__c(ZipCode__c = zc.Id, CodeInsee__c = oINSEE2.Id);
		insert new List<SICo_InseeCPJunction__c> {jo1, jo2};
		
		// Create Eligibility for Gas
		SICo_Eligibility__c oEliGas = new SICo_Eligibility__c();
		//oEliGas.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get( 'SICo_Eligibility__c_Gas' );
		oEliGas.ServiceProvider__c = oAccountGRDF.Id;
		oEliGas.INSEECode__c = oINSEE1.Id;
        oEliGas.RateZone__c = '3';
		insert oEliGas;

		// Create Eligibility for Maintenance
		SICo_Eligibility__c oEliMaintenance = new SICo_Eligibility__c();
		//oEliMaintenance.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get( 'SICo_Eligibility__c_Maintenance' );
		oEliMaintenance.ServiceProvider__c = oAccountCHAM.Id;
		oEliMaintenance.INSEECode__c = oINSEE1.Id;
		insert oEliMaintenance;

        //TEST GET WITHOUT ERROR
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Eligibility';  
        req.addParameter('insee_code', '12345');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        SICo_Eligibility_RWS.getEligibility_RWS();

        //TEST GET WITH ERROR
        req = new RestRequest(); 
        res = new RestResponse();

        req.requestURI = '/services/apexrest/Eligibility';  
        req.addParameter('insee_code', '12346');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        SICo_Eligibility_RWS.getEligibility_RWS();
	}

}