/*------------------------------------------------------------
Author:        Cyril PENGAM 
Company:       Ikumbi
Description:   Controller for the UpdateBankDetail page.
Test Class:    SICo_UpdateBankDetails_VFC_Test
History
Sept. 2016      Cyril PENGAM     Create version
------------------------------------------------------------*/
global with sharing class SICo_UpdateBankDetails_VFC {
    public Id accountId{get;set;}
    public String bankingAccountId{get;set;}
    public String zuoraUrl {get;set;}
    public String urlJs {get;set;}

    public SICo_UpdateBankDetails_VFC() {
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        bankingAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_BANKINGACCOUNTID);
        
        //create the url to Zuora
        this.zuoraUrl = ZuoraWSconfig__c.getInstance().Uri__c; 
        this.urlJs = ZuoraWSconfig__c.getInstance().ZuoraJSResource__c;
        
        try {
            accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }
        catch (exception e){
            System.debug('Unable to return ids');
        }
    }

    /**
    * Return the Zuora signature
    * @Return : The signature map as a JSON string
    */
    @RemoteAction
    global static String processSignature(){
        String sepaGatewayName = ZuoraWSconfig__c.getInstance().GatewayNameSEPA__c;
        Map<String, String> mapSignature = SICo_UtilityCommunities_CLS.callZuoraLogin(sepaGatewayName);
        String signature = JSON.serialize(mapSignature);
        return signature;
    }

    /**
    * Call the methods to desactivate the old payment method and update the default payment method
    * @Param : Account id and new payment method id
    * @Return : True if both the other methods succeeded
    */
    @RemoteAction
    global static Boolean updateDefaultAndOldPaymentMethod(Id accountId, String newPMZuoraId){
        Boolean desactivateOldPMResult = true;
        Zuora__PaymentMethod__c zpm = queryPaymentMethod(accountId);
        if (zpm != null){
            //Desactivate old Payment Method
            desactivateOldPMResult = desactivateOldPM(zpm.Zuora__External_Id__c);
        }else{
            //Put a flag in the Account in Session Cache
            String cacheKey = SICo_Constants_CLS.SESSION_CACHE_KEY_PAYMENTMETHODUPDATE + accountId;
            SICo_PlatformCacheManager_CLS.putSessionObject(cacheKey, 'true', 3600);
        }
        if (desactivateOldPMResult){
            List<Zuora__CustomerAccount__c> zuoraAccountList = [SELECT Zuora__External_Id__c 
                                                                FROM Zuora__CustomerAccount__c
                                                                WHERE Zuora__Account__c =:accountId];
            if(zuoraAccountList.size() == 1){
                Boolean updatePaymentMethodResult = updatePaymentMethod(zuoraAccountList[0].Zuora__External_Id__c,newPMZuoraId);
                if (zpm != null){
                    //Flag old Payment Method in Salesforce
                    zpm.IsPaymentMethodUpdated__c = true;
                    update zpm;
                }
                return updatePaymentMethodResult;
            }
        }
        System.debug('### error desactivateOldPMResult');
        return false;
    }

    /**
    * Call Zuora to desactivate the old payment method
    * @Param : Zuora old payment method id
    * @Return : True if successful
    */
    public static Boolean desactivateOldPM(String zopmi){
        try {
            Zuora.zApi zApiInstance = SICo_ZuoraUtilities.zuoraApiAccess(); //new Zuora.zApi();//zApiInstance.zlogin();
            Zuora.zObject pm = new Zuora.zObject('PaymentMethod');
            pm.setValue('Id',zopmi);
            pm.setValue('PaymentMethodStatus','Closed');
            List<Zuora.zObject> objs = new List<Zuora.zObject> { pm }; 
            List<Zuora.zApi.SaveResult> results = SICo_ZuoraUtilities.updateZuoraObjects(zApiInstance, objs); 
            Boolean isSuccess = false;
            for (Zuora.zApi.SaveResult result : results) { 
                if (result.Success){
                    String updatedId = result.Id;
                    isSuccess = true;
                } else {
                    SICo_ZuoraUtilities.analyzeSaveResult(result);
                    isSuccess = isSuccess || false;
                }
            }
            return isSuccess;
        }
        catch (Zuora.zRemoteException ex) {
            if ('INVALID_FIELD' == ex.code) {
                system.debug('INVALID_FIELD: '+ex);
            } 
            else {
                system.debug('zRemoteException '+ex);
            }
            return false;
        } 
        catch (Zuora.zAPIException ex) {
            ex.getMessage();
            system.debug('zAPIException'+ex);
            return false;
        } 
        catch (Zuora.zForceException ex) {
            system.debug('zForceException '+ex);
            return false;
        }
        return false;
    }

    /**
    * Call Zuora to update the default payment method
    * @Param : Zuora account id and new payment method id
    * @Return : True if successful
    */
    public static Boolean updatePaymentMethod (String bankingAccountId, String newPMZuoraId){
        try { 
            Zuora.zApi zApiInstance = SICo_ZuoraUtilities.zuoraApiAccess(); //new Zuora.zApi();//zApiInstance.zlogin();
            Zuora.zObject acc = new Zuora.zObject('Account');
            acc.setValue('Id',bankingAccountId);
            acc.setValue('DefaultPaymentMethodId',newPMZuoraId);
            String sepaGatewayName = ZuoraWSconfig__c.getInstance().GatewayNameSEPA__c;
            acc.setValue('PaymentGateway', sepaGatewayName);
            List<Zuora.zObject> objs = new List<Zuora.zObject> { acc };
            List<Zuora.zApi.SaveResult> results = SICo_ZuoraUtilities.updateZuoraObjects(zApiInstance, objs); 
            Boolean isSuccess = false;
            for (Zuora.zApi.SaveResult result : results) { 
                if (result.Success){
                    String updatedId = result.Id;
                    isSuccess = true;
                } else {
                    System.debug('### updatePaymentMethod error');
                    SICo_ZuoraUtilities.analyzeSaveResult(result);
                    isSuccess = isSuccess || false;
                }
            }
            return isSuccess;
        } catch (Zuora.zRemoteException ex) {
            if ('INVALID_FIELD' == ex.code) {
                system.debug('INVALID_FIELD: ' + ex);
            } else {
                system.debug('zRemoteException ' + ex);
            }
            return false;
        } catch (Zuora.zAPIException ex) {
            ex.getMessage();
            system.debug('zAPIException' + ex);
            return false;
        } catch (Zuora.zForceException ex) {
            system.debug('zForceException ' + ex);
            return false;
        }
        return false;
    }   

    /**
    * Query the old payment method
    * @Param : Account id
    * @Return : The old payment method
    */
    public static Zuora__PaymentMethod__c queryPaymentMethod(Id accountId){
        Zuora__PaymentMethod__c thePaymentMethod;
        List<Zuora__PaymentMethod__c> paymentMethodList = [SELECT 
                                                                Zuora__DefaultPaymentMethod__c,
                                                                Zuora__BillingAccount__r.Zuora__External_Id__c,
                                                                Zuora__External_Id__c,
                                                                IsPaymentMethodUpdated__c
                                                                FROM Zuora__PaymentMethod__c
                                                                WHERE Zuora__PaymentMethodStatus__c =: SICo_Constants_CLS.ACTIVE_PAYMENT_METHOD_STATUS
                                                                AND (Zuora__Type__c =: SICo_Constants_CLS.ZUORA_PAYMENT_METHOD_BANK_TRANSFERT OR Zuora__Type__c =: SICo_Constants_CLS.ZUORA_PAYMENT_METHOD_BANK_TRANSFERT_WHITESPACE)
                                                                AND Zuora__BillingAccount__r.Zuora__Account__c =:accountId]; //Note: there should be 1 single billing account per person account
        //If 1 single payment method is active, use it
        if (paymentMethodList.size() == 1) {
            thePaymentMethod = paymentMethodList[0];
        //if several payment methods, use the default one
        } else if (paymentMethodList.size() > 1) {
            for (Zuora__PaymentMethod__c meth : paymentMethodList) {
                if(meth.Zuora__DefaultPaymentMethod__c) thePaymentMethod = meth;
            }
        }//ENDIF siteList.size()
        return thePaymentMethod;
    }

}