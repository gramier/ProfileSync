/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Serialise & deserialize JSON CHAM
Test Class:    SICo_FluxChamModel_TEST

History
04/08/2016      Boris Castellani     Create version
07/09/2016      Boris Castellani     finalise Flux model
------------------------------------------------------------*/
public with sharing class SICo_FluxChamModel {

	// Build ChamContract - Fiche client et contrat CHAM
	public class ChamClientContrat {
		public List<ChamClient> chamClient;
		public ChamClientContrat parse (String json) {
			return (ChamClientContrat) System.JSON.deserialize(json, ChamClientContrat.class);
		}
	}

	// Build ChamDemand - Fiche Demande pour CHAM
	public class ChamRDV {
		public List<ChamDemande> chamDemande;
		public ChamRDV parse (String json) {
			return (ChamRDV) System.JSON.deserialize(json, ChamRDV.class);
		}
	}

	// Build ChamToken
	public class ChamToken {
		public String iss;
		public String exp;
		public String sub;
		public String aud;
		public chamToken(){
			this.iss = SICo_Constants_CLS.STR_CHAMTOKEN_ISS;
			this.exp = SICo_Constants_CLS.STR_CHAMTOKEN_EXP;
			this.sub = SICo_Constants_CLS.STR_CHAMTOKEN_SUB;
			this.aud = SICo_Constants_CLS.STR_CHAMTOKEN_AUD;
			}
	}

	/**********************************
	    Wrapper
	 **********************************/

	Public class ChamDemande{
		public String id_ticket_orchidee;
		public String id_orchidee;
		public String id_societe;
		public String id_agence;
		public String inter;
		public String type;
		public String motif;
		public String motif2;
		public String motif3;
		public String observationclient;
		public List<Diagnostique> diagnostique;
		public DateTime dateinter;
		public String tranc;
		public String heurd;
		public String heurf;
		public String periode;
		public String premier;
		public String numerodevis;
		public String statut_avancement;
		public DateTime date_creation;
		public DateTime date_modification;
	}

	public class Diagnostique {
		public String question;
		public String codepanne;
		public String reponse;
	}

	public class ChamClient{
		public String idTask;
		public String id_orchidee;
		public String id_societe;
		public String id_agence;
		public DateTime date_creation;
		public DateTime date_modification;
		public String clien;
		public String techn;
		public String tech_matricule;
		public String tech_agence;
		public String inter;
		public String quano;
		public String nomclient;
		public String preno;
		public String numru;
		public String quaru;
		public String rue1;
		public String rue2;
		public String rue3;
		public String codepo;
		public String ville;
		public String batim;
		public String escal;
		public String etage;
		public String porte;
		public String codepor;
		public String teldo;
		public String telmo;
		public String latitu;
		public String longit;
		public String access1;
		public String access2;
		public String observations;
		public String typlog;
		public String locpro_lp;
		public DateTime date_logement;
		public String tatva;
		public String email;
		public Gaz gaz;
		public Entretien entretien;
		public List<Appar> appar;
	}

	public class Gaz {
		public String idxcpteur;
		public String matricpteur;
		public String pce;
		public DateTime datesign;
		public DateTime debcontrat;
		public DateTime fincontrat;
	}

	public class Entretien{
		public DateTime datesign;
		public DateTime debcontrat;
		public DateTime fincontrat;
		public String typco;
		public String desigcontrat;
		public String prico;
		public String optio1;
		public String desigoptio1;
		public String optio2;
		public String desigoptio2;
		public String optio3;		
		public String desigoptio3;
		public String paye;
	}

	public class Appar {
		public String ordr;
		public String libelenerg;
		public String libelmarque;
		public String libelmodele;
		public DateTime dames;
		public String serie;
		public String mac;
	}

}