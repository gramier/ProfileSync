/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the Contrats page (Contracts).
History
25/08/2016      Mehdi BEGGAS      Create version
21/11/2016		Mehdi CHERFAOUI   IKUMBI-442: Retrieve Gas prices
07/12/2016		Mehdi CHERFAOUI	  IKUMBI-497: Handle Product-Only contracts	
------------------------------------------------------------*/
public with sharing class SICo_MyContracts_VFP {
    
    public Id accountId{get;set;}
    
    public List<SICo_Bundle__c> bundles{get;set;}
    public Map<String, SICo_Bundle__c> mapBundles{get;set;}
    public List<Zuora__Subscription__c> zuoraSubs{get;set;}
    public List<SICo_Site__c> sites{get;set;}
    public List<SICo_CR__c> crs{get;set;}
    
    public SoweeContractWrapper soweeContract{get;set;}
    public MaintenanceContractWrapper maintenanceContract{get;set;}
    public ProductContractWrapper productContract{get;set;}
    
    public boolean isSoweeContract{get;set;}
    public boolean isMaintenanceContract{get;set;}
    
    public Map<Id, Attachment> mapAttachments;
    public Map<Id, String> mapAttachmentURL;
    
    public String typeContrat{get;set;}
    public String linkDownloadAttestation{get;set;}

    private final String DOWNLOAD_PATH = '/servlet/servlet.FileDownload?file=';
    
    public SICo_MyContracts_VFP() {
        
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        doInit(requestedAccountId);
    }
    public SICo_MyContracts_VFP(String requestedAccountId) {
        doInit(requestedAccountId);
    }
    private void doInit(String requestedAccountId) {
        
        isSoweeContract = false;
        isMaintenanceContract = false;
        
        //Get AccountId
        try {
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }
        catch (exception e) {
            System.debug('Unable to return accountId');
        }
        
        if (accountId != null) {		 
            this.sites = 	[
            					SELECT Id,
		                          	BoilerBrand__c,
		                         	BoilerModel__c,
		                          	Zone__c,
		                          	Subscriber__r.IsShopCustomer__c
	                          	FROM SICo_Site__c
	                          	WHERE Subscriber__c = :accountId
	                          	AND IsSelectedInCustomerSpace__c = true
	                          	LIMIT 100
                          	];
            
            this.zuoraSubs = 	[
            						SELECT Zuora__TermStartDate__c,
										InvoicingType__c,
										OfferName__c,
										Zuora__TermEndDate__c,
										IsMaintenance__c,
										Quote__c,
										MaintenanceParts__c
                              		FROM Zuora__Subscription__c
									WHERE Zuora__Account__c = :accountId
									AND SiteId__c IN :this.sites
									AND IsContract__c = true
									LIMIT 100
								];
            
            this.crs = 	[
            				SELECT InterventionDate__c
                            FROM SICo_CR__c
                            WHERE Site__c IN :sites
                            ORDER BY InterventionDate__c
                            LIMIT 1
                        ];
                        
            if(!this.zuoraSubs.isEmpty()) {
                
                //Build Attachment Download link Map
                this.mapAttachments = new Map<Id, Attachment>([	
                												SELECT Id, 
            														Name, 
            														ParentId, 
            														CreatedDate
                                                               	FROM Attachment
                                                               	WHERE ParentId IN :zuoraSubs
                                                               	AND Name LIKE '%_CPV%'
                                                               	ORDER BY CreatedDate desc
                                                               	LIMIT 100
                                                           	]);
                mapAttachmentURL = new Map<Id, String>();
                for(Attachment att : mapAttachments.values()) {
                    if(mapAttachmentURL.get(att.ParentId) == null) {
                        mapAttachmentURL.put(att.ParentId, '/servlet/servlet.FileDownload?file=' + att.Id);
                    }
                }
                
                //construct bundle query (in order to retrieve bundle names)
                String bundleQuery = 'SELECT PublicLabel__c, PublicLabelLine2__c, TechOfferName__c, SkuNumber__c FROM SICo_Bundle__c WHERE TechOfferName__c ';
                for (Integer i = 0; i < this.zuoraSubs.size(); i++) {
                	Zuora__Subscription__c sub = this.zuoraSubs.get(i);
                	if (i == 0){
                		if (sub.OfferName__c == null) {
                    		bundleQuery += '= null';
		                }
		                else {
		                	bundleQuery += 'LIKE \'' + sub.OfferName__c + '\'';
		                }
                	}
                	else {
	                	if (sub.OfferName__c == null) {
	                    	bundleQuery += ' OR SkuNumber__c = null';
	                    }
	                	else {
	                    	bundleQuery += ' OR SkuNumber__c LIKE \'' + sub.OfferName__c + '\'';
	                	}
                	}
				}
                system.debug(bundleQuery);
                bundleQuery += ' LIMIT 1000';
                //execute bundle query
                this.bundles = Database.query(bundleQuery);
                //fill bundle map
                mapBundles = new Map<String, SICo_Bundle__c>();
                for(SICo_Bundle__c bundle : this.bundles) {
                    this.mapBundles.put(bundle.SkuNumber__c, bundle);
                }
                System.debug('mapBundles: '+mapBundles);
                //Get Prices
                Map<Id,Decimal> mapSubscriptionPriceConso = mapSubscriptionPrice(zuoraSubs, SICo_Constants_CLS.GAZ_CONSO_CATEGORY);
                Map<Id,Decimal> mapSubscriptionPriceAbo = mapSubscriptionPrice(zuoraSubs, SICo_Constants_CLS.GAZ_ABO_CATEGORY);												
                //Build Wrapper for each susbscription
                for (Zuora__Subscription__c zuora : zuoraSubs) {
                    if (zuora.IsMaintenance__c) {
                        if(zuora.Quote__c != null) {
                            if (zuora.MaintenanceParts__c == SICo_Constants_CLS.WITH_PARTS){
	                        	this.typeContrat = Label.SICo_ContractWithParts;
	                    	}
	                        if (zuora.MaintenanceParts__c == SICo_Constants_CLS.WITHOUT_PARTS){
	                        	this.typeContrat = Label.SICo_ContractWithoutParts;
	                        }
                        }
                        else {
                        	this.typeContrat = '';
						}

            			//HERE IKUMBI-560 
                        isMaintenanceContract = true;
                        maintenanceContract = new MaintenanceContractWrapper(
                        														typeContrat,
                                                                             	zuora.Zuora__TermEndDate__c,
                                                                             	sites.get(0).BoilerBrand__c,
                                                                             	sites.get(0).BoilerModel__c,
                                                                             	(crs.isEmpty() ? null : crs.get(0).InterventionDate__c),
                                                                             	zuora.OfferName__c,
                                                                             	mapAttachmentURL.get(zuora.Id),
                                                                             	accountId
                                                                         	);
                        if (!mapBundles.isEmpty()) {
                        	if (mapBundles.get(zuora.OfferName__c) != null && maintenanceContract.title == null) {
                            	maintenanceContract.title = mapBundles.get(zuora.OfferName__c).PublicLabel__c;
                            }
                        }
                    }
                    else {
                    	isSoweeContract = true;
                        Decimal priceAbo = mapSubscriptionPriceAbo.get(zuora.Id);
                        Decimal priceConso = mapSubscriptionPriceConso.get(zuora.Id);
                        soweeContract = new SoweeContractWrapper(
                        											zuora.InvoicingType__c,
                                                                	sites.get(0).Zone__c,
                                                                 	zuora.Zuora__TermStartDate__c,
                                                                 	priceAbo,
                                                                 	priceConso,
                                                                 	(crs.isEmpty() ? false : true),
                                                                 	zuora.OfferName__c,
                                                                 	mapAttachmentURL.get(zuora.Id),
                                                                 	accountId
                                                             	);
                        
                        if(!mapBundles.isEmpty()) {
                            if(mapBundles.get(zuora.OfferName__c) != null) {
                                soweeContract.title = mapBundles.get(zuora.OfferName__c).PublicLabel__c;
                                soweeContract.subTitle = mapBundles.get(zuora.OfferName__c).PublicLabelLine2__c;
                            }
                        }
                        system.debug('soweeContract: '+soweeContract);
                    }
                }
                //Retrieve attestationMaintenance
                List<SICo_CR__c> siteCr = 	[
                								SELECT Id
												FROM SICo_CR__c
												WHERE Site__c = :sites.get(0).Id
												AND FirstReturnCode__c =: SICo_Constants_CLS.CR_VEN_CODE
												ORDER BY CreatedDate DESC
												LIMIT 1
											];

                List<Attachment> attestationMaintenance;
                if(!siteCr.isEmpty()) {
                    attestationMaintenance = 	[
                    								SELECT Id
													FROM Attachment
													WHERE ParentId = :siteCr.get(0).Id
													AND Name LIKE 'CR_%'
													ORDER BY CreatedDAte DESC
													LIMIT 1
												];
                }
                
                if (attestationMaintenance != null && !attestationMaintenance.isEmpty()) {
                    linkDownloadAttestation = DOWNLOAD_PATH + attestationMaintenance.get(0).Id;
                }
            }
            else {
            	//ENDIF !this.zuoraSubs.isEmpty()

              	//If there is no Contract, we use a simplified wrapper to retrive the (single) attachment that should be available on the single non-contract subscription (IKUMBI-497) 
              	zuoraSubs = [
              					SELECT
									OfferName__c,
									MarketingOfferName__c,
									(SELECT Id FROM Attachments ORDER BY CreatedDAte DESC LIMIT 1)
								FROM Zuora__Subscription__c
								WHERE Zuora__Account__c = :accountId
								AND SiteId__c IN :this.sites
								AND IsContract__c = false
								LIMIT 100
							];
              	if (zuoraSubs.size() == 1) {
                	String productContractName = zuoraSubs[0].MarketingOfferName__c;
                	Id contractAttachmentId;
                	if (zuoraSubs[0].Attachments.size() == 1) {
                		contractAttachmentId = zuoraSubs[0].Attachments[0].Id;
            		}
                	//construct productContractWrapper with contract name and download link
                	if (productContractName != null) {
                  		//Default download link
						String downloadLink = Label.SICo_LinkCGVweb;
						//if Shop customer, use a different default document
						if (sites.get(0).Subscriber__r.IsShopCustomer__c) {
							downloadLink = Label.SICo_LinkCGVshop;
						}
						//if there is a document attached to the subscription, use it
						if (contractAttachmentId != null) {
							downloadLink = DOWNLOAD_PATH +contractAttachmentId;
						}
						this.productContract = new ProductContractWrapper(
																			productContractName, 
																			downloadLink, 
																			accountId, 
																			(crs.isEmpty() ? false : true)
																		);
                } 
            }
              
            }//END ELSE !this.zuoraSubs.isEmpty()

        }//ENDIF accountId != null
        
    }
    
    //Wrapper for Gas contract
    public class SoweeContractWrapper{
        
        public String accountId{get;set;}
        
        public String title{get;set;}
        public String subTitle{get;set;}
        public String facturation{get;set;}
        public String coveredZone{get;set;}
        public String startDate{get;set;}
        public String gasSubscription{get;set;}
        public String price{get;set;}
        public String offerName{get;set;}
        public String contractDownloadLink{get;set;}
        public String linkOldContracts{get;set;}
        public String linkAttestationsReports{get;set;}
        public String linkCancelContracts{get;set;}
        public Boolean hasReports{get;set;}
        
        public SoweeContractWrapper(String facturation, String coveredZone, Date startDate, Decimal gasSubscription, Decimal price, Boolean hasReports, String offerName, String contractDownloadLink, String accountId){
            this.accountId = accountId;
            this.facturation = (facturation == Label.SICo_SubscriptionTypeLisse ? Label.SICo_SmoothMonthlyFacturation : Label.SICo_RealMonthlyFacturation);
            this.coveredZone = (coveredZone == null ? '' : 'Zone ' + coveredZone);
            
            this.startDate = '';
            if (startDate != null) {
            	this.startDate = (startDate.day() < 10 ? '0' : '') + startDate.day() + '.';
            	this.startDate += (startDate.month() < 10 ? '0' : '') + startDate.month() + '.' + startDate.year();
            }
            
            this.gasSubscription = (gasSubscription == null ? '' : gasSubscription.setScale(2) + ' € par mois');
            this.price = (price == null ? '' : price.setScale(4) + ' € / kWh');
            this.offerName = offerName;
            this.contractDownloadLink = contractDownloadLink;
            
            this.linkOldContracts = Page.AncientContracts.getURL() + '?ConType=gas';
            this.linkAttestationsReports = Page.AttestationsReports.getURL();
            this.linkCancelContracts = Page.CancelContract.getURL() + '?ConType=gas';
            if (accountId != null) {
                this.linkOldContracts += '&accountId=' + accountId;
                this.linkAttestationsReports += '?accountId=' + accountId;
                this.linkCancelContracts += '&accountId=' + accountId;
            }
            this.hasReports = hasReports;
        }
    }
    
    //Wrapper for Maintenance Contract
    public class MaintenanceContractWrapper{
        
        public String accountId{get;set;}
        
        public String title{get;set;}
        public String contractType{get;set;}
        public String boilerBrand{get;set;}
        public String endDate{get;set;}
        public String boilerModel{get;set;}
        public String lastBoilerCheckup{get;set;}
        public String offerName{get;set;}
        public String contractDownloadLink{get;set;}
        public String linkOldContracts{get;set;}
        public String linkAttestationsReports{get;set;}
        public String linkCancelContracts{get;set;}
        public String linkMyQuotes{get;set;}
        public Boolean hasReports{get;set;}
        
        public MaintenanceContractWrapper(String contractType, Date endDate, String boilerBrand, String boilerModel, Date lastBoilerCheckup, String offerName, String contractDownloadLink, String accountId){
            this.accountId = accountId;
            this.title = contractType;
            this.contractType = contractType;
            this.boilerBrand = boilerBrand;
            
            this.endDate = '';
            if (endDate != null) {
            	this.endDate = (endDate.day() < 10 ? '0' : '') + endDate.day() + '.';
            	this.endDate += (endDate.month() < 10 ? '0' : '') + endDate.month() + '.' + endDate.year();
            }
            
            this.boilerModel = boilerModel;
            
            if (lastBoilerCheckup != null) {
                this.lastBoilerCheckup = (lastBoilerCheckup.day() < 10 ? '0' : '') + lastBoilerCheckup.day() + '.';
                this.lastBoilerCheckup += (lastBoilerCheckup.month() < 10 ? '0' : '') + lastBoilerCheckup.month() + '.' + lastBoilerCheckup.year();
            }
            
            this.offerName = offerName;
            this.contractDownloadLink = contractDownloadLink;
            
            this.linkOldContracts = Page.AncientContracts.getURL() + '?ConType=maint';
            this.linkAttestationsReports = Page.AttestationsReports.getURL();
            this.linkCancelContracts = Page.CancelContract.getURL() + '?ConType=maint';
            if (accountId != null) {
                this.linkOldContracts += '&accountId=' + accountId;
                this.linkAttestationsReports += '?accountId=' + accountId;
                this.linkCancelContracts += '&accountId=' + accountId;
                this.linkMyQuotes = Page.MyQuotes.getUrl() + '?accountId=' + accountId;	
            }
            this.hasReports = (lastBoilerCheckup != null ? true : false);
        }
    }

    //Wrapper for Product-Only contract
    public class ProductContractWrapper
    {
    	public String title{get;set;}
    	public String contractDownloadLink{get;set;}
        public String linkAttestationsReports{get;set;}
        public Boolean hasReports{get;set;}

    	public ProductContractWrapper(String title, String contractDownloadLink, String accountId, Boolean hasReports)
    	{
    		this.title = title;
    		this.contractDownloadLink = contractDownloadLink;
            this.linkAttestationsReports = Page.AttestationsReports.getURL();
            if (accountId != null) {
                this.linkAttestationsReports += '?accountId=' + accountId;
            }
            this.hasReports = hasReports;
    	}
    }
    
    /**
	* [Map of Subscriptions with the Product Charge Price for a given Product Rate Plan Category. For each category, there should be no more than 1 price on the Subscription]
	* @param  listSubs [Zuora__Subscription__c for which the Prices are needed]
	* @param  category [Category__c of the zqu__ProductRatePlanCharge__c]
	* @return          [Map of Zuora__Subscription__c and the associated Price]
	*/
    private static Map<Id,Decimal> mapSubscriptionPrice(List<Zuora__Subscription__c> listSubs, String category) {
        Map<Id,Decimal> mapSubscriptionPrice = new Map<Id,Decimal>();
        //Get ProductRatePlanCharge Ids
        Set<String>  setProductRatePlanChargeIds = setProductRatePlanChargeIds(category);
        //Retrieve Prices for the resquested Subscriptions
        List<Zuora__SubscriptionProductCharge__c> listProductCharges = 	[
        																	SELECT Zuora__Price__c, Zuora__Subscription__c 
                                                                        	FROM Zuora__SubscriptionProductCharge__c
                                                                        	WHERE Zuora__ProductRatePlanChargeId__c IN:setProductRatePlanChargeIds
                                                                        	AND Zuora__Subscription__c IN:listSubs
                                                                        	AND Zuora__Price__c > 0 LIMIT 1000
                                                                    	];
        //map Each price to its subscription
        for (Zuora__SubscriptionProductCharge__c productCharge : listProductCharges) {
            mapSubscriptionPrice.put(productCharge.Zuora__Subscription__c, productCharge.Zuora__Price__c);
        }
        return mapSubscriptionPrice;
    }
    
    /**
	* [Returns the Set of zqu__ProductRatePlanCharge__c.zqu__ZuoraId__c for a given category]
	* @param  category [Category__c value]
	* @return          [Set of associated zqu__ProductRatePlanCharge__c.zqu__ZuoraId__c]
	*/
    private static Set<String> setProductRatePlanChargeIds(String category) {
        Set<String> setProductRatePlanChargeIds = new Set<String>();
        List<zqu__ProductRatePlanCharge__c> listRatePlanChargeConso =  [
        																	SELECT zqu__ZuoraId__c 
                                                                       		FROM zqu__ProductRatePlanCharge__c 
                                                                       		WHERE Category__c =:category LIMIT 1000
                                                                   		];
        for (zqu__ProductRatePlanCharge__c prpc : listRatePlanChargeConso) {
            if (prpc.zqu__ZuoraId__c != null) {
            	setProductRatePlanChargeIds.add(prpc.zqu__ZuoraId__c);
            }
        }
        return setProductRatePlanChargeIds;
    }

}