/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Test of the controller for MyBills page (SICo_MyBills_VFC)
History
14/09/2016     Gaël BURNEAU     Create version
------------------------------------------------------------*/
@isTest
private class SICo_MyBills_VFC_TEST {

    @testSetup static void initTestData() {

		Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
		insert account;
		SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite.IsActive__c = true;
		oSICoSite.PrincipalHeatingSystemType__c = 'Gaz';
		oSICoSite.SanitaryHotWaterType__c = 'Electrique';
		oSICoSite.NoOfOccupants__c = 4;
		oSICoSite.surfaceInSqMeter__c = 80;
		oSICoSite.InseeCode__c = '44109';
		oSICoSite.ConstructionDate__c = 1980;
		oSICoSite.PCE__c = 'TestPCE';
		insert oSICoSite;

        Contact personContact = [SELECT Id, Name FROM Contact WHERE IsPersonAccount = true AND AccountId = :account.Id];

        Opportunity opp = SICo_UtilityTestDataSet.createOpportunity('Test', account.Id, oSICoSite.Id);
        insert opp;

        zqu__Quote__c quote = SICo_UtilityTestDataSet.createZuoraQuote('Test', account.Id, personContact.Id, opp.Id);
        insert quote;

        Zuora__Subscription__c subscription = SICo_UtilityTestDataSet.createZuoraSubscription('Test', account.Id, quote.Id, '00', oSICoSite.Id);
        insert subscription;

        Zuora__ZInvoice__c invoice1 = SICo_UtilityTestDataSet.createZuoraInvoice(account.Id);
        Zuora__ZInvoice__c invoice2 = SICo_UtilityTestDataSet.createZuoraInvoice(account.Id);
        Zuora__ZInvoice__c invoice3 = SICo_UtilityTestDataSet.createZuoraInvoice(account.Id);
        insert new Zuora__ZInvoice__c[] {invoice1, invoice2, invoice3};

        insert SICo_UtilityTestDataSet.createAttachment(invoice1.id);
        insert SICo_UtilityTestDataSet.createAttachment(invoice2.id);
    }

    @isTest static void constructor_test() {

		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		PageReference pref = Page.MyBills;
	    pref.getParameters().put(SICo_Constants_CLS.URL_PARAM_ACCOUNTID, account.Id);
	    Test.setCurrentPage(pref);

	    SICo_MyBills_VFC controller = new SICo_MyBills_VFC();

		System.assertEquals(account.Id, controller.accountId);

        controller = new SICo_MyBills_VFC(account.Id);
	}
}