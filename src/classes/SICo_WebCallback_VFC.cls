/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test class for the SICo_CancelContract_VFC Controller 
Test Class:    SICo_WebCallback_VFC_TEST
History
30/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
public with sharing class SICo_WebCallback_VFC {

	public Id accountId{get;set;}

	public String lastName{get;set;}
	public String firstName{get;set;}
	public String phone{get;set;}
	public String motif{get;set;}
    public String subject{get;set;}
    public Boolean isguest{get;set;}

	public List<SICo_CaseMotif__mdt> listMotif{get;set;}
    public List<SICo_CaseObjet__mdt> listObjet{get;set;}

	List<Account> userAccount{get;set;}
    
    public String linkMyCases{get;set;}

	public SICo_WebCallback_VFC() {

        //Get AccountId
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);

        if(requestedAccountId != null)
            linkMyCases = Page.Cases.getURL() + '?accountId=' + requestedAccountId;
        else
            linkMyCases = Page.Cases.getURL();
                
        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        }
        this.isguest = false;
        if(UserInfo.getUserType() == 'Guest'){

            this.isguest = true;
        }

        if(accountId != null){
	        userAccount = [SELECT Id, Phone, FirstName, LastName
	        							FROM Account
	        							WHERE Id = :accountId
	        							LIMIT 1];

	        this.phone = userAccount.get(0).Phone;
	        this.firstName = userAccount.get(0).FirstName;
	        this.lastName = userAccount.get(0).LastName;

	    }

	    listMotif = [SELECT Id, MasterLabel, SalesforceLabel__c
                        FROM SICo_CaseMotif__mdt
                        LIMIT 1000];

        listObjet = [SELECT Id, SalesforceLabel__c, Case_Motif__c
                        FROM SICo_CaseObjet__mdt
                        WHERE Case_Motif__c = :listMotif.get(0).Id
                        LIMIT 1000];

    }

    public PageReference createCase(){

        Case newCase = new Case(
            RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + SICo_Constants_CLS.WEB_CALLBACK_CASE_RECORD_TYPE),
            Status = SICo_Constants_CLS.STATUS_PICKLIST_VALUE_NEW,
            Heading__c = this.motif,
            Description = this.getDescription(accountId != null),
            Origin = SICo_Constants_CLS.ORIGIN_PICKLIST_VALUE_WEB,
            IsWebCallBack__c = true,
            Subject__c = this.subject
        );
        
        insert newCase;

        PageReference pageRef = new PageReference(linkMyCases);
        pageRef.setRedirect(true);
        return pageRef;

    }

    public String getDescription(Boolean isConnected) {
    	return 'Prénom : ' + this.firstName + '\nNom : ' + this.lastName + '\nNuméro de téléphone à appeler : ' + this.phone;
    }

    public List<SelectOption> getObjects() {

        List<SelectOption> options = new List<SelectOption>();

        for(SICo_CaseObjet__mdt obj : listObjet)
            options.add(new SelectOption(obj.SalesforceLabel__c, obj.SalesforceLabel__c));
 
        return options;
    }
     public List<SelectOption> getMotifs() {
        List<SelectOption> options = new List<SelectOption>();

        for(SICo_CaseMotif__mdt motif : listMotif)
            options.add(new SelectOption(motif.SalesforceLabel__c, motif.SalesforceLabel__c));
 
        return options;
    }
    public PageReference cancelWebCallback(){
    	return null;
    }
    public PageReference doRedirect() {

        User oUser = [SELECT Username, AccountId FROM User WHERE Id = :UserInfo.getUserId()];

        if(UserInfo.getUserType() != 'Guest'){
            PageReference pageRef = new PageReference(SICo_Constants_CLS.CUSTOMER_SEND_MESSAGE_URL+'?callback=true&accountId='+accountId);
            return pageRef;
}
                else
            return null;
    }

    public PageReference updateObjects() {
        System.debug(motif);

        listObjet = [SELECT Id, SalesforceLabel__c, MasterLabel, Case_Motif__c
                    FROM SICo_CaseObjet__mdt
                    WHERE Case_Motif__c IN (SELECT Id
                                            FROM SICo_CaseMotif__mdt
                                            WHERE MasterLabel LIKE :motif)
                    LIMIT 1000];

        System.debug(listObjet.size());

        return null;
    }

}