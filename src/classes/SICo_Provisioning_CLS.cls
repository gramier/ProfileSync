/**
* @author Boris Castellani
* @date 14/12/2016
* @LastModify 24/01/2017
*
* @description Calcul provisioning after Zuora__SubscriptionProductFeature__c insert
*/
public with sharing class SICo_Provisioning_CLS {

  private static List<Sico_AdminTaskLink__mdt> linkedTasks;
  static {
    linkedTasks = [SELECT PrimaryTask__r.DeveloperName, RelatedTask__r.DeveloperName,
                                              RelatedTask__r.Parameters__c, DeveloperName, PrimaryTask__r.Domain__c 
                                              FROM Sico_AdminTaskLink__mdt];
  }

  public static void provisioning(List<Zuora__SubscriptionProductFeature__c> subscriptionFeatures){

    /* -------- 0.Create Map subFeaturesBySouscriptionId (Zuora__Subscription__c Id => list Zuora__SubscriptionProductFeature__c) -------- */
    /* -------- & Create Map subscriptionAccount (Zuora__Subscription__c Id => Account Id) -------- */
    final Map<Id,List<Zuora__SubscriptionProductFeature__c>> subFeaturesBySouscriptionId = new Map<Id,List<Zuora__SubscriptionProductFeature__c>>();
    final Map<Id,Id> subscriptionAccount = new Map<Id,Id>();
    final Set<String> featuresCode = new Set<String>();
    for(Zuora__SubscriptionProductFeature__c aFeature : subscriptionFeatures){
      subscriptionAccount.put(aFeature.Zuora__Subscription__c, aFeature.Zuora__Account__c);
      featuresCode.add(aFeature.Zuora__FeatureCode__c);
      if(!subFeaturesBySouscriptionId.containsKey(aFeature.Zuora__Subscription__c)){
        subFeaturesBySouscriptionId.put(aFeature.Zuora__Subscription__c,new List<Zuora__SubscriptionProductFeature__c>());
      }
      subFeaturesBySouscriptionId.get(aFeature.Zuora__Subscription__c).add(aFeature);
    }

    /* -------- 1.Create Map subscriptions (Zuora__Subscription__c Id => Zuora__Subscription__c) -------- */
    final Map<Id,Zuora__Subscription__c> subscriptions = new Map<id,Zuora__Subscription__c>(
      [SELECT Id, Zuora__SubscriptionNumber__c, Zuora__Zuora_Id__c, Zuora__QuoteNumber__c,  
       Zuora__Account__c, Zuora__OriginalId__c, Zuora__PreviousSubscriptionId__c, SiteId__c
       FROM Zuora__Subscription__c 
       WHERE Zuora__Account__c IN :subscriptionAccount.values()]
    );

    system.debug('### subscriptions: '+subscriptions);

    /* -------- 2.Create Map subscriptionsBySite (SICo_Site__c Id => List Zuora__Subscription__c) -------- */
    final Map<Id,List<Id>> subscriptionsBySite = new Map<Id,List<Id>>();
    final Set<String> quoteNumbers = new Set<String>();
    for(Zuora__Subscription__c aSubscription : subscriptions.values()){
      quoteNumbers.add(aSubscription.Zuora__QuoteNumber__c);
      if(!subscriptionsBySite.containsKey(aSubscription.SiteId__c)){
        subscriptionsBySite.put(aSubscription.SiteId__c, new List<Id>());
      }
      subscriptionsBySite.get(aSubscription.SiteId__c).add(aSubscription.Id);      
    }

    /* -------- 3.Create Map sites (SICo_Site__c Id =>  SICo_Site__c) -------- */
    /* -------- & Create Map casesBySite (SICo_Site__c Id => CaseType => List Case) -------- */
    /* -------- & Create Map tasksByCase (Case Id =>  List Task) -------- */
    final Map<Id,SICo_Site__c> sites = new Map<Id,SICo_Site__c>(
      [SELECT Id, (SELECT Id, ProvisioningType__c, ContactId, Site__c FROM Requetes__r) 
       FROM SICo_Site__c
       WHERE Id IN : subscriptionsBySite.keySet()]
    );

    final Map<Id,Map<String,Case>> casesBySite = new Map<Id,Map<String,Case>>();
    final Set<Id> casesId = new Set<Id>();
    for(SICo_Site__c aSite : sites.values()){
      if(aSite.Requetes__r.Size() > 0){
        if(!casesBySite.containsKey(aSite.Id)){
          casesBySite.put(aSite.Id, new Map<String,Case>());
        }
        for(Case aCase : aSite.Requetes__r){
          casesBySite.get(aSite.Id).put(aCase.ProvisioningType__c, aCase); 
          casesId.add(aCase.Id);
        }
      }
    }

    final Map<String,List<Task>> tasksByCase  = new Map<String,List<Task>>();
    for(Case aCase : [SELECT Id, ProvisioningType__c, ContactId, Site__c, (SELECT Id, WhatId, TaskId__c, Parameters__c, Component__c FROM Tasks)
                      FROM Case WHERE Id IN :casesId]){

      if(!tasksByCase.containsKey(aCase.ProvisioningType__c)){
        tasksByCase.put(aCase.ProvisioningType__c,new List<Task>());
      }
      for(Task aTask : aCase.Tasks){
        tasksByCase.get(aCase.ProvisioningType__c).add(aTask);   
      }
    }

    /* -------- 4.Create Map subscriptionOpportunity (Zuora__Subscription__c Id => zqu__Opportunity__c) -------- */
    /* -------- & Create Map subscriptionQuote (Zuora__Subscription__c Id => zqu__Quote__c) -------- */
    final Map<Id,Id> subscriptionOpportunity = new Map<Id,Id>();
    final Map<Id,zqu__Quote__c> subscriptionQuote = new Map<Id,zqu__Quote__c>(); 
    for(zqu__Quote__c aQuote : [SELECT Id, Zqu__BillToContact__c, Zqu__Opportunity__c, Zqu__Number__c, Zqu__Opportunity__r.Site__c, 
                                Zqu__Opportunity__r.ShippingMode__c, zqu__Opportunity__r.Situation__c,zqu__Opportunity__r.CHAMEntretienTimeSlot__c, 
                                zqu__Opportunity__r.CHAMInstallationTimeSlot__c, zqu__Opportunity__r.CHAMEntretienAppointmentDate__c,
                                zqu__Opportunity__r.CHAMInstallationAppointmentDate__c, zqu__Opportunity__r.Maintenance_Mode_de_facturation__c,
                                zqu__Opportunity__r.MaintenanceParts__c, zqu__Opportunity__r.CHAMAgencyEntretien__c, zqu__Opportunity__r.CHAMAgencyInstallation__c, 
                                zqu__Opportunity__r.CHAMAgencyInstallation__r.Zone__c, zqu__Opportunity__r.CHAMAgencyEntretien__r.Zone__c,
                                zqu__Opportunity__r.ShippingAddressZipCode__c, zqu__Opportunity__r.ShippingAddressAdditionToAddress__c,
                                zqu__Opportunity__r.ShippingAddressNumberStreet__c, zqu__Opportunity__r.ShippingAddressCountry__c,
                                zqu__Opportunity__r.ShippingAddressCity__c, zqu__Opportunity__r.DeliveryPointId__c, zqu__Opportunity__r.DeliveryComments__c

                                FROM Zqu__Quote__c 
                                WHERE zqu__Number__c IN :quoteNumbers]){

      for(Zuora__Subscription__c aSubscription : subscriptions.values()){
        if(aQuote.zqu__Number__c == aSubscription.Zuora__QuoteNumber__c){
          subscriptionOpportunity.put(aSubscription.Id,aQuote.zqu__Opportunity__c);
          subscriptionQuote.put(aSubscription.Id,aQuote);
        }
      }
    }

    /* -------- 5.Create Map featuresBySubFeatureCode (Sico_AdminFeature__mdt DeveloperName => Sico_AdminFeature__mdt) -------- */
    final Map<String,List<Sico_AdminFeature__mdt>> featuresBySubFeatureCode = new Map<String,List<Sico_AdminFeature__mdt>>();
    for(Sico_AdminFeature__mdt aFeature : [ SELECT ZuoraFeatureNumber__c, Tasks__c, Tasks__r.BlockingCBPayment__c, Tasks__r.Domain__c, 
                                            Tasks__r.EndPoint__c, Tasks__r.DeveloperName, Tasks__r.EndPointType__c, Tasks__r.ProvisioningCallOut__c, 
                                            Tasks__r.TaskOrder__c, Tasks__r.Parameters__c, Tasks__r.TaskSubject__c, Tasks__r.TaskTechnique__c, 
                                            Tasks__r.RecordType__c, Tasks__r.Cases__r.Product__c, Tasks__r.Cases__r.Heading__c, Tasks__r.Cases__r.Status__c, 
                                            Tasks__r.Cases__r.Subject__c, Tasks__r.Cases__r.ProvisioningType__c
                                            FROM Sico_AdminFeature__mdt
                                            WHERE ZuoraFeatureNumber__c IN :featuresCode]){

      if(!featuresBySubFeatureCode.containsKey(aFeature.ZuoraFeatureNumber__c)){
        featuresBySubFeatureCode.put(aFeature.ZuoraFeatureNumber__c,new List<Sico_AdminFeature__mdt>()); 
      }
      featuresBySubFeatureCode.get(aFeature.ZuoraFeatureNumber__c).add(aFeature);
    }

    /* -------- 6.Create Map featuresByCaseTypeBySiteId (SICo_Site__c Id => Map case => List Task) -------- */
    Map<Id,Map<String,List<Task>>> featuresByCaseTypeBySiteId = new Map<Id,Map<String,List<Task>>>();

    List<Case> caseInsert = new List<Case>();
    List<Task> taskInsert = new List<Task>();
    String caseType ='';
    Case theCase;
    Task theTask;
    Boolean isPresent;
    for(Id aSiteId : subscriptionsBySite.keySet()){
      if(!featuresByCaseTypeBySiteId.containsKey(aSiteId)){
        featuresByCaseTypeBySiteId.put(aSiteId,new Map<String,List<Task>>());
      }
      for(Id aSubscriptionId : subscriptionsBySite.get(aSiteId)) if(subFeaturesBySouscriptionId.containsKey(aSubscriptionId)) {
        for(Zuora__SubscriptionProductFeature__c aSubscriptionProductFeature : subFeaturesBySouscriptionId.get(aSubscriptionId)){

          for(Sico_AdminFeature__mdt aFeature : featuresBySubFeatureCode.get(aSubscriptionProductFeature.Zuora__FeatureCode__c)){
            isPresent = false;         
            caseType = aFeature.Tasks__r.Cases__r.ProvisioningType__c;
            if(!casesBySite.containsKey(aSiteId)){
              casesBySite.put(aSiteId, new Map<String,Case>());
            }
            if(!casesBySite.get(aSiteId).containsKey(aFeature.Tasks__r.Cases__r.ProvisioningType__c)){
              theCase = createCase(aFeature,subscriptions.get(aSubscriptionId),subscriptionQuote.get(aSubscriptionId));
              caseInsert.add(theCase);
              casesBySite.get(aSiteId).put(theCase.ProvisioningType__c,theCase);  
            }else{
              theCase = casesBySite.get(aSiteId).get(aFeature.Tasks__r.Cases__r.ProvisioningType__c);
            }
            if(!featuresByCaseTypeBySiteId.get(aSiteId).containsKey(theCase.ProvisioningType__c)){
              featuresByCaseTypeBySiteId.get(aSiteId).put(aFeature.Tasks__r.Cases__r.ProvisioningType__c, new List<Task>());              
            }
            if(tasksByCase.containsKey(caseType)){
              for(Task aTask : tasksByCase.get(caseType)){
                if(aTask.TaskId__c == aFeature.Tasks__r.DeveloperName){
                  isPresent = true;
                  break;
                }
              }
            }
            if(isPresent == false){
              theTask = createTask(aSubscriptionId,theCase,aFeature,subscriptionQuote.get(aSubscriptionId));
              taskInsert.add(theTask);
              featuresByCaseTypeBySiteId.get(aSiteId).get(casesBySite.get(aSiteId).get(aFeature.Tasks__r.Cases__r.ProvisioningType__c).ProvisioningType__c).add(theTask);
              if(!tasksByCase.containsKey(caseType)){
                tasksByCase.put(caseType, new List<Task>()); 
              }
              tasksByCase.get(caseType).add(theTask);     
            }
          }
        }
      }
    }

    /* -------- 7.Insert new Cases -------- */
    if(!caseInsert.isEmpty()){
      INSERT caseInsert;
    }
    for(Id aSiteId : subscriptionsBySite.keySet()){
      for(String aCase : featuresByCaseTypeBySiteId.get(aSiteId).keySet()){
        for(Task aTask : featuresByCaseTypeBySiteId.get(aSiteId).get(aCase)){
          aTask.WhatId= casesBySite.get(aTask.SiteID__c).get(aCase).Id;
        }            
      }
    }

    /* -------- 8.Add Relation Parameter for Task -------- */
    if(!tasksByCase.isEmpty()){
      Set<String> codePrimaryTask = new Set<String>();
      Map<String,Sico_AdminTaskLink__mdt> codeRelatedTask = new Map<String, Sico_AdminTaskLink__mdt>();

      for(String codeCase : tasksByCase.keySet()){
        for(Task aTask : tasksByCase.get(codeCase)){
          for(Sico_AdminTaskLink__mdt aLinkedTask : linkedTasks){
            if(aLinkedTask.PrimaryTask__r.DeveloperName == aTask.TaskId__c){
              codePrimaryTask.add(aTask.TaskId__c+';'+aLinkedTask.DeveloperName);
            }
            if(aLinkedTask.RelatedTask__r.DeveloperName == aTask.TaskId__c){
              codeRelatedTask.put(aLinkedTask.DeveloperName, aLinkedTask);
            }
          }
        }
        if(!codePrimaryTask.isEmpty()){
          for(Task aTask : tasksByCase.get(codeCase)){
            for(String aCode : codePrimaryTask){
              if(aCode.split(';')[0] == aTask.TaskId__c && codeRelatedTask.containsKey(aCode.split(';')[1])) {
                Sico_AdminTaskLink__mdt aLinkedTask = codeRelatedTask.get(aCode.split(';')[1]);
                if(aLinkedTask.RelatedTask__r.Parameters__c != null){
                  String field = aLinkedTask.PrimaryTask__r.Domain__c  == LABEL.SICo_Logistique
                  ? 'Component__c'
                  : 'Parameters__c';

                  List<String> components = aLinkedTask.RelatedTask__r.Parameters__c.split(';');
                  String oldComponent = String.valueOf(aTask.get(field));
                  aTask.put(field,oldComponent.split(';')[0] + ';' + components[components.size()-1]);
                }
              }
            }
          } 
        }    
      }
    }

    /* -------- 9.Insert new Tasks -------- */
    if(!taskInsert.isEmpty()){
      INSERT taskInsert;
    }
  }

  /* -------- PRIVATE METHODE -------- */    

  @TestVisible
  private static Case createCase(Sico_AdminFeature__mdt feature, Zuora__Subscription__c zuoraSub, Zqu__Quote__c quote){
    Case newCase = new Case(
      AccountId= zuoraSub.Zuora__Account__c,
      Site__c= quote.zqu__Opportunity__r.Site__c,
      ContactId= quote.zqu__BillToContact__c,
      Heading__c= feature.Tasks__r.Cases__r.Heading__c,
      Status= feature.Tasks__r.Cases__r.Status__c,
      ProvisioningType__c= feature.Tasks__r.Cases__r.ProvisioningType__c,
      Subject__c= feature.Tasks__r.Cases__r.Subject__c,
      Product__c= feature.Tasks__r.Cases__r.Product__c
      );
    return newCase;
  }

  @TestVisible
  private static Task createTask(Id aSubscriptionId, Case aCase, Sico_AdminFeature__mdt feature, Zqu__Quote__c quote){
    Task newTask = new Task(
      Subscription__c= aSubscriptionId,
      TaskId__c = feature.Tasks__r.DeveloperName,
      Subject = feature.Tasks__r.TaskSubject__c,
      WhatId = aCase.Id,
      WhoId = aCase.ContactId,
      RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Task_' + feature.Tasks__r.RecordType__c),
      IsVisibleInSelfService = true,
      Type = aCase.ProvisioningType__c
    );
    newTask.SiteID__c= aCase.Site__c;
    newTask.OrderFlux__c = feature.Tasks__r.TaskOrder__c;
    newTask.ProvisioningCallOut__c = feature.Tasks__r.ProvisioningCallOut__c;
    newTask.Tech_ProvisioningReady__c = !feature.Tasks__r.BlockingCBPayment__c;

    /* ----- Provisionning Cham ----- */
    if(feature.Tasks__r.Domain__c == LABEL.SICo_Cham){
      final List<String> parameters = feature.Tasks__r.Parameters__c.split(';');
      if(parameters[0] == 'MAINT') {
        newTask.CHAMAgency__c= quote.zqu__Opportunity__r.CHAMAgencyEntretien__c;
        newTask.Zone__c= quote.zqu__Opportunity__r.CHAMAgencyEntretien__r.Zone__c;
        if(parameters.size() > 1){
          newTask.PlannedRangedHours__c = quote.zqu__Opportunity__r.CHAMEntretienTimeSlot__c;
          newTask.InterventionPlannedDate__c = quote.zqu__Opportunity__r.CHAMEntretienAppointmentDate__c;          
        }
      }else if(parameters[0] == 'INST') {
        newTask.CHAMAgency__c= quote.zqu__Opportunity__r.CHAMAgencyInstallation__c;
        newTask.Zone__c= quote.zqu__Opportunity__r.CHAMAgencyInstallation__r.Zone__c;    
        if(parameters.size() > 1){
          newTask.PlannedRangedHours__c= quote.zqu__Opportunity__r.CHAMInstallationTimeSlot__c;
          newTask.InterventionPlannedDate__c= quote.zqu__Opportunity__r.CHAMInstallationAppointmentDate__c;
        }
     
      }
      newTask.InterventionType__c=  parameters.size() > 1
      ? parameters[1].trim() 
      : null;
    }

    /* ----- Provisionning Logistique ----- */ 
    if(feature.Tasks__r.Domain__c == LABEL.SICo_Logistique){
      newTask.ShippingMode__c = quote.zqu__Opportunity__r.ShippingMode__c;
      newTask.ShippingAddressZipCode__c = quote.zqu__Opportunity__r.ShippingAddressZipCode__c;
      newTask.ShippingAddressAdditionToAddress__c = quote.zqu__Opportunity__r.ShippingAddressAdditionToAddress__c;
      newTask.ShippingAddressNumberStreet__c = quote.zqu__Opportunity__r.ShippingAddressNumberStreet__c;
      newTask.ShippingAddressCountry__c = quote.zqu__Opportunity__r.ShippingAddressCountry__c;
      newTask.ShippingAddressCity__c = quote.zqu__Opportunity__r.ShippingAddressCity__c;
      newTask.DeliveryPointId__c = quote.zqu__Opportunity__r.DeliveryPointId__c;
      newTask.DeliveryComment__c = quote.zqu__Opportunity__r.DeliveryComments__c; 
      newTask.DeliveryStatus__c = LABEL.SICo_CommandValide;
      newTask.LogisticProcessType__c = LABEL.SICo_Exp;
      if(aCase.ProvisioningType__c == LABEL.SICo_StationConnecte){
        newTask.Component__c = feature.Tasks__r.Parameters__c;
      }
    }

    /* ----- Provisionning Station Connectée ----- */
    if(feature.Tasks__r.Domain__c == LABEL.SICo_ServiceConnecte){
      if(aCase.ProvisioningType__c == LABEL.SICo_StationConnecte){
        newTask.Parameters__c = feature.Tasks__r.Parameters__c;
      }     
    }

    /* ----- Provisionning GRDF ----- */
    if(feature.Tasks__r.Domain__c == LABEL.SICo_Grdf){
      if(aCase.ProvisioningType__c == LABEL.SICo_Gaz){
        newTask.InterventionType__c= quote.zqu__Opportunity__r.Situation__c;
      }
    }
    return newTask;
  }
}