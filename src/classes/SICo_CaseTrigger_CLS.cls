/**
* @author Castellani Boris
* @date 12/12/16
*
* @description SICo_CaseTrigger_CLS Trigger before insert
*/
public with sharing class SICo_CaseTrigger_CLS {

	public static void HandleBeforeInsert(List<Case> css){
		Map<Id,Id> mapCase = new Map<Id,Id>();
		for(Case cs : css){
			if(cs.LeadName__c != null && cs.RecordTypeId == SICo_Utility.m_RTbyDeveloperName.get('Case_CustomerDemand')){
				mapCase.put(cs.Id,cs.LeadName__c);
			}
		}
		if(!mapCase.isEmpty()){
			Map<Id,Lead> mapLead = new Map<Id,Lead>([SELECT Id,Email FROM Lead WHERE Id IN :mapCase.values()]);
			for(Case cs : css){
				if(cs.LeadName__c != null && mapLead.containsKey(cs.LeadName__c)){
					cs.Email__c = mapLead.get(cs.LeadName__c).Email;
				}
			}
		}
	}

}//END