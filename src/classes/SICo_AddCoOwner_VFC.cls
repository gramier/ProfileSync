/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Controller for the AddCoOwner page (CommunitiesLogin)
Test Class:    SICo_AddCoOwner_VFC_Test
History
09/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
public with sharing class SICo_AddCoOwner_VFC {
	//public Id accountId = null; //AccountId for filtering all data displayed on the page
	public Id accountId{get;set;}
	public Id siteId{get;set;}

	public SICo_AddCoOwner_VFC() {
		//Get AccountId
		String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
		try{
			this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
		}catch (exception e){
			System.debug('Unable to return accountId');
		}
	}

	@AuraEnabled
	public static SICo_Site__c getSite(Id accountId){
		SICo_Site__c theSite = null;
		List<SICo_Site__c> siteList = [SELECT Id, SiteName__c, Subscriber__c, IsSelectedInCustomerSpace__c 
																		FROM SICo_Site__c
																		WHERE Subscriber__c = :accountId AND IsSelectedInCustomerSpace__c = true];

		//If 1 single site is retrieved, use it
		if(siteList.size() == 1)
		{
				theSite = siteList[0];
		//if several Sites, use the one selected in Customer Space
		}else if(siteList.size() > 1){
				for(SICo_Site__c custSite : siteList){
						if(custSite.IsSelectedInCustomerSpace__c) theSite = custSite;
				}
		}//ENDIF siteList.size()

		return theSite;
	}

	/*
	* Insert a new beneficiary
	*/
	@AuraEnabled
	public static void insertBeneficiary(Id accountId, String salutation, String firstName, String lastName){
		// get the site
		SICo_Site__c theSite = null;
		List<SICo_Site__c> siteList = [SELECT Id, SiteName__c, Subscriber__c, IsSelectedInCustomerSpace__c 
																		FROM SICo_Site__c
																		WHERE Subscriber__c = :accountId  AND IsSelectedInCustomerSpace__c = true];
		
		system.debug('### ... siteList ' + siteList);
		//If 1 single site is active, use it
		if(siteList.size() == 1)
		{
				theSite = siteList[0];
		//if several Sites, use the one selected in Customer Space
		}else if(siteList.size() > 1){
				for(SICo_Site__c custSite : siteList){
						if(custSite.IsSelectedInCustomerSpace__c) { 
							theSite = custSite;
						}
				}
		}//ENDIF siteList.size()
		
		
		// create and insert a new beneficiary
		try{
			SICo_Beneficiary__c newBeneficiary = new SICo_Beneficiary__c(	Salutation__c = salutation,
				LastName__c = lastName,
				FirstName__c = firstName,
				Site__c = theSite.Id,
				Client__c = accountId);

			insert newBeneficiary;
		}
		catch(DmlException e){
			System.debug('############## An unexpected error has occurred: ' + e.getMessage());
		}
	}
}