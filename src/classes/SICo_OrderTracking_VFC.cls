/*------------------------------------------------------------
Author:        Cyril PENGAM 
Company:       Ikumbi
Description:   Controller for the OrderTracking page. Access is "Without Sharing" due to Shipping Address stored on Opportunity
Test Class:    SICo_OrderTracking_VFC_TEST
History
Sept. 2016      Cyril PENGAM     Create version
------------------------------------------------------------*/

public without sharing class SICo_OrderTracking_VFC {

    public Id accountId{get;set;}
    public Id siteId{get;set;}

    public OrderTrackingWrapper otw{get;set;}


    /**
     * [Contructor]
     */
    public SICo_OrderTracking_VFC() {

        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        doInit(requestedAccountId);
    }
    public SICo_OrderTracking_VFC(String requestedAccountId) {
        doInit(requestedAccountId);
    }

    private void doInit(String requestedAccountId) {
        try {
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        } catch (exception e){
            System.debug('Unable to return accountId');
        }
        //If AccountId found, retrieve associated personal information
        if (accountId != null) {
            //Query Account
            Account thisAccount = queryAccount();
            Case caseEquipment = queryCaseEquipment(accountId);
            System.debug('### caseEquipment : ' + caseEquipment);
            Case caseMaintenance = queryCaseMaintenance(accountId);
            System.debug('### caseMaintenance : ' + caseMaintenance); 

            //Query Site
            SICo_Site__c thisSite = querySite();
            if (thisSite != null) siteId = thisSite.Id;

            Case caseGas = queryCaseGas(accountId, siteId);


            //Create Wrapper with the data to be displayed on the page
            if (thisAccount != null) {
                /*otw = new OrderTrackingWrapper  (
                    thisAccount,
                    (caseEquipment != null && caseEquipment.tasks.size() > 0 ? caseEquipment : null),
                    (caseMaintenance != null && caseMaintenance.events.size() > 0 ? caseMaintenance : null),
                    (caseGas != null && caseGas.tasks.size() > 0 ? caseGas : null)
                );*/

                otw = new OrderTrackingWrapper(thisAccount);
            }

        }//ENDIF (accountId != null)
    }

    public class OrderTrackingWrapper{
        //components display
        public Boolean isEquipmentVisible {get;set;}
        public Boolean isMaintenanceVisible {get;set;}
        public Boolean isGasVisible {get;set;}

        //Equipment
        public String equipmentShippingMode {get;set;}
        public DateTime equipmentTaskCreatedDate {get;set;}
        //public DateTime equipmentTaskCreatedTime {get;set;}
        public DateTime equipmentEventDateAndTime {get;set;}

        public String equipmentEventEndDateAndTime {get;set;}

        public String equipmentCRSetupReturnCode {get;set;}
        public String equipmentRDVCancelled {get;set;}
        public String equipmentAdressLine1 {get;set;}
        public String equipmentAdressLine2 {get;set;}
        public String equipmentAdressLine3 {get;set;}
        public String equipmentAdressLine4 {get;set;}
        public String equipmentAdressLine5 {get;set;}
        public String equipmentDeliveryStatus {get;set;}
        public String equipmentDeliveryTrackingNumber {get;set;}
        public String equipmentDeliveryTrackingURL {get;set;}

        //Maintenance
        public String maintenanceCRSetupReturnCode {get;set;}
        public String maintenanceRDVCancelled {get;set;}
        public DateTime maintenanceStartDateTime {get;set;} 

        public String maintenanceEndDateTime {get;set;}   

        public DateTime maintenanceCreatedDate {get;set;}
        //public DateTime maintenanceCreatedTime {get;set;}
        public String maintenanceLastStatus {get;set;}
        public Boolean maintenanceIsStatusClosed {get;set;}

        //Gas
        public String gasOmegaRequestType {get;set;}
        public DateTime gasCreatedDate {get;set;}
        //public DateTime gasCreatedTime {get;set;}
        public String gasStatus {get;set;}
        public DateTime gasInterventionPlannedDate {get;set;}
        public String gasRequestState {get;set;}

        // Develivery Statuses for equipment
        private List<String> deliveryStatusesRelais = new List<String>{ 
            System.Label.SICo_CommandeValidee,
            System.Label.SICo_EnPreparation, 
            System.Label.SICo_MisADispoTransporteur,
            System.Label.SICo_EnCoursAcheminement,
            System.Label.SICo_DispoRelaisColis, 
            System.Label.SICo_Retire
        }; // TODO move behaviour in custom metadata or at least in Custom setting
        private List<String> deliveryStatusesNoInstallNoRelais = new List<String>{ 
            System.Label.SICo_CommandeValidee,
            System.Label.SICo_EnPreparation, 
            System.Label.SICo_MisADispoTransporteur,
            System.Label.SICo_EnCoursAcheminement, 
            System.Label.SICo_Livre 
        }; // TODO move behaviour in custom metadata or at least in Custom setting
     
        public Integer equipmentStatusIndex { get; set; }

        /*
        * Initialise an OrderTrackingWrapper
        * @Param : the account for which the OrderTrackingWrapper is initialised
        */
        public OrderTrackingWrapper(Account account){
        
            final Set<String> adminTasks = new Set<String>();

            final Map<String, CommunityObjectsManagement__mdt> commObjectManag = new Map<String, CommunityObjectsManagement__mdt>();
            for(CommunityObjectsManagement__mdt c : [SELECT Code__c, Record_Type__c, Category__c, Type__c
                                                    FROM CommunityObjectsManagement__mdt
                                                    WHERE IsVisibleOrderTracking__c = true]){
                commObjectManag.put(c.Code__c, c);
                adminTasks.add(c.Code__c);
            }

            final List<Task> allTasks = SICo_DashboardActivities_CLS.getAllTasksFromAccount(account.Id, adminTasks);

            equipmentStatusIndex = -1;

            System.debug('isEmpty' + allTasks.isEmpty());

            if(!allTasks.isEmpty()){

                //Task t = allTasks.get(0);

                for(Task t : allTasks){

                    if(commObjectManag.get(t.TaskId__c).Type__c == SICo_Constants_CLS.TASK_ACTIVITY_TYPE_GAZ){
                        System.debug('############Gaz');
                        isGasVisible = true;
                        gasOmegaRequestType = '';
                        if (t.InterventionType__c != null) {
                            if (t.InterventionType__c == System.Label.SICo_OmegaMES) {
                                gasOmegaRequestType = '1';
                            } else if (t.InterventionType__c == System.Label.SICo_OmegaCHF) {
                                gasOmegaRequestType = '2';
                            }
                        }
                        gasCreatedDate = t.CreatedDate;  
                        gasStatus = (t.Status != null) ? t.Status : '';
                        gasInterventionPlannedDate = (t.InterventionPlannedDate__c != null) ? t.InterventionPlannedDate__c  : null;
                        gasRequestState = (t.RequestState__c != null) ? t.RequestState__c : ''; 
                    }
                    else if(commObjectManag.get(t.TaskId__c).Type__c == SICo_Constants_CLS.TASK_ACTIVITY_TYPE_EQUIPMENT){
                        System.debug('############Equip');
                        isEquipmentVisible = true;


                        //Case Fields
                        equipmentAdressLine2 = (t.ShippingAddressNumberStreet__c != null) ? t.ShippingAddressNumberStreet__c : '';

                        equipmentAdressLine3 = '';

                        equipmentAdressLine3 = ((t.ShippingAddressBuilding__c  != null) ? t.ShippingAddressBuilding__c : '');
                        //add a comma if needed
                        equipmentAdressLine3 += ((t.ShippingAddressBuilding__c != null && t.ShippingAddressFlat__c  != null) ? ', ' : '');
                        equipmentAdressLine3 += ((t.ShippingAddressFlat__c  != null) ? t.ShippingAddressFlat__c : '');
                        //add a comma if needed
                        equipmentAdressLine3 += (((t.ShippingAddressBuilding__c != null || t.ShippingAddressFlat__c != null) && t.ShippingAddressStairs__c  != null) ? ', ' : '');
                        equipmentAdressLine3 += ((t.ShippingAddressStairs__c  != null) ? t.ShippingAddressStairs__c : '');
                        //add a comma if needed
                        equipmentAdressLine3 += (((t.ShippingAddressBuilding__c != null || t.ShippingAddressFlat__c != null || t.ShippingAddressStairs__c != null) && t.ShippingAddressFloor__c  != null) ? ', ' : '');
                        equipmentAdressLine3 += ((t.ShippingAddressFloor__c  != null) ? t.ShippingAddressFloor__c : '');

                        equipmentAdressLine4 = (t.ShippingAddressAdditionToAddress__c != null) ? t.ShippingAddressAdditionToAddress__c : '';

                        equipmentAdressLine5 = ((t.ShippingAddressZipCode__c  != null) ? t.ShippingAddressZipCode__c : '');
                        //add a comma if needed
                        equipmentAdressLine5 += ((t.ShippingAddressZipCode__c != null && t.ShippingAddressCity__c  != null) ? ', ' : '');
                        equipmentAdressLine5 += ((t.ShippingAddressCity__c  != null) ? t.ShippingAddressCity__c : '');


                        //Task Fields
                        equipmentShippingMode = '';
                        equipmentTaskCreatedDate = t.CreatedDate; 
                        equipmentAdressLine1 = (t.DeliveryPointId__c != null) ? t.DeliveryPointId__c : '';
                        equipmentDeliveryStatus = (t.DeliveryStatus__c != null) ? t.DeliveryStatus__c : '';
                        equipmentDeliveryTrackingNumber = (t.DeliveryTrackingNumber__c != null) ? t.DeliveryTrackingNumber__c : '';
                        equipmentDeliveryTrackingURL = (t.DeliveryTrackingURL__c != null) ? t.DeliveryTrackingURL__c : '';
                        if (t.ShippingMode__c != null){
                            if (t.ShippingMode__c == System.Label.SICo_Installateur){
                                equipmentShippingMode = '1';
                            } else if (t.ShippingMode__c ==  System.Label.SICo_RelaisColis){
                                equipmentShippingMode = '2';
                                equipmentStatusIndex = getCurrentStatusIndex(equipmentDeliveryStatus, deliveryStatusesRelais);   
                            } else {
                                equipmentShippingMode = '3';
                                equipmentStatusIndex = getCurrentStatusIndex(equipmentDeliveryStatus, deliveryStatusesNoInstallNoRelais);    
                            }
                        }

                        equipmentEventDateAndTime = (t.InterventionPlannedDate__c != null) ? t.InterventionPlannedDate__c : null; 
                        equipmentEventEndDateAndTime = (t.PlannedRangedHours__c != null) ? t.PlannedRangedHours__c : null;
                        equipmentCRSetupReturnCode = (t.CRSetupReturnCode__c != null) ? t.CRSetupReturnCode__c : '';
                        equipmentRDVCancelled = (t.RequestState__c  !=  null) ? (t.RequestState__c == 'Annulée') + '' : '';

                    }
                    else if(commObjectManag.get(t.TaskId__c).Type__c == SICo_Constants_CLS.TASK_ACTIVITY_TYPE_MAINTENANCE){

                        System.debug('############Maint');
                        isMaintenanceVisible = true;
                        maintenanceCRSetupReturnCode = (t.CRSetupReturnCode__c != null) ? t.CRSetupReturnCode__c : '';
                        maintenanceRDVCancelled = (t.RequestState__c  !=  null) ? (t.RequestState__c == 'Annulée') + '' : '';
                        maintenanceStartDateTime = (t.InterventionPlannedDate__c != null) ? t.InterventionPlannedDate__c : null; 
                        maintenanceEndDateTime = (t.PlannedRangedHours__c != null) ? t.PlannedRangedHours__c : null; 
                        maintenanceCreatedDate = t.CreatedDate;  
                        maintenanceLastStatus = '';
                        System.debug('###########' + maintenanceRDVCancelled + ' ' + maintenanceCRSetupReturnCode);
                        if (maintenanceRDVCancelled != null && maintenanceRDVCancelled == 'true') {
                            maintenanceIsStatusClosed = true;
                            maintenanceLastStatus = System.Label.SICo_Annule;
                        } else if (maintenanceCRSetupReturnCode != null && maintenanceCRSetupReturnCode != '') {
                            maintenanceLastStatus = SICo_OrderTracking_VFC.getStatusFromCRSetupReturnCode(maintenanceCRSetupReturnCode);
                            maintenanceIsStatusClosed = true;                       
                        }

                    }

                }
            }

        }

        /*
        * Returns the index of the current stage in the list of the stages
        */
        private Integer getCurrentStatusIndex(String pDeliveryStatus, List<String> pDeliveryStatusList) {
            Integer vCurrentIndex = -1;
            if (pDeliveryStatus == null || pDeliveryStatus == '') {
                return vCurrentIndex;
            }
            for (Integer i = 0; i < pDeliveryStatusList.size(); i++) {
                if (pDeliveryStatusList[i] == pDeliveryStatus) {
                    vCurrentIndex = i;
                    return vCurrentIndex;
                }
                vCurrentIndex = i;
            } 
            vCurrentIndex++;

            return vCurrentIndex;
        }
    }

/**** HELPER METHODS ****/
    
    private Account queryAccount(){
        return [SELECT Id
                FROM Account
                WHERE Id =:accountId AND IsPersonAccount = true LIMIT 1];
    }

    /**
    * Get the status linked to the return code from the custom mdt
    * The code is not stored in the name  because length could be > 40
    * @Param : the code for which the status is asked
    */
    public static String getStatusFromCRSetupReturnCode(String pCode) {
        if (String.isBlank(pCode)) {
            return null;
        }
        String vStatus = null;
        List<SICo_CaseRetourStatut__mdt> vStatusReturnsMapping = [SELECT DeveloperName, Code_retour__c, Status__c FROM SICo_CaseRetourStatut__mdt];
        for (SICo_CaseRetourStatut__mdt vCurrentMapping : vStatusReturnsMapping) {  
            if (vCurrentMapping.Code_retour__c == pCode) {
                return vCurrentMapping.Status__c;
            }
        }
        

        return vStatus;
    }
 
    private Case queryCaseEquipment(Id accountId){
        List<Case> lsCases = 
                [SELECT Id,
                        Opportunity__r.ShippingAddressNumberStreet__c,
                        Opportunity__r.ShippingAddressBuilding__c,
                        Opportunity__r.ShippingAddressFlat__c,
                        Opportunity__r.ShippingAddressStairs__c,
                        Opportunity__r.ShippingAddressFloor__c,
                        Opportunity__r.ShippingAddressAdditionToAddress__c,
                        Opportunity__r.ShippingAddressZipCode__c,
                        Opportunity__r.ShippingAddressCity__c,
                        (
                            SELECT Id, Subject, ShippingMode__c, Status, InterventionPlannedDate__c, RequestState__c, CreatedDate,
                                    InterventionType__c, DeliveryPointId__c, DeliveryStatus__c, DeliveryTrackingNumber__c, DeliveryTrackingURL__c
                            FROM Tasks 
                            WHERE (Subject = :System.Label.SICo_PackStationEnergie OR Subject = :System.Label.SICo_PackStationEnergieThermo)
                            ORDER BY CreatedDate DESC
                            LIMIT 1
                        ),
                        (
                            SELECT Id, ActivityDateTime, Subject, CRSetupReturnCode__c, RDVCancelled__c, StartDateTime, EndDateTime
                            FROM Events 
                            WHERE (IsCHAMinstallation__c = true)
                            ORDER BY CreatedDate DESC
                            LIMIT 1
                        )
                FROM Case
                WHERE AccountId =:accountId
                AND RecordType.DeveloperName = :System.Label.SICo_CaseRTSystem
                AND ProvisioningType__c = :System.Label.SICo_ProvisionningObjet
                AND IsClosed = false
                ORDER BY CreatedDate DESC
                LIMIT 1];
        Case caseToReturn = null;
        if (lsCases.size() > 0){
            caseToReturn = lsCases.get(0);
        }
        return caseToReturn;
    }
    private Case queryCaseMaintenance(Id accountId){
        List<Case> lsCases = 
                [SELECT Id,
                (
                    SELECT Id, Subject, CRSetupReturnCode__c, RDVCancelled__c, CreatedDate, ActivityDateTime, StartDateTime, EndDateTime
                    FROM Events 
                    WHERE (Subject = :System.Label.SICo_RDVCHAM)
                    ORDER BY CreatedDate DESC
                    LIMIT 1
                )
                FROM Case
                WHERE AccountId =:accountId
                AND RecordType.DeveloperName = :System.Label.SICo_CaseRTSystem
                AND ProvisioningType__c = :System.Label.SICo_ProvisionningMaintenance
                AND IsClosed = false
                ORDER BY CreatedDate DESC
                LIMIT 1];
        Case caseToReturn = null;
        if (lsCases.size() > 0){
            caseToReturn = lsCases.get(0);
        }
        return caseToReturn;
    }

    private Case queryCaseGas(Id accountId, Id siteId){ 
        List<Case> lsCases = new List <Case>();
        //if (sitePce != null){
        if (siteId != null) {
            lsCases = 
                [SELECT Id,
                (
                    SELECT Id, Subject, ShippingMode__c, InterventionType__c, CreatedDate, Status, InterventionPlannedDate__c, RequestState__c
                    FROM Tasks 
                    WHERE (InterventionType__c = :System.Label.SICo_OmegaMES OR InterventionType__c = :System.Label.SICo_OmegaCHF)
                    AND NotTakenByCRC__c = false 
                    //AND SiteID__c = :siteId (SiteID__c is not populated in MVP)
                    ORDER BY CreatedDate DESC
                    LIMIT 1
                )
                FROM Case
                WHERE AccountId =:accountId
                AND RecordType.DeveloperName = :System.Label.SICo_CaseRTSystem
                AND ProvisioningType__c = :System.Label.SICo_ProvisionningGaz
                AND IsClosed = false
                ORDER BY CreatedDate DESC
                LIMIT 1]; 

            Case caseToReturn = null;
            if (lsCases.size() > 0){
                caseToReturn = lsCases.get(0);
            }
            return caseToReturn;
        }
        else {
            return null;
        }
    }

    /*
    * Returns the active site linked to the account ID 
    * accountId member must not be null
    */
    private SICo_Site__c querySite(){
        SICo_Site__c theSite = null;
        List <SICo_Site__c> siteList = [SELECT Id,
                                        PCE__c,
                                        IsSelectedInCustomerSpace__c,
                                        ProfileCompletion__c,
                                        NumberStreet__c,
                                        Floor__c,
                                        Stairs__c,
                                        AdditionToAddress__c,
                                        Building__c,
                                        Flat__c,
                                        PostalCode__c,
                                        City__c,
                                        Country__c,
                                        ResidenceType__c,
                                        HousingType__c,
                                        IsArrangedRoofPresent__c,
                                        IsFloorPresent__c,
                                        IsVerandaPresent__c,
                                        IsJointHouse__c,
                                        HousingLocationType__c,
                                        IsCornerApartment__c,
                                        IsWindowRestored__c,
                                        IsWallRestored__c,
                                        IsArrangedRoofRestored__c,
                                        surfaceInSqMeter__c,
                                        ConstructionDate__c,
                                        NoOfOccupants__c,
                                        PrincipalHeatingSystemType__c,
                                        SanitaryHotWaterType__c,
                                        GasSource__c,
                                        IsCeramicGlassPlatePresent__c,
                                        IsWineCellarPresent__c,
                                        hasSwimmingPool__c,
                                        IsAquariumPresent__c,
                                        ElectricityTariffOption__c,
                                        IsACPresent__c,
                                        SubscribedPower__c
                                        FROM SICo_Site__c
                                        WHERE Subscriber__c =:accountId LIMIT 100];
        //If 1 single site is active, use it
        if (siteList.size() == 1) {
            theSite = siteList[0];
        //if several Sites, use the one selected in Customer Space
        } else if(siteList.size() > 1) {
            for (SICo_Site__c custSite : siteList) {
                if (custSite.IsSelectedInCustomerSpace__c) {
                    theSite = custSite;
                }
            }
        }//ENDIF siteList.size()

        return theSite;
    }


} //END SICo_OrderTracking_VFC()