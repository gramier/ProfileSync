/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test SICo_FluxSubscription
History
05/07/2016      Boris Castellani     Create version
11/07/2016      Boris Castellani     Modify Name & dataSet for method
------------------------------------------------------------*/
@isTest
private class SICo_FluxSubscription_TEST {

  @isTest static void calloutFluxSubscription() {
      // Set mock callout class
      CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
   	  insert boomiConfig;
      Test.setMock(HttpCalloutMock.class, new SICo_FluxSubscription_MOCK());
      // This causes a fake response to be sent
      // from the class that implements HttpCalloutMock.
      Test.startTest();
      HttpResponse response = SICo_Utility.getCallout('POST','https://test.connect.boomi.com/ws/rest/connected-services/edelia-iot/v1/subscriptions','{'+
      '\"Action\": [{'+
      '       \"personId\": \"PER00001\",'+
      '       \"siteId\": \"SITE-0001\",'+
      '       \"activityId\": \"0013E0000039aVI\",'+
      '       \"endPoint\": \"https://localhost\",'+
      '       \"externalOfferCode\": \"0013E0000039aXI\",'+
      '       \"SubscriptionRequest\": {'+
      '           \"ts\": \"2015-11-05T08:15:30-05:00\",'+
      '           \"siteElecExternalId\": \"SITE-0001\",'+
      '           \"siteGasExternalId\": \"SITE-0002\",'+
      '           \"person\": {'+
      '                   \"title\": \"Madame\",'+
      '                   \"firstName\": \"Scarlett\",'+
      '                   \"lastName\": \"Johansson\",'+
      '                   \"address\": {'+
      '                           \"lines\": [\"11 rue de la Paix\"],'+
      '                           \"zipCode\": \"92000\",'+
      '                           \"city\": \"Courbevoie\",'+
      '                           \"country\": \"France\"'+
      '                   },'+
      '                   \"email\": \"sjohansson@test.com\",'+
      '                   \"phones\": {'+
      '                       \"portable\": \"+33643293040\",'+
      '                       \"principal\": \"+33353293041\"'+
      '                   }'+
      '           },'+
      '           \"personParameters\": {'+
      '           },'+
      '           \"siteAddress\": {'+
      '                   \"lines\": [\"15 rue de Paris\"],'+
      '                   \"zipCode\": \"92000\",'+
      '                   \"city\": \"Courbevoie\",'+
      '                   \"country\": \"France\"'+
      '           },'+
      '           \"siteParameters\": {'+
      '               },'+
      '           \"subscribedOfferExternalId\": \"BILAN_CONSO_THERM\",'+
      '           \"subscribedOfferParameters\": {'+
      ''+
      '           }'+
      '       }'+
      '   }]'+
      '}');
      // Verify that the response received contains fake values
      String contentType = response.getHeader('Content-Type');
      System.assert(contentType == 'application/json');
      System.assertEquals(200, response.getStatusCode());
      String json = response.getBody();
      SICo_FluxSubscriptionModel.ReturnRequest fluxReturnRequest = SICo_FluxSubscriptionModel.parseReturnRequest(json);
      String awaitedJSON = 'ReturnRequest:[response=(Responce:[activityId=PER00001, errorDescription=0013E0000039aVI, statusCode=null], Responce:[activityId=PER00001, errorDescription=0013E0000039aVI, statusCode=null])]';
      //System.assertEquals(String.valueof(fluxReturnRequest), awaitedJSON);
      Test.stopTest();

      }



//END
}