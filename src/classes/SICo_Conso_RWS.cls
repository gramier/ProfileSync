/**********************************************************************
 * 
 *
 * @url: /services/apexrest/conso
 * @data:
 *  {
        "categorie_conso" : "<C1|C2|C3>",
        "zone_distribution" : "<T1|T2>"
    }
*************************************************************************/
@RestResource(urlMapping='/Conso')
global with sharing class SICo_Conso_RWS {
    @HttpGet
    global static void getCategorie() {
    	Integer conso = Integer.valueOf(RestContext.request.params.get( 'conso' ));
    	RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf('{"categorie_conso":"'+ (conso > 6000 ? conso > 11000 ? 'C3' : 'C2' : 'C1') +'","zone_distribution":"'+ (conso > 6000 ? 'T2' : 'T1') + '"}');
    }
}