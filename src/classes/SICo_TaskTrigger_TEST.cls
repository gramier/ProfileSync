/**
* @author Boris Castellani
* @date 26/10/2016
*
* @description TEST SICo_TaskTrigger_CLS Call by trigger
*/

@isTest
private class SICo_TaskTrigger_TEST {

    static testMethod void handleBeforeInsert() {

     	//DataSet
		Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Cham TEST','Agency');
		agency.AgencyNumber__c = 'ANUMBER';
		Insert agency;

    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;

    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    	site.CHAMAgencyEntretien__c = agency.Id;
    	Insert site;

		Case myCase = new Case(ProvisioningType__c = 'Depannage', Site__c= site.Id, AccountId= acc.Id);
    	Insert myCase;

		// TEST & ASSERT
		Test.startTest();

    		Task myTask = SICo_UtilityTestDataSet.createTask('', 'Devis Accepté', myCase.Id, 'ChamDemand', false);
    		myTask.CHAMAgency__c = agency.Id;
    		Insert myTask;

		Test.stopTest();

		Task testTask = [SELECT Id, Zone__c FROM Task Limit 1];

		//Assert
        //System.assertEquals(testTask.Zone__c, 'Zone 1');

    }

    static testMethod void checkStatus() {
        Case aCase = new Case(Subject='test');
        insert aCase;

        Task aTask1 = new Task(WhatId=aCase.id,Status='Fermée');
        Task aTask2 = new Task(WhatId=aCase.id,Status='Fermée');

        insert new List<Task>{aTask1,aTask2};

        aTask2.Status = 'Ouverte';
        update aTask2;

    }
}