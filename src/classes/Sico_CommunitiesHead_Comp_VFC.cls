/*------------------------------------------------------------
Author:        Selim BEAUJOUR
Company:       Ikumbi
Description:   Controller for Custom Settings of the live agent in Component Sico_CommunitiesHead_COMP 
Test Class:    Sico_CommunitiesHead_COMP_VFP_TEST
History
11/1O/2016      Selim BEAUJOUR      Create version
------------------------------------------------------------*/
public with sharing class Sico_CommunitiesHead_Comp_VFC {

	public String id1 {get;set;}
	public String id2 {get;set;}
	public String urlInit {get; set;}
	public String urlJS {get;set;}
	public String idshowWhenOnline {get;set;}
	public Id accountId{get;set;}

	public Sico_CommunitiesHead_Comp_VFC() {
		String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
			try{
				this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
			}catch (exception e){
				System.debug('Unable to return accountId');
			}
		
		
		SICo_Live_Agent_Setting__c liveAgent = SICo_Live_Agent_Setting__c.getInstance();
		this.id1 = liveAgent.IdLiveAgentInit1__c;
		this.id2 = liveAgent.IdLiveAgentInit2__c;
		this.urlInit = liveAgent.URLInit__c;
		this.idshowWhenOnline = liveAgent.IdShow__c;
	}
}