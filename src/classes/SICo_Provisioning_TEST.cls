/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test SICo_Provisioning & SICo_ProvisioningTrigger
History
28/06/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
private class SICo_Provisioning_TEST {

  @testSetup
  private static void setup(){
    insert SICo_UtilityTestDataSet.createChamConfig();
    insert SICo_UtilityTestDataSet.createCustomSetting();
    insert SICo_UtilityTestDataSet.createConfig_Boomi();
    insert SICo_UtilityTestDataSet.create_GrDF_Config();

    Account account = SICo_UtilityTestDataSet.createAccount('Luke','Skywalker','PersonAccount');
    insert account;
    // Create Site
    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    insert site;
    // Create Opportunity
    Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('Skywalker_OP01',account.Id,Site.Id);
    insert opportunity;
    // Create zuora quote
    zqu__Quote__c zuoraquote = SICo_UtilityTestDataSet.createZuoraQuote('Skywalker01',account.Id,account.PersonContactId,opportunity.id, site.id);
    insert zuoraquote;
    // Create zuora subscription
    zqu__Quote__c quote= [SELECT zqu__Number__c 
                          FROM zqu__Quote__c 
                          WHERE id= :zuoraquote.id LIMIT 1];

    Zuora__Subscription__c zuoraSubscription = SICo_UtilityTestDataSet.createZuoraSubscription('Skywalker',account.id,zuoraquote.id,quote.zqu__Number__c,site.id);
    insert zuoraSubscription;
  }

  static testMethod void createsub() {

    Account account= [SELECT Id, FirstName, PersonContactId 
                      FROM Account WHERE FirstName = 'Luke'];

    Zuora__Subscription__c zuoraSubscription = [SELECT Id, Zuora__Account__c, Zuora__SubscriptionNumber__c, SiteId__c
                                                FROM Zuora__Subscription__c];

    Test.startTest();
      // Create zuora subscription product feature
      List<Zuora__SubscriptionProductFeature__c> productFeatureList = new List<Zuora__SubscriptionProductFeature__c>();
      productFeatureList.add(SICo_UtilityTestDataSet.createZuoraSubscriptionProductFeature('Fourniture de gaz','Gaz',account.id,zuoraSubscription.id));
      productFeatureList.add(SICo_UtilityTestDataSet.createZuoraSubscriptionProductFeature('O\'quilibre','Oquilibre',account.id,zuoraSubscription.id));
      insert(productFeatureList);
    Test.stopTest();

  }

  static testMethod void provisioning(){
    Account account= [SELECT Id, FirstName, PersonContactId 
                      FROM Account WHERE FirstName = 'Luke'];

    Zuora__Subscription__c zuoraSubscription = [SELECT Id, Zuora__Account__c, Zuora__SubscriptionNumber__c, SiteId__c
                                                FROM Zuora__Subscription__c];

    Zqu__Quote__c quotes = [SELECT zqu__Number__c, zqu__Opportunity__c, zqu__BillToContact__c, zqu__Opportunity__r.Site__c,
                            zqu__Opportunity__r.CHAMAgencyEntretien__c, zqu__Opportunity__r.CHAMInstallationAppointmentDate__c,
                            zqu__Opportunity__r.CHAMInstallationTimeSlot__c,zqu__Opportunity__r.CHAMEntretienAppointmentDate__c,
                            zqu__Opportunity__r.CHAMEntretienTimeSlot__c,zqu__Opportunity__r.ShippingMode__c
                            FROM Zqu__Quote__c];

    quotes.zqu__Opportunity__r.CHAMInstallationAppointmentDate__c = Date.today();           
    quotes.zqu__Opportunity__r.CHAMInstallationTimeSlot__c = '10:00 - 12:00';
    quotes.zqu__Opportunity__r.CHAMEntretienAppointmentDate__c = Date.today();           
    quotes.zqu__Opportunity__r.CHAMEntretienTimeSlot__c = '10:00 - 12:00';
    quotes.zqu__Opportunity__r.CHAMAgencyEntretien__c = account.Id;
    quotes.zqu__Opportunity__r.ShippingMode__c = 'Colissimo';
    Update quotes.zqu__Opportunity__r;

    List<Sico_AdminFeature__mdt> featuresList= [SELECT ZuoraFeatureNumber__c, Tasks__c, Tasks__r.BlockingCBPayment__c, Tasks__r.Domain__c, 
                                                Tasks__r.EndPoint__c, Tasks__r.DeveloperName, Tasks__r.EndPointType__c, Tasks__r.ProvisioningCallOut__c, 
                                                Tasks__r.TaskOrder__c, Tasks__r.Parameters__c, Tasks__r.TaskSubject__c, Tasks__r.TaskTechnique__c, 
                                                Tasks__r.RecordType__c, Tasks__r.Cases__r.Product__c, Tasks__r.Cases__r.Heading__c, Tasks__r.Cases__r.Status__c, 
                                                Tasks__r.Cases__r.Subject__c, Tasks__r.Cases__r.ProvisioningType__c
                                                FROM Sico_AdminFeature__mdt];

    Map<String,Sico_AdminFeature__mdt> features = new Map<String,Sico_AdminFeature__mdt>();
    Map<String,Sico_AdminFeature__mdt> casefeatures = new Map<String,Sico_AdminFeature__mdt>();
    for(Sico_AdminFeature__mdt aFeature : featuresList){
      features.put(aFeature.Tasks__r.DeveloperName, aFeature);
      casefeatures.put(aFeature.Tasks__r.Domain__c, aFeature);
    }


    List<Case> cases = new List<Case>();
    cases.add(SICo_Provisioning_CLS.createCase(casefeatures.get('Logistique'), zuoraSubscription, quotes));
    cases.add(SICo_Provisioning_CLS.createCase(casefeatures.get('Services connectés'), zuoraSubscription, quotes));
    cases.add(SICo_Provisioning_CLS.createCase(casefeatures.get('Cham'), zuoraSubscription, quotes));
    Insert cases;

    Test.startTest();
      Task aTask1 = SICo_Provisioning_CLS.createTask(zuoraSubscription.id,cases.get(0), features.get('SE0001'), quotes);
      Task aTask2 = SICo_Provisioning_CLS.createTask(zuoraSubscription.id,cases.get(0), features.get('SE0004'), quotes);
      Task aTask3 = SICo_Provisioning_CLS.createTask(zuoraSubscription.id,cases.get(1), features.get('IN0001'), quotes);
      Task aTask4 = SICo_Provisioning_CLS.createTask(zuoraSubscription.id,cases.get(2), features.get('MA0002'), quotes);
    Test.stopTest();

    // ASSERT
    System.assertEquals('Colissimo', aTask1.ShippingMode__c);
    System.assertEquals(aTask2.Parameters__c, features.get('SE0004').Tasks__r.Parameters__c);
    System.assertEquals(System.Label.SICo_ProvisionningObjet, aTask3.Type);
    System.assertEquals(aTask4.CHAMAgency__c, account.Id);
  }
}