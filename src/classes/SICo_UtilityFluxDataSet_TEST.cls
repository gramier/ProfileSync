/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SICo_UtilityFluxDataSet_TEST {

	@isTest static void testCreateFluxProfileElec() {
		Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
		insert account;
		account.CustomerNumber__c = account.Id;
		update account;
		List<SICo_Site__c> siteList = new List<SICo_Site__c>();
		SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
		site = SICo_UtilityTestDataSet.addFieldForSite(site);
		insert site;
		Test.startTest();
			SICo_FluxProfileModel.ElecSupplyContractChange res = SICo_UtilityFluxDataSet.createFluxProfileElec(site.Id, account.id);
		Test.stopTest();
	}
	
	@isTest static void testCreateFluxProfileGaz() {
		Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
		insert account;	
		account.CustomerNumber__c = account.Id;
		update account;
		List<SICo_Site__c> siteList = new List<SICo_Site__c>();
		SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
		site = SICo_UtilityTestDataSet.addFieldForSite(site);
		insert site;
		Test.startTest();
			SICo_FluxProfileModel.GasSupplyContractChange res = SICo_UtilityFluxDataSet.createFluxProfileGaz(site.Id, account.id);
		Test.stopTest();
	}
	
	@isTest static void testCreateFluxGenericProfile() {
		Test.startTest();
			SICo_FluxProfileModel.GenericProfile res = SICo_UtilityFluxDataSet.createFluxGenericProfile();
		Test.stopTest();
	}
	
	@isTest static void testCreateFluxProfileHousing() {
		Test.startTest();
			SICo_FluxProfileModel.Housing res = SICo_UtilityFluxDataSet.createFluxProfileHousing();
		Test.stopTest();
	}
	
	@isTest static void testCreateFluxProfileEquipments() {
		Test.startTest();
			SICo_FluxProfileModel.Equipments res = SICo_UtilityFluxDataSet.createFluxProfileEquipments();
		Test.stopTest();
	}
	
	
	@isTest static void testCreateFluxSubscriptionAction() {
		Test.startTest();
			SICo_FluxSubscriptionModel.Action res = SICo_UtilityFluxDataSet.createFluxSubscriptionAction();
		Test.stopTest();
	}
	
	@isTest static void testCreateFluxSubscriptionPhone() {
		Test.startTest();
			SICo_FluxSubscriptionModel.Phones res = SICo_UtilityFluxDataSet.createFluxSubscriptionPhone();
		Test.stopTest();
	}
	
    @isTest static void serialiseChamClientContrat() {
		SICo_FluxChamModel.ChamClientContrat chamClientContrat = SICo_UtilityFluxDataSet.createFluxChamClientContrat();
		String responceJSON = JSON.serialize(chamClientContrat);
	}

	@isTest static void serialiseChamRDV() {
		SICo_FluxChamModel.ChamRDV chamRDV = SICo_UtilityFluxDataSet.createFluxChamRDV();
		String responceJSON = JSON.serialize(chamRDV);
	}

	@isTest static void serialiseChamToken() {
		SICo_FluxChamModel.ChamToken chamToken = new SICo_FluxChamModel.ChamToken();
		String responceJSON = JSON.serialize(chamToken);
	}
}