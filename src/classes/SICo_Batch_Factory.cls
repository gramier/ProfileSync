/*------------------------------------------------------------
Author:        Gerald Ramier
Company:       Salesforce.com
Description:   Web Service REST in order to provide notify SFDC of Integration process completion for Batch processing
Test Class:    SICo_BatchNotify_RWS_TEST
History
12/08/2016     Gerald Ramier     Creation
------------------------------------------------------------*/

public with sharing class SICo_Batch_Factory {
	
	public SICo_Batch_Factory() {
		
	}

	/**
	 * Instance a new batch using the batch code
	 * @param batchCode : Batch class suffix to be instanciate
	 * @return SICO_Batch Instance
	 */
	public static SICo_Batch getBatchInstance(String batchCode) {
		System.debug('=========> : ' + batchCode);

		Type t = Type.forName('SICo_Batch_'+batchCode);
		SICo_Batch batch = (SICo_Batch) t.newInstance();
		return batch;
	}

	/**
	 * Instance and Process and new batch from API Request
	 * @param Request
	 * @return Response : Response for processing (asynchronous)
	 */
	public static SICo_Batch.Response processBatch(SICo_Batch.Request req) {
		try {
			SICo_Batch batch = getBatchInstance(req.batchCode);		
			batch.request.batchStartDate = req.batchStartDate;
			return batch.execute();
		} catch (Exception e) {
			System.debug(e.getMessage());

			SICo_Batch.Response res = new SICo_Batch.Response('',req.batchCode);
			res.hasError = true;
			res.errorMessage = e.getMessage();
			return res;
		}
	}
}