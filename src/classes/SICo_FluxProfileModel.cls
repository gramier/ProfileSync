/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Serialise & deserialize JSON profile, Electricity & Gaz Flux
Test Class:    SICo_FluxProfileModel_Test
History
18/07/2016      Boris Castellani     Create version
09/09/2016      Gerald Ramier       Added List classes
------------------------------------------------------------*/
public with sharing class SICo_FluxProfileModel {

    
    public class ListGenericProfiles {
        public List<GenericProfile> listGenericProfiles;
    }

    public class ListElecProfiles {
        public List<ElecSupplyContractChange> elecProfiles;
    }

    public class ListGasProfiles {
        public List<GasSupplyContractChange> gasProfiles;
    }

  // Build ElecSupplyContractChange
	public class ElecSupplyContractChange {
		Public Id siteId;
		public Id personId;
		public DateTime changeDate;
    public String tariff;
    public String tariffOption;
    public String subscribedPower;
    public String tpnCategory;
    public String peakLoadGroup;
    public String millesime;
    public Parameters parameters;

		public ElecSupplyContractChange parse (String json) {
			return (ElecSupplyContractChange) System.JSON.deserialize(json, ElecSupplyContractChange.class);
			}
		}

  // Build GasSupplyContractChange
	public class GasSupplyContractChange {
		Public Id siteId;
		public Id personId;
		public DateTime changeDate;
    public String tariff;
    public String option;
    public String millesime;
    public Decimal tss;
    public String zone;
    public String contractName;
    public Parameters parameters;

		public GasSupplyContractChange parse (String json) {
			return (GasSupplyContractChange) System.JSON.deserialize(json, GasSupplyContractChange.class);
			}
		}

  // Build Generic Profile
	public class GenericProfile {
		public Housing housing;
    public DateTime modificationDate;
		Public Id personExternalId;
		public Id siteExternalId;

		public GenericProfile parse (String json) {
			return (GenericProfile) System.JSON.deserialize(json, GenericProfile.class);
			}
		}

	// Build Request
	public class ReturnRequest {
		public String ownerId;
		public String statusCode;
		public String errorDescription;

		public ReturnRequest parse (String json) {
			return (ReturnRequest) System.JSON.deserialize(json, ReturnRequest.class);
			}
		}

/**********************************
    Wrapper
 **********************************/

 public class Housing {
   public Apartment apartment;
   public String constructionDate;
   public Equipments equipments;
   public HeatingSystem heatingSystem;
   public String heightUnderCeiling;
   public House house;
   public String housingType;
   public InsertWood insertWood;
   public Boolean isBBC;
   public LifeStyle lifeStyle;
   public String luminosity;
   public String occupationType;
   public String residenceType;
   public Restoration restoration;
   public String surfaceInSqMeter;
   public SwimmingPool swimmingPool;
 }

 public class Apartment {
    public String housingLocationType;
    public Boolean isCornerApartment;
    }

 public class Equipments {
    public AirConditioning airConditioning;
    public AudioEquipments audioEquipments;
    public CookingEquipments cookingEquipments;
    public ElecEquipments elecEquipments;
    public String lightingType;
    public String noOfHalogens;
    public OtherEquipments otherEquipments;
    public SanitoryHotWater sanitoryHotWater;
    }

 public class AirConditioning {
    public String instructionTypeAC;
    public String instructionValueAC;
    public Boolean isACPresent;
    public String surfaceAC;
    }

 public class AudioEquipments {
    public String otherAudioEquipmentsNumber;
    public String pcNumber;
    public String tvNumber;
    }

 public class CookingEquipments {
    public Boolean isCeramicGlassPlatePresent;
    public Boolean isElectricOvenPresent;
    public Boolean isInductionPlatePresent;
    public Boolean isIronPlatePresent;
    public Boolean isMicroWavePresent;
    public Boolean isMiniOvenPresent;
    public Boolean isGasPlatePresent;
    public Boolean isElectricAndGasPlatePresent;
    public Boolean isGasOvenPresent;
    public String gasSource;
    }

 public class ElecEquipments {
    public String americanFridgeFreezerNumber;
    public String dishwasherNumber;
    public String dryerNumber;
    public String freezerNumber;
    public String fridgeFreezerNumber;
    public String fridgeNumber;
    public String smallElecEquipmentsNumber;
    public String washerNumber;
    }

 public class OtherEquipments {
    public Boolean isAquariumPresent;
    public Boolean isWineCellarPresent;
    }

 public class SanitoryHotWater {
    public Boolean isJacuzziPresent;
    public String noOfSolarSensors;
    public String sanitaryHotWaterPractice;
    public String sanitaryHotWaterTankVolume;
    public String sanitoryHotWaterType;
    }

 public class HeatingSystem {
    public String princHeatingSystemTypeDetail;
    public String principalHeatingSystemSurface;
    public String principalHeatingSystemType;
    public String secondHeatingSystemTypeDetail;
    public String secondaryHeatingSystemSurface;
    }

 public class House {
    public Boolean isArrangedRoofPresent;
    public Boolean isFloorPresent;
    public Boolean isJointHouse;
    public Boolean isVerandaPresent;
    }

 public class InsertWood {
    public Boolean isInsertWoodPresent;
    public String numOfStereWood;
    }

 public class LifeStyle {
    public Boolean allDayPresenceIndicator;
    public Boolean dailyWindowOpenIndicator;
    public String heatReductionTypeInWEAbsence;
    public String noOfOccupants;
    public String protectionType;
    public Boolean reduceHeatInDayForLivingRoom;
    public Boolean reduceHeatInDayForOtherRooms;
    public Boolean shuttersCloseIndicator;
    public String tempInLivingRooms;
    public String tempInOtherRooms;
    public String weekEndAbsenceType;
    public String weeksOfAbsenceSummer;
    public String weeksOfAbsenceWinter;
    }

 public class Restoration {
    public Boolean isArrangedRoofRestored;
    public Boolean isExtWallRestored;
    public Boolean isWallRestored;
    public Boolean isWindowRestored;
    }

 public class swimmingPool {
    public Boolean hasSwimmingPool;
    public String noOfUsageMonths;
    public String swimmingPoolHeatingType;
    }

  // class vide => Map vide JSON
  public class Parameters {}

//END
}