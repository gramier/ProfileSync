/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Test class of the Controller for the Energy Profile pages
History
26/08/2016     Gaël BURNEAU      Create version
------------------------------------------------------------*/
@isTest
private class SICo_EnergyProfile_VFC_TEST {

	@testSetup static void initTestData() {

		Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
		insert account;
		account.CustomerNumber__c = account.Id;
		update account;

		SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite.IsActive__c = true;
		oSICoSite.PrincipalHeatingSystemType__c = 'Gaz';
		oSICoSite.SanitaryHotWaterType__c = 'Electrique';
		oSICoSite.NoOfOccupants__c = 4;
		oSICoSite.surfaceInSqMeter__c = 80;
		oSICoSite.InseeCode__c = '44109';
		oSICoSite.ConstructionDate__c = 1980;
		oSICoSite.IsSelectedInCustomerSpace__c = true;
		insert oSICoSite;
		insert SICo_UtilityTestDataSet.createSiteMeter(oSICoSite.Id);
		oSICoSite.IsActive__c = true;
		update oSICoSite;

		CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
	    insert boomiConfig;
	}

	@isTest static void methods_called_by_ligthning_test() {

		SICo_Site__c oSICoSite = [SELECT Id FROM SICo_Site__c WHERE Subscriber__r.FirstName = 'Test01' LIMIT 1];
		// Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		SICo_EnergyProfile_VFC.getCurrentSite(account.Id);
		SICo_EnergyProfile_VFC.setInformations_02(oSICoSite.Id, 'Maison', 'Principale');
		SICo_EnergyProfile_VFC.setInformations_03_Appart(oSICoSite.Id, 'RDC', true);
		SICo_EnergyProfile_VFC.setInformations_03_House(oSICoSite.Id, true, true, true, true);
		SICo_EnergyProfile_VFC.setInformations_04(oSICoSite.Id, 1990);
		SICo_EnergyProfile_VFC.setInformations_05(oSICoSite.Id, true, true, false);
		SICo_EnergyProfile_VFC.setInformations_05Bis(oSICoSite.Id, 100);
		SICo_EnergyProfile_VFC.setInformations_06(oSICoSite.Id, 4);
		SICo_EnergyProfile_VFC.setInformations_07(oSICoSite.Id, 'Electricite', null);
		SICo_EnergyProfile_VFC.setInformations_07(oSICoSite.Id, 'PompeAChaleur', null); 
		SICo_EnergyProfile_VFC.setInformations_07(oSICoSite.Id, 'Gaz', 'Moins de 5 ans');
		SICo_EnergyProfile_VFC.setInformations_07(oSICoSite.Id, 'Gaz', 'Plus de 5 ans'); 
		SICo_EnergyProfile_VFC.setInformations_08(oSICoSite.Id, 'Electrique');
		SICo_EnergyProfile_VFC.setInformations_09(oSICoSite.Id, 'Electrique');
		SICo_EnergyProfile_VFC.setInformations_09(oSICoSite.Id, 'Ville');
		SICo_EnergyProfile_VFC.setInformations_10(oSICoSite.Id, true, false, true);
		SICo_EnergyProfile_VFC.setInformations_11(oSICoSite.Id, 20, 26, false);
		SICo_EnergyProfile_VFC.setInformations_12(oSICoSite.Id, 'Base', '3kVA');
		oSICoSite = [SELECT Id, SanitaryHotWaterType__c, GasSource__c, IsWineCellarPresent__c, hasSwimmingPool__c, IsAquariumPresent__c,
							SurfaceAC__c, InstructionValueAC__c, IsACPresent__c, ElectricityTariffOption__c, SubscribedPower__c,
							PrincipalHeatingSystemType__c, BoilerAge__c, NoOfOccupants__c, surfaceInSqMeter__c,
							IsArrangedRoofRestored__c, IsWallRestored__c, IsWindowRestored__c, ConstructionDate__c, IsJointHouse__c,
							HousingLocationType__c, IsCornerApartment__c, HousingType__c, ResidenceType__c
					   FROM SICo_Site__c
					  WHERE Id = :oSICoSite.Id];
		System.assertEquals('Electrique', oSICoSite.SanitaryHotWaterType__c);
		System.assertEquals('Ville', oSICoSite.GasSource__c);
		System.assertEquals(true, oSICoSite.IsWineCellarPresent__c);
		System.assertEquals(false, oSICoSite.hasSwimmingPool__c);
		System.assertEquals(true, oSICoSite.IsAquariumPresent__c);
		System.assertEquals(20, oSICoSite.SurfaceAC__c);
		System.assertEquals(26, oSICoSite.InstructionValueAC__c);
		System.assertEquals(true, oSICoSite.IsACPresent__c);
		System.assertEquals('Base', oSICoSite.ElectricityTariffOption__c);
		System.assertEquals('3kVA', oSICoSite.SubscribedPower__c);

		System.assertEquals('Gaz', oSICoSite.PrincipalHeatingSystemType__c);
		System.assertEquals('Plus de 5 ans', oSICoSite.BoilerAge__c);
		System.assertEquals(4, oSICoSite.NoOfOccupants__c);
		System.assertEquals(true, oSICoSite.IsWindowRestored__c);
		System.assertEquals(true, oSICoSite.IsWallRestored__c);
		System.assertEquals(false, oSICoSite.IsArrangedRoofRestored__c);
		System.assertEquals(1990, oSICoSite.ConstructionDate__c);
		System.assertEquals(true, oSICoSite.IsJointHouse__c);
		System.assertEquals('RDC', oSICoSite.HousingLocationType__c);
		System.assertEquals(true, oSICoSite.IsCornerApartment__c);
		System.assertEquals('Maison', oSICoSite.HousingType__c);
		System.assertEquals('Principale', oSICoSite.ResidenceType__c);
		System.assertEquals(100, oSICoSite.surfaceInSqMeter__c);

		SICo_EnergyProfile_VFC.setInformations_11(oSICoSite.Id, null, null, true);
		oSICoSite = [SELECT Id, SurfaceAC__c, InstructionValueAC__c, IsACPresent__c
					   FROM SICo_Site__c
					  WHERE Id = :oSICoSite.Id];
		System.assertEquals(null, oSICoSite.SurfaceAC__c);
  		System.assertEquals(null, oSICoSite.InstructionValueAC__c);
  		System.assertEquals(false, oSICoSite.IsACPresent__c);
	}

	@isTest static void constructor_test() {
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		PageReference pref = Page.EnergyProfile_02;
	    pref.getParameters().put(SICo_Constants_CLS.URL_PARAM_ACCOUNTID, account.Id);
	    Test.setCurrentPage(pref);

	    Test.startTest();

	    SICo_EnergyProfile_VFC controller = new SICo_EnergyProfile_VFC();

		Test.stopTest();

		System.assertEquals(account.Id, controller.accountId);
	}
	

	@isTest static void getEnergyProfile_test() {
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];
		Test.startTest();
		SICo_EnergyProfile_VFC.EneryProfileWrap vEPW = SICo_EnergyProfile_VFC.getCurrentEnergyProfileWrapper(account.Id);

 		SICo_EnergyProfile_VFC.setInformations_02(vEPW.Id, 'Maison', 'Principale');
		SICo_EnergyProfile_VFC.setInformations_03_Appart(vEPW.Id, 'RDC', true);
		SICo_EnergyProfile_VFC.setInformations_03_House(vEPW.Id, true, true, true, true);
		SICo_EnergyProfile_VFC.setInformations_04(vEPW.Id, 1990);
		SICo_EnergyProfile_VFC.setInformations_05(vEPW.Id, true, true, false);
		SICo_EnergyProfile_VFC.setInformations_05Bis(vEPW.Id, 100);
		SICo_EnergyProfile_VFC.setInformations_06(vEPW.Id, 4);
		SICo_EnergyProfile_VFC.setInformations_07(vEPW.Id, 'Electricite', null);  
		SICo_EnergyProfile_VFC.setInformations_07(vEPW.Id, 'Gaz', 'Plus de 5 ans'); 
		SICo_EnergyProfile_VFC.setInformations_08(vEPW.Id, 'Electrique');
		SICo_EnergyProfile_VFC.setInformations_09(vEPW.Id, 'Electrique'); 
		SICo_EnergyProfile_VFC.setInformations_09(vEPW.Id, 'Ville');
		SICo_EnergyProfile_VFC.setInformations_10(vEPW.Id, true, false, true);
		SICo_EnergyProfile_VFC.setInformations_11(vEPW.Id, 20, 26, false);
		SICo_EnergyProfile_VFC.setInformations_12(vEPW.Id, 'Base', '3kVA');
			
		// reload
		vEPW = SICo_EnergyProfile_VFC.getCurrentEnergyProfileWrapper(account.Id);

		System.assert(vEPW.Id != null, 'Id should not be null');                      
		System.assert(vEPW.houseType != null, 'houseType should not be null');                
		System.assert(vEPW.residenceType != null, 'residenceType should not be null');            
		System.assert(vEPW.placementLocation != null, 'placementLocation should not be null');       
		System.assert(vEPW.positionCorner != null, 'positionCorner should not be null');          
		System.assert(vEPW.builtDate != null, 'builtDate should not be null');               
		System.assert(vEPW.roofPresent != null, 'roofPresent should not be null');             
		System.assert(vEPW.floorPresent != null, 'floorPresent should not be null');            
		System.assert(vEPW.verandaPresent != null, 'verandaPresent should not be null');          
		System.assert(vEPW.jointHouse != null, 'jointHouse should not be null');              
		System.assert(vEPW.roofRestored != null, 'roofRestored should not be null');            
		System.assert(vEPW.wallsRestored != null, 'wallsRestored should not be null');           
		System.assert(vEPW.windowsRestored != null, 'windowsRestored should not be null');         
		System.assert(vEPW.surface != null, 'surface should not be null');                 
		System.assert(vEPW.numberOfOccupants != null, 'numberOfOccupants should not be null');       
		System.assert(vEPW.heating != null, 'heating should not be null');                 
		System.assert(vEPW.age != null, 'age should not be null');                     
		System.assert(vEPW.hotWater != null, 'hotWater should not be null');                
		System.assert(vEPW.cooking != null, 'cooking should not be null');                 
		System.assert(vEPW.ceramic != null, 'ceramic should not be null');                 			 
		System.assert(vEPW.wineCellar != null, 'wineCellar should not be null');              
		System.assert(vEPW.swimmingPool != null, 'swimmingPool should not be null');            
		System.assert(vEPW.aquarium != null, 'aquarium should not be null');                			 
		System.assert(vEPW.surfaceAC != null, 'surfaceAC should not be null');               
		System.assert(vEPW.instructionValueAC != null, 'instructionValueAC should not be null');      
		System.assert(vEPW.noAC != null, 'noAC should not be null');                    
		System.assert(vEPW.electricityTariffOption != null, 'electricityTariffOption should not be null'); 
		System.assert(vEPW.subscribedPower != null, 'subscribedPower should not be null');         

		Test.stopTest();
	}

	@isTest static void callout_test() {

		Schema.DescribeFieldResult fieldResultGasTariffOption = SICo_Site__c.GasTariffOption__c.getDescribe();
		List<Schema.PicklistEntry> listGasTariffOptionPle = fieldResultGasTariffOption.getPicklistValues();
		String sGasTariffOptionPicklistValue = listGasTariffOptionPle[0].getValue();

		Integer iEstimatedGasConsumptionPicklistValue = 1000;

		SICo_EnergyProfile_VFC.EvalConsoResponseValue oEvalConsoResponseValue = new SICo_EnergyProfile_VFC.EvalConsoResponseValue(iEstimatedGasConsumptionPicklistValue, sGasTariffOptionPicklistValue);
		SICo_EnergyProfile_VFC.EvalConsoResponse oEvalConsoResponse = new SICo_EnergyProfile_VFC.EvalConsoResponse('OK', '', oEvalConsoResponseValue);
		Test.setMock(HttpCalloutMock.class, new SICo_FluxEvalConso_MOCK(200, 'OK', '{"data": '+JSON.serialize(oEvalConsoResponse)+'}', new Map<String, String>()));

		Test.startTest();

		SICo_Site__c oSICoSite = [SELECT Id FROM SICo_Site__c WHERE Subscriber__r.FirstName = 'Test01' LIMIT 1];

		SICo_EnergyProfile_VFC.getEvalConso(oSICoSite.Id);
		SICo_EnergyProfile_VFC.getEvalConsoAsync(oSICoSite.Id);
		SICo_EnergyProfile_VFC.getEvalConsoAsyncDoTreatment(oSICoSite.Id);

		oSICoSite = [SELECT Id, EstimatedGasConsumption__c FROM SICo_Site__c WHERE Subscriber__r.FirstName = 'Test01' LIMIT 1];

		Test.stopTest();
		System.assertNotEquals(null, oSICoSite.EstimatedGasConsumption__c);
	}


	@isTest static void redirect_test() {

		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];
		//Deactivate Site
		SICo_Site__c theSite = [SELECT Id FROM SICo_Site__c WHERE Subscriber__c =: account.Id LIMIT 1];
		theSite.IsActive__c = false;
		update theSite;

		PageReference pref = Page.EnergyProfile_02;
	    pref.getParameters().put(SICo_Constants_CLS.URL_PARAM_ACCOUNTID, account.Id);
	    Test.setCurrentPage(pref);

	    Test.startTest();

	    SICo_EnergyProfile_VFC controller = new SICo_EnergyProfile_VFC();
	    PageReference redirect = controller.doRedirect();

		Test.stopTest();

		System.assertEquals(Sico_Constants_CLS.CUSTOMER_NO_SITE_URL, redirect.getUrl());
	}
	
}