/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test SICo_FluxSubscriptionModel
History
04/07/2016      Boris Castellani     Create version
11/07/2016      Boris Castellani     Modify Name & dataSet for method
------------------------------------------------------------*/
@isTest
private class SICo_FluxSubscriptionModel_TEST {

    static testMethod void serialiseFluxSubscriptionModel() {
        SICo_FluxSubscriptionModel.Action newAction = SICo_UtilityFluxDataSet.createFluxSubscriptionAction();
        List<SICo_FluxSubscriptionModel.Action> newListAction = new List<SICo_FluxSubscriptionModel.Action> ();
        newListAction.add(newAction);
        newListAction.add(newAction);
        SICo_FluxSubscriptionModel.Request FluxRequest = new SICo_FluxSubscriptionModel.Request();
        FluxRequest.action = newListAction;
        String jsonRequest = JSON.serialize(FluxRequest);
    }

    static testMethod void parseFluxSubscriptionModel() {
        String json = '{'+
        '\"Action\": [{'+
        '       \"personId\": \"PER00001\",'+
        '       \"siteId\": \"SITE-0001\",'+
        '       \"activityId\": \"0013E0000039aVI\",'+
        '       \"endPoint\": \"https://localhost\",'+
        '       \"externalOfferCode\": \"0013E0000039aXI\",'+
        '       \"SubscriptionRequest\": {'+
        '           \"ts\": \"2015-11-05T08:15:30-05:00\",'+
        '           \"siteElecExternalId\": \"SITE-0001\",'+
        '           \"siteGasExternalId\": \"SITE-0002\",'+
        '           \"person\": {'+
        '                   \"title\": \"Madame\",'+
        '                   \"firstName\": \"Scarlett\",'+
        '                   \"lastName\": \"Johansson\",'+
        '                   \"address\": {'+
        '                           \"lines\": [\"11 rue de la Paix\"],'+
        '                           \"zipCode\": \"92000\",'+
        '                           \"city\": \"Courbevoie\",'+
        '                           \"country\": \"France\"'+
        '                   },'+
        '                   \"email\": \"sjohansson@test.com\",'+
        '                   \"phones\": {'+
        '                       \"portable\": \"+33643293040\",'+
        '                       \"principal\": \"+33353293041\"'+
        '                   }'+
        '           },'+
        '           \"personParameters\": {'+
        '           },'+
        '           \"siteAddress\": {'+
        '                   \"lines\": [\"15 rue de Paris\"],'+
        '                   \"zipCode\": \"92000\",'+
        '                   \"city\": \"Courbevoie\",'+
        '                   \"country\": \"France\"'+
        '           },'+
        '           \"siteParameters\": {'+
        '               },'+
        '           \"subscribedOfferExternalId\": \"BILAN_CONSO_THERM\",'+
        '           \"subscribedOfferParameters\": {'+
        ''+
        '           }'+
        '       }'+
        '   },'+
        '{'+
        '       \"personId\": \"PER00001\",'+
        '       \"siteId\": \"SITE-0001\",'+
        '       \"activityId\": \"0013E0000039aVI\",'+
        '       \"endPoint\": \"https://localhost\",'+
        '       \"SubscriptionRequest\": {'+
        '           \"ts\": \"2015-11-05T08:15:30-05:00\",'+
        '           \"siteElecExternalId\": \"SITE-0001\",'+
        '           \"siteGasExternalId\": \"SITE-0002\",'+
        '           \"subscribedOfferExternalId\": \"0013E0000039aXI\",'+
        '           \"person\": {'+
        '                   \"title\": \"Madame\",'+
        '                   \"firstName\": \"Scarlett\",'+
        '                   \"lastName\": \"Johansson\",'+
        '                   \"address\": {'+
        '                           \"lines\": [\"11 rue de la Paix\"],'+
        '                           \"zipCode\": \"92000\",'+
        '                           \"city\": \"Courbevoie\",'+
        '                           \"country\": \"France\"'+
        '                   },'+
        '                   \"email\": \"sjohansson@test.com\",'+
        '                   \"phones\": {'+
        '                   }'+
        '           },'+
        '           \"personParameters\": {'+
        '           },'+
        '           \"siteAddress\": {'+
        '                   \"lines\": [\"15 rue de Paris\"],'+
        '                   \"zipCode\": \"92000\",'+
        '                   \"city\": \"Courbevoie\",'+
        '                   \"country\": \"France\"'+
        '           },'+
        '           \"siteParameters\": {'+
        '               },'+
        '           \"externalOfferCode\": \"BILAN_CONSO_THERM\",'+
        '           \"subscribedOfferParameters\": {'+
        ''+
        '           }'+
        '       }'+
        '   }]'+
        '}';
        system.debug('json: '+json);
        SICo_FluxSubscriptionModel.Request FluxRequest = new SICo_FluxSubscriptionModel.Request().parse(json);
        System.assert(FluxRequest.action != null);
        system.debug('FluxRequest: '+FluxRequest);
    }
}