/**
* @author Sebastien Colladon
* @date 28/09/2016
*
* @description Handle Account Trigger After Update Business Rule
*/
public without sharing class SICO_AccountTrigger_CLS {

  private static final String MAINTENANCE_PREFFIX = 'MAINT';
  private static final String CERTIFICATE_PREFFIX = 'certificate';

/* -------- BEFORE METHODE -------- */

  public static void HandleBeforeUpdate(final List<Account> accounts, final Map<Id,Account> oldAccounts){
    Map<id,Account> AccountToTreat = new Map<id,Account>();
    for(Account a : accounts){
      if(a.SICO_HasSubscriptionAttachment__c && !oldAccounts.get(a.id).SICO_HasSubscriptionAttachment__c){
        AccountToTreat.put(a.id,a);
        a.SICO_HasSubscriptionAttachment__c = false;
      }
    }

    for(AggregateResult groupedResult : [ SELECT count(id) cnt, ParentId FROM Attachment 
                                          WHERE ParentId in :AccountToTreat.keySet() GROUP BY ParentId]){
      final ID accountId = (ID) groupedResult.get('ParentId');
      final Integer count = Integer.valueOf(groupedResult.get('cnt'));
      if(count == 0) {
        AccountToTreat.remove(accountId);
      }
    }

    Map<Id,Id> oppyPerAccount = new Map<Id,Id>();
    for(Opportunity o : [select id, accountid from Opportunity Where Accountid in :AccountToTreat.keySet() order by CreatedDate ASC]){
      oppyPerAccount.put(o.accountid, o.id);
    }

    Map<Id,List<Zuora__Subscription__c>> zuoraSubPerAccount = new Map<Id,List<Zuora__Subscription__c>>();
    for(Zuora__Subscription__c z : [select id, SICO_Subscribed_offers__c, Zuora__Account__c from Zuora__Subscription__c where Zuora__Account__c in :AccountToTreat.keySet()]){
      if(!zuoraSubPerAccount.containsKey(z.Zuora__Account__c)) {
        zuoraSubPerAccount.put(z.Zuora__Account__c, new List<Zuora__Subscription__c>());
      }
      zuoraSubPerAccount.get(z.Zuora__Account__c).add(z);
    }

    Map<Id,List<Attachment>> attachmentPerAccount = new Map<Id,List<Attachment>>();
    for(Attachment z : [select SystemModstamp, ParentId, OwnerId, Name, 
            LastModifiedDate, LastModifiedById, IsPrivate, IsDeleted, 
            Id, Description, CreatedDate, CreatedById, 
            ContentType, BodyLength, Body from Attachment where parentId in :AccountToTreat.keySet()]){
      if(!attachmentPerAccount.containsKey(z.ParentId)) {
        attachmentPerAccount.put(z.ParentId, new List<Attachment>());
      }
      attachmentPerAccount.get(z.ParentId).add(z);
    }

    final Map<String,String> attachNameSubscriptionNameMapping = new Map<String,String>{
      'GAZ' => 'GAZ',
      'MAINT' => 'MAINT',
      'STATION' => 'SE'
    };

    final Set<Attachment> attachmentToInsert = new Set<Attachment>();
    final Set<Attachment> attachmentToDelete = new Set<Attachment>();
    for(Id anId : AccountToTreat.keySet()) if(zuoraSubPerAccount.containsKey(anId) && attachmentPerAccount.containsKey(anId) && oppyPerAccount.containsKey(anId)) {
      for(Zuora__Subscription__c sub : zuoraSubPerAccount.get(anId)) {
        final List<Attachment> attl = attachmentPerAccount.get(anId);
        
        //TODO A REVOIR AVEC SEBASTIEN -BORIS : AJOUT DU IF
        if(String.isNotEmpty(sub.SICO_Subscribed_offers__c)){
          final Set<String> subscribedOffers = new Set<String>(sub.SICO_Subscribed_offers__c.split(';'));
        
          for(Integer i = attl.size()-1 ; i >= 0 ; --i) {          
            final Attachment tempAtt = attl[i].clone(false,false);
            if(subscribedOffers.contains(attachNameSubscriptionNameMapping.get(tempAtt.Name.split('_')[0]))) {
              tempAtt.ParentId = sub.id;
              attachmentToInsert.add(tempAtt);
              attachmentToDelete.add(attl.remove(i));
            }
          }
        }

      }
      for(Attachment att : attachmentPerAccount.get(anId)) {
        final Attachment tempAtt = att.clone(false,false);
        tempAtt.ParentId = oppyPerAccount.get(anId);
        attachmentToInsert.add(tempAtt);
        attachmentToDelete.add(att);
      }
    } 
    
    insert new List<Attachment>(attachmentToInsert);
    delete new List<Attachment>(attachmentToDelete);
  }

/* -------- AFTER METHODE -------- */

  public static void HandleAfterUpdate(final List<Account> accounts, final Map<Id,Account> oldAccounts){
    Map<Id,Account> updateEmail = new Map<Id,Account>();
    for(Account a : accounts){
      if(a.PersonEmail != oldAccounts.get(a.Id).PersonEmail){
        updateEmail.put(a.Id,a);
      }
    }
    //Update Email
    if(!updateEmail.isEmpty()){
      UpdateChildrenEmail(updateEmail);
    }
  }

/* -------- PRIVATE METHODE -------- */

  Private static void UpdateChildrenEmail(Map<Id,Account> accs){

    //Zuora__Payment__c
    List<Zuora__Payment__c> zpayments =[SELECT Id,Email__c,Zuora__Account__c FROM Zuora__Payment__c WHERE Zuora__Account__c IN :accs.keySet()];
    List<Zuora__Payment__c> paymentUpdate = new List<Zuora__Payment__c>();
    if(!zpayments.isEmpty()){
      for(Zuora__Payment__c payment : zpayments){
        paymentUpdate.add(new Zuora__Payment__c(Id=payment.Id,Email__c = accs.get(payment.Zuora__Account__c).PersonEmail));
      }
      Update paymentUpdate;
    }

    //SICo_CR__c
    List<SICo_CR__c> crs =[SELECT Id,Email__c,Account__c FROM SICo_CR__c WHERE Account__c IN:accs.keySet()];
    List<SICo_CR__c> crUpdate = new List<SICo_CR__c>();
    if(!crs.isEmpty()){
      for(SICo_CR__c cr : crs){
        crUpdate.add(new SICo_CR__c(Id=cr.Id,Email__c = accs.get(cr.Account__c).PersonEmail));
      }
      Update crUpdate;
    }
  }   
}//END