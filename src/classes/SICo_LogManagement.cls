/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Callout to the log repository
Test Class:    
History
08/07/2016      Olivier Vrbanac     Create version
------------------------------------------------------------*/
    // TODO Handle batch mode
    // TODO Handle UnitOfWork Pattern
public class SICo_LogManagement {
    private static final string SOURCE = 'SFDC';

    private static final string INFO = 'INFO';
    private static final string WARN = 'WARN';
    private static final string ERROR = 'ERROR';
    private static final string DEBUG = 'DEBUG';

    private static final String BODY_TEMPLATE = '\\"time\\":\\"{0}\\",\\"Id\\":\\"SFDC\\",\\"Category\\":\\"{1}\\",\\"Source\\":\\"'+SOURCE+'\\",\\"Application\\":\\"{2}\\",\\"Event\\":\\"{3}\\",\\"Description\\":\\"{4}\\"';

    private static final LogStash_Endpoint__mdt LogStashEndpoint = [SELECT NamedCredential__c, ShardId__c FROM LogStash_Endpoint__mdt WHERE IsActive__c = true limit 1][0];
    private static final List<String> logs = new List<String>();


    public static void addWarnLog( String application, String event, String description ) {  
        SICo_LogManagement.addLog(SICo_LogManagement.WARN,application,event,description);
    }    

    public static void addInfoLog( String application, String event, String description ) {  
        SICo_LogManagement.addLog(SICo_LogManagement.INFO,application,event,description);
    }

    public static void addErrorLog( String application, String event, String description ) {  
        SICo_LogManagement.addLog(SICo_LogManagement.ERROR,application,event,description);  
    }

    public static void addDebugLog( String application, String event, String description ) {  
        SICo_LogManagement.addLog(SICo_LogManagement.DEBUG,application,event,description);        
    }


    private static void addLog(String categorie, String application, String event, String description){
        system.debug('SICo_LogManagement : ' + description);

        logs.add(String.format(BODY_TEMPLATE, new List<String>{
            String.valueOf(DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'', 'UTC')),
            categorie,
            application,
            event,
            description
        }));
    }

    public static void sendLogsAsync(){
        if(!logs.isEmpty()){
            sendLogAsync(
                '{"records":[{"Data":"{'+
                String.join(logs, '}","PartitionKey":"'+LogStashEndpoint.ShardId__c+'},{"Data":"{')+
                '}","PartitionKey":"'+LogStashEndpoint.ShardId__c+'"}]}'
            );
            logs.clear();
        }
    }

    public static void sendLogs(){
        if(!logs.isEmpty()){
            sendLog(
                '{"records":[{"Data":"{'+
                String.join(logs, '}","PartitionKey":"'+LogStashEndpoint.ShardId__c+'},{"Data":"{')+
                '}","PartitionKey":"'+LogStashEndpoint.ShardId__c+'"}]}'
            );
            logs.clear();
        }
    }

    /*******************************************************************************************************
    * @description Methods to send a log
    * @param categorie the message categorie (INFO, WARN, ERROR, and DEBUG)
    * @param application the application where the message come from (ZUORA, DOCUSIGN, BOOMI, PVL, SFDC, INDEX, LOG)
    * @param event the event generating the log (DML, SOQL, …)
    * @param description the message itself
    * @example
    * SICo_LogManagement.sendWarnLog('test application','test event', 'test description');
    */
    private static void sendLog( String body ) {
        try{
            Http h = new Http();

             // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:'+LogStashEndpoint.NamedCredential__c+'/');
            req.setMethod('PUT');
            req.setheader('content-type','application/json');
            req.setbody(body);

            // Send the request, and return a response
            if(!Test.isRunningTest()){
                h.send(req);
            }
            System.Debug(req.getBody());
            
        } catch(Exception ex) {
            system.debug(ex);
        }
    }

    private static void sendLogAsync( String body ) {
        SICO_QueueLog queueLog = new SICO_QueueLog(body);
        if(!Test.isRunningTest()){
            System.enqueueJob(queueLog);
        }
    }

    public static void sendWarnLog( String application, String event, String description ) {

        SICo_LogManagement.addLog(
            SICo_LogManagement.WARN,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogs();
    }

    public static void sendWarnLogAsync( String application, String event, String description ) {
        SICo_LogManagement.addLog(
            SICo_LogManagement.WARN,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogsAsync();
    }

    public static void sendInfoLog( String application, String event, String description ) {  
        SICo_LogManagement.addLog(
            SICo_LogManagement.INFO,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogs();
    }

    public static void sendInfoLogAsync( String application, String event, String description ) {
        SICo_LogManagement.addLog(
            SICo_LogManagement.INFO,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogsAsync();
    }

    public static void sendErrorLog( String application, String event, String description ) {  
        SICo_LogManagement.addLog(
            SICo_LogManagement.ERROR,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogs();
    }

    public static void sendErrorLogAsync( String application, String event, String description ) {
        SICo_LogManagement.addLog(
            SICo_LogManagement.ERROR,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogsAsync();
    }

    public static void sendDebugLog( String application, String event, String description ) {  
        SICo_LogManagement.addLog(
            SICo_LogManagement.DEBUG,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogs();    
    }

    public static void sendDebugLogAsync( String application, String event, String description ) {
        SICo_LogManagement.addLog(
            SICo_LogManagement.DEBUG,
            application,
            event,
            description
        );
        SICo_LogManagement.sendLogsAsync();
    }

    private class SICO_QueueLog implements Queueable, Database.AllowsCallouts {
        private string body;

        public SICO_QueueLog(String Body) {
            this.body = Body;
        }
        public void execute(QueueableContext context) {
            SICo_LogManagement.sendLog(body);
        }
    }  
}