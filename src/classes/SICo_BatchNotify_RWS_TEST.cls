/*------------------------------------------------------------
Author:        Gerald Ramier
Company:       Salesforce.com
Description:   Web Service REST in order to provide notify SFDC of Integration process completion for Batch processing
Test Class:    N/A
History
12/08/2016     Gerald Ramier     Creation
------------------------------------------------------------*/

@isTest
private class SICo_BatchNotify_RWS_TEST {

    @testSetup
    static void test_setup() {
        //Test Data Creation

        Account acc = SICo_UtilityTestDataSet.createAccount('Gerald','Ramier','PersonAccount');
        acc.SubscriberId__c= '234567890';
        insert acc;

        SICo_Site__c st = SICo_UtilityTestDataSet.createSite(acc.Id);
        st.PCE__c = '00001';
        st.IsActive__c = true;
        insert st;

        Case cs = new Case();
        cs.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_System');
        cs.ProvisioningType__c = 'Gaz';
        cs.Status = 'Open';
        cs.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_System');
        cs.Site__c = st.Id;
        insert cs;

        System.debug('CASE CHECK : ' + cs.Site__r.Id);


        SICo_Site__c st1 = SICo_UtilityTestDataSet.createSite(acc.Id);
        st1.PCE__c = '00002';
        st1.IsActive__c = true;
        insert st1;


        SICo_Site__c st2 = SICo_UtilityTestDataSet.createSite(acc.Id);
        st2.PCE__c = '00003';
        st2.IsActive__c = true;
        insert st2;

        Task tsk = SICo_UtilityTestDataSet.CreateProvisioningTask('Demande Test 1');
        tsk.PCE__c = '00001';
        tsk.OmegaRequestId__c = 'TEST1';
        tsk.InterventionType__c = 'MES';
        tsk.BatchFlag__c = true;
        tsk.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz');
        insert tsk;    

        Task tsk1 = SICo_UtilityTestDataSet.CreateProvisioningTask('Demande Test 2');
        tsk1.PCE__c = '00002';
        tsk1.OmegaRequestId__c = 'TEST2';
        tsk1.BatchFlag__c = true;
        tsk1.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz');
        insert tsk1;  

        Task tsk2 = SICo_UtilityTestDataSet.CreateProvisioningTask('Demande Test 3');
        tsk2.PCE__c = '00002';
        tsk2.siteId__c = st.Id;
        tsk2.OmegaRequestId__c = 'TEST3';
        tsk2.BatchFlag__c = true;
        tsk2.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_CHAM');
        insert tsk2;

        System.debug(acc);
        System.debug(st);
        System.debug(cs);
        System.debug(tsk);
        System.debug(tsk1);
        System.debug(tsk2);
           
    }
    

    @isTest
    static void test_known_code_Task_GRD12() {
        
        Test.startTest();
        
        SICO_Batch.Request testRequest = new SICO_Batch.Request();
        testRequest.batchCode = 'GRD_12';
        testRequest.batchStartDate = System.today().addDays(-1);  
        System.debug(testRequest);

        SICO_Batch.Response res;
        
        res = SICo_BatchNotify_RWS.create(testRequest);
        

        Test.stopTest(); 
        System.debug(res);
        System.assertEquals(res.hasError,false);

        List<Task> lTasks = new List<Task>([SELECT siteId__c, NotTakenByCRC__c, ID, WhatId, PCE__c, OmegaRequestId__c, TaskId__c, InterventionType__c 
            FROM Task 
            WHERE 
                RecordTypeId = :SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz')]);

        System.debug('FINAL TASKS LIST ===> '+lTasks);
        for (Task t : lTasks) {
           System.debug('T::::: PCE:' + t.PCE__c + ', OMEGA: ' + t.OmegaRequestId__c + ', whatid: ' + t.WhatId + ', Siteid: ' + t.siteId__c + ' ' + t.NotTakenByCRC__c);
           System.debug('Task ID : ' + t.TaskID__c + ', InterventionType__c : ' + t.InterventionType__c);
        }
        
    }

    @isTest
    static void test_known_code_Task_CHA02B() {
        
        Test.startTest();
        
        SICO_Batch.Request testRequest = new SICO_Batch.Request();
        testRequest.batchCode = 'CHA_02B';
        testRequest.batchStartDate = System.today().addDays(-1);  
        System.debug(testRequest);

        SICO_Batch.Response res;
        
        res = SICo_BatchNotify_RWS.create(testRequest);
        

        Test.stopTest(); 
        System.debug(res);
        System.assertEquals(res.hasError,false);

        List<Task> lTasks = new List<Task>([SELECT siteId__c, NotTakenByCRC__c, ID, WhatId, PCE__c, OmegaRequestId__c, TaskId__c, InterventionType__c 
            FROM Task 
            WHERE 
                RecordTypeId = :SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_CHAM')
                
            ]);
        System.debug('FINAL TASKS LIST ===> '+lTasks);
        for (Task t : lTasks) {
           System.debug('T::::: PCE:' + t.PCE__c + ', OMEGA: ' + t.OmegaRequestId__c + ', whatid: ' + t.WhatId + ', Siteid: ' + t.siteId__c + ' ' + t.NotTakenByCRC__c);
           System.debug('Task ID : ' + t.TaskID__c + ', InterventionType__c : ' + t.InterventionType__c);
        }
        
    }

    // @isTest
    // static void test_known_code_Event() {
        
    //     Test.startTest();
        
    //     SICO_Batch.Request testRequest = new SICO_Batch.Request();
    //     testRequest.batchCode = 'CHA_02B';
    //     testRequest.batchStartDate = System.today().addDays(-1);  
    //     System.debug(testRequest);

    //     SICO_Batch.Response res;
        
    //     res = SICo_BatchNotify_RWS.create(testRequest);
        

    //     Test.stopTest(); 
    //     System.debug(res);
    //     System.assertEquals(res.hasError,false);

    //     List<Event> lTasks = new List<Event>([SELECT siteId__c, WhatId FROM Event WHERE BatchFlag__c = true]);

    //     System.debug('FINAL Event LIST ===> '+lTasks);
    //     for (Event t : lTasks)
    //         System.debug('E::::: ID'+ t.Id +' Site:' + t.siteID__c + ', whatid: ' + t.WhatId);
    // }

    @isTest
    static void test_unknwon_code() {
        
        Test.startTest();
        
        SICO_Batch.Request testRequest = new SICO_Batch.Request();
        testRequest.batchCode = 'GRD_99';
        testRequest.batchStartDate = System.today().addDays(-1);                
        SICO_Batch.Response res;
        
        res = SICo_BatchNotify_RWS.create(testRequest);
        Test.stopTest();


        System.debug(res);
        System.assertEquals(res.hasError,true);
   
    }
    
}