public class SICo_Utility {

/************ RecordTypes ************/

  // Load a Map with RecordTypes Id, Name for all Objects
  public static final Map<String, ID> m_RTbyDeveloperName;
  public static final Map<ID, String> m_DeveloperNameByRT;
  static{
    m_RTbyDeveloperName = new Map<String, ID>();
    m_DeveloperNameByRT = new Map<ID, String>();
    for (RecordType v_rt : [SELECT Id, DeveloperName, SobjectType FROM RecordType]) {
      m_RTbyDeveloperName.put(v_rt.SobjectType + '_' + v_rt.DeveloperName,v_rt.id);
      m_DeveloperNameByRT.put(v_rt.id,v_rt.DeveloperName);
    }
  }

  public static String generateRandomString(Integer len, String strType) {
    String chars = strType == 'NUMBER'
      ? '0123456789'
      : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String randStr = '';
    while (randStr.length() < len) {
      Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
      randStr += chars.substring(idx, idx+1);
    }
    return randStr;
  }

  //System.debug(SICo_Utility.generate18CharId('a114E000001aEbF'));
  public static string generate18CharId(final string id){
    final String abecedair = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ012345';
    if (String.isBlank(id) || (id.length() != 15 && id.length() != 18)) { 
      return null; 
    } 
    if (id.length() == 18) { 
      return id; 
    } 
    string suffix = '';
    for (integer i = 0; i < 3; i++) { 
      integer flags = 0; 
      for (integer j = 0; j < 5; j++) { 
        string c = id.substring(i * 5 + j,i * 5 + j + 1); //Only add to flags if c is an uppercase letter: 
        if (c.isAllUpperCase() && c >= 'A' && c <= 'Z') { 
          flags += (1 << j); 
        } 
      } 
      suffix += abecedair.substring(flags,flags+1); 
    } 
    return id + suffix; 
  }

/************ Callout ************/

  /*
  * DCO 09/08/2016	TODO => 
  * this callout method MUST be generic
  * the 'Boomi' key for CalloutConfig__c should be a param (and it should be defined as a constant)
  * the timeout should be a param AND it should never be higher then 5000
  * the Content-Type should be a param
  * the token (Authorization) should be a param
  * all System.debug should be replaced by the LogManager class
  */

  // Callout WebService
  // GRA : Updated to use Name Credentials
  public static HttpResponse getCallout(String methode,String endPoint, String body) {
    return getCallout(methode,endPoint,body,'Boomi') ;
  }

  // GRA : Added Header map in order to be used as parameters
  public static HttpResponse getCallout(String methode,String endPoint, String body, Map<String, String> headers) {
    return getCalloutBoomi(methode, endPoint, body, headers);
  }

  // Callout WebService
  public static HttpResponse getCallout(String methode,String endPoint, String body,String calloutEndpoint) {
    CalloutConfig__c boomiCS = CalloutConfig__c.getValues(calloutEndpoint);
    Http http = new Http();
    HttpRequest request = new HttpRequest();
    request.setTimeout(120000);
    request.setEndpoint(boomiCS.Endpoint__c+endPoint);
    request.setMethod(methode);
    Blob headerValue = Blob.valueOf(boomiCS.Login__c+':'+boomiCS.Password__c);
    String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
    request.setHeader('Content-Type', 'application/json;charset=UTF-8');
    request.setHeader('Authorization', authorizationHeader);
    if(methode != 'GET'){
      request.setBody(body);
    }
    HttpResponse response;
    response = http.send(request);
    return response;
  }
  // GRA: Callout using named credentials
  public static HttpResponse getCalloutBoomi(String method, String endPoint, String body) {
    Http h = new Http();
    HttpRequest req = new HttpRequest();
    req.setEndpoint('callout:Boomi/'+endPoint);
    req.setMethod(method);
    req.setheader('Content-Type', 'application/json;charset=UTF-8');
    if(method != 'GET') {
      req.setbody(body);
    }
    // Send the request, and return a response
    HttpResponse response = h.send(req);
    return response;
  }


  public static HttpResponse getCalloutBoomi(String method, String endPoint, String body, Map<String, String> headers) {
    system.debug('***getDMLStatements '+limits.getDMLStatements());
    system.debug('***getDMLRows '+limits.getDMLRows());
    system.debug('***getEmailInvocations '+limits.getEmailInvocations());
    system.debug('***getFutureCalls '+limits.getFutureCalls());
    Http h = new Http();
    HttpRequest req = new HttpRequest();
  
    //OLD Version
    CalloutConfig__c boomiCS = CalloutConfig__c.getValues('Boomi');
    req.setTimeout(120000);
    req.setEndpoint(boomiCS.Endpoint__c+endPoint);
    Blob headerValue = Blob.valueOf(boomiCS.Login__c+':'+boomiCS.Password__c);
    String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
    req.setHeader('Authorization', authorizationHeader);
    //End Old Version
    req.setMethod(method);
    for (String hd : headers.keySet()) {
      req.setHeader(hd, headers.get(hd));
    }
    if(method != 'GET') {
      req.setbody(body);
    }

    // Send the request, and return a response
    system.debug('***getDMLStatements '+limits.getDMLStatements());
    system.debug('***getDMLRows '+limits.getDMLRows());
    system.debug('***getEmailInvocations '+limits.getEmailInvocations());
    system.debug('***getFutureCalls '+limits.getFutureCalls());
    HttpResponse response = h.send(req);
    return response;
  }

  // Gestion des horaires de rendez vous CHAM
  public static DateTime[] getCreneauCham(Date dateCham, String horaireCham){
    horaireCham= horaireCham.deleteWhitespace();
    String dateStart= horaireCham.substringBeforeLast('-');
    String dateClose= horaireCham.substringAfterLast('-');
    DateTime startCham = DateTime.newInstance(dateCham.year(),dateCham.month(),dateCham.day(),Integer.valueOf(dateStart.substringBeforeLast(':')),Integer.valueOf(dateStart.substringAfterLast(':')),0);
    DateTime closeCham = DateTime.newInstance(dateCham.year(),dateCham.month(),dateCham.day(),Integer.valueOf(dateClose.substringBeforeLast(':')),Integer.valueOf(dateClose.substringAfterLast(':')),0);
    return new DateTime[]{startCham,closeCham};
  }

/************ Millesime ************/

  public static SICo_Millesime__c getMillesime(){
    return getMillesime(Date.today());
  }

  public static SICo_Millesime__c getMillesime(Date myDate){
    List<SICo_Millesime__c> datas = [SELECT Millesime__c FROM SICo_Millesime__c where StartDate__c <= :myDate and EndDate__c >= :myDate];
    if(datas.size() == 0){
      return null; 
    }
    return datas[0];
  }

  public static final List<SICo_Millesime__c> list_Millesimes;
  static{
    list_Millesimes = [SELECT Millesime__c,StartDate__c,EndDate__c FROM SICo_Millesime__c];
  }

//END
}