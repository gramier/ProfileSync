/*------------------------------------------------------------
Author:        Gerald Ramier
Company:       Salesforce.com
Description:   Web Service REST in order to provide notify SFDC of Integration process completion for Batch processing
Test Class:    SICo_BatchNotify_RWS_TEST
History
12/08/2016     Gerald Ramier     Creation
------------------------------------------------------------*/

@RestResource(urlMapping='/NotifyBatchCompletion')
global with sharing class SICo_BatchNotify_RWS  {

	/**
	 * @param
	 * @return
	 */
    @HttpPost
    global static SICO_Batch.Response create(SICO_Batch.Request batchNotification) {
        return SICO_Batch_Factory.processBatch(batchNotification);
    }

}