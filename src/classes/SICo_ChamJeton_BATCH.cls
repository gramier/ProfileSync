/**
* @author Boris Castellani
* @date 21/02/2017
* @LastModify 
*
* @description Build calendar for one week by agence account.
*/
global class SICo_ChamJeton_BATCH implements Database.Batchable<sObject> {
    public String query;

  global Database.QueryLocator start(Database.BatchableContext bc) {
    return Database.getQueryLocator([SELECT Name, ThursdayTimeRange2__c, ThursdayTimeRange3__c, ThursdayTimeRange4__c, RecordTypeId, 
                                     ThursdayTimeRange1__c, MondayTimeRange2__c, MondayTimeRange3__c, MondayTimeRange4__c, 
                                     MondayTimeRange1__c, TuesdayTimeRange2__c, TuesdayTimeRange3__c, TuesdayTimeRange4__c, 
                                     TuesdayTimeRange1__c, WednesdayTimeRange2__c, WednesdayTimeRange3__c, WednesdayTimeRange4__c, 
                                     WednesdayTimeRange1__c, SaturdayTimeRange2__c, SaturdayTimeRange3__c, SaturdayTimeRange4__c, 
                                     SaturdayTimeRange1__c, FridayTimeRange2__c, FridayTimeRange3__c, FridayTimeRange4__c, FridayTimeRange1__c 
                                     FROM Account WHERE RecordTypeId = :SICo_Utility.m_RTbyDeveloperName.get('Account_Agency')]);
  }
    
  global void execute(Database.BatchableContext BC, list<Account> accounts) {
      if(!accounts.isEmpty()){
          SICo_ChamJetonService_CLS weeklyCalendar = new SICo_ChamJetonService_CLS(); 
          weeklyCalendar.processCalendar(accounts);
      }      
  }

  global void finish(Database.BatchableContext BC) {}
}