public with sharing class SICO_ImportZuoraUsage {

    public static final String IMPORT_USAGE_INVOCE_ITEM = 'IMPORT_USAGE_INVOCE_ITEM';
    public static final String InvoceItemReportName = 'InvoiceItem';
    public static final String UsageReportName = 'ProcessedUsages';
    public static List<List<String>> allFields = new List<List<String>>();
    public static final String nulString = String.fromCharArray(new List<Integer> {0});
    
    public static Map<String, String> indexType = new Map<String, String> {
        'M' => 'Relève GRDF',
        'E' => 'Estimation',
        'A' => 'Auto-relève client',
        'T' => 'Données capteur Sowee',
        'K' => 'Rectifié',
        'C' => 'Corrigé'
	};
            
    public static Map<String, Integer> usageHeaderMap = new Map<String, Integer> {
        'BILLING_ACCOUNT_EXT_ID' => 0,
        'USAGE_EXT_ID' => 1,
        'INVOICE_EXT_ID' => 2,
        'INVOCE_ITEM_EXT_ID' => 3,
        'PRPC_EXT_ID' => 4,
        'SUB_EXT_ID' => 5,
        'START_INDEX' => 6,
        'END_INDEX' => 7,
        'START_DATE' => 8,
        'END_DATE' => 9,
        'PCS' => 10,
        'START_INDEX_TYPE' => 11,
        'END_INDEX_TYPE' => 12,
        'UOM' => 13,
        'QUANTITY' => 14,
        'CONSOMPTION_M3' => 15,
        'DATE_CSO_START' => 16,
        'DATE_CSO_END' => 17,
        'EVENT' => 18,
        'ITEM_TO_INVOICE' => 19
	};
	
    //Delete cols 17 21 25 26 27 38
    public static Map<String, Integer> invoceItemHeaderMap = new Map<String, Integer> {
        'INVOCE_ITEM_EXT_ID' => 0,
        'INVOICE_EXT_ID' => 1,
        'SUB_EXT_ID' => 2,
        'PRPC_EXT_ID' => 3,
        'SERVICE_START_DATE' => 4,
        'SERVICE_END_DATE' => 5,
        'UOM' => 6,
        'UNIT_PRICE' => 7,
        'ACCOUNTING_CODE' => 8,
        'CHARGE_AMOUNT' => 9,
        'TAX_CODE' => 10,
        'CHARGE_NAME' => 11,
        'CHARGE_DATE' => 12,
        'TAX_AMOUNT' => 13,
        'TAX_EXP_AMNT' => 14,
        'QUANTITY' => 15,
        'TAX_MODE' => 16,
        'PRODUCT_NAME' => 17,
        'CATEGORY' => 18,
        'PROCESS_TYPE' => 19,
        'CHARGE_NUM' => 20,
        'CHARGE_TYPE' => 21
	};
                            
    public static Integer MAX_NUM_LINE = 100;
    public static Integer MAX_STR_LEN = 200000;


    public static void launchProcess(String objectName) {

        // FIRST CALL - SEND QUERY
        String responseQuery = batchQuery(objectName).getBody();
        system.debug('*** responseQuery: ' + responseQuery);

        // CHECK RETURN FIRST RESPONCE
        String idJobs = '';
        if(!String.isBlank(responseQuery)){
            Map<String, Object> responseQueryMap = (Map<String, Object>) JSON.deserializeUntyped(responseQuery);
            idJobs = (String)responseQueryMap.get('id');
        }

        // SECOND CALL - CHECK STATUS
        String status = '';
        String fileId = '';
        if(idJobs != ''){
            for (Integer i = 0; i < 20; i++) {
                String responseJobStatus = checkStatus(idJobs).getBody(); 
                Map<String, Object> responseJobMap = (Map<String, Object>) JSON.deserializeUntyped(responseJobStatus);
                List<Object> respWrapper = (List<Object>) responseJobMap.get('batches');
                status = (String)((Map<String, Object>)respWrapper[0]).get('localizedStatus');
                if(status == 'completed'){
                    fileId = (String)((Map<String, Object>)respWrapper[0]).get('fileId');
                    break;
                }
            }
        }

        // THIRD CALL - GET DATA
        if(fileId != ''){
            String responseFile = getData(fileId).getBody();
            System.debug('### responseFile '+responseFile);
            while (!String.isBlank(responseFile)){
                List<String> tmpLines = new List<String>();
                Boolean isLastLine = false;
                try {
                	Integer endIndex = responseFile.length() >= MAX_STR_LEN ? MAX_STR_LEN : responseFile.length();
				    String subResponseFile = responseFile.substring(0, endIndex);
				    isLastLine = !subResponseFile.contains('\n');
		            
                    tmpLines = !isLastLine 
				    	? subResponseFile.split('\n', MAX_NUM_LINE)
				    	: new List<String> {subResponseFile};

		    		String lastLine = tmpLines.size() > 1
			        	? tmpLines.remove(tmpLines.size() - 1)
			        	: '';

                    responseFile = lastLine + responseFile.substring(endIndex, responseFile.length());
                    if (!tmpLines.isEmpty()) {
                        SICO_ImportZuoraUsage.ImportZuoraUsageInvoceItemQueue izq = new SICO_ImportZuoraUsage.ImportZuoraUsageInvoceItemQueue(tmpLines, objectName);
                        SICO_QueueChaining_Utility.queueChainingOrchestrator.addQueue(izq);     
                        tmpLines = new List<String>();
                    }
                } catch (Exception e) {
                    System.debug('### An error occured on runAndRetrieveReport: ' + e.getMessage());
                }
                if (isLastLine) break;
            }
            // add the Usage import
            if(SICO_ImportZuoraUsage.UsageReportName.equals(objectName)) {
                SICO_QueueChaining_Utility.queueChainingOrchestrator.addQueue(new InvoiceRollupQueue());
            } else if (SICO_ImportZuoraUsage.InvoceItemReportName.equals(objectName)) {
                SICO_QueueChaining_Utility.queueChainingOrchestrator.addQueue(new SICO_ImportZuoraUsage.LaunchZuoraUsageImportQueue());
            }
            
            if(!Test.isRunningTest()){
                system.debug('### runandRetrieveReport - launching Queue: ');
                SICO_QueueChaining_Utility.queueChainingOrchestrator.executeQueueChain();
                system.debug('### runandRetrieveReport - Ended Queue: ');
            }  
        }
    }

    // Callout WebService
    public static HttpResponse basicCallout(String method,String endPoint, String body) {
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:Zuora_api/'+endPoint);
        request.setMethod(method);

        //Blob headerValue = Blob.valueOf('{!$Credential.UserName}:{!$Credential.Password}');
        //String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

        request.setHeader('Content-Type', 'application/json');
        //request.setHeader('Authorization', authorizationHeader);
        if (!String.isBlank(body) && method == 'POST') {
            request.setBody(body);
        }

        Http http = new Http();
        HttpResponse response = http.send(request);
        return response;
    }

    public static HTTPResponse checkStatus(String idJob){
        String  endpoint = 'batch-query/jobs/' + idJob;
        return basicCallout('GET', endpoint, null);
    }

    public static HTTPResponse getData(String idFile){
        String  endpoint = 'file/' + idFile;
        return basicCallout('GET', endpoint, null);
    }

    public static HTTPResponse batchQuery(String objectName){
        String  endpoint = 'batch-query/';
		
		Integer numberOfDays = -5;
		if (!String.isBlank(System.Label.ZuoraImportUsageDate) ) {
			try {
				numberOfDays = -1 * Integer.valueOf(System.Label.ZuoraImportUsageDate);
			} catch(Exception e) {
				numberOfDays = -5;
			} 
		}
		
        Date dt = Date.today();
        dt = dt.addDays(numberOfDays);
        List<String> rrs = String.valueOf(dt).split(' ');

        String query = '';
        if(InvoceItemReportName.equals(objectName)){
            query = 'select Id,Invoice.Id,Subscription.Id,ProductRatePlanCharge.Id,ServiceStartDate,ServiceEndDate,UOM,UnitPrice,AccountingCode,ChargeAmount,TaxCode,ChargeName,ChargeDate,TaxAmount,'+
                    'TaxExemptAmount,Quantity,TaxMode,Product.Name,ProductRatePlanCharge.Category__c,ProcessingType,RatePlanCharge.ChargeNumber,ProductRatePlanCharge.ChargeType,Account.Currency '+ 
                    'from InvoiceItem where Invoice.Status = \'Posted\' and UpdatedDate >= \''+ rrs[0] +'\'';
        }else{
            query = 'select Account.Id,Usage.Id,Invoice.Id,InvoiceItem.Id,ProductRatePlanCharge.Id,Subscription.Id,Usage.MeterReadStart__c,Usage.MeterReadEnd__c,Usage.StartDateTime,Usage.EndDateTime,'+
                    'Usage.PCS__c,Usage.MeterReadStartType__c,Usage.MeterReadEndType__c,Usage.UOM,Usage.Quantity,Usage.m3Volume__c,Usage.DateCsoStart__c,Usage.DateCsoEnd__c,Usage.Event__c,'+
                    'Usage.ExtraBilledLine__c from ProcessedUsage where Invoice.Status = \'Posted\' and Usage.UpdatedDate >= \''+ rrs[0] +'\'';
        }

        String body= '{'+
                     '"format" : "csv",'+
                     '"version" : "1.2",'+
                     '"name" : "'+objectName+'",'+
                     '"encrypted" : "none",'+
                     '"useQueryLabels" : "true",'+
                     '"partner" : "salesforce",'+
                     '"project" : "'+UserInfo.getOrganizationId()+'",'+
                     '"dateTimeUtc" : "true",'+
                     '"queries"  : [ {'+
                        '"name" : "'+objectName+'",'+
                        '"query" : "'+query+'",'+
                        '"type" : "zoqlexport"'+
                        '}]'+
                     '}';

        return basicCallout('POST',endpoint,body);
    }
    
    public static void importZuoraReportJob (List<String> lines,String objectName) {
        system.debug('### runandRetrieveReport - Launched importZuoraReportJob');
        if (lines != null && !lines.isEmpty()) {
            parseCSV(lines, false);
            
            system.debug('### runandRetrieveReport - allFields: ' + allFields.size());

            Map<String, SiCo_InvoiceItem__c> ilis = new Map<String, SiCo_InvoiceItem__c>();
            Map<String, Usage__c> usages = new Map<String, Usage__c>(); 
            
            Set<String> zSubIds = new Set<String>();
            for (Integer i = 0; i < allFields.size(); i++) {
                Integer tmpSubExtIdPosition = InvoceItemReportName.equalsIgnoreCase(objectName)
                    ? invoceItemHeaderMap.get('SUB_EXT_ID')
                    : usageHeaderMap.get('SUB_EXT_ID');
                zSubIds.add(allFields[i][tmpSubExtIdPosition]);
            }
            
            List<Zuora__Subscription__c> subscrip = [SELECT Id,Zuora__External_Id__c,Zuora__OriginalId__c FROM Zuora__Subscription__c where ZuoraOriginalIndexedId__c = :zSubIds];
            
            Map<String, String> subscripOriginalIdMap = new Map<String, String>();
            for (Zuora__Subscription__c zSub : subscrip) {
                subscripOriginalIdMap.put(zSub.Zuora__OriginalId__c, zSub.Zuora__External_Id__c);
            }

            for(List<String> fields : allFields){
                system.debug('*** fields: ' + fields);
                system.debug('*** subscripOriginalIdMap: ' + subscripOriginalIdMap);
                if(InvoceItemReportName.equalsIgnoreCase(objectName)){
                    SiCo_InvoiceItem__c tmpIli = createInvoceItem (fields, subscripOriginalIdMap); 
                    ilis.put(tmpIli.Zuora_External_ID__c, tmpIli);
                }  
                if(UsageReportName.equalsIgnoreCase(objectName)){
                    Usage__c tmpUsg = createUsage (fields, subscripOriginalIdMap);
                    usages.put(tmpUsg.ZuoraExternalId__c, tmpUsg);
                }
            }

            system.debug('### runandRetrieveReport - ilis: ' + ilis.size());
            system.debug('### runandRetrieveReport - usages: ' + usages.size());
            
            Database.UpsertResult[] iliRes = database.upsert(ilis.values(), SiCo_InvoiceItem__c.Fields.Zuora_External_ID__c, false);
            for (Database.UpsertResult res : iliRes) {
            	if (!res.isSuccess()) {
            		system.debug('### runandRetrieveReport - failed upsert: ' + res);
            	}
            }
            
            Database.UpsertResult[] usgRes = database.upsert(usages.values(), Usage__c.Fields.ZuoraExternalId__c, false);
            for (Database.UpsertResult res : usgRes) {
            	if (!res.isSuccess()) {
            		system.debug('### runandRetrieveReport - failed upsert: ' + res);
            	}
            }
        }
    }
    
    public static void parseCSV(List<String> lines, Boolean skipHeaders) { 

        String modifyData = null;
        String originalData = null;
        for(String line: lines) {
            if (line.replaceAll(',','').trim().length() == 0) break;
            while (line.substringBetween('"','"')  != null){
                modifyData = line.substringBetween('"','"');
                originalData = '"'+modifyData+'"';
                modifyData = modifyData.replace(',','.');
                line = line.replace(originalData,modifyData);
                modifyData = null;
                originalData = null;
            }
            List<String> fields = line.split(','); 
            allFields.add(fields);
        }
        if (skipHeaders) allFields.remove(0);
    }
    
    public static SiCo_InvoiceItem__c createInvoceItem (List<String> line, Map<String, String> subscripOriginalIdMap) {
        SiCo_InvoiceItem__c ili = new SiCo_InvoiceItem__c();
        system.debug('### ili draft line: ' + line);
        try {
            Zuora__ZInvoice__c  zinv = new Zuora__ZInvoice__c  (Zuora__External_Id__c = line[invoceItemHeaderMap.get('INVOICE_EXT_ID')]);
            zqu__ProductRatePlanCharge__c zprpc = new zqu__ProductRatePlanCharge__c (zqu__ZuoraId__c = line[invoceItemHeaderMap.get('PRPC_EXT_ID')]);
            
            ili.InvoiceId__r = zinv;
            ili.ProductRatePlanChargeId__r = zprpc;
            
            ili.Zuora_External_ID__c = line[invoceItemHeaderMap.get('INVOCE_ITEM_EXT_ID')];
            ili.ProductName__c = line[invoceItemHeaderMap.get('PRODUCT_NAME')];
            ili.UOM__c = line[invoceItemHeaderMap.get('UOM')];
            ili.AccountingCode__c = line[invoceItemHeaderMap.get('ACCOUNTING_CODE')];
            ili.Category__c = line[invoceItemHeaderMap.get('CATEGORY')];
            ili.ChargeName__c = line[invoceItemHeaderMap.get('CHARGE_NAME')];
            ili.ChargeNumber__c = line[invoceItemHeaderMap.get('CHARGE_NUM')];
            ili.ChargeType__c = line[invoceItemHeaderMap.get('CHARGE_TYPE')];
            ili.ProcessingType__c = line[invoceItemHeaderMap.get('PROCESS_TYPE')];
            ili.TaxMode__c = line[invoceItemHeaderMap.get('TAX_MODE')];
            ili.TaxCode__c = line[invoceItemHeaderMap.get('TAX_CODE')];
            ili.SubscriptionNumber__c = subscripOriginalIdMap.get(line[invoceItemHeaderMap.get('SUB_EXT_ID')]);
            
            ili.ChargeAmount__c = formatDecimal (line[invoceItemHeaderMap.get('CHARGE_AMOUNT')]);
            ili.Quantity__c = formatDecimal (line[invoceItemHeaderMap.get('QUANTITY')]);
            ili.TaxAmount__c = formatDecimal (line[invoceItemHeaderMap.get('TAX_AMOUNT')]);
            ili.TaxExemptAmount__c = formatDecimal (line[invoceItemHeaderMap.get('TAX_EXP_AMNT')]);
            ili.TotalAmount__c =  formatDecimal (line[invoceItemHeaderMap.get('UNIT_PRICE')]);
            ili.UnitPrice__c = formatDecimal (line[invoceItemHeaderMap.get('UNIT_PRICE')]);
            
            String tmpChargeDate = line[invoceItemHeaderMap.get('CHARGE_DATE')];
            ili.ChargeDate__c = !String.isBlank(tmpChargeDate)
                ? buildDateFromDateTime(tmpChargeDate) : null;
            
            String tmpServiceStartDate = line[invoceItemHeaderMap.get('SERVICE_START_DATE')];
            ili.ServiceStartDate__c = !String.isBlank(tmpServiceStartDate)
                ? buildDateFromDateTime(tmpServiceStartDate) : null;
            
            String tmpServiceEndDate = line[invoceItemHeaderMap.get('SERVICE_END_DATE')];
            ili.ServiceEndDate__c = !String.isBlank(tmpServiceEndDate)
                ? buildDateFromDateTime(tmpServiceEndDate) : null;
            
            system.debug('### ili: ' + ili);
        } catch (Exception e) {
            System.debug('###  An error occurred on ' + IMPORT_USAGE_INVOCE_ITEM + ': ' + SICo_Constants_CLS.SICO + ' - ' + e.getMessage());
        }     
        return ili;
    }
    
    public static Usage__c  createUsage (List<String> line, Map<String, String> subscripOriginalIdMap){     
        Usage__c usage = new Usage__c ();
        system.debug('### usage draft line: ' + line);
        try {
            Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__External_Id__c = line[usageHeaderMap.get('BILLING_ACCOUNT_EXT_ID')]);
            Zuora__Subscription__c zsub = new Zuora__Subscription__c (Zuora__External_Id__c = subscripOriginalIdMap.get(line[usageHeaderMap.get('SUB_EXT_ID')]));
            Zuora__ZInvoice__c  zinv = new Zuora__ZInvoice__c  (Zuora__External_Id__c = line[usageHeaderMap.get('INVOICE_EXT_ID')]);
            SiCo_InvoiceItem__c  zili = new SiCo_InvoiceItem__c  (Zuora_External_ID__c = line[usageHeaderMap.get('INVOCE_ITEM_EXT_ID')]);
            zqu__ProductRatePlanCharge__c zprpc = new zqu__ProductRatePlanCharge__c (zqu__ZuoraId__c = line[usageHeaderMap.get('PRPC_EXT_ID')]);
            
            usage.ZuoraExternalId__c = line[usageHeaderMap.get('USAGE_EXT_ID')];
            
            usage.BillingAccount__r = zca;
            usage.Invoice__r = zinv;
            usage.InvoiceItem__r = zili;
            usage.ProductRatePlanCharge__r = zprpc;
            usage.Subscription__r = zsub;
            
            usage.UOM__c = line[usageHeaderMap.get('UOM')];
            usage.Quantity__c = formatDecimal (line[usageHeaderMap.get('QUANTITY')]);
            
            usage.Event__c = line.size() > usageHeaderMap.get('EVENT') 
            	? line[usageHeaderMap.get('EVENT')]
            	: null;
            usage.ExtraBilledLine__c =  line.size() > usageHeaderMap.get('ITEM_TO_INVOICE') 
            	? line[usageHeaderMap.get('ITEM_TO_INVOICE')]
            	: null;
            
            //if it's a Gas Usage
            //if (line.size() > 15) {
            	try {
		            usage.ConsumptionM3__c = formatDecimal (line[usageHeaderMap.get('CONSOMPTION_M3')]);
		            usage.StartIndex__c = formatDecimal (line[usageHeaderMap.get('START_INDEX')]);
		            usage.EndIndex__c = formatDecimal (line[usageHeaderMap.get('END_INDEX')]);
		            usage.PCS__c = formatDecimal (line[usageHeaderMap.get('PCS')]);
		            
		            String tmpStartDate = line[usageHeaderMap.get('START_DATE')];
		            usage.StartDate__c = !String.isBlank(tmpStartDate) 
		                ? buildDateFromDateTime(tmpStartDate) : null;
		            
		            String tmpEndDate = line[usageHeaderMap.get('END_DATE')];
		            usage.EndDate__c = !String.isBlank(tmpEndDate)
		                ? buildDateFromDateTime(tmpEndDate) : null;
		            
		            String tmpStartIndexType = line[usageHeaderMap.get('START_INDEX_TYPE')];
		            usage.StartIndexType__c = indexType.get(tmpStartIndexType);
		            
		            String tmpEndIndexType = line[usageHeaderMap.get('END_INDEX_TYPE')];
		            usage.EndTypeIndex__c = indexType.get(tmpEndIndexType);
		            
		            String tmpCsoStartDate = line[usageHeaderMap.get('DATE_CSO_START')];
		            usage.StartDateCSO__c = !String.isBlank(tmpStartDate) 
		                ? buildDateFromDate(tmpCsoStartDate) : null;
		            
		            String tmpCsoEndDate = line[usageHeaderMap.get('DATE_CSO_END')];
		            usage.EndDateCSO__c = !String.isBlank(tmpCsoEndDate)
		                ? buildDateFromDate(tmpCsoEndDate) : null;
            	} catch (Exception e) {
            		System.debug('### An error occurred on ' + IMPORT_USAGE_INVOCE_ITEM + ' (some mandatory value is missing): ' + SICo_Constants_CLS.SICO + ' - ' + e.getMessage());
            	}
            //}
            
            system.debug('### usage: ' + usage);
        } catch (Exception e) {
            System.debug('###  An error occurred on ' + IMPORT_USAGE_INVOCE_ITEM + ': ' + SICo_Constants_CLS.SICO + ' - ' + e.getMessage());
        }
        return usage;
    }
    
    public static Date buildDateFromDateTime (String strDate) {
        try {
            if (!String.isBlank(strDate)) {
                List<String> partDate = strDate.split('T');
                List<String> tmpDate = partDate[0].split(' ')[0].split('-');
                Date dt = Date.newInstance (Integer.valueOf(tmpDate.get(0).trim()), Integer.valueOf(tmpDate.get(1).trim()), Integer.valueOf(tmpDate.get(2).trim()));
                return dt;
            }
        } catch (Exception e) {
            System.debug('###  An error occurred on ' + IMPORT_USAGE_INVOCE_ITEM + ': ' + SICo_Constants_CLS.SICO + ' - ' + e.getMessage());
        }
        return null;
    }
    
    public static Date buildDateFromDate (String strDate) {
        try {
            if (!String.isBlank(strDate)) {
                List<String> tmpDate = strDate.split('/');
                Date dt = Date.newInstance (Integer.valueOf(tmpDate.get(2).trim()), Integer.valueOf(tmpDate.get(0).trim()), Integer.valueOf(tmpDate.get(1).trim()));
                return dt;
            }
        } catch (Exception e) {
            System.debug('###  An error occurred on ' + IMPORT_USAGE_INVOCE_ITEM + ': ' + SICo_Constants_CLS.SICO + ' - ' + e.getMessage());
        }
        return null;
    }
    
    public static Decimal formatDecimal (String strDecimal) {
        try {
            if (!String.isBlank(strDecimal)) {
                return Decimal.valueOf(strDecimal.replace(',', ''));
            }
        } catch (Exception e) {
            System.debug('###  An error occurred on ' + IMPORT_USAGE_INVOCE_ITEM + ': ' + SICo_Constants_CLS.SICO + ' - ' + e.getMessage());
        }
        return null;
    }
    
    public class SICO_ImportZuoraUsageException extends Exception {}
    
    public class ImportZuoraUsageInvoceItemQueue extends SICO_QueueChaining_Utility {
        public List<String> lines;
        private String objectName;
        public ImportZuoraUsageInvoceItemQueue(List<String> lines,String objectName) {
            this.lines = lines;
            this.objectName=objectName;
        }
        public override void doJob() {
            SICO_ImportZuoraUsage.importZuoraReportJob(this.lines,this.objectName);
        }
    }
    
    public class LaunchZuoraUsageImportQueue extends SICO_QueueChaining_Utility {
        public LaunchZuoraUsageImportQueue() { }     
        public override void doJob() {
            Database.executeBatch(new SICo_Batch_BILL_RUN(SICO_ImportZuoraUsage.UsageReportName));                
        }
    }
    
    public class InvoiceRollupQueue extends SICO_QueueChaining_Utility {     
        public InvoiceRollupQueue() { }
        public override void doJob() {
            Database.executeBatch(new Sico_InvoiceRollup());                       
        }
    }

}