@isTest
private class SICo_UpdateBankDetails_VFCNoAllData_Test {
	@isTest 
    static void test_cache() {
    	//Insert new community user
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;
        communityUser = [SELECT AccountId, ContactId, ProfileId FROM User WHERE Id = :communityUser.Id LIMIT 1];

    	//Insert custom settings
    	ZuoraWSconfig__c zc = new ZuoraWSconfig__c();
		zc.Uri__c = 'Test Uri';
		zc.PageIdSEPA__c = 'Test PageId';
        zc.PageIdCB__c = 'Test PageId';
        zc.ExternalPageIdSEPA__c = 'Test PageId';
        zc.ExternalPageIdCB__c = 'Test PageId';
        zc.GatewayNameSEPA__c  = 'SEPA';
        zc.SetupOwnerId = communityUser.ProfileId;
		insert zc;

        //select account
        Account acc = [SELECT Id, IsPersonAccount FROM Account WHERE Id = :communityUser.AccountId LIMIT 1]; 
 
    	//Run as Community User
        system.runAs(communityUser){
            Test.startTest();
        	Test.setMock(HttpCalloutMock.class, new SICo_UpdateBankDetails_Mock());
            //set controller
            SICo_UpdateBankDetails_VFC controller = new SICo_UpdateBankDetails_VFC();
            //set VF
            Test.setCurrentPageReference(new PageReference('Page.UpdateBankDetails'));
            //set url parameters
			System.currentPageReference().getParameters().put('AccountId', communityUser.AccountId);
			String jsonSignature = SICo_UpdateBankDetails_VFC.processSignature();            
			Boolean updateDefaultAndOldPaymentMethodResult = SICo_UpdateBankDetails_VFC.updateDefaultAndOldPaymentMethod(acc.Id, 'testNewZuoraPaymentMethodId');
			 
			// Get back value from cache
            String paymentMethodRecentlyUpdated = (String)SICo_PlatformCacheManager_CLS.getSessionObject(SICo_Constants_CLS.SESSION_CACHE_KEY_PAYMENTMETHODUPDATE + communityUser.AccountId);
           		 
            Test.stopTest(); 
            //asserts
            System.assert(paymentMethodRecentlyUpdated != null);
        }
	}
}