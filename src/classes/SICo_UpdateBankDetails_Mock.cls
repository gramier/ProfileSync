@isTest
public class SICo_UpdateBankDetails_Mock implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {

        // Create a fake response
        HttpResponse res = new HttpResponse();

        res.setHeader('Content-Type', 'application/json');     
    	res.setStatus('OK');
    	res.setBody('{"signature":"test","token":"test","tenantId":"test","key":"test"}');

        return res;
    }
}