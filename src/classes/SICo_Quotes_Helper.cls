/*------------------------------------------------------------
Author:        David LEROUX
Company:       Ikumbi Solutions
Description:   Helper for MyQuotes page.
Test Class:    SICo_MyQuotes_VFC_TEST
History
21/10/2016     David LEROUX     Create version
------------------------------------------------------------*/
public with sharing class SICo_Quotes_Helper {

	// quote Const status
	public static String STATUS_ACCEPTED        = System.Label.SICo_QuoteAccepted;
	public static String STATUS_REFUSED         = System.Label.SICo_QuoteRefused;
	public static String STATUS_OBSOLETE        = System.Label.SICo_QuoteObsolete; 
	public static String STATUS_VALIDATION_TODO = System.Label.SICo_QuoteValidationTodo; 


	public SICo_Quotes_Helper() {	}

	//Quote Wrapper (used to display only few information of the quote)
	public class QuoteWrapLite {		
		@AuraEnabled
		public String quoteId { get; set; }
		@AuraEnabled
		public DateTime creationDate { get; set; }
		@AuraEnabled
		public String numberId { get; set; }
		@AuraEnabled 
		public String statusLabel { get; set; }
		@AuraEnabled
		public DateTime acceptationDate { get; set; } 
		@AuraEnabled
		public String status { get; set; }
	}


	//Quote Wrapper (used to display full information of the quote including quote lines)
	public class QuoteDetailWrap {
		@AuraEnabled
		public String quoteId { get; set; }
		@AuraEnabled
		public DateTime creationDate { get; set; }
		@AuraEnabled
		public String numberId { get; set; }
		@AuraEnabled 
		public String status { get; set; }
		@AuraEnabled
		public DateTime acceptationDate { get; set; }  
		@AuraEnabled
		public Decimal totalPriceHT { get; set; }
		@AuraEnabled
		public Decimal totalVAT { get; set; }
		@AuraEnabled
		public Decimal totalPriceTTC { get; set; }
		// Address
		@AuraEnabled
		public String firstName { get; set; }
		@AuraEnabled
		public String lastName { get; set; }
		@AuraEnabled
		public String addrFlat { get; set; }
		@AuraEnabled
		public String addrBuilding { get; set; }
		@AuraEnabled
		public String addrZipCode { get; set; }
		@AuraEnabled
		public String addrAddition { get; set; }
		@AuraEnabled
		public String addrStair { get; set; }
		@AuraEnabled
		public String addrFloor { get; set; }
		@AuraEnabled
		public String addrStreetNumber { get; set; }
		@AuraEnabled
		public String addrCountry { get; set; }
		@AuraEnabled
		public String addrCity { get; set; }
		// Quote lines
		@AuraEnabled
		public QuoteLineWrap[] quoteLines { get; set; }
	}	

	// Quote lines wrap
	public class QuoteLineWrap {
		@AuraEnabled
		public Decimal lineNumber { get; set; }
		@AuraEnabled
		public String label { get; set; }
		@AuraEnabled
		public Decimal quantity { get; set; }
		@AuraEnabled
		public Decimal priceHT { get; set; }
		@AuraEnabled
		public String taxRate { get; set; }
		@AuraEnabled
		public Decimal priceTTC { get; set; }
		@AuraEnabled
		public String quoteLineType { get; set; }
		@AuraEnabled
		public Boolean isTyped { get; set; }
	}

	/**
	* @Param : 
	* pQuoteId => Id of the quote
	* pQuote => Object view
	* pStatus => Status to set
	* @Return :
	* pQuote with the updated status
	*/
	public static QuoteDetailWrap changeQuoteStatus(String pQuoteId, QuoteDetailWrap pQuote, String pStatus) {
		if (pQuoteId == null || pQuote == null) {
			return null;
		}
		try {
			MaintenancyQuote__c vMQuote = [SELECT Id, MaintenancyQuoteNumber__c FROM MaintenancyQuote__c wHERE Id = :pQuoteId];
 			vMQuote.MaintenancyQuoteStatus__c = pStatus;
 			update vMQuote;
 			pQuote.status = pStatus;
		} catch (Exception e) {
			System.debug('Error : No valid Id entered');
			return null;
		}

		return pQuote;
	}

	/**
	* Returns the quote number (needed in the titles of the VFP) to be displayed
	* @Param : id of the quote
	*/
	public static String getQuoteNumberById(String pId) {
		if (pId == null || String.isBlank(pId)) {
			return null;
		}
		try {
			MaintenancyQuote__c vQuote = [SELECT Id, MaintenancyQuoteNumber__c FROM MaintenancyQuote__c wHERE Id = :pId];
			return vQuote.MaintenancyQuoteNumber__c;
		} catch (Exception e) {
			System.debug('Error : No valid Id entered');
		}

		return null;
	}

	/**
	* Get the detail of the quote
	* @Param : the quote id asked 
	*/
	@AuraEnabled
	public static QuoteDetailWrap getQuoteDetailById(String pQuoteId) {
		if (pQuoteId == null) {
			return null;
		}
		MaintenancyQuote__c vMQuote = [SELECT Id, CreatedDate, MaintenancyQuoteNumber__c, MaintenancyQuoteStatus__c, AcceptedDate__c,
												TotalHTAmount__c, TotalTTCAmount__c, TotalVatAmount__c, 
												Customer__r.FirstName, Customer__r.LastName, 
												Customer__r.ContactAddressFlat__c, 
												Customer__r.ContactAdresseBuilding__c,
												Customer__r.ContactAddressZipCode__c,
												Customer__r.ContactAddressAdditionToAddress__c,
												Customer__r.ContactAddressStair__c,
												Customer__r.ContactAddressFloor__c,
												Customer__r.ContactAddressNumberStreet__c,
												Customer__r.ContactAddressCountry__c,
												Customer__r.ContactAddressCity__c,
													(SELECT Id,
														LineNumber__c, Label__c, Quantity__c, TaxRate__c, BillingType__c, IsBillable__c,
														UnitPriceWithoutTax__c, LineTotalHTAmount__c, LineTotalAmount__c, LineTotalVatAmount__c
													FROM Lignes_de_devis__r)
											FROM MaintenancyQuote__c
											WHERE Id = :pQuoteId]; 
		QuoteDetailWrap vDetailQuote = parseMaintenancyQuoteToDetailQuote(vMQuote); 

		return vDetailQuote;
	}

	/*
	* Parse a quote SFDC object (maintenancy quote) into a quote detail wrap
	* @param : the quote to be parsed
	*/
	public static QuoteDetailWrap parseMaintenancyQuoteToDetailQuote(MaintenancyQuote__c pMQuote) { 
		if (pMQuote == null) {
			return null;
		}
		QuoteDetailWrap vDetailQuote    = new QuoteDetailWrap();
		vDetailQuote.quoteId            = pMQuote.Id; 			
		vDetailQuote.numberId           = pMQuote.MaintenancyQuoteNumber__c;	
		vDetailQuote.creationDate       = pMQuote.CreatedDate;
		vDetailQuote.status             = pMQuote.MaintenancyQuoteStatus__c;
		vDetailQuote.acceptationDate    = pMQuote.AcceptedDate__c; 
		vDetailQuote.totalPriceHT       = pMQuote.TotalHTAmount__c;
		vDetailQuote.totalVAT           = pMQuote.TotalVatAmount__c;
		vDetailQuote.totalPriceTTC      = pMQuote.TotalTTCAmount__c;	

		// Parse address
		vDetailQuote = parseAddressMaintenancyQuoteToDetailQuote(pMQuote, vDetailQuote);
		// Parse lines of quote
		vDetailQuote = parseLinesQuoteMaintenancyQuoteToDetailQuote(pMQuote, vDetailQuote);
		
				
		return vDetailQuote;
	}


	/**
	* Parse the lines quote included in a quote SFDC object (maintenancy quote) into a quote detail wrap
	* @param : 
	* pMQuote : the quote to be parsed
	* pQuoteDetail : the quote wrapper in progress to parse
	*/
	public static QuoteDetailWrap parseLinesQuoteMaintenancyQuoteToDetailQuote(MaintenancyQuote__c pMQuote, QuoteDetailWrap pQuoteDetail) { 
		if (pQuoteDetail == null || pMQuote == null) {
			return null;
		}
		pQuoteDetail.quoteLines = new List<QuoteLineWrap>();
		if (pMQuote.Lignes_de_devis__r != null && pMQuote.Lignes_de_devis__r.size() > 0) {
			for (MaintenancyLineOfQuote__c vMQuoteLine : pMQuote.Lignes_de_devis__r) {
				QuoteLineWrap vQuoteLineWrap = new QuoteLineWrap();
				vQuoteLineWrap.lineNumber    = vMQuoteLine.LineNumber__c;
				vQuoteLineWrap.label         = vMQuoteLine.Label__c;
				vQuoteLineWrap.quantity      = vMQuoteLine.Quantity__c;
				vQuoteLineWrap.priceHT       = vMQuoteLine.UnitPriceWithoutTax__c;
				vQuoteLineWrap.taxRate       = vMQuoteLine.TaxRate__c;	
				vQuoteLineWrap.priceTTC      = vMQuoteLine.LineTotalAmount__c;
				vQuoteLineWrap.quoteLineType = vMQuoteLine.BillingType__c;
				vQuoteLineWrap.isTyped       = !vMQuoteLine.IsBillable__c;
				pQuoteDetail.quoteLines.add(vQuoteLineWrap);
			}
		}

		return pQuoteDetail;
	}

	/**
	* Parse the adress information included in a quote SFDC object (maintenancy quote) into a quote detail wrap
	* @param : 
	* pMQuote : the quote to be parsed
	* pQuoteDetail : the quote wrapper in progress to parse
	*/
	public static QuoteDetailWrap parseAddressMaintenancyQuoteToDetailQuote(MaintenancyQuote__c pMQuote, QuoteDetailWrap pQuoteDetail) { 
		if (pQuoteDetail == null || pMQuote == null) {
			return null;
		}

		if (pMQuote.Customer__r != null) {
			pQuoteDetail.firstName		  = pMQuote.Customer__r.FirstName;
			pQuoteDetail.lastName		  = pMQuote.Customer__r.LastName;
			pQuoteDetail.addrFlat         = pMQuote.Customer__r.ContactAddressFlat__c;
			pQuoteDetail.addrBuilding     = pMQuote.Customer__r.ContactAdresseBuilding__c;
			pQuoteDetail.addrZipCode      = pMQuote.Customer__r.ContactAddressZipCode__c;
			pQuoteDetail.addrAddition     = pMQuote.Customer__r.ContactAddressAdditionToAddress__c;
			pQuoteDetail.addrStair        = pMQuote.Customer__r.ContactAddressStair__c;
			pQuoteDetail.addrFloor        = pMQuote.Customer__r.ContactAddressFloor__c;
			pQuoteDetail.addrStreetNumber = pMQuote.Customer__r.ContactAddressNumberStreet__c;
			pQuoteDetail.addrCountry      = pMQuote.Customer__r.ContactAddressCountry__c;
			pQuoteDetail.addrCity         = pMQuote.Customer__r.ContactAddressCity__c;
		}

		return pQuoteDetail;
	}

	/**
	* Get the list of quotes of a given account as a wrap lite object
	* @Param : the account id associated to the quotes
	*/
	public static List<QuoteWrapLite> getQuotesWrappedByAccountId(String pAccountId) {
		if (pAccountId == null) {
			return null;
		}
		List<QuoteWrapLite> vQuoteWrapList = new List<QuoteWrapLite>(); 
		List<MaintenancyQuote__c> vQuoteList = [SELECT Id, CreatedDate, MaintenancyQuoteNumber__c, MaintenancyQuoteStatus__c, AcceptedDate__c
													FROM MaintenancyQuote__c
													WHERE Customer__c = :pAccountId
													AND Site__c IN (SELECT Id 
																		FROM SICo_Site__c
																		WHERE Subscriber__c = :pAccountId 
																		AND IsSelectedInCustomerSpace__c = true)
													ORDER BY CreatedDate DESC]; 
		if (vQuoteList != null && vQuoteList.size() > 0) {
			vQuoteWrapList = parseMaintenancyQuoteListToQWL(vQuoteList);
		} else {
			return null;
		}

		return vQuoteWrapList;
	}

	/**
	* Parse a list of quote SFDC object (maintenancy quote) into a List of wrapper quote
	* @param : The list of quotes to be parsed
	*/
	public static List<QuoteWrapLite> parseMaintenancyQuoteListToQWL(List<MaintenancyQuote__c> pMQuotes) { 
		if (pMQuotes == null || pMQuotes.size() == 0) {
			return null;
		}

		List<QuoteWrapLite> vQuoteWrapList = new List<QuoteWrapLite>(); 
		for (MaintenancyQuote__c vMQuote : pMQuotes) {
			QuoteWrapLite vQuoteWrap = parseMaintenancyQuoteToQWL(vMQuote);
			vQuoteWrapList.add(vQuoteWrap);
		}

		return vQuoteWrapList;
	}

	/**
	* Parse a quote SFDC object (maintenancy quote) into a wrapper quote
	* @param : The quote to be parsed
	*/
	public static QuoteWrapLite parseMaintenancyQuoteToQWL(MaintenancyQuote__c pMQuote) { 
		if (pMQuote == null) {
			return null;
		}

		QuoteWrapLite vQuote   = new QuoteWrapLite();
		vQuote.quoteId         = pMQuote.Id;
		vQuote.creationDate    = pMQuote.CreatedDate;
		vQuote.numberId        = pMQuote.MaintenancyQuoteNumber__c;
		vQuote.status          = pMQuote.MaintenancyQuoteStatus__c;
		vQuote.acceptationDate = pMQuote.AcceptedDate__c;
		if (vQuote.status == STATUS_ACCEPTED) {
			vQuote.statusLabel = String.format(System.Label.SICo_QuoteAcceptedDetail, new String[]{vQuote.acceptationDate.format('dd/MM/yyyy')});
		} else {
			vQuote.statusLabel = pMQuote.MaintenancyQuoteStatus__c;
		}
 
		return vQuote;
	}

}