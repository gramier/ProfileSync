global class SICo_OwnershipMgt_BATCH implements Database.Batchable<sObject> {
    public String query;
    public String ownerId;

    global SICo_OwnershipMgt_BATCH(String query, String ownerId) {
        this.query = query;
        this.ownerId = ownerId;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
 
       for ( sObject so : scope ) {
           so.put( 'OwnerId', this.ownerId );
       }
       update scope;
    }
    
    global void finish(Database.BatchableContext BC) {

    }
}
