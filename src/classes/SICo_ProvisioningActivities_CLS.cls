/**
* @author Boris Castellani
* @date 13/07/2016
* @LastModify 11/01/2017
*
* @description Calcul provisioning after Subscription insert
*/
public with sharing class SICo_ProvisioningActivities_CLS {

  /* ----- EDELIA OSF ----- */    
  private static Map<Id,Task> subscriptions;
  private static Map<ID,Sico_AdminTask__mdt> subscriptionFeatures;
  /* ----- GRDF ----- */
  private static Map<Id,Task> grdf;
  private static Map<ID,Sico_AdminTask__mdt> grdfFeatures;
  /* ----- CHAM ----- */
  private static Map<ID,Task> chamClients;
  private static Map<ID,Sico_AdminTask__mdt> chamClientFeatures;
  private static Map<id,Task> chamDemandes;
  private static Map<id,Sico_AdminTask__mdt> chamDemandeFeatures;

  // lunch Provisioning Activities callout with list of Task
  public static Boolean launchProvisioningActivitiesSync(List<Task> tasksList, SICo_FluxSubscriptionModel.Address newSiteAdress){
    executePreTreatment(tasksList);
    return launchFlux(true, newSiteAdress);
  }

  public static void launchProvisioningActivitiesAsync(List<Task> tasksList){
    executePreTreatment(tasksList);
    launchFlux(false,null);
  }

  public static Boolean launchFlux(boolean sync, SICo_FluxSubscriptionModel.Address newSiteAdress) {        
    Boolean subResult = true;
    Boolean chamCliResult = true;
    Boolean chamDemResult = true;
    Boolean grdfResult = true;

    if(subscriptions != null && !subscriptions.isEmpty()){
      System.debug('### launchFlux subscriptions :' + subscriptions);
      subResult = fluxSubscription(sync,newSiteAdress);
    }
    if(grdf != null && !grdf.isEmpty()){
      System.debug('### launchFlux grdf :' + grdf);
      grdfResult = fluxGRDFTask(sync);
    }
    if(chamClients != null && !chamClients.isEmpty()){
      System.debug('### launchFlux chamClients :' + chamClients);
      chamCliResult = fluxChamClient(sync);
    }
    if(chamDemandes != null && !chamDemandes.isEmpty()){
      System.debug('### launchFlux chamDemandes :' + chamDemandes);
      chamDemResult = fluxChamDemande(sync);
    }
    return subResult && chamCliResult && chamDemResult && grdfResult;
  }

  private static void executePreTreatment(List<Task> tasksList){

    final Map<String,Task> tasks = new Map<String,Task>();
    subscriptions = new Map<Id,Task>();
    subscriptionFeatures = new Map<Id,Sico_AdminTask__mdt>();            
    grdf = new Map<Id,Task>();
    grdfFeatures = new Map<Id,Sico_AdminTask__mdt>();
    chamClients = new Map<Id,Task>();
    chamClientFeatures = new Map<Id,Sico_AdminTask__mdt>();
    chamDemandes = new Map<Id,Task>();
    chamDemandeFeatures = new Map<Id,Sico_AdminTask__mdt>();

    if(tasksList.size() >0){
      // Check if the task belongs to Provisioning
      for(Task aTask : tasksList) if(aTask != null) {
        if(aTask.ProvisioningCallOut__c == true && aTask.FluxStatus__c!= 'OK'){
          tasks.put(aTask.TaskId__c, aTask);
        }
      }
    }

    if(tasks.size() >0){        
      List<Sico_AdminTask__mdt> adminTasks = [SELECT DeveloperName, Domain__c, EndPoint__c, EndPointType__c,
                                              Parameters__c, TaskOrder__c
                                              FROM Sico_AdminTask__mdt 
                                              WHERE DeveloperName in :tasks.keyset()
                                              ORDER BY TaskOrder__c DESC];

      Task aTask = new Task();
      for (Sico_AdminTask__mdt adminTask : adminTasks){
        if(adminTask.Domain__c == 'Services connectés'){
          aTask = tasks.get(adminTask.DeveloperName);
          subscriptions.put(aTask.id,aTask); 
          subscriptionFeatures.put(aTask.id,adminTask); 
        }
        if(adminTask.Domain__c == 'GrDF'){
          aTask = tasks.get(adminTask.DeveloperName);
          grdf.put(aTask.id,aTask); 
          grdfFeatures.put(aTask.id,adminTask); 
        }
        if(adminTask.Domain__c == 'Cham'){
          aTask = tasks.get(adminTask.DeveloperName);
          System.Debug('### aTask: '+aTask);
          System.Debug('### aTask.InterventionType__c: '+aTask.InterventionType__c);
          if(aTask.InterventionType__c != null){
            chamDemandes.put(aTask.id,aTask); 
            chamDemandeFeatures.put(aTask.id,adminTask);  
            System.Debug('### chamDemandes: '+chamDemandes);
            System.Debug('### chamDemandeFeatures '+chamDemandeFeatures);
          }else{
            chamClients.put(aTask.id,aTask); 
            chamClientFeatures.put(aTask.id,adminTask);                         
            System.Debug('### chamClients: '+chamClients);
            System.Debug('### chamClientFeatures: '+chamClientFeatures);
          }
        }
      }
    }
  }

  private static Boolean fluxSubscription(boolean sync,SICo_FluxSubscriptionModel.Address newSiteAdress){
    Boolean result = true;
    try{
      String messageJSON = SICo_FluxSubscription_CLS.buildFluxSubscription(subscriptions, subscriptionFeatures, newSiteAdress);
      String endpoint = subscriptionFeatures.values()[0].EndPoint__c;
      String endPointType = subscriptionFeatures.values()[0].EndPointType__c;
      if(messageJSON !=''){
        if(sync){
          result = SICo_FluxSubscription_CLS.executeFluxSubscription(endPointType, endpoint, messageJSON);
        }else{
          SICo_FluxSubscription_CLS.executeFluxSubscriptionAsync(endPointType, endpoint, messageJSON);
        }
      }
    }catch(Exception e) {
      System.debug(e);
      SICo_LogManagement.addErrorLog(SICo_Constants_CLS.SICO, 'SICo_ProvisioningActivities_CLS - fluxSubscription', e.getMessage());
      result = false;
    }
    return result;
  }

  private static Boolean fluxChamClient(boolean sync){
    Boolean result = true;
    try{
      String messageJSON = SICo_FluxCham_CLS.builCHamClientContrat(chamClients);
      String endpoint = chamClientFeatures.values()[0].EndPoint__c;
      String endPointType = chamClientFeatures.values()[0].EndPointType__c;
      if(messageJSON !=''){
        if(sync){
          result = SICo_FluxCham_CLS.executeFluxCham(endPointType, endpoint, messageJSON);
        }else{
          SICo_FluxCham_CLS.executeFluxChamAsync(endPointType, endpoint, messageJSON);
        }
      }
    }catch(Exception e) {
      System.debug(e);
      SICo_LogManagement.addErrorLog(SICo_Constants_CLS.SICO, 'SICo_ProvisioningActivities_CLS - fluxChamClient', e.getMessage());
      result = false;
    }
    return result;
  }

  private static Boolean fluxChamDemande(boolean sync){
    Boolean result = true;
    try{
      String messageJSON = SICo_FluxCham_CLS.builCHamDemande(chamDemandes);
      String endpoint = chamDemandeFeatures.values()[0].EndPoint__c;
      String endPointType = chamDemandeFeatures.values()[0].EndPointType__c;
      if(messageJSON !=''){
        if(sync){
          result = SICo_FluxCham_CLS.executeFluxCham(endPointType, endpoint, messageJSON);
        }else{
          SICo_FluxCham_CLS.executeFluxChamAsync(endPointType, endpoint, messageJSON);
        }
      }
    }catch(Exception e) {
      System.debug(e);
      SICo_LogManagement.addErrorLog(SICo_Constants_CLS.SICO, 'SICo_ProvisioningActivities_CLS - fluxChamDemande', e.getMessage());
      result = false;
    }  
    return result;
  }

  private static Boolean fluxGRDFTask(boolean sync){
    Boolean result = true;
    try{
      String messageJSON = SICo_FormulaireDemandeGRDF.buildFluxFormulaireDemandeGRDF(grdf);
      String endpoint = grdfFeatures.values()[0].EndPoint__c;
      String endPointType = grdfFeatures.values()[0].EndPointType__c;
      if(messageJSON !=''){
        if(sync){
         result = SICo_FormulaireDemandeGRDF.executeTaskCallout(endPointType, endpoint, messageJSON);
        }else{
          SICo_FormulaireDemandeGRDF.executeTaskCalloutAsync(endPointType, endpoint, messageJSON);
        }
      }
    }catch(Exception e) {
      System.debug(e);
      SICo_LogManagement.addErrorLog(SICo_Constants_CLS.SICO, 'SICo_ProvisioningActivities_CLS - fluxGRDFTask', e.getMessage());
      result = false;
    }
    return result;
  }
}