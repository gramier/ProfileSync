/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Class in order to provide eligibility ( GAS & MAINTENANCE )
Test Class:    SICo_Eligibility_RWS_TEST
History
27/07/2016     	Olivier Vrbanac     Create & develope first version
09/08/2016		Dario Correnti		Add RateZone__c to WS response
------------------------------------------------------------*/
global with sharing class SICo_Eligibility_CLS {

	public static SICo_Eligibility__c getGasEligibility( String sINSEECode ) {

        String rtGaz = SICo_Utility.m_RTbyDeveloperName.get( 'SICo_Eligibility__c_Gas' );
		//Boolean bGaz = false;

    	// GAS ELIGIBILITY FOR THE INSEE CODE
        SICo_Eligibility__c oGasEligibility;
    try{
		oGasEligibility = [ SELECT Id, RateZone__c 
										        FROM SICo_Eligibility__c  
											    WHERE INSEECode__r.INSEECode__c = : sINSEECode AND RecordTypeId = : rtGaz
											    LIMIT 1 ];
                        } catch (Exception ex){}

		return oGasEligibility;

	}

	public static Account getMaintenanceEligibility( String sINSEECode ) {

        String rtMaintenance = SICo_Utility.m_RTbyDeveloperName.get( 'SICo_Eligibility__c_Maintenance' );
        Account oAgency = null;

		//MAINTENANCE ELIGIBILITY FOR THE INSEE CODE
        SICo_Eligibility__c oMaintenanceEligibility;
      try{
		oMaintenanceEligibility = [ SELECT Id, ServiceProvider__c
											            FROM SICo_Eligibility__c 
											            WHERE INSEECode__r.INSEECode__c = : sINSEECode AND RecordTypeId = : rtMaintenance
											            LIMIT 1 ];
                                } catch(Exception ex){}

		// VERIFY AT LEAST ONE RECORD IS AVAILABLE AND RETRIEVE RELATED AGENCY
		if ( oMaintenanceEligibility != null ) {

			oAgency = [ SELECT Id, Name, Phone, ShippingAddress
						FROM Account
						WHERE id = : oMaintenanceEligibility.ServiceProvider__c 
						LIMIT 1 ];

		}

		return oAgency;

	}

	public static List<Account> getMaintenanceEligibility(List<String> sl_INSEECode) {
		String rtMaintenance = SICo_Utility.m_RTbyDeveloperName.get( 'SICo_Eligibility__c_Maintenance' );

        Set<ID> is_serviceProvider = new Set<ID>();
        for(SICo_Eligibility__c o_eligibility : [SELECT Id, ServiceProvider__c
									             FROM SICo_Eligibility__c 
									             WHERE INSEECode__r.INSEECode__c in : sl_INSEECode AND RecordTypeId = : rtMaintenance]) {
        	is_serviceProvider.add(o_eligibility.ServiceProvider__c);
        }

        return [SELECT Id, Name, Phone, ShippingAddress
				FROM Account
				WHERE id in : is_serviceProvider];
	}

	webservice static ID checkMaintenanceEligibilty(String s_postalCode) {
		String rtMaintenance = SICo_Utility.m_RTbyDeveloperName.get( 'SICo_Eligibility__c_Maintenance' );

		try {
			Map<Id, SICo_INSEE__c> codeInsee = new Map<Id, SICo_INSEE__c>([SELECT Id FROM SICo_INSEE__c 
            	Where Id IN (SELECT CodeInsee__c FROM SICo_InseeCPJunction__c WHERE ZipCode__r.ZipCode__c = :s_postalCode)]);
            	
        	return [SELECT Id, ServiceProvider__c
					             FROM SICo_Eligibility__c 
					             WHERE INSEECode__c in :codeInsee.keySet() 
					             AND RecordTypeId = : SICo_Utility.m_RTbyDeveloperName.get( 'SICo_Eligibility__c_Maintenance' )
					             LIMIT 1][0].ServiceProvider__c;
        } catch (Exception ex){
        	system.debug(ex);
        	return null;
        }
	}

}