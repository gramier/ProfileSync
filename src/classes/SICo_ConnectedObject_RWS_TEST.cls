/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Test Class of SICo_ConnectedObject_RWS
Test Class:    SICo_ConnectedObject_RWS_TEST
History
08/07/2016      Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@isTest
public with sharing class SICo_ConnectedObject_RWS_TEST {
	
    static testMethod void testPost() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/ConnectedObject'; 

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        //TEST POST WITHOUT ERROR
        Boolean results = SICo_ConnectedObject_RWS.declareConnectedObject_RWS( '0123456789', '01I580000003BML', 'Station', 'INSERT' );
        results = SICo_ConnectedObject_RWS.declareConnectedObject_RWS( '0123456789', '01I580000003BML', 'Station', 'RETURN' );
        //TEST POST WITH ERROR
        results = SICo_ConnectedObject_RWS.declareConnectedObject_RWS( null, null, null, null );

    }
    
}