global class SICo_Z_PaymentMethodHandler_CLS {

    global SICo_Z_PaymentMethodHandler_CLS () {

    }
    
    @Future(callout=true)
    global static void attachZPaymentMethods(Set<String> zIds) {
        try {
        
        Map<String,Zuora__CustomerAccount__c> billingAccountsMap = new Map<String,Zuora__CustomerAccount__c>();

        List<Zuora__CustomerAccount__c> bAccountList = [SELECT Id, Zuora__Zuora_Id__c, Tech_PaymentCB__c, Tech_PaymentSEPA__c, Zuora__Account__c 
        	FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c in :zIds];
		
		system.debug('### attachZPaymentMethods zIds: ' + zIds);
		system.debug('### attachZPaymentMethods bAccountList: ' + bAccountList);
		
        for (Zuora__CustomerAccount__c ba : bAccountList) {
          billingAccountsMap.put(ba.Zuora__Zuora_Id__c, ba);
        }
		system.debug('### attachZPaymentMethods billingAccountsMap: ' + billingAccountsMap);

        // First try CB
        Set<String> errors = firstTryCB(billingAccountsMap);
        system.debug('### attachZPaymentMethods error first CB: ' + errors);
        if (!errors.isEmpty()) {
        	secondTryCB(billingAccountsMap,errors);
        }
        system.debug('### attachZPaymentMethods error second CB: ' + errors);
        trySepa(billingAccountsMap);

        List<Zuora__CustomerAccount__c> bas = new List<Zuora__CustomerAccount__c>();
        List<Id> accounts = new List<Id>();
        system.debug('### attachZPaymentMethods accounts: ' + accounts);
        for(String s : billingAccountsMap.keySet()){
        	system.debug('### attachZPaymentMethods billingAccountsMap.get(s): ' + billingAccountsMap.get(s));
          	if(!errors.contains(s) && (String.isNotBlank(billingAccountsMap.get(s).Tech_PaymentSEPA__c) || String.isNotBlank(billingAccountsMap.get(s).Tech_PaymentCB__c)) ){
	            Zuora__CustomerAccount__c ba = new Zuora__CustomerAccount__c(Id = billingAccountsMap.get(s).Id);
	            ba.Tech_PaymentOK__c = true;
	            ba.Tech_PaymentCB__c = null;
	            ba.Tech_PaymentSEPA__c = null;
	            bas.add(ba);
	            accounts.Add(billingAccountsMap.get(s).Zuora__Account__c);
          	}
        }
        system.debug('### attachZPaymentMethods accounts: ' + accounts);
        system.debug('### attachZPaymentMethods bas: ' + bas);
        if (!bas.isEmpty()) {
        	update bas;
        }

        readyAccountTask(accounts);

      } catch (Exception ex) {
        SICo_LogManagement.sendErrorLog( 
            'SICo',
            'ZuoraSEPACall.SendSecondPaymentMethod',
            String.valueOf( ex )
        );
        throw ex;
      }
    }
     
    public static Set<String> firstTryCB(Map<String,Zuora__CustomerAccount__c> billingAccountsMap){
      	ZuoraWSconfig__c zc = ZuoraWSconfig__c.getInstance();
      	String gwCB = (zc != null && !String.isBlank(zc.GatewayNameCB__c))
          ? zc.GatewayNameCB__c : Label.SICO_PAYMENT_GATEWAY_CB;
		
		system.debug('### firstTryCB billingAccountsMap: ' + billingAccountsMap);
      	Zuora.zApi zApiInstance = new Zuora.zApi();
      	if (!Test.isRunningTest()) {
      		zApiInstance.zlogin();
      	}
		
		String bAccountFilter = '';
		for(String bAccId : billingAccountsMap.keySet()) {
			bAccountFilter += (!String.isBlank(bAccountFilter) ? ' OR ' : '' ) 
				+ 'AccountID =\'' + bAccId + '\'';
		}
		
		Map<String, Map<String, String>> invoiceByZAccountId = new Map<String, Map<String, String>>();
		if (!String.isBlank(bAccountFilter)) {
			try {
			    String zoql = 'SELECT Id, Balance, AccountID from Invoice Where ' + bAccountFilter;
			    system.debug('### firstTryCB zoql for Invoice: ' + zoql);
			    
			    List<Zuora.zObject> invzobjs = new List<Zuora.zObject>();
			    if (!Test.isRunningTest()) {
			    	invzobjs = zApiInstance.zquery(zoql);
			    }
			    system.debug('### firstTryCB invzobjs from Z: ' + invzobjs);
		
			    for (Zuora.zObject o : invzobjs) {
			    	system.debug('### firstTryCB obj Zo: ' + o);
			    	String zAccId = (String)o.getValue('AccountId');
			    	String zInvId = (String)o.getValue('Id');
			    	Decimal zAccBalance = (Decimal)o.getValue('Balance');
			    	if (zAccId != null && zInvId != null && zAccBalance > 0) {
						invoiceByZAccountId.put(zAccId, new Map<String, String> {'invoiceID' => zInvId, 'Balance' => String.valueOf(zAccBalance)});
				        system.debug('### firstTryCB invoiceByZAccountId.put((String)o.getValue(AccountId)): ' + invoiceByZAccountId.get(zAccId));
			    	}
			    }
		    } catch (Exception ex) {
			    system.debug('### An error occurred on getting Invoice : ' + ex.getMessage());
		    }
		}
		
		system.debug('### firstTryCB invoiceByZAccountId : ' + invoiceByZAccountId);
		
      	Set<String> errors = new Set<String>();
      	List<Zuora.zObject> acczobs = new List<Zuora.zObject>();
      	List<Zuora.zObject> pzobjs = new List<Zuora.zObject>();
      	for(String s : billingAccountsMap.keySet()) {
        	if(String.isNotBlank(billingAccountsMap.get(s).Tech_PaymentCB__c)){
	          	Zuora.zObject acc = new Zuora.zObject('Account');
	          	acc.setValue('Id',s);
	          	acc.setValue('DefaultPaymentMethodId', billingAccountsMap.get(s).Tech_PaymentCB__c);
	          	acc.setValue('AutoPay',true);
	          	acc.setValue('PaymentGateway', gwCB); // Label
	          	acczobs.add(acc);
	          	
	          	if (invoiceByZAccountId.containsKey(s)) {
		          	Zuora.zObject payment = new Zuora.zObject('Payment');
					payment.setValue('AccountId', s);
					Date d = date.today();
					String dt = DateTime.newInstance(d.year(), d.month(), d.day()).format('yyyy-MM-dd');
					payment.setValue('EffectiveDate', dt);
					payment.setValue('PaymentMethodId', billingAccountsMap.get(s).Tech_PaymentCB__c);
					payment.setValue('Status', 'Processed');
					payment.setValue('Gateway', gwCB);
					payment.setValue('InvoiceId', invoiceByZAccountId.get(s).get('invoiceID'));
					payment.setValue('AppliedInvoiceAmount', invoiceByZAccountId.get(s).get('Balance'));
					payment.setValue('Type', 'Electronic');
					pzobjs.add(payment);
	          	}
        	}
      	}
   		system.debug('### firstTryCB acczobs: ' + acczobs);
   		system.debug('### firstTryCB pzobjs: ' + pzobjs);
   		
      	if (!acczobs.isEmpty()) {
        	List<Zuora.zApi.SaveResult> results = new List<Zuora.zApi.SaveResult>();
        	if (!Test.isRunningTest()) {
        		results = zApiInstance.zupdate(acczobs);
        	
	        	if (!pzobjs.isEmpty()) {
	        		results.addAll(zApiInstance.zcreate(pzobjs));
	        	}
        	}
        	
        	system.debug('### firstTryCB results zOpt: ' + results);
        	
        	Boolean errorOccurred = false;
        	String bPaymentFilter = '';
        	for(Zuora.zApi.SaveResult aRes : results) {
          		//if(!aRes.success) {
          		//	errorOccurred = true;
          		//} else {
          		if(aRes.Id != null) {
          			bPaymentFilter += (!String.isBlank(bPaymentFilter) ? ' OR ' : '' ) 
						+ 'Id =\'' + aRes.Id + '\'';
          		}
        	}
        	
        	if (!String.isBlank(bPaymentFilter)) {
        		try {
				    String zoql = 'SELECT Id, AccountID, Status from Payment Where ' + bPaymentFilter + '';
				    system.debug('### firstTryCB zoql for Payment: ' + zoql);
				    List<Zuora.zObject> payzobjs = new List<Zuora.zObject>();
				    if (!Test.isRunningTest()) {
				    	payzobjs = zApiInstance.zquery(zoql);
				    }
				    system.debug('### firstTryCB payzobjs from Z: ' + payzobjs);
			
				    for (Zuora.zObject o : payzobjs) {
				    	system.debug('### firstTryCB payStatusobj Zo: ' + o);
				    	String zAccId = (String)o.getValue('AccountId');
				    	String zPayStatus = (String)o.getValue('Status');
				    	if (zAccId != null && zPayStatus != 'Processed') {
					        Zuora.zApi.SaveResult tmpRes = new Zuora.zApi.SaveResult();
				            tmpRes.Success = false;
				            tmpRes.Id = zAccId;
				            system.debug('### firstTryCB custom error SaveResult: ' + tmpRes);
				            results.add(tmpRes);
				    	}
				    }
			    } catch (Exception ex) {
				    system.debug('### An error occurred on getting Invoice : ' + ex.getMessage());
			    }
        	}
        	
        	Set<String> zIdFailed = new Set<String>();
        	for(Zuora.zApi.SaveResult aRes : results) {
          		if(!aRes.success) {
          			errorOccurred = true;
          			if (aRes.Id != null) {
          				zIdFailed.add(aRes.Id);
          			}
          		}
        	}
				    
        	if (errorOccurred) {
        		if (zIdFailed.isEmpty()) {
        			errors.addAll(billingAccountsMap.keySet());
        		} else {
        			errors.addAll(zIdFailed);
        		}
        	}
      	}
      	system.debug('### firstTryCB errors: ' + errors);
      	return errors;
    } 

    public static void secondTryCB(Map<String,Zuora__CustomerAccount__c> billingAccountsMap, Set<String> errors){
      	ZuoraWSconfig__c zc = ZuoraWSconfig__c.getInstance();
      	String gwCB = (zc != null && !String.isBlank(zc.GatewayNameCB__c))
			? zc.GatewayNameCB__c : Label.SICO_PAYMENT_GATEWAY_CB;

      	Zuora.zApi zApiInstance = new Zuora.zApi();
      	if (!Test.isRunningTest()) {
      		zApiInstance.zlogin();
      	}

      	List<Zuora.zObject> zobs = new List<Zuora.zObject>();
      	for(String s : errors) {
        	if(billingAccountsMap.containskey(s) && String.isNotBlank(billingAccountsMap.get(s).Tech_PaymentCB__c)){
          		Zuora.zObject acc = new Zuora.zObject('Account');
          		acc.setValue('Id',s);
          		acc.setValue('DefaultPaymentMethodId', billingAccountsMap.get(s).Tech_PaymentCB__c);
          		acc.setValue('PaymentGateway', gwCB);
          		zobs.add(acc);
        	}
      	}

          
      	if (!zobs.isEmpty()) {
        	List<Zuora.zApi.SaveResult> results = new List<Zuora.zApi.SaveResult>();
        	if (!Test.isRunningTest()) {
        		results.addAll(zApiInstance.zupdate(zobs));
        	}
        	
	        for(Zuora.zApi.SaveResult aRes : results) {
	          	if(!aRes.success && aRes.Id != null) {
	            	errors.add(aRes.Id);
				} 
			}
      	}
    }

    public static void trySepa(Map<String,Zuora__CustomerAccount__c> billingAccountsMap){
      ZuoraWSconfig__c zc = ZuoraWSconfig__c.getInstance();
        String gwSepa = (zc != null && !String.isBlank(zc.GatewayNameSEPA__c))
          ? zc.GatewayNameSEPA__c : 'SEPA';

      Zuora.zApi zApiInstance = new Zuora.zApi();
      if (!Test.isRunningTest()) {
      	zApiInstance.zlogin();
      }

      List<Zuora.zObject> zobs = new List<Zuora.zObject>();
      for(String s : billingAccountsMap.keySet()) {
        if(String.isNotBlank(billingAccountsMap.get(s).Tech_PaymentSEPA__c)){
          Zuora.zObject acc = new Zuora.zObject('Account');
          acc.setValue('Id',s);
          acc.setValue('DefaultPaymentMethodId',billingAccountsMap.get(s).Tech_PaymentSEPA__c);
          acc.setValue('AutoPay',true);
          acc.setValue('PaymentGateway', gwSepa);
          zobs.add(acc);
        }
      }
      
      if (!zobs.isEmpty() && !Test.isRunningTest()) {
        zApiInstance.zupdate(zobs);
      }
    }

    public static void readyAccountTask(List<Id> accountIds) {
    	system.debug('### SICo_Z_PaymentMethodHandler_CLS.readyAccountTask accountIds: ' + accountIds);	
	  	Set<Id> recordTypeIds = new Set<Id>();
	  	for(SICo_AdminTask__mdt aService : [SELECT RecordType__c FROM SICo_AdminTask__mdt]){
	    	recordTypeIds.add(SICo_Utility.m_RTbyDeveloperName.get('Task_' + aService.RecordType__c));
	  	}
      	system.debug('### SICo_Z_PaymentMethodHandler_CLS.readyAccountTask recordTypeIds: ' + recordTypeIds);
      	
      	Set<Id> contactIds = new Set<Id>();
      	List<Account> accounts = [SELECT Id, PersonContactId FROM Account WHERE Id IN :accountIds];
      	
      	for (Account acc : accounts) {
      		contactIds.add(acc.PersonContactId);
      	}
      	system.debug('### SICo_Z_PaymentMethodHandler_CLS.readyAccountTask contactIds: ' + contactIds);
      
      	List<Task> tasks = new List<Task>();
      	for(Task aTask : [SELECT id, Tech_ProvisioningReady__c, ProvisioningCallOut__c 
                        FROM Task 
                        WHERE whoId IN :contactIds 
                        AND Tech_ProvisioningReady__c = false 
                        AND RecordTypeId IN :recordTypeIds]){
        	aTask.Tech_ProvisioningReady__c = true;
        	//aTask.ProvisioningCallOut__c = true;
        	tasks.add(aTask);
      	}
      	system.debug('### readyAccountTask tasks: ' + tasks);
      
      	update tasks;
    }
}