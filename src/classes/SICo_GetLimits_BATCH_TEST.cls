/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Test Class for Batch Limits on SICo Objects and API Limits
Test Class:
History
22/09/2016     Olivier Vrbanac    Create version
------------------------------------------------------------*/
@isTest
public class SICo_GetLimits_BATCH_TEST {

  @testSetup
  static void setup(){

    Test.startTest();
      insert new CalloutConfig__c(
        Name = 'Salesforce',
        Endpoint__c = 'https://test.salesforce.com',
        isActive__c = true
      );

      insert new JWTSettings__c(
        Name = 'Salesforce',
        Aud__c = 'https://test.salesforce.com',
        Exp__c = 4,
        Iss__c = '3MVG9w8uXui2aB_oXTI53irn8KJEnrRm1_CaUN4wEic9Bk1KCsizwnQMrGXcnBWrWk8eRdqY8Q3KNt5hGBsFT',
        Sub__c = 'c43@c43.fr.xxx'
      );

      ID rtServiceProvider = SICo_Utility.m_RTbyDeveloperName.get('Account_ServiceProvider');
      Account oServiceProvider = new Account( Name = 'CHAM', RecordTypeId = rtServiceProvider );
      insert oServiceProvider;
      ID rtAgence = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
      Account oAgence = new Account( Name = 'Agence CHAM', ParentId = oServiceProvider.Id, RecordTypeId = rtAgence );
      insert oAgence;
      Lead oLead = SICo_UtilityTestDataSet.createLead( oAgence.Id, 'Colissimo' );
      insert oLead;
      Account oAccount = SICo_UtilityTestDataSet.createAccount( 'Gerald', 'Ramier', 'PersonAccount' );
      insert oAccount;
      SICo_Site__c oSite = SICo_UtilityTestDataSet.createSite( oAccount.Id );
      insert oSite;
      Asset oAsset = new Asset( AccountId = oAccount.Id, Name = 'Station', Option__c = 'avec thermostat', Site__c = oSite.Id, ObjectType__c = 'Station' );
      insert oAsset;
      SICo_Beneficiary__c oBeneficiary = SICo_UtilityTestDataSet.createBeneficiary( oAccount.Id, oSite.Id );
      insert oBeneficiary;
      Opportunity oOpp = SICo_UtilityTestDataSet.createOpportunity( 'Opportunité', oAccount.Id, oSite.Id );
      insert oOpp;
      Case oCase = SICo_UtilityTestDataSet.createCase( 'Objet', oAccount.Id, oOpp.Id, oSite.Id );
      insert oCase;
      Task oTask = SICo_UtilityTestDataSet.createTask( 'IN0002', 'Task de test', oCase.Id, 'Logistique', false );
      insert oTask;
    Test.stopTest();
  }

  @isTest
  public static void SICo_GetLimits_BATCH_TEST() {
    Test.startTest();
      //Test.setMock( HttpCalloutMock.class, new SICo_GetLimits_MOCK() );
      Database.executeBatch( new SICo_GetLimits_BATCH() );
    Test.stopTest();
  }
}