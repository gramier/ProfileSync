/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Test of the controller for the Community Forgot Password page (SICo_ForgotPassword_VFC)
History
04/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
private class SICo_ForgotPassword_VFC_TEST {

    /**
     * [Test of login() method, with Invalid email]
     */
    @isTest static void forgotPassword_noUsername() {
        Test.startTest();
        PageReference pref = Page.ForgotPassword;
        pref.getParameters().put('startUrl', SICo_Constants_CLS.CUSTOMER_COMMUNITY_START_URL);
        Test.setCurrentPageReference(pref);
        // Instantiate a new controller with all parameters in the page
        SICo_ForgotPassword_VFC controller = new SICo_ForgotPassword_VFC();
        controller.email = 'test@salesforce.com';
        controller.closeErrorPopup();
        Test.stopTest();
        System.assertNotEquals(null, controller.forgotPassword()); 
        System.assertEquals(Page.CommunitiesSelfReg.getUrl(), controller.register());
    }

    /**
     * [Test of login() method, with 1 matching username]
     */
    @isTest static void forgotPassword_oneUsername_asCRC() {
        //Create 1 test user
        Account testAccount = new Account (Name = 'Test');
        insert testAccount;
        Contact testContact = new Contact(AccountId = testAccount.Id, LastName = 'Test');
        insert testContact;
        User testUser = SICo_UtilityTestDataSet.createCommunityUser(testContact.Id);
        insert testUser;
        Test.startTest();
        // Instantiate a new controller with all parameters in the page
        PageReference pref = Page.ForgotPassword;
        Test.setCurrentPageReference(pref);

        SICo_ForgotPassword_VFC controller = new SICo_ForgotPassword_VFC();
        controller.email = testUser.Email;
        controller.userId = testUser.Id;
        Test.stopTest();
        System.assertNotEquals(null, controller.forgotPassword()); 
        System.assertNotEquals(null, controller.register());
    }

    /**
     * [Test of login() method, with 1 matching username]
     */
    @isTest static void forgotPassword_oneUsername_asCustomer() {
        //Create 1 test user
        Account testAccount = new Account (Name = 'Test');
        insert testAccount;
        Contact testContact = new Contact(AccountId = testAccount.Id, LastName = 'Test');
        insert testContact;
        User testUser = SICo_UtilityTestDataSet.createCommunityUser(testContact.Id);
        insert testUser;
        System.runAs(testUser){
            Test.startTest();
            // Instantiate a new controller with all parameters in the page
            PageReference pref = Page.ForgotPassword;
            Test.setCurrentPageReference(pref);

            SICo_ForgotPassword_VFC controller = new SICo_ForgotPassword_VFC();
            controller.email = testUser.Email;
            Test.stopTest();
            System.assertNotEquals(null, controller.forgotPassword()); 
            System.assertNotEquals(null, controller.register());            
        }

    }

}