/**
* @author Sebastien Colladon
* @date 05/08/2016
*
* @description Queueable utility to chain queue job
*/
public abstract class SICO_QueueChaining_Utility implements Queueable {

    public static SICO_QueueChainOrchestrator queueChainingOrchestrator = new SICO_QueueChainOrchestrator();

    /*******************************************************************************************************
    * @description The next queue
    */
    private Queueable next;

    /*******************************************************************************************************
    * @description Constructor
    * @param next a Queueable to call after
    */
    public SICO_QueueChaining_Utility(Queueable next) {
        this.addNext(next);
    }

    /*******************************************************************************************************
    * @description Constructor by default
    */
    public SICO_QueueChaining_Utility(){}

    /*******************************************************************************************************
    * @description add the next queue to the current queue
    * @param next queue to execute after the current one
    */
    public void addNext(Queueable next) {
        this.next = next;
    }

    /*******************************************************************************************************
    * @description execute the Queue (run the job)
    * @param context the context
    */
    public void execute(QueueableContext context) {
        this.doJob();
        this.callNext();
    }

    /*******************************************************************************************************
    * @description doJob do the actual treatment
    */
    protected abstract void doJob();

    /*******************************************************************************************************
    * @description call the next queue
    */
    protected void callNext() {
        if(this.next != null) {
            System.enqueueJob(next);
        }
    }

    /**
    * @author Sebastien Colladon
    * @date 12/08/2016
    *
    * @description Orchestrator for Queue Chaining
    */
    public class SICO_QueueChainOrchestrator {
        // TODO test enqueue and then add a new queue in the list
        private SICO_QueueChaining_Utility first;
        private SICO_QueueChaining_Utility last;
        private SICO_QueueChaining_Utility finalCall;

        /*******************************************************************************************************
        * @description Add a queue to the queue chain
        * @param aQueue to add in the queue chain
        */
        public void addQueue(SICO_QueueChaining_Utility aQueue) {
            if(this.first == null) {
                this.first = aQueue;
                this.last = this.first;
            } else {
                this.last.addNext(aQueue);
                this.last = aQueue;
            }
        }

        /*******************************************************************************************************
        * @description Add a queue at the beginning of the queue chain
        * @param aQueue to add in the queue chain
        */
        public void addFirst(SICO_QueueChaining_Utility aQueue) {
            if(this.first == null){
                this.addQueue(aQueue);
            } else {
                aQueue.addNext(this.first);
                this.first = aQueue;
            }
        }

        /*******************************************************************************************************
        * @description Add a queue that will be executed after all other queue no matter when it is added
        * @param aQueue to add in the queue chain
        */
        public void addLast(SICO_QueueChaining_Utility aQueue) {
            this.finalCall = aQueue;
        }

        /*******************************************************************************************************
        * @description Execute the first queue of the queue chain to trigger all the chain
        */
        public void executeQueueChain() {
            if(this.finalCall != null) {
                this.addQueue(this.finalCall);
                this.finalCall = null;
            }
            if(this.first != null) {
                System.enqueueJob(this.first);
                this.first = null;
                this.last = null;
            }
        }
    }
}