/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Test Class of SICo_CustomerSite_RWS
Test Class:    SICo_CustomerSite_RWS_TEST
History
19/07/2016      Olivier Vrbanac     Create & develope first version
26/09/2016      Anouar Badri         Modification version
------------------------------------------------------------*/
@isTest
public with sharing class SICo_CustomerSite_RWS_TEST {

    @testSetup
    private static void setup(){
        insert SICo_UtilityTestDataSet.createChamConfig();
        insert SICo_UtilityTestDataSet.createCustomSetting();
        insert SICo_UtilityTestDataSet.createConfig_Boomi();
        insert SICo_UtilityTestDataSet.create_GrDF_Config();

        test.startTest();

        Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
        account.CustomerNumber__c = '123';
        insert account;
        // Create Site
        SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
        insert site;
        // Create Opportunity
        Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('opp001',account.Id,Site.Id);
        insert opportunity;
        // Create case
        Case myCase = SICo_UtilityTestDataSet.createCase('Objet',account.Id,opportunity.Id,site.Id);
        insert myCase;
        // Create related Case Tasks
        List<Task> listTask = new List<Task>();
        listTask.add(SICo_UtilityTestDataSet.createTask('GA0001','Subscription 001',myCase.Id,'TaskProvisioning',True));
        /*listTask.add(SICo_UtilityTestDataSet.createTask('GA0003','Subscription 002',myCase.Id,'TaskProvisioning',True));
        listTask.add(SICo_UtilityTestDataSet.createTask('SE0003','Provisioning 001',myCase.Id,'TaskProvisioning',True));
        listTask.add(SICo_UtilityTestDataSet.createTask('SE0004','Provisioning 002',myCase.Id,'TaskProvisioning',True));*/
        test.stopTest();
        system.debug('#> listTask ' + listTask);
        insert listTask;
        // Create zuora quote
        zqu__Quote__c zuoraquote = SICo_UtilityTestDataSet.createZuoraQuote('q01',account.Id,account.PersonContactId,opportunity.id, site.id);
        insert zuoraquote;
        // Create zuora subscription
        zqu__Quote__c quoteNumber = [SELECT zqu__Number__c FROM zqu__Quote__c WHERE ID= :zuoraquote.id LIMIT 1];
        
        
        Zuora__Subscription__c zuoraSubscription = SICo_UtilityTestDataSet.createZuoraSubscription('subGaz',account.id,zuoraquote.id,quoteNumber.zqu__Number__c,site.Id);
        insert zuoraSubscription;
        
        //run trigger update
        zuoraSubscription.Name = 'subscriptGaz';
        update zuoraSubscription;
    }
	
    static testMethod void testGetAndPost() {
        // Create account
        Account account = [Select id, CustomerNumber__c from Account limit 1];
        SICo_Site__c site = [Select id, SiteNumber__c, NumberStreet__c, AdditionalInformation__c, PostalCode__c, City__c, Country__c,IsActive__c from SICo_Site__c limit 1];
        Zuora__Subscription__c zuoraSubscription = [Select id from Zuora__Subscription__c limit 1];

        Test.setMock(HttpCalloutMock.class, new SICo_GlobalRespond_MOCK());
        Test.startTest();
            // Create zuora subscription product feature
            List<Zuora__SubscriptionProductFeature__c> productFeatureList = new List<Zuora__SubscriptionProductFeature__c>();
            productFeatureList.add(SICo_UtilityTestDataSet.createZuoraSubscriptionProductFeature('Fourniture de gaz','Gaz',account.id,zuoraSubscription.id));
            productFeatureList.add(SICo_UtilityTestDataSet.createZuoraSubscriptionProductFeature('O\'quilibre','Oquilibre',account.id,zuoraSubscription.id));
            SICo_Provisioning_CLS.provisioning(productFeatureList);

            //AB
            //WRAPPER CUSTOMER INFO
            String CustomerNumber = '123';
            String sDeviceDetails = '';
            Boolean bIsGasCustomer = false;
            Boolean bHasMaintenanceSchedule = false;
            SICo_CustomerSite_RWS.CustomerInfo oCI = new SICo_CustomerSite_RWS.CustomerInfo( 
                CustomerNumber,site.Id,site.SiteNumber__c,site.NumberStreet__c,site.AdditionalInformation__c,
                site.PostalCode__c,site.City__c,site.Country__c,sDeviceDetails,bHasMaintenanceSchedule,
                site.IsActive__c,bIsGasCustomer,false, false
            );
        
            //TEST GET WITHOUT ERROR
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/CustomerSite';  
            req.addParameter('CustomerNumber', account.CustomerNumber__c);

            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;

            //FIXME
            SICo_CustomerSite_RWS.getUniqueSiteInfo_RWS();
            
            //TEST GET WITH ERROR
            req = new RestRequest(); 
            res = new RestResponse();

            req.requestURI = '/services/apexrest/CustomerSite';  
            req.addParameter('CustomerNumber', '012345678910111213');

            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;

            //FIXME
            SICo_CustomerSite_RWS.getUniqueSiteInfo_RWS();
    		
            //TEST POST WITHOUT ERROR
            req = new RestRequest(); 
            res = new RestResponse();

            req.requestURI = '/services/apexrest/CustomerSite'; 

            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            //TEST POST WITHOUT ERROR
            Boolean dResults = SICo_CustomerSite_RWS.confirmSiteInfo(account.CustomerNumber__c, 
                                                                    String.valueOf( site.Id ), 
                                                                    site.SiteNumber__c, 
                                                                    site.NumberStreet__c, 
                                                                    site.AdditionalInformation__c, 
                                                                    site.PostalCode__c, 
                                                                    site.City__c, 
                                                                    site.Country__c, 
                                                                    'Station sans thermostat',
                                                                    true, 
                                                                    false, 
                                                                    true, 
                                                                    true,
                                                                    false);
            //TEST POST WITH FUNCTIONAL ERROR
            dResults = SICo_CustomerSite_RWS.confirmSiteInfo(account.CustomerNumber__c, 
                                                             String.valueOf( site.Id ), 
                                                             site.SiteNumber__c, 
                                                             site.NumberStreet__c, 
                                                             site.AdditionalInformation__c, 
                                                             site.PostalCode__c, 
                                                             site.City__c, 
                                                             site.Country__c, 
                                                             'Station sans thermostat',
                                                             true, 
                                                             true, 
                                                             true,
                                                             true,
                                                             false);
    		//TEST POST WITH TECHNICAL ERROR
            dResults = SICo_CustomerSite_RWS.confirmSiteInfo(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        Test.stopTest();
    }
    
}