/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test class for the SICo_CancelContract_VFC Controller 
Test Class:    SICo_CancelContract_VFC_TEST
History
30/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
@isTest
public class SICo_CancelContract_VFC_TEST {

	//set test data
	@testSetup
	static void testData() {

		User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
	    insert communityUser;
	}

	@isTest
	static void testCasesWithoutURLIdGas() {

		User us = [SELECT Id, accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User
	    system.runAs(us){
	    	PageReference pRef = new PageReference(Page.CancelContract.getUrl() + '?accountId=' + us.accountId + '&conType=gas');
            /*PageReference pRef = Page.CancelContract;
        	pRef.getParameters().put('accountId', us.accountId);
        	pRef.getParameters().put('conType', 'gas');*/
	        Test.setCurrentPageReference(pRef);
	        Test.startTest();
	            SICo_CancelContract_VFC controller = new SICo_CancelContract_VFC();

		        controller.createCase();

		        controller.getObjects();

		        controller.getWcb();

		        controller.cancelCancel();

		        List<Case> listCases = [SELECT Id, Description FROM Case WHERE AccountId = :us.accountId];
		        System.assertNotEquals(listCases.size(), null);
            	
	        Test.stopTest();
        }
	}

	@isTest
	static void testCasesWithoutURLIdMaint() {

		User us = [SELECT Id, accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User
	    system.runAs(us){
	        Test.startTest();
	            SICo_CancelContract_VFC controller = new SICo_CancelContract_VFC();
	            PageReference pRef = Page.CancelContract;
            	pRef.getParameters().put('accountId', us.accountId);
            	pRef.getParameters().put('conType', 'maint');
		        Test.setCurrentPageReference(pRef);

		        controller.createCase();

		        controller.getObjects();

		        controller.getWcb();

		        controller.cancelCancel();

		        List<Case> listCases = [SELECT Id, Description FROM Case WHERE AccountId = :us.accountId];
		        System.assertNotEquals(listCases.size(), null);
            	
	        Test.stopTest();
        }
	}
    
    @IsTest
    static void testCasesWithURLId() {
        
        //Run as Community User
        Test.startTest();
            SICo_CancelContract_VFC controller = new SICo_CancelContract_VFC();
            PageReference pRef = Page.CancelContract;
            Test.setCurrentPageReference(pRef);
            
            System.assertEquals(controller.listMotif.size(), 0);
            System.assertEquals(controller.listObjet.size(), 0);
        
        Test.stopTest();
}

}