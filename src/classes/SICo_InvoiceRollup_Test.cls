@isTest
private class SICo_InvoiceRollup_Test {

  private static Double AMOUNT = 10.0;
  private static Double TAX_AMOUNT = 1.0;

  @testSetup
  private static void setup(){
    Test.startTest();
    Zuora__ZInvoice__c invoice = new Zuora__ZInvoice__c(Name = 'test');

    insert invoice;

    List<String> categories = new List<String>{
      'GAZ-ABO',
      'MAINT-MEN-POS-SPIE',
      'MAINT-ANN-PIE',
      'MATER-ET',
      'MATER-SE-PROMO',
      'MAINT-MEN-NEG-SPIE',
      'INST',
      'INST-PROMO',
      'MATER-SE-REDUC',
      'MATER-SE-MEN',
      'PENALITE-INTERET',
      'GAZ-CTA',
      'MAINT-MEN-POS-SPIE',
      'PSP-GrDF',
      'MAINT-PSP'
    };

    List<Double> taux = new List<Double>{5.5,10.0,20.0};

    List<SiCo_InvoiceItem__c> invoiceItems = new List<SiCo_InvoiceItem__c>();

    Integer i = 0;
    for(String aCategory : categories) {
      invoiceItems.add(new SiCo_InvoiceItem__c(
        Name ='test',
        ChargeAmount__c = AMOUNT,
        TaxAmount__c = TAX_AMOUNT,
        Quantity__c = 1,
        Category__c = aCategory,
        InvoiceId__c = invoice.id,
        SICO_TauxTVAWF__c = taux[Math.mod((i++),taux.size())]
        )
      );
    }

    System.debug(invoiceItems);

    insert invoiceItems;



    List<Usage__c> usages = new List<Usage__c>();
    for(SiCo_InvoiceItem__c a : invoiceItems) {
      System.debug(a);
      usages.add(new usage__c(Name='test', InvoiceItem__c = a.id));
    }

    insert usages;

    Test.stopTest();
  }

    @isTest
  private static void testRollUp() {

    Test.startTest();
      Database.executeBatch(new SICo_InvoiceRollup());
    Test.stopTest();
        
  }
}