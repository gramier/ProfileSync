/*------------------------------------------------------------
Author:        Abd-slem OUALI
Company:       Ikumbi
Description:   Helper for the offers component of the Dashboard page. Access is "Without Sharing" due to Shipping Address stored on Opportunity
Test Class:    SICo_Dashboard_VFC_TEST
History
29/09/2016      Abd-slem OUALI      Create version
------------------------------------------------------------*/

public with sharing class SICo_DashboardActivities_CLS {
 
    /**
    * Invoices Wrapper class
    */
    public class ActivityLightWrapper {
        public String description;
        public DateTime eventDate;
        public String mType;
        public String mainTitle;
        public Boolean isPast;
        public Integer priority; // used to manage display priority
        public Id mId;
    }

    // Events type (contract source)
    private Enum EventsType { MAINTENANCE, GAZ, EQUIPMENT, QUOTE }
    // Constant type of event
    public static final String RDV_TYPE = 'RDV';
    public static final String FACTURE_TYPE = 'Facture';
    public static final String QUOTE_TYPE = 'Quote';

    public static final String GAZ_TYPE = 'GAZ';
    public static final String EQUIPMENT_TYPE = 'EQUIPMENT';
    public static final String MAINTENANCE_TYPE = 'MAINTENANCE';

    // List of priorities
    public static final Integer PRIORITY_EQUIP_RDV                 = 0;
    public static final Integer PRIORITY_MAINTENANCE_RDV           = 1;
    public static final Integer PRIORITY_GAZ_RDV                   = 2;
    public static final Integer PRIORITY_INVOICE                   = 3;
    public static final Integer PRIORITY_QUOTE_TODO		           = 4;
    public static final Integer PRIORITY_EQUIP_RDV_DONE            = 5;
    public static final Integer PRIORITY_EQUIP_ORDER_STATUS        = 6;
    public static final Integer PRIORITY_MAINTENANCE_RDV_DONE      = 7; 
    public static final Integer PRIORITY_GAZ_RDV_DONE              = 8;
    public static final Integer PRIORITY_QUOTE_DONE		           = 9;


    /**** HELPER METHODS ****/

    public static List<ActivityLightWrapper> getQuotes(String pAccountId) {
    	if (pAccountId == null) {
            return null;
        }

        List<ActivityLightWrapper> vActivitiesQuote = new List<ActivityLightWrapper>();
		// Quotes
        List<MaintenancyQuote__c> vQuotes = queryQuotes(pAccountId);
        if (vQuotes != null && vQuotes.size() > 0) {
        	for (MaintenancyQuote__c vQuote : vQuotes) {
        		ActivityLightWrapper vActivityQuote = getALWFromQuote(vQuote, EventsType.QUOTE);
        		if (vActivityQuote != null) {
                    vActivitiesQuote.add(vActivityQuote);
        		}
        	}
        }

        return vActivitiesQuote;
    }

    /**
    * Query Zuora invoices and wrap it in ActivityLightWrapper class
    * @param: pAccountId => Account id mandatory
    */
    public static List<ActivityLightWrapper> getUnpaidInvoices(String pAccountId) {
        if (pAccountId == null) {
            return null;
        }
        // List to return
        List<ActivityLightWrapper> vInvoicesList = new List<ActivityLightWrapper>();
        
        // Query Zuora invoices
        List<Zuora__ZInvoice__c> vZInvoicesList = queryInvoice(pAccountId);
        if (vZInvoicesList != null && vZInvoicesList.size() > 0) {
            for (Zuora__ZInvoice__c vZInvoice : vZInvoicesList) {
            	if (!vZInvoice.Attachments.isEmpty()) {
	                ActivityLightWrapper vInvoice = new ActivityLightWrapper(); 

	                vInvoice.description = String.format(System.Label.SICo_Dashboard_UnpaidBill, new String[]{ getFormattedDateFr(vZInvoice.Zuora__InvoiceDate__c) });
	                vInvoice.eventDate = vZInvoice.Zuora__InvoiceDate__c;
	                vInvoice.mainTitle = System.Label.SICo_Invoice;  
	                vInvoice.mType = FACTURE_TYPE;
	                vInvoice.priority = PRIORITY_INVOICE;
	                // Once filled in, add it to the list returned
	                vInvoicesList.add(vInvoice);
	            }
            }
        }

        return vInvoicesList;
    }

    /**
    * Returns a formatted date
    * @param: pDate => The date to be formatted in FR format
    */ 
    public static String getFormattedDateFr(DateTime pDate) {
        return String.valueOf(pDate.day()) + ' ' + SICo_Constants_CLS.MONTHES.get(pDate.month()).toUpperCase() + ' ' + String.valueOf(pDate.year()); 
    }

    /**
    * Returns a list of light activity object from a task
    * @param: pAccountId => Id of the account
    */
    public static List<ActivityLightWrapper> getTasksAndEvents(String pAccountId) {

        List<ActivityLightWrapper> vTasksAndEventsList = new List<ActivityLightWrapper>();
        
        final Set<String> adminTasks = new Set<String>();

        final Map<String, CommunityObjectsManagement__mdt> commObjectManag = new Map<String, CommunityObjectsManagement__mdt>();
        for(CommunityObjectsManagement__mdt c : [SELECT Code__c, Record_Type__c, Category__c, Type__c
                                                FROM CommunityObjectsManagement__mdt
                                                WHERE IsVisibleDashboard__c = true]){
            commObjectManag.put(c.Code__c, c);
            adminTasks.add(c.Code__c);
        }

        final List<Task> allTasks = getAllTasksFromAccount(pAccountId, adminTasks);

        for (Task t : allTasks) {
        // put the code here
            ActivityLightWrapper a = new ActivityLightWrapper();
            a.mType = RDV_TYPE;
              
            //check if Task is of type "Gaz"
            if(commObjectManag.get(t.TaskId__c).Type__c == SICo_Constants_CLS.TASK_ACTIVITY_TYPE_GAZ){
                a.mainTitle = System.Label.SICo_Meeting;  
                a.eventDate = t.InterventionPlannedDate__c;
                if (t.InterventionPlannedDate__c >= Datetime.now()) {
                    a.isPast = false;
                    a.priority = PRIORITY_GAZ_RDV;
                    a.description = System.Label.SICo_Dashboard_Activation + ' ' + getFormattedDateFr(t.InterventionPlannedDate__c); 
                } 
                else { // past events
                    if (t.RequestState__c == System.Label.SICo_Cloturee) { //comparer label et valeur de liste
                        a.isPast = true;
                        a.priority = PRIORITY_GAZ_RDV_DONE;
                        a.description = System.Label.SICo_Dashboard_Activation + ' ' + getFormattedDateFr(t.InterventionPlannedDate__c); 
                    } 
                    else {
                        // continue with the next task because past event doesn't match criterias
                        continue;
                    }
                }
                vTasksAndEventsList.add(a);
            }
            //check if Task is of type "Equipement"
            else if(commObjectManag.get(t.TaskId__c).Type__c == SICo_Constants_CLS.TASK_ACTIVITY_TYPE_EQUIPMENT){
                //check Task rerpesent a meeting or not
                if(commObjectManag.get(t.TaskId__c).Category__c == RDV_TYPE){
                    //if the task is a meeting
                    if (t.InterventionPlannedDate__c >= Datetime.now()) {
                        a.priority = PRIORITY_EQUIP_RDV;
                        //a.description = String.format(System.Label.SICo_Dashboard_MeetingInstallationPlanned, 
                        a.description = String.format(System.Label.SICo_Dashboard_MeetingInstallationRangedHours,
                            new String[] { getFormattedDateFr(t.InterventionPlannedDate__c), 
                                t.PlannedRangedHours__c });
                    }
                    else{
                        if(t.RequestState__c  != System.Label.SICo_Annule){
                            a.priority = PRIORITY_EQUIP_RDV_DONE;
                            a.description = System.Label.SICo_Dashboard_MeetingInstallation + ' ' + getFormattedDateFr(t.InterventionPlannedDate__c);
                        }
                    }
                }
                else{
                    //if the task isn't a meeting
                    a.eventDate = t.CreatedDate;                
                    a.isPast = true;
                    a.mainTitle = System.Label.SICo_FollowingOrder;   
                    a.priority = PRIORITY_EQUIP_ORDER_STATUS;
                    if(t.DeliveryStatus__c != null){
                        a.description =  System.Label.SICo_Dashboard_OrderEquipStatus + ' ' +  t.DeliveryStatus__c;
                    }
                    else{
                        a.description =  System.Label.SICo_Dashboard_OrderEquipStatus + ' ' +  'indisponible';
                    }
                }
                vTasksAndEventsList.add(a);
            }
            //check if Task is of type "Maintenance"
            else if(commObjectManag.get(t.TaskId__c).Type__c == SICo_Constants_CLS.TASK_ACTIVITY_TYPE_MAINTENANCE){
                if(t.InterventionPlannedDate__c != null){
                    if (t.InterventionPlannedDate__c >= Datetime.now()) {
                        a.priority = PRIORITY_MAINTENANCE_RDV;
                        //a.description = String.format(System.Label.SICo_Dashboard_FirstMeetingMaintenance,
                        a.description = String.format(System.Label.SICo_Dashboard_FirstMeetingMaintenanceRangedHours, 
                            new String[] { getFormattedDateFr(t.InterventionPlannedDate__c),
                                t.PlannedRangedHours__c});
                    }
                    else{
                        if(t.RequestState__c  != System.Label.SICo_Annule){
                            a.priority = PRIORITY_MAINTENANCE_RDV_DONE;
                            a.description = System.Label.SICo_Dashboard_FirstMeeting + ' ' + getFormattedDateFr(t.InterventionPlannedDate__c);
                        }
                    }
                    vTasksAndEventsList.add(a);
                }
            }
        }

        return vTasksAndEventsList;
    }

    public static List<Task> getAllTasksFromAccount(String pAccountId, Set<String> adminTasks){

        List<Task> allTasks= [SELECT WhatId, 
                                    AccountId, 
                                    SiteID__c, 
                                    Subscription__c, 
                                    Id, 
                                    Status, 
                                    Subject, 
                                    CreatedDate, 
                                    ActivityDate, 
                                    InterventionPlannedDate__c, 
                                    InterventionType__c, 
                                    PlannedRangedHours__c, 
                                    RequestState__c, 
                                    OmegaRequestId__c, 
                                    CRSetupReturnCode__c, 
                                    TaskId__c, 
                                    ShippingAddressAdditionToAddress__c, 
                                    ShippingAddressCity__c, 
                                    ShippingAddressCountry__c, 
                                    ShippingAddressNumberStreet__c, 
                                    ShippingAddressZipCode__c, 
                                    ShippingDate__c, 
                                    ShippingMode__c, 
                                    DeliveryComment__c, 
                                    DeliveryPointId__c, 
                                    DeliveryReturnComment__c, 
                                    DeliveryReturnFeedback__c, 
                                    DeliveryStatus__c, 
                                    DeliveryTrackingNumber__c, 
                                    DeliveryTrackingReturnNumber__c, 
                                    DeliveryTrackingURL__c, 
                                    RecordTypeId,
                                    ShippingAddressFloor__c,
                                    ShippingAddressBuilding__c,
                                    ShippingAddressFlat__c,
                                    ShippingAddressStairs__c
                                FROM Task
                                WHERE IsClosed = false
                                  AND AccountId = :pAccountId
                                  AND TaskId__c IN : adminTasks
                                  ORDER BY CreatedDate DESC];

        return allTasks;
    }

    /**
    * Returns a light activity object from a quote
    * @param: pQuote => a quote, mandatory ohterwise returns null. Also, returns null  
    */
    public static ActivityLightWrapper getALWFromQuote(MaintenancyQuote__c pQuote, EventsType pEventType) {
        if (pQuote == null) {
            return null;
        }
        ActivityLightWrapper vActivityQuote = new ActivityLightWrapper();

        vActivityQuote.description = String.format(System.Label.SICo_QuoteDesc, new String[] { pQuote.MaintenancyQuoteNumber__c, pQuote.MaintenancyQuoteStatus__c });
        vActivityQuote.eventDate = pQuote.CreatedDate;
        vActivityQuote.mType = QUOTE_TYPE;
        vActivityQuote.mainTitle = System.Label.SICo_Quote;
        vActivityQuote.isPast = true;
        vActivityQuote.mId = pQuote.Id;
        vActivityQuote.priority = (pQuote.MaintenancyQuoteStatus__c == 'A valider' ? PRIORITY_QUOTE_TODO : PRIORITY_QUOTE_DONE);

        return vActivityQuote;
    }  

    /**** QUERY METHODS ****/
 
    private static List<Zuora__ZInvoice__c> queryInvoice(String pAccountId){
        return [SELECT Id, Zuora__InvoiceDate__c, TotalInvoiceAmountTTC__c, NextInvoiceDate__c,
                       	Zuora__Status__c, Zuora__Balance2__c, IsPaymentInProgress__c, IsUnpaid__c,
						(SELECT Id FROM Attachments ORDER BY CreatedDate DESC LIMIT 1) //IKUMBI-480
                FROM Zuora__ZInvoice__c
                WHERE Zuora__Account__c = :pAccountId
                AND Zuora__ZInvoice__c.isUnpaid__c = true 
                ORDER BY Zuora__InvoiceDate__c DESC LIMIT 5];
    }

    private static SICo_Site__c querySite(String pAccountId){
        SICo_Site__c theSite = null;
        List <SICo_Site__c> siteList = [SELECT Id,
                                        PCE__c,
                                        IsSelectedInCustomerSpace__c,
                                        ProfileCompletion__c,
                                        NumberStreet__c,
                                        Floor__c,
                                        Stairs__c,
                                        AdditionToAddress__c,
                                        Building__c,
                                        Flat__c,
                                        PostalCode__c,
                                        City__c,
                                        Country__c,
                                        ResidenceType__c,
                                        HousingType__c,
                                        IsArrangedRoofPresent__c,
                                        IsFloorPresent__c,
                                        IsVerandaPresent__c,
                                        IsJointHouse__c,
                                        HousingLocationType__c,
                                        IsCornerApartment__c,
                                        IsWindowRestored__c,
                                        IsWallRestored__c,
                                        IsArrangedRoofRestored__c,
                                        surfaceInSqMeter__c,
                                        ConstructionDate__c,
                                        NoOfOccupants__c,
                                        PrincipalHeatingSystemType__c,
                                        SanitaryHotWaterType__c,
                                        GasSource__c,
                                        IsCeramicGlassPlatePresent__c,
                                        IsWineCellarPresent__c,
                                        hasSwimmingPool__c,
                                        IsAquariumPresent__c,
                                        ElectricityTariffOption__c,
                                        IsACPresent__c,
                                        SubscribedPower__c
                                        FROM SICo_Site__c
                                        WHERE Subscriber__c =:pAccountId LIMIT 100];
        //If 1 single site is active, use it
        if (siteList.size() == 1) {
            theSite = siteList[0];
        //if several Sites, use the one selected in Customer Space
        } else if(siteList.size() > 1) {
            for (SICo_Site__c custSite : siteList) {
                if (custSite.IsSelectedInCustomerSpace__c) {
                    theSite = custSite;
                }
            }
        }//ENDIF siteList.size()

        return theSite;
    }


    private static List<MaintenancyQuote__c> queryQuotes(String pAccountId) {
    	List<MaintenancyQuote__c> vQuoteList = [SELECT Id, CreatedDate, MaintenancyQuoteNumber__c, MaintenancyQuoteStatus__c, AcceptedDate__c
													FROM MaintenancyQuote__c
													WHERE Customer__c = :pAccountId
													AND Site__c IN (SELECT Id 
																		FROM SICo_Site__c
																		WHERE Subscriber__c = :pAccountId 
																		AND IsSelectedInCustomerSpace__c = true)
													AND (MaintenancyQuoteStatus__c = 'A valider'
														OR (MaintenancyQuoteStatus__c = 'Accepté' AND CreatedDate = LAST_N_DAYS:30))
													ORDER BY CreatedDate DESC]; 
		return vQuoteList;
    }

}