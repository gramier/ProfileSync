/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Utility class for test Flux class (dataSet)
Test Class:    Flux Class Test call this class
History
04/08/2016      Boris Castellani     Create & develope first version
------------------------------------------------------------*/
public with sharing class SICo_UtilityFluxDataSet {

		/***************************************
			DATA SET FLUX GAZ
		***************************************/

		// Create & Return: ElecSupplyContractChange
		public static SICo_FluxProfileModel.ElecSupplyContractChange createFluxProfileElec(ID siteID, ID accountId){
			SICo_FluxProfileModel.ElecSupplyContractChange fluxProfileElec = new SICo_FluxProfileModel.ElecSupplyContractChange();
			fluxProfileElec.siteId= siteId;
			fluxProfileElec.personId= accountId;
			fluxProfileElec.changeDate= Datetime.valueOfGmt('2015-11-05 08:15:30');
			fluxProfileElec.tariff= 'BLEU';
			fluxProfileElec.tariffOption= 'HP_HC';
			fluxProfileElec.subscribedPower='9';
			fluxProfileElec.parameters= new SICo_FluxProfileModel.Parameters();
			return fluxProfileElec;
			}

		/***************************************
			DATA SET FLUX ELEC
		***************************************/

		// Create & Return: GasSupplyContractChange
		public static SICo_FluxProfileModel.GasSupplyContractChange createFluxProfileGaz(ID siteID, ID accountId){
			SICo_FluxProfileModel.Parameters parameter = new SICo_FluxProfileModel.Parameters();
			SICo_FluxProfileModel.GasSupplyContractChange fluxProfileGaz = new SICo_FluxProfileModel.GasSupplyContractChange();
			fluxProfileGaz.siteId= siteId;
			fluxProfileGaz.personId= accountId;
			fluxProfileGaz.changeDate= Datetime.valueOfGmt('2015-11-05 08:15:30');
			fluxProfileGaz.tariff= 'GAZ_PF2_1';
			fluxProfileGaz.option= 'Conso1';
			fluxProfileGaz.millesime='1';
			fluxProfileGaz.zone='Z4';
			fluxProfileGaz.parameters= parameter;
			return fluxProfileGaz;
			}

		/***************************************
			DATA SET FLUX GENERIC PROFILE
		***************************************/

		// Create & Return: GenericProfile
		public static SICo_FluxProfileModel.GenericProfile createFluxGenericProfile(){
			SICo_FluxProfileModel.GenericProfile genericProfile = new SICo_FluxProfileModel.GenericProfile();
			genericProfile.housing= createFluxProfileHousing();
			genericProfile.modificationDate= Datetime.valueOfGmt('2015-11-05 08:15:30');
			return genericProfile;
			}

		// Create & Return: Housing
		public static SICo_FluxProfileModel.Housing createFluxProfileHousing(){
			SICo_FluxProfileModel.Housing housing = new SICo_FluxProfileModel.Housing();
			housing.apartment= createFluxProfileApartment();
			housing.constructionDate= '2001';
			housing.equipments= createFluxProfileEquipments();
			housing.heatingSystem= createFluxProfileHeatingSystem();
			housing.heightUnderCeiling= '10';
			housing.house= createFluxProfileHouse();
			housing.housingType= 'Maison';
			housing.insertWood= createFluxProfileInsertWood();
			housing.isBBC= false;
			housing.lifeStyle= createFluxProfileLifeStyle();
			housing.luminosity= 'Peu';
			housing.occupationType= 'Proprietaire';
			housing.residenceType= 'Principale';
			housing.restoration= createFluxProfileRestoration();
			housing.surfaceInSqMeter='80.0';
			housing.swimmingPool= createFluxProfileSwimmingPool();
			return housing;
			}

		// Create & Return: Apartment
		private static SICo_FluxProfileModel.Apartment createFluxProfileApartment(){
			SICo_FluxProfileModel.Apartment apartment = new SICo_FluxProfileModel.Apartment();
			apartment.housingLocationType= 'RDC';
			apartment.isCornerApartment= false;
			return apartment;
			}

		// Create & Return: Equipments
		public static SICo_FluxProfileModel.Equipments createFluxProfileEquipments(){
			SICo_FluxProfileModel.Equipments equipment = new SICo_FluxProfileModel.Equipments();
			equipment.airConditioning= createFluxProfileAirConditioning();
			equipment.audioEquipments= createFluxProfileAudioEquipments();
			equipment.cookingEquipments= createFluxProfileCookingEquipments();
			equipment.elecEquipments= createFluxProfileCookingElecEquipments();
			equipment.lightingType= 'FluoCompact';
			equipment.noOfHalogens= '1';
			equipment.otherEquipments= createFluxProfileOtherEquipments();
			equipment.sanitoryHotWater= createFluxProfileSanitoryHotWater();
			return equipment;
			}

		// Create & Return: SICo_FluxProfileModel.AirConditioning
		private static SICo_FluxProfileModel.AirConditioning createFluxProfileAirConditioning(){
			SICo_FluxProfileModel.AirConditioning airConditioning = new SICo_FluxProfileModel.AirConditioning();
			airConditioning.instructionTypeAC= 'Constante';
			airConditioning.instructionValueAC= '23.0';
			airConditioning.isACPresent= true;
			airConditioning.surfaceAC= '80.0';
			return airConditioning;
			}

		// Create & Return: SICo_FluxProfileModel.AudioEquipments
		private static SICo_FluxProfileModel.AudioEquipments createFluxProfileAudioEquipments(){
			SICo_FluxProfileModel.AudioEquipments audioEquipments = new SICo_FluxProfileModel.AudioEquipments();
			audioEquipments.otherAudioEquipmentsNumber= '5';
			audioEquipments.pcNumber= '2';
			audioEquipments.tvNumber= '3';
			return audioEquipments;
			}

		// Create & Return: SICo_FluxProfileModel.CookingEquipments
		private static SICo_FluxProfileModel.cookingEquipments createFluxProfileCookingEquipments(){
			SICo_FluxProfileModel.CookingEquipments cookingEquipments = new SICo_FluxProfileModel.CookingEquipments();
			cookingEquipments.isCeramicGlassPlatePresent= true;
			cookingEquipments.isElectricOvenPresent= false;
			cookingEquipments.isInductionPlatePresent= true;
			cookingEquipments.isIronPlatePresent= false;
			cookingEquipments.isMicroWavePresent= false;
			cookingEquipments.isMiniOvenPresent= true;
			cookingEquipments.isGasPlatePresent= true;
			cookingEquipments.isElectricAndGasPlatePresent= true;
			cookingEquipments.isGasOvenPresent= true;
			cookingEquipments.gasSource= 'Ville';
			return cookingEquipments;
			}

		// Create & Return: SICo_FluxProfileModel.ElecEquipments
		private static SICo_FluxProfileModel.ElecEquipments createFluxProfileCookingElecEquipments(){
			SICo_FluxProfileModel.ElecEquipments elecEquipments = new SICo_FluxProfileModel.ElecEquipments();
			elecEquipments.americanFridgeFreezerNumber='0';
			elecEquipments.dishwasherNumber='1';
			elecEquipments.dryerNumber='5';
			elecEquipments.freezerNumber='1';
			elecEquipments.fridgeFreezerNumber='1';
			elecEquipments.fridgeNumber='1';
			elecEquipments.smallElecEquipmentsNumber='1';
			elecEquipments.washerNumber='0';
			return elecEquipments;
			}

		// Create & Return: SICo_FluxProfileModel.OtherEquipments
		private static SICo_FluxProfileModel.OtherEquipments createFluxProfileOtherEquipments(){
			SICo_FluxProfileModel.OtherEquipments otherEquipments = new SICo_FluxProfileModel.OtherEquipments();
			otherEquipments.isAquariumPresent= true;
			otherEquipments.isWineCellarPresent= false;
			return otherEquipments;
			}

		// Create & Return: SICo_FluxProfileModel.SanitoryHotWater
		private static SICo_FluxProfileModel.SanitoryHotWater createFluxProfileSanitoryHotWater(){
			SICo_FluxProfileModel.SanitoryHotWater sanitoryHotWater = new SICo_FluxProfileModel.SanitoryHotWater();
			sanitoryHotWater.isJacuzziPresent= false;
			sanitoryHotWater.noOfSolarSensors= '3.0';
			sanitoryHotWater.sanitaryHotWaterPractice= 'Douches';
			sanitoryHotWater.sanitaryHotWaterTankVolume='3000.0';
			sanitoryHotWater.sanitoryHotWaterType='Electrique';
			return sanitoryHotWater;
			}

		// Create & Return: SICo_FluxProfileModel.HeatingSystem
		private static SICo_FluxProfileModel.HeatingSystem createFluxProfileHeatingSystem(){
			SICo_FluxProfileModel.HeatingSystem heatingSystem = new SICo_FluxProfileModel.HeatingSystem();
			heatingSystem.princHeatingSystemTypeDetail= 'Convecteur';
			heatingSystem.principalHeatingSystemSurface= '70';
			heatingSystem.principalHeatingSystemType= 'Electricite';
			heatingSystem.secondaryHeatingSystemSurface= '10';
			return heatingSystem;
			}

		// Create & Return: SICo_FluxProfileModel.House
		private static SICo_FluxProfileModel.House createFluxProfileHouse(){
			SICo_FluxProfileModel.House house = new SICo_FluxProfileModel.House();
			house.isArrangedRoofPresent= false;
			house.isFloorPresent= true;
			house.isJointHouse= false;
			house.isVerandaPresent= false;
			return house;
			}

		// Create & Return: SICo_FluxProfileModel.InsertWood
		private static SICo_FluxProfileModel.InsertWood createFluxProfileInsertWood(){
			SICo_FluxProfileModel.InsertWood insertWood = new SICo_FluxProfileModel.InsertWood();
			insertWood.isInsertWoodPresent= false;
			insertWood.numOfStereWood= '1.0';
			return insertWood;
			}

		// Create & Return: SICo_FluxProfileModel.House
		private static SICo_FluxProfileModel.LifeStyle createFluxProfileLifeStyle(){
			SICo_FluxProfileModel.LifeStyle lifeStyle = new SICo_FluxProfileModel.LifeStyle();
			lifeStyle.allDayPresenceIndicator= false;
			lifeStyle.dailyWindowOpenIndicator= false;
			lifeStyle.heatReductionTypeInWEAbsence= 'Sans';
			lifeStyle.noOfOccupants= '3';
			lifeStyle.protectionType= 'Persienne';
			lifeStyle.reduceHeatInDayForLivingRoom= false;
			lifeStyle.reduceHeatInDayForOtherRooms= false;
			lifeStyle.shuttersCloseIndicator= true;
			lifeStyle.tempInLivingRooms= '19.0';
			lifeStyle.tempInOtherRooms= '18.0';
			lifeStyle.weekEndAbsenceType= '1sur3';
			lifeStyle.weeksOfAbsenceSummer= '4';
			lifeStyle.weeksOfAbsenceWinter= '3';
			return lifeStyle;
			}

		// Create & Return: SICo_FluxProfileModel.Restoration
		private static SICo_FluxProfileModel.Restoration createFluxProfileRestoration(){
			SICo_FluxProfileModel.Restoration restoration = new SICo_FluxProfileModel.Restoration();
			restoration.isArrangedRoofRestored= false;
			restoration.isExtWallRestored= false;
			restoration.isWallRestored= false;
			restoration.isWindowRestored= false;
			return restoration;
			}

		// Create & Return: SICo_FluxProfileModel.SwimmingPool
		private static SICo_FluxProfileModel.SwimmingPool createFluxProfileSwimmingPool(){
			SICo_FluxProfileModel.SwimmingPool swimmingPool = new SICo_FluxProfileModel.SwimmingPool();
			swimmingPool.hasSwimmingPool= true;
			swimmingPool.noOfUsageMonths= '2';
			swimmingPool.swimmingPoolHeatingType= '2';
			return swimmingPool;
			}

	/***************************************
	    DATA SET FLUX SUBSCRIPTION
	 ***************************************/

		// Create & Return: SICo_FluxSouscriptionModel.Action
		public static SICo_FluxSubscriptionModel.Action createFluxSubscriptionAction(){
			SICo_FluxSubscriptionModel.Action newAction = new SICo_FluxSubscriptionModel.Action();
			newAction.personId = SICo_Utility.generateRandomString(18,'ALL');
			newAction.siteId = SICo_Utility.generateRandomString(18,'ALL');
			newAction.activityId = SICo_Utility.generateRandomString(18,'ALL');
			newAction.endPoint = 'https://localhostTESTC43/FluxSouscriptionModel';
			newAction.externalOfferCode = 'BILAN_CONSO_THERM';
			newAction.SubscriptionRequest = createFluxSubscriptionRequest();
			return newAction;
			}

		// Create & Return: SICo_FluxSouscriptionModel.SubscriptionRequest
		private static SICo_FluxSubscriptionModel.SubscriptionRequest createFluxSubscriptionRequest(){
			SICo_FluxSubscriptionModel.SubscriptionRequest newSubscriptionRequest = new SICo_FluxSubscriptionModel.SubscriptionRequest();
			newSubscriptionRequest.ts = Datetime.now();
			newSubscriptionRequest.siteElecExternalId = SICo_Utility.generateRandomString(6,'ALL');
			newSubscriptionRequest.siteGasExternalId = SICo_Utility.generateRandomString(6,'ALL');
			newSubscriptionRequest.subscribedOfferExternalId = SICo_Utility.generateRandomString(18,'ALL');
			newSubscriptionRequest.person = createFluxSubscriptionPerson();
			newSubscriptionRequest.siteAddress = createFluxSubscriptionAdress();
			newSubscriptionRequest.PersonParameters = new SICo_FluxSubscriptionModel.PersonParameters();
			newSubscriptionRequest.SiteParameters = new SICo_FluxSubscriptionModel.SiteParameters();
			newSubscriptionRequest.SubscribedOfferParameters = new SICo_FluxSubscriptionModel.SubscribedOfferParameters();
			return newSubscriptionRequest;
			}

		// Create & Return: SICo_FluxSouscriptionModel.Person
		private static SICo_FluxSubscriptionModel.Person createFluxSubscriptionPerson(){
			SICo_FluxSubscriptionModel.Person newPerson = new SICo_FluxSubscriptionModel.Person();
			newPerson.title = 'Madame';
			newPerson.firstName = 'Boomi';
			newPerson.lastName = 'Monica';
			newPerson.address = createFluxSubscriptionAdress();
			newPerson.email = 'sjohansson@testC43.com';
			newPerson.phones = createFluxSubscriptionPhone();
			return newPerson;
			}

		// Create & Return: SICo_FluxSouscriptionModel.Address
		private static SICo_FluxSubscriptionModel.Address createFluxSubscriptionAdress(){
			SICo_FluxSubscriptionModel.Address newAdress = new SICo_FluxSubscriptionModel.Address();
			List<String> newLines = new List<String>();
			newLines.add('11 rue de la Paix');
			newAdress.lines = newLines;
			newAdress.zipCode = '92000';
			newAdress.city = 'Courbevoie';
			newAdress.country = 'France';
			return newAdress;
			}

		// Create & Return: SICo_FluxSouscriptionModel.Phones
		// parameter: n/a
		public static SICo_FluxSubscriptionModel.Phones createFluxSubscriptionPhone(){
			SICo_FluxSubscriptionModel.Phones newPhones = new SICo_FluxSubscriptionModel.Phones();
			newPhones.portable = '+33643293040';
			newPhones.principal = '+33353293041';
			return newPhones;
			}

	/***************************************
	    DATA SET FLUX CHAM CLIENT-CONTRAT
	 ***************************************/

  // Create & Return: SICo_FluxSouscriptionModel.Address
	public static SICo_FluxChamModel.ChamClientContrat createFluxChamClientContrat(){
		SICo_FluxChamModel.ChamClientContrat chamClientContrat = new SICo_FluxChamModel.ChamClientContrat();
		List<SICo_FluxChamModel.ChamClient> chamClients = new List<SICo_FluxChamModel.ChamClient>();
		chamClients.add(createFluxChamClient());
		chamClients.add(createFluxChamClient());
		chamClientContrat.chamClient = chamClients;
		return chamClientContrat;
	}

	// Create & Return: SICo_FluxChamModel.Info
	private static SICo_FluxChamModel.ChamClient createFluxChamClient(){
		SICo_FluxChamModel.ChamClient chamClient = new SICo_FluxChamModel.ChamClient();
		chamClient.idTask = SICo_Utility.generateRandomString(18,'ALL');
		chamClient.id_orchidee = SICo_Utility.generateRandomString(36,'ALL');
		chamClient.id_societe = 'C';
		chamClient.id_agence = SICo_Utility.generateRandomString(10,'ALL');
		chamClient.date_creation = Datetime.now();
		chamClient.date_modification = Datetime.now();
		chamClient.clien = SICo_Utility.generateRandomString(10,'NUMBER');
		chamClient.techn = SICo_Utility.generateRandomString(4,'NUMBER');
		chamClient.tech_matricule = SICo_Utility.generateRandomString(15,'ALL');
		chamClient.tech_agence = SICo_Utility.generateRandomString(10,'ALL');
		chamClient.inter = SICo_Utility.generateRandomString(10,'NUMBER');
		chamClient.quano = 'Mr.';
		chamClient.nomclient = 'Boomi';
		chamClient.preno = 'Pascal';
		chamClient.numru = '16';
		chamClient.quaru = 'avenue';
		chamClient.rue1 = 'Jean de Maurin';
		chamClient.codepo = '44000';
		chamClient.ville = 'Nantes';
		chamClient.batim = '12B';
		chamClient.escal = '1';
		chamClient.etage = '4';
		chamClient.codepor = '1624B';
		chamClient.porte = 'A44';
		chamClient.latitu = '43.4322';
		chamClient.longit = '2.4569';
		chamClient.teldo = '0299564231';
		chamClient.telmo = '0687162543';
		chamClient.access1 = 'test';
		chamClient.access2 = 'test';
		chamClient.observations = 'malentendant';		
		chamClient.typlog = 'MAISON';
		chamClient.locpro_lp = 'L';
		chamClient.date_logement = Datetime.valueOfGmt('2015-11-05 08:15:30');
		chamClient.tatva = '20';
		chamClient.email = 'pascalBoomi@C43.com';
		chamClient.gaz = createFluxChamGaz();
		chamClient.entretien = createFluxChamEntretient();
		chamClient.appar = createFluxChamAppar();
		return chamClient;
	}

	// Create & Return: SICo_FluxChamModel.Gaz
	private static SICo_FluxChamModel.Gaz createFluxChamGaz(){
		SICo_FluxChamModel.Gaz gaz = new SICo_FluxChamModel.Gaz();
		gaz.idxcpteur = SICo_Utility.generateRandomString(6,'MUMBER');
		gaz.matricpteur = SICo_Utility.generateRandomString(3,'MUMBER');
		gaz.pce = SICo_Utility.generateRandomString(9,'MUMBER');
		gaz.datesign = DateTime.now();
		gaz.debcontrat = DateTime.now();
		gaz.fincontrat = DateTime.now().addYears(3);
		return gaz;
	}

	// Create & Return: SICo_FluxChamModel.Entretient
	private static SICo_FluxChamModel.Entretien createFluxChamEntretient(){
		SICo_FluxChamModel.Entretien entretient = new SICo_FluxChamModel.Entretien();
		entretient.datesign = DateTime.now();
		entretient.debcontrat = DateTime.now();
		entretient.fincontrat = DateTime.now().addYears(3);
		entretient.typco = 'EC';
		entretient.desigcontrat = 'ENTRETIEN CHAUDIERE';
		entretient.prico = '9';
		entretient.optio1 = SICo_Utility.generateRandomString(6,'MUMBER');
		entretient.desigoptio1 = 'Remplacement de chaudière';
		entretient.optio2 = SICo_Utility.generateRandomString(6,'MUMBER');
		entretient.desigoptio2 = 'Assistance';
		entretient.optio3 = SICo_Utility.generateRandomString(6,'MUMBER');
		entretient.desigoptio3 = 'SAV';
		entretient.paye = 'O';
		return entretient;
	}

	// Create & Return: SICo_FluxChamModel.Appar
	private static List<SICo_FluxChamModel.Appar> createFluxChamAppar(){
		List<SICo_FluxChamModel.Appar> appar = new List<SICo_FluxChamModel.Appar>();
		SICo_FluxChamModel.Appar appareil = new SICo_FluxChamModel.Appar();
		appareil.ordr = SICo_Utility.generateRandomString(2,'ALL');
		appareil.libelenerg = 'GAZ';
		appareil.libelmarque = 'Themaplus';
		appareil.libelmodele = 'Condens F35';
		appareil.dames = Datetime.valueOfGmt('2015-11-05 08:15:30');
		appareil.serie = SICo_Utility.generateRandomString(9,'MUMBER');
		appareil.mac = SICo_Utility.generateRandomString(15,'ALL');
		appar.add(appareil);
		return appar;
	}

	/***************************************
			DATA SET FLUX CHAM REQUEST
	 ***************************************/

	public static SICo_FluxChamModel.ChamRDV createFluxChamRDV(){
		List<SICo_FluxChamModel.ChamDemande> chamDemandeList = new List<SICo_FluxChamModel.ChamDemande>();
		chamDemandeList.add(createFluxChamDemande());
		chamDemandeList.add(createFluxChamDemande());
		SICo_FluxChamModel.ChamRDV chamRDV = new SICo_FluxChamModel.ChamRDV();
		chamRDV.chamDemande = chamDemandeList;
		return chamRDV;
	}

	public static SICo_FluxChamModel.ChamDemande createFluxChamDemande(){
		SICo_FluxChamModel.ChamDemande chamDemande = new SICo_FluxChamModel.ChamDemande();
		chamDemande.id_ticket_orchidee = SICo_Utility.generateRandomString(18,'ALL');
		chamDemande.id_orchidee = SICo_Utility.generateRandomString(36,'ALL');
		chamDemande.id_societe = SICo_Utility.generateRandomString(5,'ALL');
		chamDemande.id_agence = 'D';
		chamDemande.inter = SICo_Utility.generateRandomString(10,'NUMBER');
		chamDemande.type = 'Dépannage';
		chamDemande.motif = 'Visite Annuelle';
		chamDemande.motif2 = 'Prévue dans le contrat';
		chamDemande.motif3 = 'Visite obligatoire';
		chamDemande.observationclient = 'Bouton rouge allumé';
		chamDemande.dateinter = Datetime.now();
		chamDemande.tranc = 'Avant';
		chamDemande.heurd = '11:00';
		chamDemande.heurf = '12:00';
		chamDemande.periode = 'M';
		chamDemande.premier = 'O';
		chamDemande.numerodevis = SICo_Utility.generateRandomString(10,'ALL');
		chamDemande.statut_avancement = 'en cours';
		chamDemande.date_creation = Datetime.now();
		chamDemande.date_modification = Datetime.now();
		List<SICo_FluxChamModel.Diagnostique> diagnostiqueList = new List<SICo_FluxChamModel.Diagnostique>();
		diagnostiqueList.add(createFluxDiagnostique());
		diagnostiqueList.add(createFluxDiagnostique());
		chamDemande.diagnostique = diagnostiqueList;

		return chamDemande;
	}

	// Create & Return: SICo_FluxChamModel.Person
	private static SICo_FluxChamModel.Diagnostique createFluxDiagnostique(){
		SICo_FluxChamModel.Diagnostique diagnostique = new SICo_FluxChamModel.Diagnostique();
		diagnostique.question = 'Besoin de vérification';
		diagnostique.codepanne = 'E534';
		diagnostique.reponse = 'blabla';
		return diagnostique;
	}
}