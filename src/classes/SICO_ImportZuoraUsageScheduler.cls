global class SICO_ImportZuoraUsageScheduler implements Schedulable {
   global void execute(SchedulableContext SC) {
      Database.executeBatch(new SICo_Batch_BILL_RUN(SICO_ImportZuoraUsage.InvoceItemReportName)); 
   }
}