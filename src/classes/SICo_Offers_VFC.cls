/*------------------------------------------------------------
Author:        Abd-slem OUALI
Company:       Ikumbi
Description:   Controller for the offers component
Test Class:    SICo_Offers_VFC_TEST
History
11/08/2016      Abd-slem OUALI      Create version
25/11/2016      Mehdi CHERFAOUI     Refactoring
------------------------------------------------------------*/
public with sharing class SICo_Offers_VFC { 

	private static Integer SE_TH_GAZ = 1; // Offer 1 => SE+TH+GAZ
	private static Integer SE_GAZ    = 2; // Offer 2 => SE+GAZ
	private static Integer SE_TH     = 3; // Offer 3 => SE+TH
	private static Integer SE        = 4; // Offer 4 => SE
	private static Integer GAZ       = 5; // Offer 5 => GAZ
	private static Integer MAINT     = 6; // Offer 6 => MAINT
 
	/**
	 * [Contructor]
	 */
	public SICo_Offers_VFC() {}

	@AuraEnabled
	public static MyOffersWrapper getMOW(String pAccountId) { 
		if (pAccountId == null) {
			return null;
		} 
		MyOffersWrapper vMow = null;
		String vAccountId = SICo_UtilityCommunities_CLS.getAccountId(pAccountId);		 
		//If AccountId found, retrieve associated personal information
		if (vAccountId != null) { 
			//Query Site
			SICo_Site__c vSite = querySite(vAccountId);
			if (vSite != null) {
				String subscribedOffers = (vSite.SICO_Subscribed_offers__c != null ? vSite.SICO_Subscribed_offers__c : '');
				//CPE TODO : une fois décidé si on utilise un custom setting ou metadata pour mapper les valeurs de SICO_Subscribed_offers__c 
				// et le nom de l'offre (voir SICO-2038) il faudra supprimer la requête des souscriptions et la partie du wrapper qui les utilise
				//Query Subscription
				List<Zuora__Subscription__c> vSubscriptionsList = querySubscriptionList(vAccountId);
 
				//Query number of subscriptions
				Integer vNumberSubscription = queryNumberSubscriptions(vAccountId);
				if (!vSubscriptionsList.isEmpty() && vNumberSubscription != 0 && vSite != null ) {
					//Create Wrapper with the data to be displayed on the page
					vMow = new MyOffersWrapper(subscribedOffers, vSubscriptionsList);
				}
			}
		}
		return vMow;
	}

	public class MyOffersWrapper {
		//Données
		@AuraEnabled
		public String offerLabel {get; set;} 
		@AuraEnabled
		public Integer offerNumber {get; set;} 
		@AuraEnabled
		public Boolean hasContract {get; set;} 

		public MyOffersWrapper(String subscribedOffers, List<Zuora__Subscription__c> listZS) {
			if (subscribedOffers != '') {
				hasContract = true;
				if (subscribedOffers.contains('SE')) { //SE
					offerNumber = SE;
					if (subscribedOffers.contains('TH')) { // SE_TH
						offerNumber = SE_TH;
						if (subscribedOffers.contains('GAZ')) { //SE_TH_GAZ
							offerNumber = SE_TH_GAZ;
						}
					} else if (subscribedOffers.contains('GAZ')) { //SE_GAZ
						offerNumber = SE_GAZ;
					}
				} else if (subscribedOffers.contains('MAINT') && listZS.size() == 1) { // MAINT and no other subscription
					offerNumber = MAINT;
				} else if (subscribedOffers.contains('GAZ')) { // GAZ alone
					offerNumber = GAZ;
				}
			}
			//Label 
			if (listZS[0].MarketingOfferName__c != null) {
				offerLabel = listZS[0].MarketingOfferName__c;
			}
		}
	}


	/**** HELPER METHODS ****/
	public static Integer queryNumberSubscriptions(String pAccountId) {
		return [SELECT count()
		        FROM Zuora__Subscription__c
		        WHERE Zuora__Account__c = :pAccountId
		       ];

	}

	public static SICo_Site__c querySite(String pAccountId) {
		SICo_Site__c theSite = null;
		List <SICo_Site__c> siteList = [SELECT Id, SICO_Subscribed_offers__c, IsSelectedInCustomerSpace__c FROM SICo_Site__c WHERE Subscriber__c = :pAccountId LIMIT 100];
		//If 1 single site is retreived, use it
		if (siteList.size() == 1) {
			theSite = siteList[0];
			//if several Sites, use the one selected in Customer Space
		} else if (siteList.size() > 1) {
			for (SICo_Site__c custSite : siteList) {
				if (custSite.IsSelectedInCustomerSpace__c) theSite = custSite;
			}
		}//ENDIF siteList.size()
		return theSite;
	}

	public static List<Zuora__Subscription__c> querySubscriptionList(String pAccountId) {
		return [SELECT Id,
		        OfferName__c,
		        MarketingOfferName__c
		        FROM Zuora__Subscription__c
		        WHERE Zuora__Account__c = :pAccountId
		       ];
	}

} //END SICo_Offers_VFC()