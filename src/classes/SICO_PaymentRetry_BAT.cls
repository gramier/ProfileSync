global class SICO_PaymentRetry_BAT implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT id, Tech_PaymentCB__c, Tech_PaymentSEPA__c, Tech_PaymentOK__c, Zuora__Account__c 
                                         FROM Zuora__CustomerAccount__c 
                                         WHERE Tech_PaymentOK__c = false]);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        SICo_Z_CustomerAccountTriggerhandler_CLS.HandlePaymentAssociationProcess( (List<Zuora__CustomerAccount__c>) scope);
    }

    global void finish(Database.BatchableContext BC) {
    }
}