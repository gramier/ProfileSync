/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Mock responce for SICo_FluxSubscription
History
13/07/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
global with sharing class SICo_PairObject_MOCK implements HttpCalloutMock {

	private integer code;

	public SICo_PairObject_MOCK(integer code) {
		this.code = code;
	}

	public SICo_PairObject_MOCK() {
		this.code = 200;
	}	

	// Implement this interface method
	global HTTPResponse respond( HTTPRequest request ) {

		// Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(this.code);
        return res;

	}

}