/**
* @author Boris Castellani
* @date 25/10/2016
*
* @description Handle MaintenancyQuote Trigger Business Rule
*/
public with sharing class SICo_MaintenancyQuoteTrigger_CLS {

/* -------- BEFORE METHODE -------- */

	public static void handleBeforeInsert(List<MaintenancyQuote__c> allMaintenancyQuotes){
		Map<Id,Account> accs = getAccount(allMaintenancyQuotes);
		if(!accs.isEmpty()){
			for(MaintenancyQuote__c devis : allMaintenancyQuotes){
				if(devis.MaintenancyQuoteStatus__c == 'A valider' && accs.containsKey(devis.Customer__c)){
					devis.Email__c =  accs.get(devis.Customer__c).PersonEmail;
				}
			}
		}		
	}

/* -------- AFTER METHODE -------- */

	public static void handleAfterInsert(final List<MaintenancyQuote__c> allMaintenancyQuotes){
		createCaseAndTask(allMaintenancyQuotes);
	}

	public static void handleAfterUpdate(final List<MaintenancyQuote__c> oldMaintenancyQuotes, final Map<Id, MaintenancyQuote__c> allMaintenancyQuotes){
		//Prepare map of New quotes
		Map<Id, MaintenancyQuote__c> mapNewMaintenancyQuote = new Map<Id, MaintenancyQuote__c>();
		for(MaintenancyQuote__c newNewMaintenancyQuote : allMaintenancyQuotes.values()){
				mapNewMaintenancyQuote.put(newNewMaintenancyQuote.Id, newNewMaintenancyQuote);
			}
		//Remove previously Accepted Quotes
		for(MaintenancyQuote__c devis : oldMaintenancyQuotes){
			if(devis.MaintenancyQuoteStatus__c == 'Accepté'){
				mapNewMaintenancyQuote.remove(devis.Id);	
			}
		}
		//Deal with remaining Quotes
		if(!mapNewMaintenancyQuote.isEmpty()){
			createCaseAndTask(mapNewMaintenancyQuote.values());			
		}
	}

/* -------- PRIVATE METHODE -------- */

	private static Map<Id,Account> getAccount(List<MaintenancyQuote__c> allMaintenancyQuotes){
		Set<Id> accId = new Set<Id>();
		for(MaintenancyQuote__c devis : allMaintenancyQuotes){
			if(devis.Customer__c != null){
				accId.add(devis.Customer__c);
			}
		}
		return new Map<Id,Account>([SELECT Id, PersonEmail FROM Account WHERE Id in :accId]);	
	}

	private static void createCaseAndTask(final List<MaintenancyQuote__c> allMaintenancyQuotes){
		final Map<Id,MaintenancyQuote__c> maintenancyQuotes = new Map<Id,MaintenancyQuote__c>();
		final Map<String,Case> casesMap = new Map<String,Case>();
		final Set<Id> accountsId = new Set<Id>();
		final Set<Id> siteId = new Set<Id>();
		for (MaintenancyQuote__c devis : allMaintenancyQuotes) {
			if(devis.MaintenancyQuoteStatus__c == 'Accepté'){
				maintenancyQuotes.put(devis.Id, devis);
				accountsId.add(devis.Customer__c);
				siteId.add(devis.Site__c);
			}
		}
		List<Case> cases = [SELECT ProvisioningType__c, Site__c, AccountId, Status, ContactId FROM Case 
												WHERE Status != 'Closed' AND AccountId IN :accountsId 
												AND Site__c IN :siteId AND RecordType.DeveloperName = 'CustomerDemand'];

		if(!cases.isEmpty()){
			for(Case myCase : cases){
				String key = String.valueOf(myCase.AccountId)+String.valueOf(myCase.Site__c);
				casesMap.put(key,myCase);
			}		
		}
		if(!maintenancyQuotes.isEmpty()){
			final List<Task> tasks = new List<Task>();
			final Map<Id,Case> oldCase = new Map<Id,Case> ();
			final Map<Id,Case> newCase = new Map<Id,Case> ();
			final Map<Id,SICo_Site__c> sites = new Map<Id,SICo_Site__c>([	
				SELECT Id, CHAMAgencyEntretien__c, Subscriber__c, Subscriber__r.PersonContactId 
				FROM SICo_Site__c 
				WHERE Subscriber__c IN :accountsId ]);

			for(MaintenancyQuote__c devis : maintenancyQuotes.values()){
				String key = String.valueOf(devis.Customer__c)+String.valueOf(devis.Site__c);
				if(casesMap.containsKey(key)){
					oldCase.put(devis.Id,casesMap.get(key));
				}else{
					newCase.put(devis.Id,createCase(devis));
				}
			}
			for(Case myCase : newCase.values()){
				myCase.ContactId = sites.get(myCase.Site__c).Subscriber__r.PersonContactId;
			}
			if(!newCase.isEmpty()){
				// INSERT & MERGE CASE
				oldCase.putAll(SICo_WithoutSharing_CLS.insertCases(newCase));
			}

			for(Id devisId : oldCase.keyset()){
				Case caseNow = oldCase.get(devisId);
				tasks.add(createTask(caseNow, maintenancyQuotes.get(devisId), sites.get(caseNow.Site__c)));
			}
			if(!tasks.isEmpty()){
				SICo_WithoutSharing_CLS.insertTasks(tasks);
			}
		}
	}

	private static Case createCase(MaintenancyQuote__c devis){
		Case myCase = new Case(
			RecordTypeId= SICo_Utility.m_RTbyDeveloperName.get('Case_CustomerDemand'),
			ProvisioningType__c = 'Depannage',
			Site__c= devis.Site__c,
			AccountId= devis.Customer__c,
			Subject__c = 'Partenaires',
			Heading__c = 'CHAM Demande Intervention Dépannage',
			Origin = 'Flux'
		);
		return myCase;
	}

	private static Task createTask(Case myCase, MaintenancyQuote__c devis, SICo_Site__c site){
		Task aTask = new Task(
			RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Task_ChamDemand'),
			Subject= 'Devis Accepté',
      WhatId= myCase.Id,
     	WhoId= myCase.ContactId,
      InterventionType__c = 'DEVIS',
      CHAMAgency__c = site.CHAMAgencyEntretien__c,
      MaintenancyQuote__c = devis.Id,
      MaintenancyQuoteNumber__c = devis.MaintenancyQuoteNumber__c
		);
		aTask.TaskId__c = 'CH0001';
		aTask.Status = 'Ouverte';
		aTask.ProvisioningCallOut__c = true;
		aTask.FluxStatus__c = 'Traitement non démarré';
		aTask.Tech_ProvisioningReady__c = true;
		aTask.OrderFlux__c = 2;
		return aTask;
	}  
}