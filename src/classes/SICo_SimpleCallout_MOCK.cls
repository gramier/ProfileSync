/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Mock for testing simple callouts in Test Classes
History
13/01/2017     Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
public class SICo_SimpleCallout_MOCK implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;

    public SICo_SimpleCallout_MOCK(Integer code, String status, String body) {
        this.code = code;
        this.status = status;
        this.body = body;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }

}