public with sharing class SICo_DocusignStatusTrigger_CLS {

	public static void HandleAfterInsert(List<dsfs__DocuSign_Status__c> docStatus) {
		List<ID> accnts = new List<ID>();

		System.debug('### => ' + docStatus);

		for (dsfs__DocuSign_Status__c doc : docStatus) {
			System.debug('### => ' + doc);
			if (doc.dsfs__Envelope_Status__c == 'Completed') {

				SICo_UpdateBankDetails_VFC.updateDefaultAndOldPaymentMethodAsynch(doc.dsfs__Company__c, doc.Tech_ZPaymentMethodId__c);
				accnts.add(doc.dsfs__Company__r.Id);
			}
		}
	}
    
}