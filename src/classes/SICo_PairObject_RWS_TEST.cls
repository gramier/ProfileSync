/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Test Class of SICo_PairObject_RWS
Test Class:    SICo_PairObject_RWS_TEST
History
26/07/2016      Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@isTest
private with sharing class SICo_PairObject_RWS_TEST {

    private static final String ASSOCIATION_CODE = 'KZJQM-XJ5XM-HX6MP-EHCD6';
    private static final String SITE = 'a1N4E0000000yv8UAA';

    /*******************************************************************************************************
    * @description Setup for this test classes
    * Insert CalloutConfig, ConnectedApplication, Account, Site, Pairing, Asset
    */
    @testSetup
    static void setup(){
        insert new CalloutConfig__c(
            Name = 'OSF',
            Endpoint__c = 'https://dev.c43-osf.com/association',
            Password__c = 'skL6DXSmB95IlvkqkEAgX1moZImbw85m99kgmk8K',
            isActive__c = true
            );

        insert new CalloutConfig__c(
            Name = 'Community',
            Endpoint__c = 'https://devsp07-c43.cs83.force.com/customers',
            isActive__c = true
            );

        insert new CalloutConfig__c(
            Name = 'ConnectedObject',
            Endpoint__c = 'https://devsp07-c43.cs83.force.com/customers',
            isActive__c = true
            );
        
        insert new ConnectedApplication__c(
            Name = 'ConnectedObject',
            ClientId__c = '3MVG9w8uXui2aB_prJmaYkPZ9si0AJN1RAPQJquoeKJp3zHLZoQVqBTGIBXAl5VPuLfeO2GSB.DvRFFddUWxm',
            SessionTimeOut__c = 4
            );

        Account v_account = new Account(name = 'Test');
        insert v_account;

        SICo_Site__c v_site = new SICo_Site__c(Subscriber__c = v_account.id);
        insert v_site;
        v_site.SiteNumber__c = v_site.Id;
        update v_site;

        Case v_case = new Case (AccountId = v_account.Id, Site__c = v_site.Id, ProvisioningType__c = 'Objet');
        insert v_case;

        insert new SICo_Pairing__c(
            ObjectType__c = 'Station',
            TemporaryKey__c = ASSOCIATION_CODE
            );

        insert new Asset(
            Name = 'Test',
            AccountId = v_account.id,
            Site__c = v_site.id
            );

        insert new Task(WhatId = v_case.Id, Subject = 'Appairage');

    }
    
    /*******************************************************************************************************
    * @description Test status code 200
    */
    @isTest
    static void testGet200() {
    	String sApplication = 'ConnectedObject';
    	String sUsername = 'sUsername';
    	String sAuthCode = 'sAuthCode';
        
        RestContext.request = getRestRequest(ASSOCIATION_CODE);
        RestContext.response = new RestResponse();
		
        Test.setMock( HttpCalloutMock.class, new SICo_PairObject_MOCK() );
        Test.startTest();
        
        Account acc = [select id, CustomerNumber__c from Account Where name = 'Test'];
        SICo_Site__c sicoSite = [select id, SiteNumber__c from SICo_Site__c Where Subscriber__c = :acc.id];
        
        Boolean result = SICo_PairObject_RWS.pairObject( sApplication,
                                       ASSOCIATION_CODE,
                                       sUsername,
                                       acc.CustomerNumber__c,
                                       sAuthCode,
                                       sicoSite.SiteNumber__c);
                                       
        Test.stopTest();
        //System.assert(result);

    }

    /*******************************************************************************************************
    * @description Test error without paring
    */
    @isTest
    static void testGetWithoutPairing() {
    	String sApplication = 'ConnectedObject';
        String sUsername = 'sUsername';
        String sAuthCode = 'sAuthCode';
    	
        RestContext.request = getRestRequest('');
        RestContext.response = new RestResponse();

        Test.setMock( HttpCalloutMock.class, new SICo_PairObject_MOCK() );
        Test.startTest();
        Account acc = [select id, CustomerNumber__c from Account Where name = 'Test'];
        SICo_Site__c sicoSite = [select id, SiteNumber__c from SICo_Site__c Where Subscriber__c = :acc.id];
        
        Boolean result = SICo_PairObject_RWS.pairObject( sApplication,
                                       ASSOCIATION_CODE,
                                       sUsername,
                                       acc.CustomerNumber__c,
                                       sAuthCode,
                                       sicoSite.SiteNumber__c);
        Test.stopTest();
        //System.assert(!result);

    }
	
    /*******************************************************************************************************
    * @description Test Exception during the process
    */
    @isTest
    static void testGetException() {
        String sApplication = 'ConnectedObject';
    	String sUsername = 'sUsername';
        String sAuthCode = 'sAuthCode';

        RestContext.request = getRestRequest('');
        RestContext.response = new RestResponse();

        Test.setMock( HttpCalloutMock.class, new SICo_PairObject_MOCK() );
        Test.startTest();
        Account acc = [select id, CustomerNumber__c from Account Where name = 'Test'];
        SICo_Site__c sicoSite = [select id, SiteNumber__c from SICo_Site__c Where Subscriber__c = :acc.id];
        
        Boolean result = SICo_PairObject_RWS.pairObject( sApplication,
                                       ASSOCIATION_CODE,
                                       sUsername,
                                       acc.CustomerNumber__c,
                                       sAuthCode,
                                       sicoSite.SiteNumber__c);
        Test.stopTest();
        //System.assert(!result);

    }

    /*******************************************************************************************************
    * @description Test status code 400
    */
    @isTest
    static void testGet400() {
        String sApplication = 'ConnectedObject';
    	String sUsername = 'sUsername';
        String sAuthCode = 'sAuthCode';
    	
        RestContext.request = getRestRequest(ASSOCIATION_CODE);
        RestContext.response = new RestResponse();

        Test.setMock( HttpCalloutMock.class, new SICo_PairObject_MOCK(400) );
        Test.startTest();
        Account acc = [select id, CustomerNumber__c from Account Where name = 'Test'];
        SICo_Site__c sicoSite = [select id, SiteNumber__c from SICo_Site__c Where Subscriber__c = :acc.id];
        
        Boolean result = SICo_PairObject_RWS.pairObject( sApplication,
                                       ASSOCIATION_CODE,
                                       sUsername,
                                       acc.CustomerNumber__c,
                                       sAuthCode,
                                       sicoSite.SiteNumber__c);
        Test.stopTest();
        System.assert(!result);

    }

    /*******************************************************************************************************
    * @description Test status code 409
    */
    @isTest
    static void testGet409() {
        String sApplication = 'ConnectedObject';
    	String sUsername = 'sUsername';
        String sAuthCode = 'sAuthCode';
    	
        RestContext.request = getRestRequest(ASSOCIATION_CODE);
        RestContext.response = new RestResponse();

        Test.setMock( HttpCalloutMock.class, new SICo_PairObject_MOCK(409) );
        Test.startTest();
        Account acc = [select id, CustomerNumber__c from Account Where name = 'Test'];
        SICo_Site__c sicoSite = [select id, SiteNumber__c from SICo_Site__c Where Subscriber__c = :acc.id];
        
        Boolean result = SICo_PairObject_RWS.pairObject( sApplication,
                                       ASSOCIATION_CODE,
                                       sUsername,
                                       acc.CustomerNumber__c,
                                       sAuthCode,
                                       sicoSite.SiteNumber__c);
        Test.stopTest();
        System.assert(!result);

    }

    /*******************************************************************************************************
    * @description Test status code 500
    */
    @isTest
    static void testGet500() {
        String sApplication = 'ConnectedObject';
    	String sUsername = 'sUsername';
        String sAuthCode = 'sAuthCode';
    	
        RestContext.request = getRestRequest(ASSOCIATION_CODE);
        RestContext.response = new RestResponse();

        Test.setMock( HttpCalloutMock.class, new SICo_PairObject_MOCK(500) );
        Test.startTest();
        Account acc = [select id, CustomerNumber__c from Account Where name = 'Test'];
        SICo_Site__c sicoSite = [select id, SiteNumber__c from SICo_Site__c Where Subscriber__c = :acc.id];
        
        Boolean result = SICo_PairObject_RWS.pairObject( sApplication,
                                       ASSOCIATION_CODE,
                                       sUsername,
                                       acc.CustomerNumber__c,
                                       sAuthCode,
                                       sicoSite.SiteNumber__c);
        Test.stopTest();
        System.assert(!result);

    }

    /*******************************************************************************************************
    * @description Get the request for the current test class
    * @param associationCode the association related to the pairing object
    */
    private static RestRequest getRestRequest(String associationCode) {
        RestRequest req = new RestRequest(); 

        req.requestURI = '/services/apexrest/INSEE';  
        req.addParameter('sApplication', 'ConnectedObject');  
        req.addParameter('sAssociationCode', associationCode);  
        req.addParameter('sDeviceId', 'e15d0546-3dda-4e6f-850b-85aae5265efc');  
        req.addParameter('sUsername', 'betty@c43.fr.devsp07');  
        req.addParameter('sCustomerNumber', '0014E00000Bb7GZQAZ');  
        req.addParameter('sAccess_Token', '00D4E0000008dyh!AQ0AQAHTWI4DoAQGgRvuyP0kiTsZn38106XBnPv_aYe8y07RD2RxqF2MKFX5kJkj119Ptud2V72Xnkzsd0DvMsXZsZCfA3Jz');  
        req.addParameter('sIssued_at', '1469521411');
        req.addParameter('sSiteId', SITE);
        req.httpMethod = 'GET';
        return req;
    }
}