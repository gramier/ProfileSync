/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Test of controller SICo_UnpaidBills_VFC
History
16/09/2016     Gaël BURNEAU     Create version
------------------------------------------------------------*/
@isTest(SeeAllData=True)//Required to access Zuora WSLD
private class SICo_UnpaidBills_VFC_TEST {

    public static String communityUserName = 'myUserCommunityTest1@test.com';

    @isTest static void constructor_test() {
        //Insert new community user
        User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
        communityUser1.userName = SICo_UnpaidBills_VFC_TEST.communityUserName;
        insert communityUser1;
        communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

        //select account
        Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

        Zuora__CustomerAccount__c vZAccount = new Zuora__CustomerAccount__c();
        vZAccount.Zuora__External_Id__c = 'externalID';
        vZAccount.Zuora__Account__c = acc1.Id;
        insert vZAccount;

        /*Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
        insert account;
        */
        SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(acc1.Id);
        oSICoSite.IsActive__c = true;
        oSICoSite.PrincipalHeatingSystemType__c = 'Gaz';
        oSICoSite.SanitaryHotWaterType__c = 'Electrique';
        oSICoSite.NoOfOccupants__c = 4;
        oSICoSite.surfaceInSqMeter__c = 80;
        oSICoSite.InseeCode__c = '44109';
        oSICoSite.ConstructionDate__c = 1980;
        oSICoSite.PCE__c = 'TestPCE';
        insert oSICoSite;

/*
        CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
        insert boomiConfig;
*/

        Contact personContact = [SELECT Id, Name FROM Contact WHERE IsPersonAccount = true AND AccountId = :acc1.Id];

        Opportunity opp = SICo_UtilityTestDataSet.createOpportunity('Test', acc1.Id, oSICoSite.Id);
        insert opp;

        zqu__Quote__c quote = SICo_UtilityTestDataSet.createZuoraQuote('Test', acc1.Id, personContact.Id, opp.Id);
        insert quote;

        Zuora__CustomerAccount__c customerAccount = new Zuora__CustomerAccount__c(Zuora__Account__c = acc1.Id, Zuora__BillCycleDay__c = 'AA TEST');
        insert customerAccount;

        Zuora__Subscription__c subscription =
            SICo_UtilityTestDataSet.createZuoraSubscription('Test', acc1.Id, quote.Id, '00', oSICoSite.Id);
        subscription.Zuora__CustomerAccount__c = customerAccount.Id;
        insert subscription;

        Zuora__ZInvoice__c invoice1 = SICo_UtilityTestDataSet.createZuoraInvoice(acc1.Id);
        invoice1.Zuora__Balance2__c = 10;
        invoice1.IsPaymentInProgress__c = false;
        // TODO invoice1.TotalInvoiceAmountTTC__c = 2013;
        Zuora__ZInvoice__c invoice2 = SICo_UtilityTestDataSet.createZuoraInvoice(acc1.Id);
        invoice2.Zuora__Balance2__c = 10;
        invoice2.Zuora__TotalAmount__c = 15;
        invoice2.IsPaymentInProgress__c = false;
        // TODO invoice1.TotalInvoiceAmountTTC__c = 2013;
        Zuora__ZInvoice__c invoice3 = SICo_UtilityTestDataSet.createZuoraInvoice(acc1.Id);
        invoice3.Zuora__Balance2__c = 15;
        invoice3.IsPaymentInProgress__c = false; 
        invoice3.ScheduleDebitDate__c = Date.today() - 1;
        invoice3.Zuora__TotalAmount__c = 156;
        insert new Zuora__ZInvoice__c[] { invoice1, invoice2, invoice3 };

        SiCo_InvoiceItem__c invoice1Item1 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice1.Id, 'invoice1Item1', 10);
        SiCo_InvoiceItem__c invoice2Item1 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice2.Id, 'invoice2Item1', 5);
        SiCo_InvoiceItem__c invoice2Item2 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice2.Id, 'invoice2Item2', 5);
        SiCo_InvoiceItem__c invoice3Item1 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice3.Id, 'invoice3Item1', 15);

        insert new SiCo_InvoiceItem__c[] { invoice1Item1, invoice2Item1, invoice2Item2, invoice3Item1 }; 


        insert SICo_UtilityTestDataSet.createAttachment(invoice1.id);
        insert SICo_UtilityTestDataSet.createAttachment(invoice2.id);

        User vCommunityUser = [SELECT Id, userName, AccountId FROM User WHERE UserName = :SICo_UnpaidBills_VFC_TEST.communityUserName];
        Account vAccount = [SELECT Id FROM Account WHERE Id = :vCommunityUser.AccountId LIMIT 1];

        System.debug('##########################' + [SELECT Id FROM Account WHERE Id = :vCommunityUser.AccountId]);
        List<Zuora__ZInvoice__c> vInvoices = [SELECT Id,Zuora__Balance2__c, IsPaymentInProgress__c FROM Zuora__ZInvoice__c];

        System.debug('##########################' + vInvoices);

        System.runAs(vCommunityUser) {
            PageReference pref = Page.UnpaidBills;
            pref.getParameters().put(SICo_Constants_CLS.URL_PARAM_ACCOUNTID, vAccount.Id);
            //pref.getParameters().put('pRefId', 'test');
            Test.setCurrentPage(pref);

            SICo_UnpaidBills_VFC controller = new SICo_UnpaidBills_VFC();

            System.assertEquals(vAccount.Id, controller.accountId);
        }
    }

    @isTest static void callZuoraPayment_test() {
        //Insert new community user
        User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
        communityUser1.userName = SICo_UnpaidBills_VFC_TEST.communityUserName;
        insert communityUser1;
        communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

        //select account
        Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

        Zuora__CustomerAccount__c vZAccount = new Zuora__CustomerAccount__c();
        vZAccount.Zuora__External_Id__c = 'externalID';
        vZAccount.Zuora__Account__c = acc1.Id;
        insert vZAccount;

        /*Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
        insert account;
        */
        SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(acc1.Id);
        oSICoSite.IsActive__c = true;
        oSICoSite.PrincipalHeatingSystemType__c = 'Gaz';
        oSICoSite.SanitaryHotWaterType__c = 'Electrique';
        oSICoSite.NoOfOccupants__c = 4;
        oSICoSite.surfaceInSqMeter__c = 80;
        oSICoSite.InseeCode__c = '44109';
        oSICoSite.ConstructionDate__c = 1980;
        oSICoSite.PCE__c = 'TestPCE';
        insert oSICoSite;

/*
        CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
        insert boomiConfig;
*/

        Contact personContact = [SELECT Id, Name FROM Contact WHERE IsPersonAccount = true AND AccountId = :acc1.Id];

        Opportunity opp = SICo_UtilityTestDataSet.createOpportunity('Test', acc1.Id, oSICoSite.Id);
        insert opp;

        zqu__Quote__c quote = SICo_UtilityTestDataSet.createZuoraQuote('Test', acc1.Id, personContact.Id, opp.Id);
        insert quote;

        Zuora__CustomerAccount__c customerAccount = new Zuora__CustomerAccount__c(Zuora__Account__c = acc1.Id, Zuora__BillCycleDay__c = 'AA TEST');
        insert customerAccount;

        Zuora__Subscription__c subscription =
            SICo_UtilityTestDataSet.createZuoraSubscription('Test', acc1.Id, quote.Id, '00', oSICoSite.Id);
        subscription.Zuora__CustomerAccount__c = customerAccount.Id;
        insert subscription;

        Zuora__ZInvoice__c invoice1 = SICo_UtilityTestDataSet.createZuoraInvoice(acc1.Id);
        invoice1.Zuora__Balance2__c = 10;
        invoice1.IsPaymentInProgress__c = false;
        // TODO invoice1.TotalInvoiceAmountTTC__c = 2013;
        Zuora__ZInvoice__c invoice2 = SICo_UtilityTestDataSet.createZuoraInvoice(acc1.Id);
        invoice2.Zuora__Balance2__c = 10;
        invoice2.Zuora__TotalAmount__c = 15;
        invoice2.IsPaymentInProgress__c = false;
        // TODO invoice1.TotalInvoiceAmountTTC__c = 2013;
        Zuora__ZInvoice__c invoice3 = SICo_UtilityTestDataSet.createZuoraInvoice(acc1.Id);
        invoice3.Zuora__Balance2__c = 15;
        invoice3.IsPaymentInProgress__c = false; 
        invoice3.ScheduleDebitDate__c = Date.today() - 1;
        invoice3.Zuora__TotalAmount__c = 156;
        insert new Zuora__ZInvoice__c[] { invoice1, invoice2, invoice3 };

        SiCo_InvoiceItem__c invoice1Item1 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice1.Id, 'invoice1Item1', 10);
        SiCo_InvoiceItem__c invoice2Item1 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice2.Id, 'invoice2Item1', 5);
        SiCo_InvoiceItem__c invoice2Item2 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice2.Id, 'invoice2Item2', 5);
        SiCo_InvoiceItem__c invoice3Item1 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(invoice3.Id, 'invoice3Item1', 15);

        insert new SiCo_InvoiceItem__c[] { invoice1Item1, invoice2Item1, invoice2Item2, invoice3Item1 }; 


        insert SICo_UtilityTestDataSet.createAttachment(invoice1.id);
        insert SICo_UtilityTestDataSet.createAttachment(invoice2.id);

        User vCommunityUser = [SELECT Id, userName, AccountId FROM User WHERE UserName = :SICo_UnpaidBills_VFC_TEST.communityUserName];
        Account vAccount = [SELECT Id FROM Account WHERE Id = :vCommunityUser.AccountId LIMIT 1]; 
        List<Zuora__ZInvoice__c> theInvoices = [SELECT ID,Zuora__Balance2__c,TotalInvoiceAmountTTC__c, Zuora__BillingAccount__r.Zuora__Default_Payment_Method__r.Zuora__Active__c,  IsPaymentInProgress__c, Zuora__Account__c, IsUnpaid__c FROM Zuora__ZInvoice__c];
        System.debug('theInvoices: '+theInvoices);
        System.runAs(vCommunityUser) { 
            SICo_UnpaidBills_VFC vController = new SICo_UnpaidBills_VFC(vAccount.Id);
            //SICo_UnpaidBills_VFC vController = new SICo_UnpaidBills_VFC(acc1.Id);
            vController.getDashboardLink();
            SICo_UnpaidBills_VFC.UnpaidInvoiceWrapper uiw = new SICo_UnpaidBills_VFC.UnpaidInvoiceWrapper(invoice1, invoice1.Zuora__InvoiceDate__c + '', null, invoice1.Id);
            uiw.isChecked = true;
            uiw.dInvoiceAmount = 10;
            if(vController.listUnpaidInvoiceWrappers != null) vController.listUnpaidInvoiceWrappers.add(uiw);
            String vResult = vController.callZuoraPayment('null');
            System.assert(vResult != null);
            //Zuora methods - no assert
            PageReference vResultPr = vController.proceedToPayment();
            String vResult2 = vController.updateInvoices();
        }
    }


    @isTest 
    static void processSignature_test() {
        //Insert new community user
        User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
        communityUser1.userName = SICo_UnpaidBills_VFC_TEST.communityUserName;
        insert communityUser1;
        communityUser1 = [SELECT AccountId, ContactId, ProfileId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

        //select account
        Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

        Zuora__CustomerAccount__c vZAccount = new Zuora__CustomerAccount__c();
        vZAccount.Zuora__External_Id__c = 'externalID';
        vZAccount.Zuora__Account__c = acc1.Id;
        insert vZAccount;

        ZuoraWSconfig__c zc = new ZuoraWSconfig__c();
		zc.Uri__c = 'Test Uri';
		zc.PageIdSEPA__c = 'Test PageId';
        zc.PageIdCB__c = 'Test PageId';
        zc.ExternalPageIdSEPA__c = 'Test PageId';
        zc.ExternalPageIdCB__c = 'Test PageId';
        zc.GatewayNameSEPA__c  = 'SEPA';
        zc.SetupOwnerId = communityUser1.ProfileId;
		insert zc;

        User vCommunityUser = [SELECT Id, userName, AccountId FROM User WHERE UserName = :SICo_UnpaidBills_VFC_TEST.communityUserName];
        Account vAccount = [SELECT Id FROM Account WHERE Id = :vCommunityUser.AccountId LIMIT 1]; 
        
        System.runAs(vCommunityUser) { 
            Test.startTest();
        	Test.setMock(HttpCalloutMock.class, new SICo_UpdateBankDetails_Mock());        
            SICo_UnpaidBills_VFC vController = new SICo_UnpaidBills_VFC(vAccount.Id);
            //SICo_UnpaidBills_VFC vController = new SICo_UnpaidBills_VFC(acc1.Id);
            String jsonSignature = SICo_UnpaidBills_VFC.processSignature();
            Test.stopTest();

            System.assert(jsonSignature == '{"key":"test","token":"test","signature":"test","pageId":"Test PageId","tenantId":"test"}', 'Fail due to JSON signature that is: ' + jsonSignature);
        }
    }
}